package com.andreasgroentved.rechordnize

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.andreasgroentved.rechordnize.domain.IDomain
import com.andreasgroentved.rechordnize.ui.inject.component.ApplicationComponent
import com.andreasgroentved.rechordnize.ui.inject.component.DaggerApplicationComponent
import com.andreasgroentved.rechordnize.ui.inject.module.ApplicationModule
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.core.CrashlyticsCore
import io.fabric.sdk.android.Fabric
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject


open class ChordApp : Application() {

    lateinit var component: ApplicationComponent

    @Inject
    lateinit var domain: IDomain

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        component = DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this, true)).build()
            .apply { inject(this@ChordApp) }
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        val crashlyticsCore = CrashlyticsCore.Builder().build()
        Fabric.with(this, Crashlytics.Builder().core(crashlyticsCore).build())


        CoroutineScope(Dispatchers.Default).launch {
            domain.getSound().loadAllSound(domain.getSoundType())
        }
    }
}
