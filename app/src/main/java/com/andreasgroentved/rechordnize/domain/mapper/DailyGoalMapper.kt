package com.andreasgroentved.rechordnize.domain.mapper

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.andreasgroentved.rechordnize.IData
import com.andreasgroentved.rechordnize.model.DailyGoal
import com.andreasgroentved.rechordnize.model.skill.DailySkill
import com.andreasgroentved.rechordnize.model.skill.SkillType
import com.andreasgroentved.rechordnize.model.util.DateUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


class DailyGoalMapper(private val data: IData) {
    //TODO retry mekanisme

    @WorkerThread
    suspend fun getDailySkill(date: String, name: SkillType): DailySkill? = data.getDailySkill(date, name)

    fun getDailySkills(date: String): LiveData<List<DailySkill>> = data.getDailySkills(date)

    fun updateTodayGoal(dailyGoal: DailyGoal) = data.updateGoal(dailyGoal)


    @WorkerThread
    suspend fun getTodayGoal(): DailyGoal? {
        val date = DateUtil.dateFormat.format(Date())
        val dailyGoalFromDb = data.getDailyGoalSync(date)

        return if (dailyGoalFromDb != null) {
            Timber.i("daily goal from db")
            dailyGoalFromDb
        } else {
            Timber.i("daily goal from internet")
            tryDailyGoalFromServer()
        }
    }

    fun getDailyGoalCompletions(): LiveData<Int> = data.getDailyGoalCompletions()

    @WorkerThread
    private suspend fun tryDailyGoalFromServer(): DailyGoal? {
        return suspendCoroutine { continuation ->
            val call = data.getTodayGoal()
            call.enqueue(object : Callback<DailyGoal> {
                override fun onFailure(call: Call<DailyGoal>, t: Throwable) {
                    continuation.resume(null)
                    Timber.d(t)
                }

                override fun onResponse(call: Call<DailyGoal>, response: Response<DailyGoal>) {
                    if (response.isSuccessful) {
                        response.body()?.let { dailyGoal ->
                            dailyGoal.date = (dailyGoal.date.split("T").firstOrNull()
                                    ?: dailyGoal.date).replace("-", "")
                            println(dailyGoal.date)
                            GlobalScope.launch(Dispatchers.IO) {
                                data.insertDailyGoal(dailyGoal)
                            }
                        }
                        continuation.resume(response.body())
                    } else {
                        continuation.resume(null)
                        Timber.d(response.errorBody()?.toString() ?: "no error message")
                    }
                }
            })
        }
    }


}
