package com.andreasgroentved.rechordnize.domain.mapper

import android.os.AsyncTask
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.andreasgroentved.rechordnize.IData
import com.andreasgroentved.rechordnize.model.level.LevelData
import com.andreasgroentved.rechordnize.model.level.LevelIdModifier
import com.andreasgroentved.rechordnize.model.level.LevelModifier
import com.andreasgroentved.rechordnize.model.level.LevelSelectionHolder
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.util.PlayableComparator
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import java.util.*
import javax.inject.Singleton


@Singleton
class LevelMapper(private val data: IData) {

    fun deleteLevel(levelId: String): Boolean = let {
        data.deleteModifierById(levelId)
        data.deleteLevel(levelId)
    }


    fun getLevelCount(): LiveData<Int> = data.getLevelCount()

    @WorkerThread //TODO
    fun getLevelWithChords(levelId: String): LiveData<LevelData> = //TODO brug noget SQL i stedet for
            MediatorLiveData<LevelData>().apply {
                addSource(getLevel(levelId)) { lData ->
                    AsyncTask.execute {
                        //TODO ikke async her
                        val chords = lData.playables.map { it.name }
                        val playables = loadChords(chords.toTypedArray()).toMutableList()
                        addMissingChords(playables, lData)
                        PlayableComparator().apply { Collections.sort(playables, this); Collections.sort(lData.playables, this) }

                        (0 until playables.size).forEach { playables[it].extra = lData.playables[it].extra }
                        lData.playables = playables
                        lData.levelModifiers = getLevelModifier(lData.id)
                        this.postValue(lData)
                    }
                }
            }


    private fun addMissingChords(playables: MutableList<Playable>, lData: LevelData) {
        if (playables.size == lData.playables.size) return
        lData.playables.filterNot { playables.contains(it) }.forEach { playables.add(it) }
    }

    private fun getLevelModifier(id: String): MutableList<LevelModifier> =
            data.getLevelModifiers(id).asSequence().map { LevelModifier.fromInt(it.modifier) }.toMutableList()


    fun insertLevelData(levelData: LevelData): String {
        if (levelData.id === "-1") levelData.id = getRandomID()
        data.insertLevelData(levelData)
        data.insertModifiers(levelData.levelModifiers.map { LevelIdModifier(levelData.id, it.mod) }.toTypedArray())
        return levelData.id
    }


    private fun loadChords(chordNames: Array<String>): List<Playable> = data.loadChords(chordNames)

    fun getLevel(id: String): LiveData<LevelData> = MediatorLiveData<LevelData>().apply {
        //todo gør i SQL
        this.addSource(data.getLevel(id)) {
            (GlobalScope + Dispatchers.IO).launch {
                it.levelModifiers = getLevelModifier(it.id)
                postValue(it)
            }
        }
    }

    fun getLevelSync(id: String): LevelData? = data.getLevelSync(id)?.apply { this.levelModifiers = getLevelModifier(id) } //TODO suspend

    fun updateLevel(levelData: LevelData) = data.updateLevelData(levelData)
    private fun getRandomID(): String = UUID.randomUUID().toString()

    fun getLevelsWithDifficulties(diff: IntArray): LiveData<List<LevelData>> = data.getLevelsWithDifficulties(diff)
    fun loadLevelIDsWithDifficulties(difficulties: Array<Int>): LiveData<List<LevelSelectionHolder>> =
            data.loadLevelIDsWithDifficulties(difficulties)

    fun getLevelsWithDifficultiesCount(diff: IntArray): Int = data.getLevelsWithDifficultiesCount(diff)

    fun getUnlockedNumberOfLevels(playableExtra: PlayableExtra): Int = data.getUnlockedNumberOfLevels(playableExtra)
    fun increaseNumberOfUnlockedLevels(playableExtra: PlayableExtra) = data.increaseNumberOfUnlockedLevels(playableExtra)
    fun setNumOfUnlockedLevels(playableExtra: PlayableExtra, number: Int) = data.setNumOfUnlockedLevels(playableExtra, number)

    fun getLevelCompletions(): Long = data.getLevelCompletions()
    fun setLevelCompletions(levelCompletions: Long) = data.setLevelCompletions(levelCompletions)

}