package com.andreasgroentved.rechordnize.domain.level

import com.andreasgroentved.rechordnize.model.level.LevelData
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.util.inRange
import com.andreasgroentved.rechordnize.util.DebugUtil
import kotlin.random.Random

class OfflineLevel(levelData: LevelData) : Level(levelData) {


    override fun getNext() {
        repeat(sequenceLength) {
            val probabilityArray = getProbabilities(getMaxVal())
            val randomNumber = getRandomNum(probabilityArray.sum())
            val luckyNumber = 0f
            current.add(getPlayable(luckyNumber, probabilityArray, randomNumber))
        }
    }

    private fun getPlayable(luckyNumber: Float, probabilityArray: List<Float>, randomNumber: Float): Playable {
        var luckyNumber = luckyNumber
        for ((index, playable) in getPossiblePlayables().withIndex()) {
            val chordChance = probabilityArray[index]
            if (inRange(luckyNumber, luckyNumber + chordChance, randomNumber)) return playable
            luckyNumber += chordChance
        }
        if (DebugUtil.isDebug()) throw RuntimeException("Input, gave no output")
        else {//TODO noget firebase her
            return getPossiblePlayables().first()
        }
    }

    private fun getProbabilities(maxVal: Int): List<Float> =
            levelData.playables.map { (maxVal - getTotalPlaysOfCurrent(it.name)) * (1f / maxVal) }.map { if (it.isNaN()) 1f else it }


    private fun getMaxVal(): Int = (roundNum + sequenceLength) + current.size
    private fun getRandomNum(probabilitySum: Float) = Random.nextFloat() * probabilitySum

    private fun getTotalPlaysOfCurrent(name: String) =
            levelPlay.plays.filter { it.name == name }.size + current.filter { it.name == name }.size
}