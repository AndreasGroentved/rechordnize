package com.andreasgroentved.rechordnize.domain

import com.andreasgroentved.rechordnize.model.util.ChordDictionary
import com.andreasgroentved.rechordnize.model.level.LevelData
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra.*
import com.andreasgroentved.rechordnize.model.skill.Skill
import com.andreasgroentved.rechordnize.model.skill.SkillType
import javax.inject.Singleton



@Singleton
class DatabaseSetup(private val data: com.andreasgroentved.rechordnize.Data) {

    private val chordDictionary = ChordDictionary()
    fun initialDb() {//TODO check om er init
        addAllChordsToDB()
        addAllProgressionsToDb()
        addAllTypesToDb()
        addAllIntervals()
        addAllSkills()

        val levelData: List<LevelData> = data.getAllInitialLevels()
        data.insertLevelDatas(levelData)
    }

    private fun addAllChordsToDB() {
        val roots = chordDictionary.order.map { it.capitalize() }
        addAllChordPlays(roots, CHORD)
    }

    private fun addAllChordPlays(roots: Collection<String>, playableExtra: PlayableExtra) {
        val playables = mutableListOf<Playable>()
        roots.forEach { chordName ->
            chordDictionary.chordShapeMap.keys.forEach { type ->
                val chord = "$chordName $type"
                playables.add(Playable(chord, extra = playableExtra))
            }
        }
        data.createInitialChords(playables)
    }

    private fun addAllProgressionsToDb() {
        val allProgs = listOf("I", "bII", "II", "bIII", "III", "IV", "bV", "V", "bVI", "VI", "bVII", "VII")
        addAllChordPlays(allProgs, CHORD_PROGRESSION)
    }

    private fun addAllIntervals() {
        val intervals = chordDictionary.nameToDistanceMap.keys
        val playables = intervals.map { Playable(it, extra = INTERVAL) }
        data.createInitialChords(playables)
    }

    private fun addAllTypesToDb() {
        val playables = chordDictionary.chordShapeMap.keys.map { Playable(it, extra = CHORD_TYPE) }
        data.createInitialChords(playables)
    }

    private fun addAllSkills() {
        SkillType.values().filter { it != SkillType.ERROR }.forEach { data.insertSkill(Skill(it, 0)) }
    }


}