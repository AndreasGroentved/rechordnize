package com.andreasgroentved.rechordnize.domain.mapper

import androidx.lifecycle.LiveData
import com.andreasgroentved.rechordnize.IData
import com.andreasgroentved.rechordnize.model.skill.DailySkill
import com.andreasgroentved.rechordnize.model.skill.Skill
import com.andreasgroentved.rechordnize.model.skill.SkillType
import com.andreasgroentved.rechordnize.model.util.DateUtil
import java.util.*
import javax.inject.Singleton


@Singleton
class SkillMapper(private val data: IData) {//TODO tjekke om der er metoder fra andre mappers, der hører til her

    private fun updateSkill(skill: Skill) = data.updateSkill(skill)
    fun getSkill(type: SkillType): LiveData<Skill> = data.getSkill(type)
    fun getSkills(): LiveData<List<Skill>> = data.getSkills()

    fun updateSkillDifference(skillType: SkillType, difference: Int) {
        val skill = data.getSkillSync(skillType)
        skill.best += difference
        skill.current += difference
        updateSkill(skill)
    }

    fun updateDailySkill(skillType: SkillType, difference: Int, condition: Boolean) {
        val date = DateUtil.dateFormat.format(Date())
        val todaySkill = data.getDailySkillSync(date, skillType) ?: DailySkill(skillType, 0, date)

        condition.takeIf { !condition }?.let { todaySkill.value = -1 }
        data.updateDailySkill(todaySkill.apply { value += difference })
    }


    fun updateSkillCurrentConditional(skillType: SkillType, correct: Boolean) {
        val skill = data.getSkillSync(skillType)
        if (correct) skill.current += 1
        else skill.current = 0
        if (skill.current > skill.best) skill.best = skill.current
        updateSkill(skill)
    }


}