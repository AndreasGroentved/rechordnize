package com.andreasgroentved.rechordnize.domain.sound

import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra.*
import com.andreasgroentved.rechordnize.model.util.ChordDictionary
import com.andreasgroentved.rechordnize.model.util.PlayableUtil
import kotlin.random.Random


object SoundShape {
    private val chordDictionary = ChordDictionary()

    //TODO vurder om det ikke er spøjst, at interval tilføjere rod, men progression ikke gør //TODO udgangspunkt lille input, men performance
    fun getSoundShapes(names: List<Pair<String, PlayableExtra>>, root: String = "c", ascending: Boolean = true,
                       shapeExtraData: List<PlayableExtra> = listOf(INVALID), randomRoot: Boolean = false): List<Pair<String, PlayableExtra>> = names.mapIndexed { index, it ->
        if (it.second == INTERVAL)
            getSoundChapeOfInterval(index, names, it, root, ascending, randomRoot)
        else
            getSoundShapeOfChordNameTypeAndProgression(it, root)
    }.flatten()


    private fun getSoundShapeOfChordNameTypeAndProgression(it: Pair<String, PlayableExtra>, root: String): List<Pair<String, PlayableExtra>> =
            when (it.second) {
                CHORD_TYPE -> (root.capitalize() + " " + it.first) to getBarreShape(CHORD_ANY, it.first)
                CHORD_PROGRESSION -> chordDictionary.getChordFromProgressionName(it.first, root).let { progName -> progName to getChordShape(CHORD_ANY, PlayableUtil.getChordExtension(progName)) }
                in PlayableExtra.getChordGroup() -> it.first to getChordShape(it.second, PlayableUtil.getChordExtension(it.first))
                else -> TODO()
            }.let { namesOfChords -> listOf(namesOfChords) }


    private fun getSoundChapeOfInterval(index: Int, names: List<Pair<String, PlayableExtra>>, it: Pair<String, PlayableExtra>, root: String, ascending: Boolean,
                                        randomRoot: Boolean): List<Pair<String, PlayableExtra>> {
        return if (index > 0 && names[index - 1].second == INTERVAL) emptyList()
        else {
            val intervals = mutableListOf(it.first)
            var runIndex = index
            while (runIndex + 1 < names.size && names[runIndex + 1].second == INTERVAL) {
                intervals.add(names[runIndex + 1].first)
                runIndex++
            }

            chordDictionary.getIntervalMultiple(intervals, root, ascending, randomRoot).map { it to INTERVAL }
        }
    }


    fun getRandomRoot(): String = chordDictionary.order[Random.nextInt(chordDictionary.order.size)]

    private fun getChordShape(chordShape: PlayableExtra, extension: String): PlayableExtra = when (chordShape) {
        CHORD_ANY, CHORD, BARRE, BARRE_E_SHAPE, BARRE_A_SHAPE, BARRE_C_SHAPE -> getBarreShape(chordShape, extension)
        POWER, POWER_A_SHAPE, POWER_E_SHAPE -> getPowerShape(chordShape)
        else -> chordShape
    }

    private fun getPowerShape(chordShape: PlayableExtra): PlayableExtra =
            if (chordShape == POWER) {
                if (Random.nextBoolean()) POWER_A_SHAPE else POWER_E_SHAPE
            } else chordShape

    //TODO ikke her
    private fun getBarreShape(chordShape: PlayableExtra, extension: String): PlayableExtra = when (chordShape) {
        BARRE, CHORD_ANY, CHORD, POWER_A_SHAPE, POWER_E_SHAPE -> { //TODO ryd op
            val shapes = chordDictionary.chordShapeMap[extension]!!.keys.toList()
            val shapeNum = Random.nextInt(shapes.size)
            shapes[shapeNum]
        }
        else -> chordShape
    }

}