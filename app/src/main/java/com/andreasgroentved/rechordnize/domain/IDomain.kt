package com.andreasgroentved.rechordnize.domain

import androidx.lifecycle.LiveData
import com.andreasgroentved.rechordnize.domain.sound.ISoundMapper
import com.andreasgroentved.rechordnize.model.DailyGoal
import com.andreasgroentved.rechordnize.model.Instrument
import com.andreasgroentved.rechordnize.model.level.LevelData
import com.andreasgroentved.rechordnize.model.level.LevelSelectionHolder
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.playable.PlayableNameWithExtra
import com.andreasgroentved.rechordnize.model.skill.DailySkill
import com.andreasgroentved.rechordnize.model.skill.Skill
import com.andreasgroentved.rechordnize.model.skill.SkillType
import com.andreasgroentved.rechordnize.model.usage.Usage
import javax.inject.Singleton

@Singleton
interface IDomain {
    fun getLevelCount(): LiveData<Int>
    fun deleteLevel(levelId: String): Boolean
    fun initialDb()
    fun getLevelWithChords(levelId: String): LiveData<LevelData>
    fun insertLevelData(levelData: LevelData): String
    fun updateLevel(levelData: LevelData)
    fun updateChord(chord: Playable)
    fun getSimplePlayables(playableNames: Array<String>): LiveData<List<PlayableNameWithExtra>>
    fun stopChord()
    fun getLevelSync(id: String): LevelData?
    fun getLevelsWithDifficulties(diff: IntArray): LiveData<List<LevelData>>
    fun getLevelIDsWithDifficulties(difficulties: Array<Int>): LiveData<List<LevelSelectionHolder>>
    suspend fun getTodayGoal(): DailyGoal?
    suspend fun getDailySkill(date: String, name: SkillType): DailySkill?
    fun getDailySkills(date: String): LiveData<List<DailySkill>>
    fun getDailyGoalCompletions(): LiveData<Int>
    fun getLevelsWithDifficultiesCount(diff: IntArray): Int
    fun getChord(chordName: String): LiveData<Playable>
    fun setIsFirstTime(istFirstTime: Boolean)
    fun isFirstTime(): Boolean
    fun getAllChords(): LiveData<List<Playable>>
    fun updateUsageToday(timeAdded: Long, today: String)
    fun getUsageFromThisWeek(): LiveData<List<Usage>>
    fun getUsageFromThisMonth(): LiveData<List<Usage>>
    fun getUsageGoal(): Int
    fun getSound(): ISoundMapper
    fun loadIntervals(): LiveData<List<Playable>>
    fun loadProgressions(): LiveData<List<Playable>>
    fun loadTypes(): LiveData<List<Playable>>
    fun updateTodayGoal(dailyGoal: DailyGoal)
    fun getAllSkills(): LiveData<List<Skill>>
    fun getSkill(skillType: SkillType): LiveData<Skill>
    fun updateSkillDifference(skillType: SkillType, difference: Int)
    fun updateSkillCurrentConditional(skillType: SkillType, correct: Boolean)
    fun updateDailySkill(skillType: SkillType, difference: Int, condition: Boolean)
    fun getUnlockedNumberOfLevels(playableExtra: PlayableExtra): Int
    fun increaseNumberOfUnlockedLevels(playableExtra: PlayableExtra)
    fun setNumOfUnlockedLevels(playableExtra: PlayableExtra, number: Int)
    fun getExp(): Long
    fun setExp(exp: Long)
    fun addExp(exp: Long)
    fun getSoundType(): Instrument
    fun getStartDate(): Long
    fun setStartDate(startDate: Long)
    fun getLevelCompletions(): Long
    fun setLevelCompletions(levelCompletions: Long)
    fun updatePlayables(playable: List<Playable>)
    fun setToken(token: String)
    fun getToken(): String?
}