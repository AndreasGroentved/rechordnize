package com.andreasgroentved.rechordnize.domain.sound

import android.annotation.SuppressLint
import android.content.Context
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.SoundPool
import android.os.Build
import com.andreasgroentved.rechordnize.domain.sound.SoundName.getAllSounds
import com.andreasgroentved.rechordnize.domain.sound.SoundName.getEarlySounds
import com.andreasgroentved.rechordnize.domain.sound.SoundName.getPrefix
import com.andreasgroentved.rechordnize.model.Instrument
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import timber.log.Timber
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit.MILLISECONDS
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

@Singleton
class SoundPlayer @Inject constructor(private val context: Context) {
    private lateinit var soundPool: SoundPool
    private var mapOfSounds = ConcurrentHashMap<String, Int>()

    init {
        setUpSoundPool()
    }


    @SuppressLint("CheckResult")
    fun playSoundsSeparated(sounds: List<List<String>>, delayIndividualNotes: Long = 100, delayForItems: Long = 500, instrument: Instrument) {
        chordStop()
        Observable.fromIterable(sounds)
                .zipWith(Observable.interval(0, delayForItems, MILLISECONDS), BiFunction { item: List<String>, _: Long -> item })
                .subscribe {
                    Observable.fromIterable(it)
                            .zipWith(Observable.interval(delayIndividualNotes, MILLISECONDS), BiFunction { item: String, _: Long -> item })
                            .subscribe({
                                val prefix = getPrefix(instrument)
                                soundPool.play(mapOfSounds[prefix + it]!!, 1f, 1f, 111111, 0, 1f)
                            }, {
                                Timber.i(it)
                            })
                }
    }


    fun isLoaded(sounds: List<String>, instrument: Instrument) = sounds.all {
        loadedArr.containsKey(mapOfSounds["${getPrefix(instrument)}$it"] ?: -1)
    }

    suspend fun loadSounds(sounds: List<String>, instrument: Instrument): Boolean {
        val s = sounds.map { getPrefix(instrument) + it }.chunked(5)
        s.forEach {
            loadSoundFiles(it)
        }
        return true
    }

    private val loadedArr = ConcurrentHashMap<Int, Boolean>()

    //Listeners kan ikke afregistreres, giver lidt mindre smuk kode, når alle lyde hentes, når programmet starter, men lyde også hentes, hvis
//de ikke allerede er, inden de forsøges afspillet
    private suspend fun loadSoundFiles(sounds: List<String>): Boolean =
            suspendCoroutine { cont ->
                val listener = SoundPool.OnLoadCompleteListener { s, sampleId, status ->
                    loadedArr.putIfAbsent(sampleId, true)
                    evaluateLoadStatus(sounds, cont)
                }
                soundPool.setOnLoadCompleteListener(listener)
                sounds.asSequence().associate {
                    loadSound(it)
                }.forEach { entry ->
                    if (entry.value == -1) {
                        evaluateLoadStatus(sounds, cont)
                        return@forEach
                    }
                    val already = mapOfSounds.putIfAbsent(entry.key, entry.value)
                    already?.let {
                        println("unload") //TODO
                        soundPool.unload(entry.value)
                    }
                }
            }

    private fun allLoaded(sounds: List<String>) = sounds.all {
        loadedArr[mapOfSounds[it] ?: -1] == true
    }


    private fun evaluateLoadStatus(sounds: List<String>, cont: Continuation<Boolean>) {
        if (allLoaded(sounds)) {
            try {
                println("loaded enough")
                cont.resume(true)
            } catch (e: Exception) {
                Timber.e("Already resumed")
            }
        }
    }

    private fun loadSound(it: String): Pair<String, Int> =
            try {
                if (loadedArr.containsKey(mapOfSounds[it] ?: -1)) it to -1
                else {
                    val id = identifiers[it]!!
                    val soundId = soundPool.load(context, id, 0)
                    it to soundId
                }
            } catch (e: Exception) {
                println(e.toString())
                println("unexpected file loading problem")
                it to -1
            }


    val identifiers = getAllSounds(Instrument.GUITAR).map { it to context.resources.getIdentifier(it, "raw", context.packageName) }.toMap()

    suspend fun loadAllSounds(instrument: Instrument) {
        loadSoundFiles(getEarlySounds(instrument))
    }

    fun chordStop() {
        mapOfSounds.values.forEach { soundPool.stop(it) } //TODO kan nok godt optimeres -> dette loop kører 117 gange...
    }

    private fun setUpSoundPool() {
        if (lollipopOrGreater()) buildFromAPI21()
        else buildBeforeAPI21()
    }

    private fun lollipopOrGreater() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP

    @SuppressLint("NewApi")
    private fun buildFromAPI21() {
        val audioAttributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).build()
        soundPool = SoundPool.Builder().setAudioAttributes(audioAttributes).setMaxStreams(20).build()
    }

    @Suppress("DEPRECATION")
    private fun buildBeforeAPI21() {
        soundPool = SoundPool(10, AudioManager.STREAM_MUSIC, 0)
    }

}

object SoundName {
    @JvmStatic
    fun getEarlySounds(instrument: Instrument): List<String> =
            listOf("e6", "a5", "d4", "g3", "b2", "e1")
                    .map { fret ->
                        (0..12).map { getPrefix(instrument) + fret + it }
                    }.flatten() /*+ (16..36).map { "${getPrefix(instrument)}e1$it" } *///e1 strengen har flere toner - for at dække flere oktaver

    @JvmStatic
    fun getAllSounds(instrument: Instrument): List<String> =
            listOf("e6", "a5", "d4", "g3", "b2", "e1")
                    .map { fret ->
                        (0..15).map { getPrefix(instrument) + fret + it }
                    }.flatten() + (16..36).map { "${getPrefix(instrument)}e1$it" } //e1 strengen har flere toner - for at dække flere oktaver


    @JvmStatic
    fun getPrefix(instrument: Instrument) = when (instrument) {
        Instrument.GUITAR -> "g"
        Instrument.PIANO -> "p"
    }

}