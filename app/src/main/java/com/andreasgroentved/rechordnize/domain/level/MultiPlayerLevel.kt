package com.andreasgroentved.rechordnize.domain.level

import com.andreasgroentved.rechordnize.model.level.LevelData

class MultiPlayerLevel(levelData: LevelData) : Level(levelData) {
    override fun hasLevelEnded() = hasEnded //TODO slet?
    override fun getNext() {
        if (override.isNotEmpty()) current = override.toMutableList()
        else throw IllegalStateException("override should have been seet")
    }

}