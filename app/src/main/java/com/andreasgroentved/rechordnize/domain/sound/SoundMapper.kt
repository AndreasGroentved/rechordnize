package com.andreasgroentved.rechordnize.domain.sound

import com.andreasgroentved.rechordnize.model.Instrument
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra.*
import com.andreasgroentved.rechordnize.model.util.ChordDictionary
import com.andreasgroentved.rechordnize.model.util.PlayableUtil
import javax.inject.Singleton


interface ISoundMapper {
    suspend fun loadSounds(instrument: Instrument, sounds: List<String>): Boolean
    fun isLoaded(sounds: List<String>, instrument: Instrument): Boolean
    suspend fun loadAllSound(instrument: Instrument)
    fun stop(): Unit
    suspend fun play(playables: List<Pair<String, PlayableExtra>>, instrument: Instrument, time: Long = -1, repetitions: Int = 1)
    fun getSounds(name: String, chordShape: PlayableExtra): List<String>
}

@Singleton
class SoundMapper(private val soundPlayer: SoundPlayer) : ISoundMapper {

    private val chordDictionary = ChordDictionary() //TODO
    override suspend fun loadSounds(instrument: Instrument, sounds: List<String>) = soundPlayer.loadSounds(sounds, instrument)
    override fun isLoaded(sounds: List<String>, instrument: Instrument) = soundPlayer.isLoaded(sounds, instrument)
    override suspend fun loadAllSound(instrument: Instrument) = soundPlayer.loadAllSounds(instrument)
    override fun stop(): Unit = soundPlayer.chordStop()

    //TODO ikke uniform
    //TODO SoundMapper enum --> i forhold til, hvordan der skal afspilles
    override suspend fun play(playables: List<Pair<String, PlayableExtra>>, instrument: Instrument, time: Long, repetitions: Int) { //TODO håndter liste af extraData, fjern return
        if (playables.isEmpty()) return

        val soundsToPlay = playables.map { getSounds(it.first.toLowerCase(), it.second) }
        if (playables[0].second == INTERVAL) {
            val notesSeparated: List<List<String>> = soundsToPlay.flatMap { listOf(it) } //TODO ikke her, input = output?
            soundPlayer.playSoundsSeparated(notesSeparated, 500, 500, instrument)
        } else {
            soundPlayer.playSoundsSeparated(soundsToPlay, 50, 1500, instrument)
        }
    }

    override fun getSounds(name: String, chordShape: PlayableExtra): List<String> {
        return when (chordShape) {
            CHORD, CHORD_ANY, BARRE_D_SHAPE, OPEN, BARRE, BARRE_E_SHAPE, BARRE_A_SHAPE, BARRE_C_SHAPE, POWER, POWER_A_SHAPE, POWER_E_SHAPE -> getChord(name, chordShape)
            INTERVAL -> getNotes(name)
            CHORD_PROGRESSION -> TODO()
            CHORD_TYPE, INVALID, ANY -> TODO()
        }
    }

    private fun getNotes(name: String): List<String> =
        listOf(chordDictionary.getFretForNote(name))

    private fun getChord(chordName: String, chordShape: PlayableExtra): List<String> {
        val base: String = PlayableUtil.getChordBase(chordName)
        val extension = PlayableUtil.getChordExtension(chordName)
        val relativeShape =
            if (chordShape == OPEN) chordDictionary.openChordMap.getValue(chordName.toLowerCase())
            else chordDictionary.chordShapeMap.getValue(extension).getValue(chordShape)
        val lowestString = getLowestString(relativeShape)
        val numOfStrings = relativeShape.size
        val offset = getFretNumNoteOnString(base, getLowestString(relativeShape), relativeShape, chordDictionary) - relativeShape[relativeShape.size - lowestString]
        return (0 until numOfStrings)
            .filter { relativeShape[it] != -1 }
            .map { chordDictionary.getSoundNameOnString(numOfStrings - it)[relativeShape[it] + offset] }
    }

    companion object {
        fun getLowestString(relativeShape: List<Int>): Int {
            var index = 0
            while (relativeShape[index++] == -1); //-1 = tom streng
            return relativeShape.size - (index - 1)
        }

        fun getFretNumNoteOnString(note: String, string: Int, relativeShape: List<Int> = listOf(), chordDictionary: ChordDictionary): Int {
            val note = (if (note.last().isDigit()) note.dropLast(1) else note).decapitalize()
            val stringList = chordDictionary.getSoundOnString(string)
            var indexOfNote = relativeShape[relativeShape.size - string]
            while (note != stringList[indexOfNote++].dropLast(1));
            return indexOfNote - 1
        }

    }

}