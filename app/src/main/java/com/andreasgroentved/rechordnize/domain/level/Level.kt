package com.andreasgroentved.rechordnize.domain.level

import com.andreasgroentved.rechordnize.domain.sound.SoundShape
import com.andreasgroentved.rechordnize.model.level.LevelData
import com.andreasgroentved.rechordnize.model.level.LevelModifier
import com.andreasgroentved.rechordnize.model.level.LevelModifier.*
import com.andreasgroentved.rechordnize.model.level.LevelPlay
import com.andreasgroentved.rechordnize.model.level.SimpleLevelType
import com.andreasgroentved.rechordnize.model.playable.PlayData
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.playable.addGuess
import com.andreasgroentved.rechordnize.model.util.EnumUtil
import kotlin.random.Random

abstract class Level(val levelData: LevelData) {

    //TODO support for flere typer
    var current = mutableListOf<Playable>(); protected set
    private var soundData: List<Pair<String, PlayableExtra>> = emptyList()
    var roundNum = 0; protected set
    val levelPlay: LevelPlay = LevelPlay()
    protected var root: String
    var sequenceLength = lengthOfSequence()
    protected var hasEnded = false
    //LevelModifiers
    private val isRepeatPlayingNew = levelData.levelModifiers.contains(REPEAT_PLAYS_NEW)
    private val isRootMoving = !levelData.levelModifiers.contains(STATIC_ROOT)
    private val isRootFirst = levelData.levelModifiers.contains(PROGRESSION_ROOT_FIRST)
    private val anyFirstProgNote = levelData.levelModifiers.contains(ANY_FIRST_PROGRESSION_CHORD)
    private val isDescending = levelData.levelModifiers.contains(DESCENDING)
    private val isAscending = levelData.levelModifiers.contains(ASCENDING)
    private val isRootNumberRandom = levelData.levelModifiers.contains(RANDOM_ROOT)
    protected var override = emptyList<Playable>()

    abstract fun getNext()
    open fun hasLevelEnded() = numberOfRounds() < roundNum + 1 || hasEnded


    init {
        levelPlay.plays.clear()
        root = when {
            levelData.levelModifiers.intersect(listOf(C_SCALE)).size == 1 -> "c"
            levelData.levelModifiers.contains(STATIC_ROOT) -> SoundShape.getRandomRoot()
            else -> ""
        }
        levelPlay.numberOfRounds = numberOfRounds()
    }


    fun reset() {
        levelPlay.plays.clear(); levelPlay.guessCount = 0; roundNum = 0; soundData = emptyList()
    }

    fun setNextOverride(override: List<Playable>) {
        this.override = override
    }

    fun setEndOverride() {
        hasEnded = true
    }


    fun next() {
        if (hasLevelEnded()) return
        else {
            current.clear()
            getNext()
            addRoot()
            setSoundShapes()
            this.override = emptyList()
        }
        roundNum++
    }

    //Værdi kan skifte alt efter @isRepeatPlayingNew
    fun getSoundDataOrChangedSoundData(): List<Pair<String, PlayableExtra>> {
        if (isRepeatPlayingNew) setSoundShapes()
        return soundData
    }

    fun levelEndUpdate() {
        levelData.plays++
        setLevelHistory()
    }

    private fun setLevelHistory() {
        if (levelData.levelModifiers.isEmpty()) levelData.levelModifiers.add(NONE)
        levelData.levelHistory.playMap[levelData.levelModifiers] = levelPlay
    }


    fun guess(guess: List<String>, time: Long, cheat: Boolean = false) {
        val avgTime = time / sequenceLength
        levelPlay.guessCount++
        current.forEachIndexed { index, playable ->
            val wasGuessRight = playable.name == guess[index] || cheat
            playable.addGuess(avgTime, wasGuessRight)
            updateLevelPlay(wasGuessRight, playable, avgTime)
        }
    }

    private fun updateLevelPlay(wasGuessRight: Boolean, playable: Playable, time: Long) {
        levelPlay.plays.add(PlayData(playable.name, time, wasGuessRight))
    }


    private fun setSoundShapes() {
        soundData = SoundShape.getSoundShapes(current.map { it.name to it.extra }, root, getAscOrDesc(), current.map { it.extra }, isRootNumberRandom)
    }

    private fun getAscOrDesc() = when {
        isAscending && isDescending -> Random.nextBoolean()
        isAscending -> true
        isDescending -> false
        else -> true
    }

    private fun addRoot() {
        if (isRootMoving) root = SoundShape.getRandomRoot()
        addProgressionRoots()
    }


    private fun addProgressionRoots() {
        val indices = current.mapIndexed { index, playable -> if (playable.extra == PlayableExtra.CHORD_PROGRESSION) index else -1 }.filter { it != -1 }.toMutableList()

        var lastAddedIndex = -99
        for (newIndex in indices) {
            if (newIndex != lastAddedIndex + 1) addRootToCurrent(newIndex) //Kun en I akkord til progression
            lastAddedIndex = newIndex
        }
    }

    private fun addRootToCurrent(index: Int) { //TODO måske ikke den samme rod på alle!, TODO find ud af, hvad denne todo betyder...
        if (isRootFirst) {
            current.add(index,
                    if (anyFirstProgNote) {
                        val possibleFirsts = getPossiblePlayables().filter { it.name.run { startsWith("I ") } }
                        possibleFirsts[Random.nextInt(possibleFirsts.size)]
                    } else {
                        getPossiblePlayables().firstOrNull { it.name == "I maj" }
                                ?: getPossiblePlayables().first()
                    })
        }
    }


    private fun lengthOfSequence(): Int {
        val lengthMod = levelData.levelModifiers.intersect(listOf(LENGTH_1, LENGTH_2, LENGTH_3, LENGTH_4, LENGTH_5, LENGTH_6, LENGTH_7))
        return if (lengthMod.size == 1) LevelModifier.lengthMatcher(lengthMod.first()) else 1
    }


    private fun getPossiblePlayables(simpleLevelType: SimpleLevelType): List<Playable> {
        val group = EnumUtil.getExtraGroupFromSimpleLevelType(simpleLevelType)
        return if (SimpleLevelType.ANY == simpleLevelType) levelData.playables else levelData.playables.filter { it.extra in group }
    }


    fun getPossiblePlayables(): List<Playable> = levelData.playables
    fun numberOfRounds() = levelPlay.numberOfRounds
    fun getLengthOfCurrent() = current.size
    fun setNumberOfPlays(numberOfPlays: Int) = run { levelPlay.numberOfRounds = numberOfPlays }

}