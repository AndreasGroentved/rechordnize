package com.andreasgroentved.rechordnize.domain.mapper

import androidx.lifecycle.LiveData
import com.andreasgroentved.rechordnize.IData
import com.andreasgroentved.rechordnize.model.skill.SkillType.*
import com.andreasgroentved.rechordnize.model.usage.Usage
import com.andreasgroentved.rechordnize.model.util.DateUtil
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class UsageMapper @Inject constructor(private val data: IData) {
    fun getUsage(date: String): Usage? = data.getUsage(date)
    fun getUsageFromTo(fromDate: String, toDate: String): LiveData<List<Usage>> = data.getUsageFromTo(fromDate, toDate)
    fun getLastChecked(): String = data.getLastChecked()
    fun setLastChecked(lastChecked: String) = data.setLastChecked(lastChecked)
    fun getDaysInARow(): Int = data.getDaysInARow()
    fun getUsageGoal(): Int = data.getUsageGoal()
    fun updateUsage(usage: Usage) = data.updateUsage(usage)


    fun getUsageFromThisWeek(): LiveData<List<Usage>> {
        val startAndEndOfWeek = DateUtil.getStartAndEndOfWeek(Date().time)
        return getUsageFromTo(startAndEndOfWeek.first, startAndEndOfWeek.second)
    }

    fun getUsageFromThisMonth(): LiveData<List<Usage>> {
        val (month, year) = DateUtil.getMonthAndYear(Date())
        val startAndEndOfMonth = DateUtil.getStartAndEndOfMonth(month, year)
        return getUsageFromTo(startAndEndOfMonth.first, startAndEndOfMonth.second)
    }

    fun updateUsageToday(timeAdded: Long, today: String) {
        val usage = getUsage(today) ?: Usage(0, today)

        val dailyGoalInMs = getUsageGoal() * 60 /*s*/ * 1_000/*ms*/
        val metDailyGoal = usage.time >= dailyGoalInMs

        saveTimeRelatedSkills(timeAdded, usage)

        updateInARowAndTotalDays(today, dailyGoalInMs)
        if (metDailyGoal && getLastChecked() <= today) {
            setLastChecked(DateUtil.getDayAfter(today))
        } else {
            setLastChecked(today)
        }
    }

    private fun updateInARowAndTotalDays(today: String, dailyGoalInMs: Int) {
        val (inARow, totalDays) = getInARowAndTotalDays(today, dailyGoalInMs, data.getTotalDays(), getLastChecked(), getDaysInARow(), data)
        saveTotalDaysAndInARow(today, inARow, totalDays)
    }

    private fun saveTimeRelatedSkills(timeAdded: Long, usage: Usage) {
        usage.time += timeAdded
        saveTotalPractise(timeAdded)
        saveLongestPractise(usage)
        updateUsage(usage)
    }

    private fun saveLongestPractise(usage: Usage) {
        val longestPractiseSkill = data.getSkillSync(LONGEST_PRACTISE)

        if (longestPractiseSkill.best < usage.time)
            longestPractiseSkill.best = usage.time

        longestPractiseSkill.current = usage.time
        data.updateSkill(longestPractiseSkill)
    }

    private fun saveTotalPractise(timeAdded: Long) {
        val totalPractise = data.getSkillSync(TOTAL_PRACTISE_TIME)
        totalPractise.current += timeAdded
        totalPractise.best += timeAdded
        data.updateSkill(totalPractise)
    }


    private fun saveTotalDaysAndInARow(today: String, inARow: Int, totalDays: Long) {
        updateTotalDays(totalDays)
        updateInARow(inARow, today)
    }

    private fun updateTotalDays(totalDays: Long) {
        val totalDaysSkill = data.getSkillSync(TOTAL_DAYS)  //TODO tjek om ændring -> undgå db kald
        totalDaysSkill.best = totalDays
        data.updateSkill(totalDaysSkill)
        data.setTotalDays(totalDays)
    }

    private fun updateInARow(inARow: Int, today: String) {
        data.setDaysInARow(inARow, today)
        val daysInARowSkill = data.getSkillSync(DAYS_IN_A_ROW)
        daysInARowSkill.current = inARow.toLong()
        if (inARow > daysInARowSkill.best) daysInARowSkill.best = inARow.toLong()
        data.updateSkill(daysInARowSkill)
    }

    /**
     * Returns days in a row and total days. Assumes that {lastChecked} is one day ahead of last time these two values were incremented
     */
    fun getInARowAndTotalDays(today: String, usageGoal: Int, totalDays: Long, lastChecked: String, currentInARow: Int, data: IData): Pair<Int, Long> {
        var day = lastChecked
        var inARow = currentInARow
        while (day <= today) { //har til formål at sætte i dag, og nulle de dage i mellem i dag og sidste gang, der blev opdateret
            val isToday = day == today
            val usage = data.getUsage(day)

            if (usage == null) {
                inARow = 0
            } else {
                if (usage.time < usageGoal && isToday.not()) inARow = 0
                else {
                    if (isToday && usage.time >= usageGoal) {
                        return Pair(inARow + 1, totalDays + 1)
                    } //else ville totalDays og inARow allerede være opdaterede
                }
            }
            day = DateUtil.getDayAfter(day)
        }
        return Pair(inARow, totalDays)
    }

}