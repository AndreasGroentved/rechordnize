package com.andreasgroentved.rechordnize.domain.mapper

import androidx.lifecycle.LiveData
import com.andreasgroentved.rechordnize.IData
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.playable.PlayableNameWithExtra
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class PlayableMapper @Inject constructor(private val data: IData) {

    fun updateChord(chord: Playable) = data.updateChord(chord)
    fun updatePlayables(playable: List<Playable>) = data.updatePlayables(playable)
    fun getChord(chordName: String): LiveData<Playable> = data.getChord(chordName)
    fun getAllChords(): LiveData<List<Playable>> = data.getAllChords()

    fun loadIntervals(): LiveData<List<Playable>> = data.loadIntervals()
    fun loadProgressions(): LiveData<List<Playable>> = data.loadProgressions()
    fun loadTypes(): LiveData<List<Playable>> = data.loadTypes()

    fun getSimplePlayables(playableNames: Array<String>): LiveData<List<PlayableNameWithExtra>> = data.getSimplePlayables(playableNames)

}