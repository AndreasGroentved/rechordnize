package com.andreasgroentved.rechordnize.domain.mapper

import com.andreasgroentved.rechordnize.IData
import com.andreasgroentved.rechordnize.model.Instrument
import com.andreasgroentved.rechordnize.model.Instrument.GUITAR
import com.andreasgroentved.rechordnize.model.Instrument.PIANO
import javax.inject.Singleton


@Singleton
class GeneralMapper(private val data: IData) { //TODO tjekke om der er metoder fra andre mappers, der hører til her

    fun setIsFirstTime(istFirstTime: Boolean): Unit = data.setIsFirstTime(istFirstTime)
    fun getIsFirstTime(): Boolean = data.getIsFirstTime()

    fun getStartDate(): Long = data.getStartDate()
    fun setStartDate(startDate: Long) = data.setStartDate(startDate)
    fun setToken(token: String) = data.setToken(token)
    fun getToken(): String? = data.getToken()

    fun getSoundType(): Instrument = when (data.getSoundType()) {
        "guitar" -> GUITAR
        "piano" -> PIANO
        else -> GUITAR
    }

}