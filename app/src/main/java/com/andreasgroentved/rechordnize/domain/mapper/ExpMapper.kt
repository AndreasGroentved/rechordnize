package com.andreasgroentved.rechordnize.domain.mapper

import com.andreasgroentved.rechordnize.IData
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ExpMapper @Inject constructor(private val data: IData) {
    fun getExp(): Long = data.getExp()
    fun setExp(exp: Long) = data.setExp(exp)
    fun addExp(exp: Long) = data.addExp(exp)
}
