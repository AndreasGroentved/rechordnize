package com.andreasgroentved.rechordnize.domain

import androidx.lifecycle.LiveData
import com.andreasgroentved.rechordnize.domain.mapper.*
import com.andreasgroentved.rechordnize.domain.sound.ISoundMapper
import com.andreasgroentved.rechordnize.model.DailyGoal
import com.andreasgroentved.rechordnize.model.Instrument
import com.andreasgroentved.rechordnize.model.level.LevelData
import com.andreasgroentved.rechordnize.model.level.LevelSelectionHolder
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.playable.PlayableNameWithExtra
import com.andreasgroentved.rechordnize.model.skill.DailySkill
import com.andreasgroentved.rechordnize.model.skill.SkillType
import com.andreasgroentved.rechordnize.model.usage.Usage
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
open class Domain @Inject constructor(private val soundPlayer: ISoundMapper, private val playableMapper: PlayableMapper, private val levelMapper: LevelMapper, private val usageMapper: UsageMapper, private val generalMapper: GeneralMapper, private val skillMapper: SkillMapper, private val databaseSetup: DatabaseSetup, private val expMapper: ExpMapper, private val dailyGoalMapper: DailyGoalMapper) : IDomain {

    override fun getLevelCount(): LiveData<Int> = levelMapper.getLevelCount()
    override fun deleteLevel(levelId: String): Boolean = levelMapper.deleteLevel(levelId)
    override fun initialDb() = databaseSetup.initialDb()
    override fun getLevelWithChords(levelId: String): LiveData<LevelData> = levelMapper.getLevelWithChords(levelId)
    override fun insertLevelData(levelData: LevelData): String = levelMapper.insertLevelData(levelData)
    override fun updateLevel(levelData: LevelData) = levelMapper.updateLevel(levelData)
    override fun updateChord(chord: Playable) = playableMapper.updateChord(chord)
    override fun updatePlayables(playable: List<Playable>) = playableMapper.updatePlayables(playable)
    override fun getSimplePlayables(playableNames: Array<String>): LiveData<List<PlayableNameWithExtra>> = playableMapper.getSimplePlayables(playableNames)
    override fun stopChord() = soundPlayer.stop()
    override fun getLevelSync(id: String): LevelData? = levelMapper.getLevelSync(id)
    override fun getLevelsWithDifficulties(diff: IntArray): LiveData<List<LevelData>> = levelMapper.getLevelsWithDifficulties(diff)
    override fun getLevelIDsWithDifficulties(difficulties: Array<Int>): LiveData<List<LevelSelectionHolder>> = levelMapper.loadLevelIDsWithDifficulties(difficulties)
    override suspend fun getTodayGoal() = dailyGoalMapper.getTodayGoal()
    override suspend fun getDailySkill(date: String, name: SkillType): DailySkill? = dailyGoalMapper.getDailySkill(date, name)
    override fun getDailySkills(date: String): LiveData<List<DailySkill>> = dailyGoalMapper.getDailySkills(date)
    override fun getDailyGoalCompletions(): LiveData<Int> = dailyGoalMapper.getDailyGoalCompletions()
    override fun getLevelsWithDifficultiesCount(diff: IntArray): Int = levelMapper.getLevelsWithDifficultiesCount(diff)
    override fun getChord(chordName: String): LiveData<Playable> = playableMapper.getChord(chordName)
    override fun setIsFirstTime(istFirstTime: Boolean): Unit = generalMapper.setIsFirstTime(istFirstTime)
    override fun isFirstTime(): Boolean = generalMapper.getIsFirstTime()
    override fun getAllChords(): LiveData<List<Playable>> = playableMapper.getAllChords()
    override fun updateUsageToday(timeAdded: Long, today: String): Unit = usageMapper.updateUsageToday(timeAdded, today)
    override fun getUsageFromThisWeek(): LiveData<List<Usage>> = usageMapper.getUsageFromThisWeek()
    override fun getUsageFromThisMonth(): LiveData<List<Usage>> = usageMapper.getUsageFromThisMonth()
    override fun getUsageGoal() = usageMapper.getUsageGoal()
    override fun getSound(): ISoundMapper = soundPlayer
    override fun loadIntervals(): LiveData<List<Playable>> = playableMapper.loadIntervals()
    override fun loadProgressions(): LiveData<List<Playable>> = playableMapper.loadProgressions()
    override fun loadTypes(): LiveData<List<Playable>> = playableMapper.loadTypes()
    override fun updateTodayGoal(dailyGoal: DailyGoal) = dailyGoalMapper.updateTodayGoal(dailyGoal)
    override fun getAllSkills() = skillMapper.getSkills()
    override fun getSkill(skillType: SkillType) = skillMapper.getSkill(skillType)
    override fun updateSkillDifference(skillType: SkillType, difference: Int) = skillMapper.updateSkillDifference(skillType, difference)
    override fun updateSkillCurrentConditional(skillType: SkillType, correct: Boolean) = skillMapper.updateSkillCurrentConditional(skillType, correct)
    override fun updateDailySkill(skillType: SkillType, difference: Int, condition: Boolean) = skillMapper.updateDailySkill(skillType, difference, condition)
    override fun getUnlockedNumberOfLevels(playableExtra: PlayableExtra): Int = levelMapper.getUnlockedNumberOfLevels(playableExtra)
    override fun increaseNumberOfUnlockedLevels(playableExtra: PlayableExtra) = levelMapper.increaseNumberOfUnlockedLevels(playableExtra)
    override fun setNumOfUnlockedLevels(playableExtra: PlayableExtra, number: Int) = levelMapper.setNumOfUnlockedLevels(playableExtra, number)
    override fun getExp(): Long = expMapper.getExp()
    override fun setExp(exp: Long) = expMapper.setExp(exp)
    override fun addExp(exp: Long) = expMapper.addExp(exp)
    override fun getSoundType(): Instrument = generalMapper.getSoundType()
    override fun getStartDate(): Long = generalMapper.getStartDate()
    override fun setStartDate(startDate: Long) = generalMapper.setStartDate(startDate)
    override fun getLevelCompletions(): Long = levelMapper.getLevelCompletions()
    override fun setLevelCompletions(levelCompletions: Long) = levelMapper.setLevelCompletions(levelCompletions)
    override fun setToken(token: String) = generalMapper.setToken(token)
    override fun getToken(): String? = generalMapper.getToken()
}
