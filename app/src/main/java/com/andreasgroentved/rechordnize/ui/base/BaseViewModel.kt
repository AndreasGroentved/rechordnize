package com.andreasgroentved.rechordnize.ui.base

import androidx.lifecycle.ViewModel
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.domain.IDomain
import com.andreasgroentved.rechordnize.ui.inject.component.DaggerActivityComponent
import com.andreasgroentved.rechordnize.ui.inject.module.ActivityModule
import javax.inject.Inject

abstract class BaseViewModel(application: ChordApp) : ViewModel() {

    @Inject
    lateinit var domain: IDomain

    init {
        initializeDagger(this, application)
    }

    open fun <T : BaseViewModel> initializeDagger(viewModel: T, application: ChordApp) {
        try {
            DaggerActivityComponent
                .builder()
                .applicationComponent(application.component)
                .activityModule(ActivityModule())
                .build().inject(viewModel)
        } catch (e: Exception) {
            println("testing...")
        }
    }

}