package com.andreasgroentved.rechordnize.ui.select_level_type

import android.os.Bundle
import android.view.*
import androidx.navigation.findNavController
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.model.level.LevelDifficulty
import com.andreasgroentved.rechordnize.ui.base.BaseFragment
import com.andreasgroentved.rechordnize.ui.base.LevelSelectionData
import kotlinx.android.synthetic.main.fragment_level_overview.view.*

class FragmentSelectLevelType : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_level_overview, container, false).apply {
            button_goToChords.setOnClickListener { navigateToLevelSelection(LevelDifficulty.getChordLevels(), this) }
            button_goToChordTypes.setOnClickListener { navigateToLevelSelection(LevelDifficulty.getTypeLevels(), this) }
            button_goToChordsChordProgressions.setOnClickListener { navigateToLevelSelection(LevelDifficulty.getProgressionLevels(), this) }
            button_goToIntervals.setOnClickListener { navigateToLevelSelection(LevelDifficulty.getIntervalLevels(), this) }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    private fun navigateToLevelSelection(levelInt: List<LevelDifficulty>, root: View) {
        val action = FragmentSelectLevelTypeDirections.actionSelectFragmentToDestinationLevelSelection()
            .setLevelSelectionData(LevelSelectionData(levelInt.map { it.intVal }.toIntArray()))
        root.findNavController().navigate(action)
    }
}