package com.andreasgroentved.rechordnize.ui.level

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.LevelViewModelFactory
import com.andreasgroentved.rechordnize.ui.playableinfo.PlayableInfoFragment
import com.andreasgroentved.rechordnize.util.SnackUtil
import kotlinx.android.synthetic.main.level_activity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LevelActivity : AppCompatActivity() {


    private lateinit var viewModel: LevelViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.level_activity)
        val factory = LevelViewModelFactory(application as ChordApp)
        viewModel = ViewModelProvider(this, factory).get(LevelViewModel::class.java)
        viewModel.initLevelData(levelDataFromBundle.levelId, levelDataFromBundle.levelDifficultyNum, levelDataFromBundle.questionNumber).observe(this, Observer {
            setUp()
            setTitle()
        })
        setupActionBar(Navigation.findNavController(this, R.id.nav_host))

    }


    private fun setupActionBar(nav: NavController) {
        setSupportActionBar(toolbar_level)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = levelDataFromBundle.levelTitle
        NavigationUI.setupActionBarWithNavController(this, nav)
    }

    private fun setUp() {
        viewModel.getIncorrectGuess().observe(this, Observer {
            SnackUtil.getSnack(it, nav_host.view!!, this, 2000).show()
        })
        viewModel.getFinalGuess().observe(this, Observer {
            if (!it) return@Observer
            viewModel.seenFinalGuess() //TODO kig på det her
            findNavController(nav_host.id).navigate(R.id.playResultFragment)

        })


        viewModel.showLoadingLiveData.observe(this, Observer {
            if (it) {
                loading_progress_xml.visibility = View.VISIBLE
            } else {
                loading_progress_xml.visibility = View.GONE
            }
        })


    }

    private fun setTitle() {
        viewModel.title = levelDataFromBundle.levelTitle.capitalize()
        viewModel.getTitle().observe(this, Observer {
            supportActionBar?.title = it
        })

    }

    override fun onBackPressed() {
        nav_host.childFragmentManager.fragments.firstOrNull { it is PlayableInfoFragment }?.let {
            super.onBackPressed()
            return
        }
        AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Quit practicing?")
                .setPositiveButton("Yes") { _, _ ->
                    finish()
                }
                .setNegativeButton("No") { _, _ -> }.setIcon(0).show()
    }


    private val levelDataFromBundle: DataForLevel  by lazy {
        intent?.extras?.let { LevelActivityArgs.fromBundle(it).dataForLevel }
                ?: throw RuntimeException("invalid data for level activity")
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_level, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.action_resetLevel -> {
                (lifecycleScope).launch {
                    withContext(Dispatchers.IO) { viewModel.resetLevel() }
                    withContext(Dispatchers.Main) {
                        ChordStartFragmentDirections.actionChordStartFragmentSelf().let {
                            Navigation.findNavController(this@LevelActivity, R.id.nav_host).popBackStack(R.id.chordStartFragment, false)
                        }
                    }
                }
            }
        }
        return true
    }

}