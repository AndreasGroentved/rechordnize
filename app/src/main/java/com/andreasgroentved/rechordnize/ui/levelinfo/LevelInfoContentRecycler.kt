package com.andreasgroentved.rechordnize.ui.levelinfo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.base.ClickListener
import com.andreasgroentved.rechordnize.ui.base.RecyclerTouchListener
import kotlinx.android.synthetic.main.card_playable.view.*
import kotlinx.android.synthetic.main.header_and_recycler_card.view.*
import kotlinx.android.synthetic.main.level_info_card_infoarea.view.*


class LevelInfoContentRecycler(var levelInfoViewData: LevelInfoViewData) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): RecyclerView.ViewHolder =
            when (getItemViewType(position)) {
                info -> InfoHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.level_info_card_infoarea, viewGroup, false))
                difficulty -> DifficultyHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.level_info_card_difficultyselection, viewGroup, false))
                playables -> PlayablesHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.header_and_recycler_card, viewGroup, false))
                else -> throw TODO("out of bounds")
            }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = when (holder) {
        is InfoHolder -> holder.set(levelInfoViewData)
        is DifficultyHolder -> holder.set(levelInfoViewData)
        is PlayablesHolder -> holder.set(levelInfoViewData.playableTypeHeader, levelInfoViewData.adapterDataWithNameAndLevel)
        else -> throw TODO("out of bounds")
    }

    private val info = 0
    private val difficulty = 1
    private val playables = 2

    override fun getItemViewType(position: Int): Int = if (position == info) info else if (position == difficulty) difficulty else playables
    override fun getItemCount(): Int = 3


    class InfoHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun set(levelInfoViewData: LevelInfoViewData) {
            itemView.level_info_text.text = levelInfoViewData.infoText
        }
    }



    class PlayablesHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun set(playableTypeHeader: String, adapterDataWithNameAndLevel: List<Pair<String, Int>>) {
            val adapter = PlayableAdapter()
            itemView.generic_recycler.adapter = adapter
            itemView.title.text = playableTypeHeader
            adapter.nameLevelPairs = adapterDataWithNameAndLevel
            itemView.generic_recycler.layoutManager = GridLayoutManager(itemView.context, 2).apply {
                orientation = LinearLayoutManager.HORIZONTAL
            }

            itemView.generic_recycler.addOnItemTouchListener(RecyclerTouchListener(itemView.context, itemView.generic_recycler, object : ClickListener {
                override fun onLongClick(view: View, position: Int) {}
                override fun onClick(view: View, position: Int) {
                    LevelInfoFragmentDirections.actionLevelInfoFragmentToChordInfoFragment().apply {
                        chord = adapter.nameLevelPairs[position].first
                    }.let { view.findNavController().navigate(it) }
                }
            }))
        }
    }

    class PlayableAdapter : RecyclerView.Adapter<PlayableAdapter.PlayableHolder>() {
        var nameLevelPairs: List<Pair<String, Int>> = emptyList()
            set(value) {
                field = value; notifyDataSetChanged()
            }

        override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): PlayableHolder =
                PlayableHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.card_playable, viewGroup, false))

        override fun onBindViewHolder(holder: PlayableHolder, position: Int) =
                nameLevelPairs[position].let { holder.set(it.first, it.second.toLong()) }


        override fun getItemCount(): Int = nameLevelPairs.size

        class PlayableHolder(view: View) : RecyclerView.ViewHolder(view) {
            fun set(name: String, level: Long) {
                itemView.lvl_value.text = "$level"
                itemView.playable_name.text = name
            }
        }
    }
}