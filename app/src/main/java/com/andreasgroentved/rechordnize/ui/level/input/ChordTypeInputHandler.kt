package com.andreasgroentved.rechordnize.ui.level.input

import androidx.lifecycle.LiveData
import com.andreasgroentved.rechordnize.model.util.ChordDictionary
import com.andreasgroentved.rechordnize.model.util.PlayableUtil


class ChordTypeInputHandler(sequenceLength: Int, possiblePlayables: List<String>) : ChordInputHandler(sequenceLength, possiblePlayables)  /*InputHandler(sequenceLength, possiblePlayables) */{


    //TODO firstlevel
    private val secondLevel: Map<String, List<String>> = buildSecondLevel()
    override fun getInputList(text: String): List<List<String>> = getSecondLevel()
    override fun getGuess(value: String): String = value
    override fun isInputAGuess(text: String): Boolean = inputLevel == 0 || text == "skip"
    override fun getInputs(): LiveData<List<List<String>>> = input
    override fun formatInput(inputList: List<List<String>>): List<List<String>> = inputList
    override fun getTopLevel(): List<List<String>> = getSecondLevel()


    private fun getSecondLevel(): List<List<String>> = secondLevel.map {
        mutableListOf(it.key).apply { addAll(it.value) }
    } //TODO performance?

/*    override fun buildSecondLevel(): Map<String, List<String>> { //TODO klon
        val secondMap = mutableMapOf<String, MutableList<String>>()
        val playableExtensions = possiblePlayables.asSequence().map { PlayableUtil.getChordExtension(it) }.toSet()
        playableExtensions.forEach {
            val mapping = chordDictionary.typeToGrouping[it]
                ?: error("Type: _${it}_ cannot be matched with a group")
            if (secondMap.containsKey(mapping)) secondMap[mapping]!!.add(it)
            else secondMap[it] = mutableListOf(it)
        }
        return secondMap
    }*/
}