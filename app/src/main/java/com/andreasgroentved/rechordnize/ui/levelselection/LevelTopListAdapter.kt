package com.andreasgroentved.rechordnize.ui.levelselection

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.base.ClickListener
import com.andreasgroentved.rechordnize.ui.base.RecyclerTouchListener
import kotlinx.android.synthetic.main.recycler_and_header.view.*

class LevelTopListAdapter : RecyclerView.Adapter<LevelTopListAdapter.LevelHolder>() {

    var viewData: LevelSelectionViewModel.LevelSelectionViewData? = null
        set(other) {
            field = other; notifyDataSetChanged()
        }


    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): LevelHolder =
            LevelHolder(
                    LayoutInflater.from(viewGroup.context).inflate(
                            R.layout.recycler_and_header, viewGroup, false
                    )
            )

    override fun onBindViewHolder(holder: LevelHolder, position: Int) {
        holder.set(viewData!!.list[position])
    }

    class LevelHolder(view: View) : RecyclerView.ViewHolder(view) {
        val levelListAdapter = LevelListAdapter()

        init {

            view.recycler.apply {
                adapter = levelListAdapter
                layoutManager = GridLayoutManager(view.context, 2).apply {
                    orientation = LinearLayoutManager.HORIZONTAL
                }
            }
        }

        fun set(difficultyList: LevelSelectionViewModel.DifficultyList) {
            itemView.recycler_header.text = difficultyList.header
            if (difficultyList.list.size < 4) {
                itemView.recycler.layoutManager = GridLayoutManager(itemView.context, 1).apply {
                    orientation = LinearLayoutManager.HORIZONTAL
                }
            }

            levelListAdapter.difficultyList = difficultyList
            itemView.recycler.addOnItemTouchListener(
                    RecyclerTouchListener(itemView.context.applicationContext, itemView.recycler.recycler, clickListener(difficultyList.listIndex))
            )
        }

        private fun clickListener(startIndex: Int) = object : ClickListener {
            override fun onLongClick(view: View, position: Int) {}
            override fun onClick(view: View, position: Int) {
                if (levelListAdapter.isLocked(position)) {
                    LevelSelectionFragment.showSnackBar("Get three notes in the previous level to unlock this level", view)
                    return
                }

                LevelSelectionFragmentDirections.actionDestinationLevelSelectionToLevelInfoFragment(levelListAdapter.getItem(position).id).apply {
                    levelid = levelListAdapter.getItem(position).id
                    levelnum = startIndex + position + 1
                }.let { Navigation.findNavController(view).navigate(it) }
            }
        }
    }

    override fun getItemCount(): Int = viewData?.list?.size ?: 0
}
