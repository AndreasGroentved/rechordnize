package com.andreasgroentved.rechordnize.ui.multiplayer

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.general.LeftRightHolder

class MultiPlayerResultAdapter(private val playerNameToScore: List<Pair<String, Int>>) : Adapter<LeftRightHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): LeftRightHolder =
            LeftRightHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.list_item_two_spread, viewGroup, false))

    override fun onBindViewHolder(holder: LeftRightHolder, position: Int) {
        val (left, right) = playerNameToScore[position]
        holder.updateView(left, right.toString())
    }

    override fun getItemCount() = playerNameToScore.size
}