package com.andreasgroentved.rechordnize.ui.level

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.base.BaseFragment
import com.andreasgroentved.rechordnize.ui.base.ClickListener
import com.andreasgroentved.rechordnize.ui.base.RecyclerTouchListener
import com.andreasgroentved.rechordnize.util.toSpanned
import kotlinx.android.synthetic.main.fragment_chord_result.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus

class PlayResultFragment : BaseFragment() { //TODO rename


    private lateinit var viewModel: LevelViewModel
    private lateinit var guessAdapter: GuessListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_chord_result, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(requireActivity()).get(LevelViewModel::class.java)
        setList()
        setUp()
    }

    override fun onResume() {
        super.onResume()
        (activity as? AppCompatActivity)?.supportActionBar?.title = viewModel.getTitle().value
    }

    private fun setUp() {
        viewModel.getResultPlayablesLive().observe(this, Observer {
            guessAdapter.list = it
        })
        viewModel.getResultInfoText().observe(this, Observer {
            chord_result_info.text = it.toSpanned()
        })

        chord_result_to_next_button.setOnClickListener {
            (lifecycleScope + Dispatchers.Default).launch {
                val hasEnded = viewModel.hasLevelEnded()
                viewModel.next()
                (if (hasEnded) PlayResultFragmentDirections.actionPlayResultFragmentToResultFragment()
                else PlayResultFragmentDirections.actionPlayResultFragmentToInputFragment())
                        .let {
                            (lifecycleScope + Dispatchers.Main).launch {
                                findNavController().navigate(it)
                            }
                        }
            }
        }
    }

    private fun setList() {
        guessAdapter = GuessListAdapter()
        recycler_playable_result.adapter = guessAdapter
        recycler_playable_result.layoutManager = GridLayoutManager(context, 24).apply {
            spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int) = 24
            }
        }
        recycler_playable_result.addOnItemTouchListener(RecyclerTouchListener(requireContext(), recycler_playable_result, clickListener()))
    }

    private fun clickListener() = object : ClickListener {
        override fun onLongClick(view: View, position: Int) {}
        override fun onClick(view: View, position: Int) {
            PlayResultFragmentDirections.actionPlayResultFragmentToChordInfoFragmentLevel().apply {
                chord = viewModel.getResultPlayables()[position].name
            }.let { findNavController().navigate(it) }
        }
    }

}