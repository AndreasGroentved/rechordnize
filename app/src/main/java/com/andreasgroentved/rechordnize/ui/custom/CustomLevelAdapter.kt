package com.andreasgroentved.rechordnize.ui.custom

import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.playable.getSimpleString
import com.andreasgroentved.rechordnize.model.util.PlayableUtil
import com.andreasgroentved.rechordnize.model.util.SortBy
import kotlinx.android.synthetic.main.list_item_custom.view.*

class CustomLevelAdapter : RecyclerView.Adapter<CustomLevelAdapter.ChordStatHolder>() { //TODO ryd op i "kloge" adaptere

    var playables = listOf<Playable>()
    val selectedMap = mutableMapOf<String, Boolean>()
    var sortBy = SortBy.NAME

    fun selectAll(select: Boolean) {
        playables.forEach { (name) -> selectedMap[name] = select }
    }

    fun select(playables: List<String>) {
        playables.forEach { selectedMap[it] = true }
    }

    private fun getHeaderText(): String = if (playables.isEmpty()) "Loading" else playables[0].extra.getSimpleString()

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): ChordStatHolder {
        val name = if (position == 0) "" else playables[position - 1].name
        return ChordStatHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.list_item_custom, viewGroup, false), name)
    }

    override fun onBindViewHolder(holder: ChordStatHolder, position: Int) {
        if (position == 0) holder.setHeader(sortBy)
        else holder.set(playables[position - 1], sortBy, selectedMap[playables[position - 1].name] == true)
    }

    inner class ChordStatHolder(view: View, name: String) : RecyclerView.ViewHolder(view) {
        private val header = true
        private val selected = true

        init {
            view.custom_checkbox.tag = name
            view.custom_chord_info.tag = name
            setEmpty("n/a")
            view.custom_checkbox.setOnCheckedChangeListener { w, b -> selectedMap[w.tag.toString()] = b }
        }

        private fun setEmpty(name: String, isSelected: Boolean = false) = updateView(name, "n/a", header.not(), isSelected)
        fun setHeader(sortBy: SortBy): Unit = updateView("Chord", if (sortBy == SortBy.NAME) "" else SortBy.getString(sortBy), header, selected)

        fun set(playable: Playable, sortBy: SortBy, isSelected: Boolean) {
            if (playable.performances.isEmpty()) updateView(
                    playable.name,
                    if (sortBy == SortBy.NAME) ""
                    else getNAOrNot(playable, sortBy),
                    header.not(), isSelected
            )
            else updateView(playable.name, PlayableUtil.getValue(sortBy, playable), !header, isSelected)
        }

        private fun getNAOrNot(playable: Playable, sortBy: SortBy) = when (sortBy) {
            SortBy.LEVEL, SortBy.STREAK, SortBy.CORRECT, SortBy.PLAYS -> PlayableUtil.getValue(sortBy, playable)
            else -> "n/a"
        }

        private fun updateView(name: String, value: String, isHeader: Boolean, isSelected: Boolean = false) {
            itemView.apply {
                custom_chord_value.text = value
                if (isHeader || name === "n/a") {
                    custom_chord_name.text = getHeaderText(); custom_chord_value.text = value
                    custom_chord_info.visibility = GONE; custom_checkbox.visibility = GONE
                    isClickable = false; isFocusable = false
                } else {
                    isClickable = true; isFocusable = true
                    custom_chord_info.tag = name; setOnClickListener { v -> v.custom_checkbox.performClick() }
                    custom_chord_info.setOnClickListener { v ->
                        CustomLevelFragmentDirections.actionCustomLevelFragmentToChordInfoFragment().apply {
                            chord = v.tag.toString()
                        }.let {
                            v.findNavController().navigate(it)
                        }
                    }
                    custom_checkbox.tag = name; custom_checkbox.isChecked = isSelected
                    custom_chord_name.text = name
                    custom_chord_info.visibility = VISIBLE; custom_checkbox.visibility = VISIBLE
                }
            }
        }
    }

    override fun getItemCount() = playables.size + 1
}