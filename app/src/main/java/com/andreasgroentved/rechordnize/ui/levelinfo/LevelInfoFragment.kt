package com.andreasgroentved.rechordnize.ui.levelinfo

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.LevelInfoViewModelFactory
import com.andreasgroentved.rechordnize.ui.base.BaseFragment
import kotlinx.android.synthetic.main.levelinfo_content.*
import kotlinx.android.synthetic.main.main_activity.*

class LevelInfoFragment : BaseFragment() {

    private lateinit var viewModel: LevelInfoViewModel
    private lateinit var levelPlayableListAdapter: LevelInfoContentRecycler

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.levelinfo_content, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val factory = LevelInfoViewModelFactory(requireActivity().application as ChordApp)
        viewModel = ViewModelProvider(this, factory).get(LevelInfoViewModel::class.java)

        viewModel.getLevelInfoViewData(getLevelDataFromBundle(), getLevelIdFromBundle()).observe(this, Observer {
            if (it.isDeleteAble) setDeleteEnabled(true)
            setAdapter(it)
            setTile(it)
        })

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_levelinfo, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun setAdapter(levelInfoViewData: LevelInfoViewData) {
        levelPlayableListAdapter = LevelInfoContentRecycler(levelInfoViewData)
        level_info_recycler.adapter = levelPlayableListAdapter
        level_info_recycler.layoutManager = GridLayoutManager(context, 1)
    }


    override fun onResume() {
        super.onResume()
        activity?.bottom_navigation?.visibility = View.GONE
    }


    private fun setTile(levelInfoViewData: LevelInfoViewData) {
        activity?.toolbar?.let {
            it.title = levelInfoViewData.title
        }
    }

    private fun setDeleteEnabled(enabled: Boolean) {
        if (enabled) {
            activity?.toolbar?.menu?.also { menu ->
                menu.findItem(R.id.menu_item_delete)?.apply { isVisible = true }?.setOnMenuItemClickListener {
                    AlertDialog.Builder(requireContext()).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Delete level?").setPositiveButton("Yes") { _, _ ->
                        viewModel.deleteLevel().observe(this, Observer { activity?.onBackPressed() })
                    }.setNegativeButton("No") { _, _ -> }.setIcon(0).show()
                    true
                }
            }
        }
    }


    private fun getLevelDataFromBundle(): String = arguments?.let { LevelInfoFragmentArgs.fromBundle(it).levelid }?.toString()
            ?: "0"

    private fun getLevelIdFromBundle(): Int = arguments?.let { LevelInfoFragmentArgs.fromBundle(it).levelnum }
            ?: -1
}