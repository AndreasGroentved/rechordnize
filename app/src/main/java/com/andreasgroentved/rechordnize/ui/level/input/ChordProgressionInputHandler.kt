package com.andreasgroentved.rechordnize.ui.level.input

import androidx.lifecycle.LiveData


class ChordProgressionInputHandler(sequenceLength: Int, possiblePlayables: List<String>) : ChordInputHandler(sequenceLength, possiblePlayables) {
    private val secondLevel: List<List<String>> by lazy { buildSecondLevel().map { mutableListOf(it.key).apply { addAll(it.value) } } }
    private val topLevel: List<List<String>> = listOf(listOf("Steps") + possiblePlayables.map { it.split(" ")[0] }.toSet().toList())
    override fun getGuess(value: String): String = "$firstInput $value"
    override fun getInputList(text: String): List<List<String>> = secondLevel
    override fun isInputAGuess(text: String): Boolean = inputLevel == 1 || text == "skip"
    override fun getInputs(): LiveData<List<List<String>>> = input
    override fun formatInput(inputList: List<List<String>>): List<List<String>> = inputList
    override fun getTopLevel(): List<List<String>> = topLevel


}