package com.andreasgroentved.rechordnize.ui.levelinfo

import com.andreasgroentved.rechordnize.model.level.LevelModifier

data class LevelInfoViewData(val gradeNormal: Int, val gradeChallenging: Int, val gradeExtreme: Int, val adapterDataWithNameAndLevel: List<Pair<String, Int>>, val infoText: String, val title: String, val spanSize: Int, val isDeleteAble: Boolean, val modifiers: Map<Int, List<LevelModifier>>/* TODO til strings*/, val id: String, val playableTypeHeader: String)

