package com.andreasgroentved.rechordnize.ui.level

import android.graphics.Color

open class OutputHandler {

    fun formOutput(): String = formatTextRight()

    private fun formatTextRight(): String {
        return "<font color =  ${Color.BLACK}  > You are correct, " +
                "<br> the right answer is </font>"
    }


}