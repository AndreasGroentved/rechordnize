package com.andreasgroentved.rechordnize.ui.multiplayer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.base.BaseFragment
import kotlinx.android.synthetic.main.results_fragment.*

class MultiPlayerResultFragment : BaseFragment() {
    private lateinit var levelViewModel: MultiPlayerViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.multiplayer_fragment_results_, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        levelViewModel = ViewModelProvider(requireActivity()).get(MultiPlayerViewModel::class.java)
        levelViewModel.setLoading()
        setFields()
    }


    private fun setFields() {
        val adapter = MultiPlayerResultContentAdapter()
        results_recycler.adapter = adapter
        results_recycler.layoutManager = GridLayoutManager(context, 1)
        levelViewModel.endGameData.observe(viewLifecycleOwner, Observer { viewData ->
            adapter.levelViewData = viewData
            adapter.notifyDataSetChanged()
            levelViewModel.stopLoading()
        })
    }


    override fun onResume() {
        super.onResume()
        (activity as? AppCompatActivity)?.supportActionBar?.title = "Result"
    }

}