package com.andreasgroentved.rechordnize.ui.levelinfo

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.BulletSpan
import android.view.LayoutInflater
import androidx.fragment.app.DialogFragment
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.model.level.LevelModifier
import kotlinx.android.synthetic.main.dialog_level_info.view.*

class LevelDifficultyDialog(private val difficultyItems: List<LevelModifier>) : DialogFragment() {

    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = context?.let {
        val inflater = LayoutInflater.from(it)
        val dialogView = inflater.inflate(R.layout.dialog_level_info, null)
        val dialog = AlertDialog.Builder(it).setView(dialogView).create()
        dialogView.difficulty_info.text = buildBulletList(it)
        return dialog
    } ?: throw RuntimeException("invalid context")


    private fun buildBulletList(context: Context): SpannableStringBuilder {
        val arr = difficultyItems.asSequence().filter {
            when (it) {
                LevelModifier.INVERSIONS -> false
                else -> true
            }
        }.map { LevelModifier.getDescriptionText(it) }.toList()

        val bulletGap: Int = dp(10, context).toInt()
        val ssb = SpannableStringBuilder()

        arr.forEachIndexed { index, item ->
            val ss = SpannableString(item)
            ss.setSpan(BulletSpan(bulletGap), 0, item.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            ssb.append(ss)
            if (index + 1 < arr.size) ssb.append("\n")
        }
        return ssb
    }

    private fun dp(dp: Int, context: Context): Float = context.resources.displayMetrics.density * dp


}