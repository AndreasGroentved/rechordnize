package com.andreasgroentved.rechordnize.ui.dailygoal

sealed class DailyViewData

data class DailyGoalViewError(val error: String = "Error getting daily goal, click to try again") : DailyViewData()
data class DailyGoalViewData(val text: String = "Error", val currentValue: Long = -1, val outOf: Long = -1, val dateString: String = "Error") : DailyViewData()
