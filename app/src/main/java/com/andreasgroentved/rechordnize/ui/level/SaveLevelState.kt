package com.andreasgroentved.rechordnize.ui.level

import androidx.annotation.WorkerThread
import com.andreasgroentved.rechordnize.domain.IDomain
import com.andreasgroentved.rechordnize.model.level.LevelModifier
import com.andreasgroentved.rechordnize.model.level.LevelState
import com.andreasgroentved.rechordnize.model.skill.SkillType
import com.andreasgroentved.rechordnize.model.util.DateUtil
import com.andreasgroentved.rechordnize.model.util.EnumUtil
import com.andreasgroentved.rechordnize.model.util.toPlayableExtra
import kotlin.math.min

open class SaveLevelState(private val domain: IDomain) {

    private val MaxGuessTime = 20_000L

    @WorkerThread
    fun evaluateState(lState: LevelState, totalTime: Long) {
        updateUsage(lState, totalTime)
        updateSkills(lState)
        updateExp(lState)
        savePlayables(lState)

        if (!lState.isComplete) return
        if (lState.newGrade >= 3) updateResultDailySkills(lState)
        if (lState.isBetter) {
            domain.updateLevel(lState.newLevelData)
            updateResultSkill(lState)
        }
        if (shouldUnlockMoreLevels(lState))
            domain.increaseNumberOfUnlockedLevels(lState.newLevelData.levelDifficulty.toPlayableExtra())
    }

    private fun shouldUnlockMoreLevels(lState: LevelState) =
        lState.newGrade >= 3 && lState.oldGrade < 3
                && !lState.isCustom
                && lState.difficultyModifiers.any { it == LevelModifier.LEVEL_1 }

    private fun savePlayables(levelState: LevelState) =
        levelState.newLevelData.playables.let { domain.updatePlayables(it) }


    private fun updateExp(levelState: LevelState) {
        domain.setExp(levelState.expAfter)
    }

    private fun updateUsage(levelState: LevelState, totalTime: Long) {
        domain.getLevelCompletions().let { domain.setLevelCompletions(it + 1) }
        val practiseTime = min(totalTime, MaxGuessTime * levelState.rounds.size)
        domain.updateUsageToday(practiseTime, DateUtil.getToday())
        domain.updateDailySkill(SkillType.TOTAL_PRACTISE_TIME, practiseTime.toInt(), true)
    }


    private fun updateResultDailySkills(lState: LevelState) {
        val levelSkillType = EnumUtil.getLevelSkillTypeModifier(lState.difficultyModifiers)
        val levelTypeSkillType = EnumUtil.getLevelTypeSkillType(lState.newLevelData.levelDifficulty)
        if (levelSkillType == SkillType.ERROR) return
        domain.updateDailySkill(levelSkillType, 1, true)
        domain.updateDailySkill(SkillType.LEVELS_COMPLETED, 1, true)
        domain.updateDailySkill(levelTypeSkillType, 1, true)
    }


    private fun updateResultSkill(lState: LevelState) {
        val levelSkillType = EnumUtil.getLevelSkillTypeModifier(lState.difficultyModifiers)
        val levelTypeSkillType = EnumUtil.getLevelTypeSkillType(lState.newLevelData.levelDifficulty)
        if (levelSkillType == SkillType.ERROR) return
        domain.updateSkillDifference(levelSkillType, 1)
        domain.updateSkillDifference(SkillType.LEVELS_COMPLETED, 1)
        domain.updateSkillDifference(levelTypeSkillType, 1)
    }


    private fun updateSkills(levelState: LevelState) {
        levelState.rounds.forEach { round ->
            round.guessList.forEach { guessList ->
                guessList.forEachIndexed { index, guess ->
                    val isCorrect = round.correctAnswer[index] == guess || guess.contains("skip") //TODO er nok fjernet
                    domain.updateSkillCurrentConditional(SkillType.CORRECT_IN_A_ROW, isCorrect)
                    domain.updateDailySkill(SkillType.CORRECT_IN_A_ROW, 1, isCorrect)
                    if (isCorrect) {
                        domain.updateDailySkill(SkillType.NUM_OF_CORRECT, 1, true)
                        domain.updateSkillDifference(SkillType.NUM_OF_CORRECT, 1)
                    }
                    domain.updateDailySkill(SkillType.NUM_OF_GUESSES, 1, true)
                    domain.updateSkillDifference(SkillType.NUM_OF_GUESSES, 1)
                }
            }
        }
    }


}