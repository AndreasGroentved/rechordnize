package com.andreasgroentved.rechordnize.ui.intro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.IntroViewModelFactory
import kotlinx.android.synthetic.main.pager_item_withpicture.*

class IntroFragment : Fragment() {


    private lateinit var viewModel: IntroViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(if (getLayoutType() == 1) R.layout.pager_item_withpicture else R.layout.pager_item_withoutpicture, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val factory = IntroViewModelFactory(requireActivity().application as ChordApp)
        viewModel = ViewModelProvider(this, factory).get(IntroViewModel::class.java)

        viewpager_title.text = getString(getTitle())
        viewpager_text.text = getString(getTextId())
        viewpager_parent.setBackgroundColor(viewModel.getColor(getIndex()))
        if (getLayoutType() == 1)
            viewpager_image.setImageDrawable(requireContext().getDrawable(getDrawable()))

    }


    private fun getLayoutType(): Int = arguments?.getInt("type") ?: 0
    private fun getTextId(): Int = arguments?.getInt("text") ?: -1
    private fun getDrawable(): Int = arguments?.getInt("drawable") ?: -1
    private fun getTitle(): Int = arguments?.getInt("title") ?: -1
    private fun getIndex(): Int = arguments?.getInt("index") ?: 111111

}