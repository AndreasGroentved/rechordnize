package com.andreasgroentved.rechordnize.ui.general

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.model.playable.PlayableNameWithExtra
import kotlinx.android.synthetic.main.card_playable.view.lvl_value
import kotlinx.android.synthetic.main.card_playable.view.playable_name
import kotlinx.android.synthetic.main.card_playablewithindicator.view.*

class LevelPlayableListAdapter(private val withIndicator: Boolean = false) : RecyclerView.Adapter<LevelPlayableListAdapter.PlayableHolder>() {

    var list: List<PlayableNameWithExtra> = mutableListOf()
        set(other) {
            field = other; notifyDataSetChanged()
        }


    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): PlayableHolder =
        if (withIndicator)
            PlayableHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.card_playablewithindicator, viewGroup, false))
        else
            PlayableHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.card_playable, viewGroup, false))

    override fun onBindViewHolder(holder: PlayableHolder, position: Int) =
        if (withIndicator)
            list[position].let { holder.set(it.name, it.exp, it.boolean, it.expGained) }
        else
            list[position].let { holder.set(it.name, it.exp) }


    class PlayableHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun set(name: String, level: Long) {
            itemView.lvl_value.text = "$level"
            itemView.playable_name.text = name
        }

        fun set(name: String, level: Long, levelUp: Boolean, plusExp: Long) {
            itemView.lvl_value.text = "$level"
            itemView.playable_name.text = name
            itemView.level_indicator_imageView.visibility = if (levelUp) View.VISIBLE else View.GONE
            itemView.lvl_plusExp.text = "+${plusExp}xp"
        }
    }

    override fun getItemCount(): Int = list.size
}