package com.andreasgroentved.rechordnize.ui.level

import com.andreasgroentved.rechordnize.domain.level.Level
import com.andreasgroentved.rechordnize.domain.level.OfflineLevel
import com.andreasgroentved.rechordnize.model.level.LevelData
import com.andreasgroentved.rechordnize.model.level.LevelModifier
import com.andreasgroentved.rechordnize.model.level.LevelState
import com.andreasgroentved.rechordnize.model.level.Round
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.util.AlphaNumComparator
import com.andreasgroentved.rechordnize.model.util.PlayableUtil
import com.andreasgroentved.rechordnize.model.util.ExpUtil

open class LevelStateHandler {

    fun initialize(startingExp: Long, levelData: LevelData, isCustomLevel: Boolean, level: Level = OfflineLevel(levelData)) {
        levelState = LevelState(expBefore = startingExp, isCustom = isCustomLevel, newLevelData = levelData, expAfter = startingExp, difficultyModifiers = levelData.levelModifiers)
        levelData.playables = levelData.playables.sortedWith(Comparator { s1: Playable, s2: Playable -> AlphaNumComparator.compare(s1.name, s2.name) })
        this.level = level
    }


    fun isInitialized() = ::level.isInitialized

    lateinit var level: Level
    lateinit var levelState: LevelState


    fun resetLevelState(exp: Long) {
        levelState.apply {
            expBefore = exp
            expAfter = exp
            rounds.clear()
            totalTime = 0
            level.reset()
            levelUpSet.clear()
            levelUps = 0
            playableToExpMap.clear()
        }
    }

    private fun updateStars(newLevelData: LevelData, oldStars: Int, newStars: Int) {
        when {
            newLevelData.stars == 0 -> newLevelData.stars += newStars
            newStars < oldStars -> return
            else -> newLevelData.stars += newStars - oldStars
        }
    }

    private fun isBetter(oldLData: LevelData?, newLData: LevelData, levelModifiers: List<LevelModifier>) =
        PlayableUtil.isFirstLevelPlayBetterThanSecond(
            newLData.levelHistory.playMap[levelModifiers],
            oldLData?.levelHistory?.playMap?.get(levelModifiers),
            levelState.rounds.size
        )


    fun updateStateFinal(oldLevelData: LevelData?) {
        updatePlayablesExp()
        level.levelEndUpdate()

        levelState.expAfter += levelState.rounds.sumBy { it.expMap.values.sum().toInt() }.toLong()
        levelState.newLevelData = levelState.newLevelData
        levelState.oldLevelData = oldLevelData
        levelState.difficultyModifiers = levelState.newLevelData.levelModifiers
        levelState.isBetter = isBetter(oldLevelData, levelState.newLevelData, levelState.difficultyModifiers)
        levelState.oldGrade = getPlayGrade(oldLevelData, levelState.difficultyModifiers)
        levelState.newGrade = getPlayGrade(levelState.newLevelData, levelState.difficultyModifiers)
        levelState.isComplete = isLevelComplete()
        levelState.levelUps = getLevelUps()
        updateStars(levelState.newLevelData, levelState.oldGrade, levelState.newGrade)
    }

    private fun getLevelUps() =
        (ExpUtil.getLevelFromExp(levelState.expAfter) - ExpUtil.getLevelFromExp(levelState.expBefore)).toInt()


    private fun getPlayGrade(leveldata: LevelData?, difficultyModifiers: List<LevelModifier>): Int =
        leveldata?.let {
            leveldata.levelHistory.playMap[difficultyModifiers]?.let { levelPlay -> PlayableUtil.grade(levelPlay, levelPlay.numberOfRounds) }
        } ?: 0

    private fun isLevelComplete(): Boolean = level.hasLevelEnded()

    private fun updatePlayablesExp() {
        levelState.newLevelData.playables.forEach {
            val oldLevel = ExpUtil.getLevelFromExp(it.exp)
            val expGained = levelState.playableToExpMap[it.name] ?: 0
            it.exp += expGained
            val newLevel = ExpUtil.getLevelFromExp(it.exp)
            if (oldLevel < newLevel) levelState.levelUpSet.add(it.name)
        }
    }

    fun updateStateBeforeRound() {
        level.next()
        updateRoundBefore()
    }


    private fun updateRoundBefore() {
        val round = Round(number = level.roundNum, correctAnswer = level.current.map { it.name })
        levelState.rounds.add(round)
    }


    fun updateStateDuringRound(guesses: List<String>, time: Long) {
        levelState.rounds.last().also { it.time = time; it.guessList.add(guesses) }
        updateRound(levelState.rounds.last(), time, levelState.newLevelData.playables.size)
        level.guess(guesses, time)
    }


    private fun updateRound(round: Round, time: Long, possibleCount: Int) {
        val sequenceLength = round.correctAnswer.size
        val averageTime = time / sequenceLength

        val correctInSequence =
            round.correctAnswer.filterIndexed { index, s -> s == round.guessList.last()[index] }.count()

        round.correctAnswer.forEachIndexed { index, current ->
            updateGuess(current, round, index, averageTime, sequenceLength, correctInSequence, possibleCount)
        }
    }

    private fun updateGuess(current: String, round: Round, index: Int, averageTime: Long, sequenceLength: Int, correctInSequence: Int, possibleCount: Int) {
        val isCorrect = current == round.guessList.last().getOrNull(index) ?: ""
        val grade = PlayableUtil.averageTimeToGrade(isCorrect, averageTime)
        val expGained = ExpUtil.gradeGuess(grade, sequenceLength, correctInSequence, possibleCount)
        val previousExp = round.expMap[current] ?: 0
        round.expMap[current] = previousExp + expGained
        levelState.playableToExpMap[current] = (levelState.playableToExpMap[current]
            ?: 0) + expGained
    }


}