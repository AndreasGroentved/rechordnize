package com.andreasgroentved.rechordnize.ui.intro

import android.app.Application
import android.graphics.Color
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.ui.base.BaseViewModel


class IntroViewModel(application: Application) : BaseViewModel(application as ChordApp) {

    fun getColor(index:Int) = when (index) {
        0 -> Color.parseColor("#ee0290")
        1 -> Color.parseColor("#916eff")
        2 -> Color.parseColor("#90ee02")
        else -> Color.BLACK
    }
}