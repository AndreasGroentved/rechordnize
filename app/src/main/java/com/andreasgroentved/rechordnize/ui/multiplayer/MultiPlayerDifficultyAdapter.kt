package com.andreasgroentved.rechordnize.ui.multiplayer

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.andreasgroentved.rechordnize.R
import kotlinx.android.synthetic.main.card_multiplayer_level_group_upper_lower.view.*

class MultiPlayerDifficultyAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var list: List<String> = mutableListOf()
        set(other) {
            field = other; notifyDataSetChanged()
        }


    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): RecyclerView.ViewHolder =
            when (getItemViewType(position)) {
                header -> MultiPlayerHeaderHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.multiplayer_card_info, viewGroup, false))
                else -> MultiPlayerUpperLowerHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.multiplayer_card_level_item, viewGroup, false))
            }

    override fun onBindViewHolder(itemHolder: RecyclerView.ViewHolder, position: Int) = when (itemHolder) {
        is MultiPlayerHeaderHolder -> Unit
        is MultiPlayerUpperLowerHolder -> itemHolder.set(list[position - 1])
        else -> throw RuntimeException("very unintended")
    }

    override fun getItemCount(): Int = list.size + 1


    private val header = 0

    override fun getItemViewType(position: Int): Int = if (position == header) header else 1


    class MultiPlayerHeaderHolder(view: View) : RecyclerView.ViewHolder(view)
    class MultiPlayerUpperLowerHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun set(upper: String) {
            itemView.upper.text = upper
        }
    }
}