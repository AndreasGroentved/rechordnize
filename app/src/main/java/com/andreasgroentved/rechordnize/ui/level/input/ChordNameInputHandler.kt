package com.andreasgroentved.rechordnize.ui.level.input

import androidx.lifecycle.LiveData


class ChordNameInputHandler(sequenceLength: Int, possiblePlayables: List<String>) : ChordInputHandler(sequenceLength, possiblePlayables) {

    override fun getGuess(value: String): String = "$firstInput $value"
    private val secondLevel: List<List<String>> by lazy { buildSecondLevel().map { listOf(it.key) + it.value } }
    override fun isInputAGuess(text: String): Boolean = inputLevel == 1 || text == "skip" /*text.length == 1 || text.length == 2 && !text[1].isDigit()*/
    override fun getInputs(): LiveData<List<List<String>>> = input //TODO ikke helt sikker på, hvor skal overrides
    override fun getTopLevel(): List<List<String>> = listOf(getChordNames(possiblePlayables).apply { add(0, "Chords") })


    private fun getChordNames(playableNames: List<String>): MutableList<String> = playableNames.asSequence()
            .map { getNamesWithoutExtension(it) }
            .toSet().toMutableList()


    private fun getNamesWithoutExtension(name: String) = name.split(" ")[0]
    override fun formatInput(inputList: List<List<String>>): List<List<String>> = inputList
    override fun getInputList(text: String): List<List<String>> = secondLevel

}