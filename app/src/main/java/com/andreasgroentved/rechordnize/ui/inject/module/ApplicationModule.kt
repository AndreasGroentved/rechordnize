package com.andreasgroentved.rechordnize.ui.inject.module

import android.app.Application
import android.content.Context
import android.content.res.Resources
import androidx.room.Room
import com.andreasgroentved.rechordnize.Data
import com.andreasgroentved.rechordnize.IData
import com.andreasgroentved.rechordnize.backend.DailyGoalService
import com.andreasgroentved.rechordnize.db.PlayableDatabase
import com.andreasgroentved.rechordnize.domain.DatabaseSetup
import com.andreasgroentved.rechordnize.domain.Domain
import com.andreasgroentved.rechordnize.domain.IDomain
import com.andreasgroentved.rechordnize.domain.mapper.*
import com.andreasgroentved.rechordnize.domain.sound.ISoundMapper
import com.andreasgroentved.rechordnize.domain.sound.SoundMapper
import com.andreasgroentved.rechordnize.domain.sound.SoundPlayer
import com.andreasgroentved.rechordnize.pref.AppPreferencesHelper
import com.andreasgroentved.rechordnize.pref.PrefHelper
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: Application, private val debug: Boolean) { //Kig på reference til application

    @Singleton
    @Provides
    fun provideApplication(): Application = application

    @Singleton
    @Provides
    fun provideRetroFit(ok: OkHttpClient): Retrofit = Retrofit.Builder()
            .client(ok)
            .baseUrl(if (debug) "https://rechordnizebackend.azurewebsites.net" else "")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Singleton
    @Provides
    fun providesOk(): OkHttpClient = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .build()

    @Singleton
    @Provides
    fun provideDailyGoal(data: Data): DailyGoalMapper = DailyGoalMapper(data)


    @Singleton
    @Provides
    fun provideGoalService(retrofit: Retrofit): DailyGoalService = retrofit.create(DailyGoalService::class.java)

    @Singleton
    @Provides
    fun provideContext(): Context = application.applicationContext

    @Singleton
    @Provides
    fun provideDatabase(context: Context): PlayableDatabase =
            Room.databaseBuilder(context, PlayableDatabase::class.java, "5").build()

    @Singleton
    @Provides
    fun provideResources(context: Context): Resources = context.resources

    @Singleton
    @Provides
    fun provideDataAccess(database: PlayableDatabase, pref: PrefHelper, dailyGoalService: DailyGoalService): IData =
            Data(database, pref, dailyGoalService)

    @Singleton
    @Provides
    fun provideLogicAccess(
            soundMapper: ISoundMapper, chordMapper: PlayableMapper, levelMapper: LevelMapper, databaseSetup: DatabaseSetup, usageMapper: UsageMapper, generalMapper: GeneralMapper, skillMapper: SkillMapper, expMapper: ExpMapper, dailyGoalMapper: DailyGoalMapper
    ): IDomain = Domain(soundMapper, chordMapper, levelMapper, usageMapper, generalMapper, skillMapper, databaseSetup, expMapper, dailyGoalMapper)

    @Singleton
    @Provides
    fun providesPrefHelper(context: Context): PrefHelper = AppPreferencesHelper(context)

    @Singleton
    @Provides
    fun provideSoundPlayer(context: Context): SoundPlayer = SoundPlayer(context)

    @Singleton
    @Provides
    fun provideChordMapper(data: Data): PlayableMapper = PlayableMapper(data)

    @Singleton
    @Provides
    fun provideExpMapper(data: Data): ExpMapper = ExpMapper(data)

    @Singleton
    @Provides
    fun provideLevelMapper(data: Data): LevelMapper = LevelMapper(data)

    @Singleton
    @Provides
    fun provideGeneralMapper(data: Data): GeneralMapper = GeneralMapper(data)

    @Singleton
    @Provides
    fun provideUsageMapper(data: Data): UsageMapper = UsageMapper(data)

    @Singleton
    @Provides
    fun provideSkillMapper(data: Data): SkillMapper = SkillMapper(data)

    @Singleton
    @Provides
    fun provideDatabaseSetup(data: Data): DatabaseSetup = DatabaseSetup(data)

    @Singleton
    @Provides
    fun provideSoundMapper(soundPlayer: SoundPlayer): ISoundMapper = SoundMapper(soundPlayer)


    @Singleton
    @Provides
    fun provideGson() = Gson()

    //TODO string get

}