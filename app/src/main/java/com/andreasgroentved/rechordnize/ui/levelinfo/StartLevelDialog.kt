package com.andreasgroentved.rechordnize.ui.levelinfo

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.SeekBar
import androidx.navigation.NavController
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.level.DataForLevel
import kotlinx.android.synthetic.main.start_level_dialog.view.*

class StartLevelDialog {

    companion object {

        @SuppressLint("InflateParams")
        fun buildDialog(levelDiff: Int, levelId: String, levelTitle: String, context: Context, nav: NavController): AlertDialog {
            val view = LayoutInflater.from(context)!!.inflate(R.layout.start_level_dialog, null)
            val alertDialog = AlertDialog.Builder(context).setView(view).create()
            setStartLevelButton(view, levelId, levelDiff, levelTitle, alertDialog, nav)
            view.slider_text.text = "Select number of questions \n"
            setSlider(view)
            return alertDialog
        }

        private fun setStartLevelButton(view: View, levelId: String, levelDiff: Int, levelTitle: String, alertDialog: AlertDialog, nav: NavController) {
            view.start_level_button.setOnClickListener {
                val questionNum = ((view.slider_number_of_chords.progress) + 20)
                LevelInfoFragmentDirections.actionLevelInfoFragmentToMainActivity2(
                    DataForLevel(levelId, levelTitle, levelDiff, questionNum)
                ).let { nav.navigate(it) }
                alertDialog.dismiss()
            }
        }

        private fun setSlider(view: View) {
            val slider = view.slider_number_of_chords
            slider.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                    view.number_of_questions.text = "Number of questions ${((seekBar?.progress ?: 0) + 20)}"
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit
                override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit
            })

        }
    }

}
