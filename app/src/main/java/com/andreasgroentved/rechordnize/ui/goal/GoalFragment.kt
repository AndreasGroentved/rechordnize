package com.andreasgroentved.rechordnize.ui.goal

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.GoalViewModelFactory
import com.andreasgroentved.rechordnize.ui.base.BaseFragment
import kotlinx.android.synthetic.main.goal_fragment.*

class GoalFragment : BaseFragment() {

    private lateinit var viewModel: GoalsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.goal_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val factory = GoalViewModelFactory(requireActivity().application as ChordApp)
        viewModel = ViewModelProvider(this, factory).get(GoalsViewModel::class.java)
        setList(achievement_recycler)
        activity?.title = "Goals"
    }

    private fun setList(recyclerLayout: RecyclerView) {
        val goalAdapter = GoalAdapter()

        viewModel.getAllSkills().observe(this, Observer {
            goalAdapter.skills = it
        })

        recyclerLayout.adapter = goalAdapter
        recyclerLayout.layoutManager = GridLayoutManager(requireContext(), 1)
    }
}