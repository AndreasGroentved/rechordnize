package com.andreasgroentved.rechordnize.ui.multiplayer

import com.andreasgroentved.rechordnize.ui.general.AnimationProgressBarViewData


data class MultiPlayerResultViewData(
        override val expGained: Long, override val levelsGained: Int, override val levelBefore: Int, override val expBefore: Long,
        override val expAfter: Long, override val finalProgress: Int, override val beforeProgress: Int,
        val adapterData: List<Pair<String, Int>>, val levelAverageTime: String,
        val levelAccuracy: String, val grade: String, val resultComment: String
) : AnimationProgressBarViewData