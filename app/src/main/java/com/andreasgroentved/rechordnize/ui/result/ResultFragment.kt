package com.andreasgroentved.rechordnize.ui.result

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.base.BaseFragment
import com.andreasgroentved.rechordnize.ui.level.LevelViewModel
import kotlinx.android.synthetic.main.results_fragment.*

class ResultFragment : BaseFragment() {

    private lateinit var levelViewModel: LevelViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.results_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        levelViewModel = ViewModelProvider(requireActivity()).get(LevelViewModel::class.java)
        levelViewModel.setLoading()
        setFields()
    }


    private fun setFields() {
        val adapter = ResultContentAdapter()
        results_recycler.adapter = adapter
        results_recycler.layoutManager = GridLayoutManager(requireContext(), 1)

        levelViewModel.levelEndData.observe(this, Observer { viewData ->
            adapter.levelViewData = viewData; adapter.notifyDataSetChanged()
            levelViewModel.stopLoading()
        })
    }


    override fun onResume() {
        super.onResume()
        (activity as? AppCompatActivity)?.supportActionBar?.title = "Result"
    }


}