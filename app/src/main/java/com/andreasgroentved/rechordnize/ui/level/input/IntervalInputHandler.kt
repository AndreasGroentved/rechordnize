package com.andreasgroentved.rechordnize.ui.level.input

import androidx.lifecycle.LiveData


class IntervalInputHandler(sequenceLength: Int, possiblePlayables: List<String>) : InputHandler(sequenceLength, possiblePlayables) {

    private val oneToSix = listOf("P1", "m2", "M2", "m3", "M3", "P4")
    private val sevenToTwelve = listOf("A4", "P5", "m6", "M6", "m7", "M7")
    private val thirteenToEighteen = listOf("P8", "m9", "M9", "m10", "M10", "P11")
    private val nineTeenToTwentyFive = listOf("A11", "P12", "m13", "M13", "m14", "M14", "P15")
    private val secondLevel: Map<String, List<String>> = buildSecondLevel()

    override fun getGuess(value: String): String = value
    override fun isInputAGuess(text: String): Boolean = inputLevel == 0 || text == "skip"
    override fun getInputs(): LiveData<List<List<String>>> = input //TODO ikke helt sikker på, hvor skal overrides


    override fun getTopLevel(): List<List<String>> = getSecondLevel()

    private fun getSecondLevel(): List<List<String>> = secondLevel.map { listOf(it.key) + it.value }

    override fun buildSecondLevel(): Map<String, List<String>> {
        val intervalNames = possiblePlayables
        return intervalNames.groupBy { getStepMapping(it) }
    }

    private fun getStepMapping(intervalName: String) = when (intervalName) {
        in oneToSix -> "0 to 5 half steps"
        in sevenToTwelve -> "6 to 11 half steps"
        in thirteenToEighteen -> "12 to 17 half steps"
        in nineTeenToTwentyFive -> "18 to 24 half steps"
        else -> "error half steps"
    }


    override fun formatInput(inputList: List<List<String>>): List<List<String>> = inputList
    override fun getInputList(text: String): List<List<String>> = emptyList() //TODO fjern, eller i hvert fald fjern argument

}