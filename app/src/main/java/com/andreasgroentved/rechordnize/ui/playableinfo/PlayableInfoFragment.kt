package com.andreasgroentved.rechordnize.ui.playableinfo


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.util.ChordDictionary
import com.andreasgroentved.rechordnize.model.util.PlayableUtil
import com.andreasgroentved.rechordnize.ui.PlayableInfoViewModelFactory
import com.andreasgroentved.rechordnize.ui.base.BaseFragment
import com.andreasgroentved.rechordnize.ui.general.Diagrams
import kotlinx.android.synthetic.main.chord_info_chord.*
import kotlinx.android.synthetic.main.chord_info_interval.*
import kotlinx.android.synthetic.main.chord_info_interval.view.*
import kotlinx.android.synthetic.main.chord_info_progression.*
import kotlinx.android.synthetic.main.chord_info_type.*
import kotlinx.android.synthetic.main.main_activity.*
import kotlinx.android.synthetic.main.playable_info_fragment.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus

class PlayableInfoFragment : BaseFragment() {

    private lateinit var viewModel: PlayableInfoViewModel
    private lateinit var listAdapter: PlayableAdapter
    private val chordDictionary = ChordDictionary()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.playable_info_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val factory = PlayableInfoViewModelFactory(requireActivity().application as ChordApp)
        viewModel = ViewModelProvider(this, factory).get(PlayableInfoViewModel::class.java)
        setUpDiagrams()
    }

    private fun setUpDiagrams() {
        viewModel.getPlayable(getChordExtra()).observe(this, Observer {
            setChordData(it)
            activity?.toolbar?.title = viewModel.getTitle(it)
            stats_header.text = getString(R.string.stats)
            when (it.extra) {
                in PlayableExtra.getChordGroup() -> setUpChord(it.name)
                PlayableExtra.INTERVAL -> setUpInterVal(it.name)
                PlayableExtra.CHORD_PROGRESSION -> setUpProgression(it.name)
                PlayableExtra.CHORD_TYPE -> setUpType(it.name)
                else -> throw RuntimeException("unexpected playable extra type: value " + it.extra)
            }
        })
    }

    private fun setUpChord(playableName: String) {
        val extension = PlayableUtil.getChordExtension(playableName) //TODO viewmodel
        val openShapes = if (chordDictionary.openChordMap[playableName.toLowerCase()] != null) listOf(PlayableExtra.OPEN) else emptyList()
        val shapes = PlayableUtil.removeDuplicate(openShapes, chordDictionary.chordShapeMap.getValue(extension).keys.toList(), playableName.toLowerCase())
        val viewArea: View = layoutInflater.inflate(R.layout.chord_info_chord, info_area)
        playable_header.text = "Chord"
        playable_info_text.text = getString(R.string.playable_info_text, playableName, "chord")
        val adapter = ChordDiagramAdapter(playableName, PlayableExtra.CHORD, shapes, childFragmentManager)
        diagram_pager.adapter = adapter
        viewArea.viewTreeObserver.run {
            addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    if (isAlive) removeOnGlobalLayoutListener(this)
                    diagram_pager?.adapter?.notifyDataSetChanged()
                }
            })
        }
    }

    private fun setUpInterVal(playableName: String) {
        val viewArea: View = layoutInflater.inflate(R.layout.chord_info_interval, info_area)
        val distance: Int = chordDictionary.nameToDistanceMap[playableName]!!
        val s = if (distance != 1) "s" else ""
        chord_info_interval_header.text = getString(R.string.step_distance, distance.toString(), s)
        playable_header.text = "Interval"
        playable_info_text.text = getString(R.string.playable_info_text, playableName, "interval")
        info_view_parent_interval.setOnClickListener {
            (lifecycleScope + Dispatchers.Default).launch {
                viewModel.playIntervalOrTypeSound(playableName, PlayableExtra.INTERVAL)
            }
        }

        viewArea.interval_diagram.apply {
            viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    if (viewTreeObserver.isAlive) viewTreeObserver.removeOnGlobalLayoutListener(this)    //TODO trådning?
                    setImageBitmap(Diagrams.buildIntervalDiagram(distance, width, height, resources))
                }
            })
        }
    }

    private fun setUpType(playableName: String) {
        layoutInflater.inflate(R.layout.chord_info_type, info_area)
        val intervals = chordDictionary.chordTypeToNotes[playableName]!!
        type_info.text = intervals
        playable_info_text.text = getString(R.string.playable_info_text, playableName, "chord type")
        playable_header.text = "Chord type"
        info_view_parent_type.setOnClickListener {
            (lifecycleScope + Dispatchers.Default).launch {
                viewModel.playIntervalOrTypeSound(playableName, PlayableExtra.CHORD_TYPE)
            }
        }
/*
        type_notes.text = viewModel.getIntervalString(intervals)*/

    }


    private fun setUpProgression(playableName: String) {
        layoutInflater.inflate(R.layout.chord_info_progression, info_area)
        val progStep = chordDictionary.progressionToName[playableName.split(" ")[0]]!!.toString() //TODO
        prog_info.text = getString(R.string.x_step_of_the_sacle, progStep)
        playable_header.text = "Chord progression"
        playable_info_text.text = getString(R.string.playable_info_text, playableName, "chord progression")
        info_view_parent_progression.setOnClickListener {
            (lifecycleScope + Dispatchers.Default).launch {
                viewModel.playProgression(playableName)
            }
        }
        chord_info_progression_header.text = playableName
    }


    private fun setChordData(playable: Playable) {
        val names = resources.getStringArray(R.array.chord_info_left).toList()
        listAdapter = PlayableAdapter(playable, names)
        stats_header.text = getString(R.string.stats)
        chord_info_chord_recycler.layoutManager = GridLayoutManager(requireContext(), 1)
        chord_info_chord_recycler.adapter = listAdapter
        listAdapter.notifyDataSetChanged()
    }

    private fun getChordExtra(): String = arguments?.let { PlayableInfoFragmentArgs.fromBundle(it).chord }
            ?: "error"

}
