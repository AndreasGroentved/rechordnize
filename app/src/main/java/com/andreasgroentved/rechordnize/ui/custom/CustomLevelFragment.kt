package com.andreasgroentved.rechordnize.ui.custom

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.model.level.LevelData
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.util.SortBy
import com.andreasgroentved.rechordnize.ui.CustomViewModelFactory
import com.andreasgroentved.rechordnize.ui.base.BaseFragment
import com.andreasgroentved.rechordnize.util.SnackUtil
import kotlinx.android.synthetic.main.fragment_custom.*
import kotlinx.android.synthetic.main.main_activity.*


class CustomLevelFragment : BaseFragment() {

    private lateinit var listAdap: CustomLevelAdapter
    private lateinit var viewModel: CustomViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_custom, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val factory = CustomViewModelFactory(requireActivity().application as ChordApp)
        viewModel = ViewModelProvider(this, factory).get(CustomViewModel::class.java)
        getLists()

        continue_learning.setOnClickListener {
            if (viewModel.getSelected(listAdap.selectedMap).size < 3)
                SnackUtil.getSnack("Select at least 3", custom_recycler, requireContext()).show()
            else
                startLevel()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_custom, menu)
        activity?.toolbar?.let {
            it.menu?.let { setSelectAllOfTypeMenu(it) }
            it.title = viewModel.getTitle()
        }
    }

    private fun getLists() {
        listAdap = CustomLevelAdapter()
        custom_recycler.adapter = listAdap
        custom_recycler.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
        custom_recycler.layoutManager = GridLayoutManager(requireContext(), 1)
        val type = getPlayableTypeFromBundle()

        viewModel.getAllPlayableOfType(type).observe(this, Observer {
            println(it.sortedWith(Comparator { a: Playable, b: Playable -> b.lastPlayed.compareTo(a.lastPlayed) }).map { it.name + "," + it.lastPlayed })
            listAdap.playables = it
            chord_practise_text.text = viewModel.getHeaderText()
        })

        viewModel.sortLiveData().observe(this, Observer {
            listAdap.notifyDataSetChanged()
        })
    }


    private fun startLevel() {
        val levelData: LevelData = viewModel.getLevelData(listAdap.selectedMap)
        viewModel.saveLevel(levelData).observe(this, Observer { id ->
            CustomLevelFragmentDirections.actionCustomLevelFragmentToLevelInfoFragment(id).apply {
                levelnum = -1 //TODO custom titel
            }.let { findNavController().navigate(it) }
        })
    }


    private fun setSelectAllOfTypeMenu(menu: Menu) {
        if (viewModel.shouldShowSelectAllOfType()) menu.findItem(R.id.customMenu_selectType).isVisible = true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.customMenu_check_all_none -> checkAllNone(item)
            R.id.customMenu_sortOrder -> sortOrder(item)
            R.id.customMenu_sortBy -> sortBy()
            R.id.customMenu_selectType -> selectType()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun sortBy() {
        val sortByDialog = SortDialog(viewModel.sortByList)
        sortByDialog.getReturnData().observe(this, Observer {
            viewModel.sortType = SortBy.fromString(it)
            listAdap.sortBy = viewModel.sortType
            sortByDialog.dismiss()
            sort()
        })
        fragmentManager?.let { sortByDialog.show(it, "sortBy") }
    }

    private fun selectType() {
        val sortByDialog = SortDialog(viewModel.getSelectAllOfType())
        sortByDialog.getReturnData().observe(this, Observer {
            val mapOfPlayables = listAdap.selectedMap
            val listOfPlayables = listAdap.playables
            viewModel.getSelectFromType(mapOfPlayables, it, listOfPlayables)
            listAdap.notifyDataSetChanged()
            sortByDialog.dismiss()
        })
        fragmentManager?.let { sortByDialog.show(it, "selectType") }
    }

    private fun sortOrder(item: MenuItem) {
        if (viewModel.sortAsc) item.setIcon(R.drawable.sort_desc)
        else item.setIcon(R.drawable.sort_asc)
        viewModel.sortAsc = !viewModel.sortAsc
        sort()
    }

    private fun checkAllNone(item: MenuItem) {
        viewModel.allChecked = !viewModel.allChecked //TODO invert...?
        item.icon = if (viewModel.allChecked) resources.getDrawable(R.drawable.check_box_marked, null) else resources.getDrawable(R.drawable.check_box_unmarked, null)
        listAdap.selectAll(viewModel.allChecked) //TODO tjek om er nok
        listAdap.notifyDataSetChanged()
    }


    private fun sort(): Unit = viewModel.sort()
    private fun getPlayableTypeFromBundle(): Int = arguments?.let { CustomLevelFragmentArgs.fromBundle(it).playabletype }
            ?: 5

    override fun onStop() {
        super.onStop()
        viewModel.selection = viewModel.getSelected(listAdap.selectedMap)
    }

    override fun onResume() {
        super.onResume()
        listAdap.select(viewModel.selection)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.bottom_navigation?.visibility = View.GONE
    }


}