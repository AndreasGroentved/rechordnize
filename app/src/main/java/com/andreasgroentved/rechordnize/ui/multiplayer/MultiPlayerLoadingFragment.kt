package com.andreasgroentved.rechordnize.ui.multiplayer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.base.BaseFragment
import kotlinx.android.synthetic.main.multiplayer_fragment_waiting.*

class MultiPlayerLoadingFragment : BaseFragment() {

    private lateinit var viewModel: MultiPlayerViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.multiplayer_fragment_waiting, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(requireActivity()).get(MultiPlayerViewModel::class.java)
        setFields()
    }

    private fun setFields() {
        setAdapter()
        start_level.setOnClickListener {
            if (viewModel.isReady) {
                viewModel.unReadyForGame()
                start_level.text = "Ready"
            } else {
                viewModel.readyForGame()
                start_level.text = "Not ready"
            }
        }

        viewModel.startingTimerMediator.observe(viewLifecycleOwner, Observer {
            waiting_header.text = it
        })
    }

    private fun setAdapter() {
        val adapter = MultiplayerPlayerListAdapter()
        player_recycler.layoutManager = GridLayoutManager(requireContext(), 1)
        player_recycler.adapter = adapter
        viewModel.playerListMediator.observe(viewLifecycleOwner, Observer { adapter.list = it })

    }

    override fun onResume() {
        super.onResume()
        (activity as? AppCompatActivity)?.supportActionBar?.title = "Connecting..."
    }

}