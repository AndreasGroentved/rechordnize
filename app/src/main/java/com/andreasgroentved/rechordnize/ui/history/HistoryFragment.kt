package com.andreasgroentved.rechordnize.ui.history

import android.graphics.Typeface.BOLD
import android.graphics.Typeface.NORMAL
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.HistoryViewModelFactory
import com.andreasgroentved.rechordnize.ui.base.BaseFragment
import kotlinx.android.synthetic.main.history_fragment.*

class HistoryFragment : BaseFragment() {
    private lateinit var viewModel: HistoryViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.history_fragment, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val factory = HistoryViewModelFactory(requireActivity().application as ChordApp)

        viewModel = ViewModelProvider(this, factory).get(HistoryViewModel::class.java)
        viewModel.refreshUsageToday()

        setUpWeek()
        setUpMonth()
        button_month.performClick()
    }

    private fun setUpMonth() {
        button_month.setOnClickListener {
            if (viewModel.monthList.isEmpty()) {
                viewModel.getMonth().observe(this, Observer {
                    showMonth()
                })
            } else {
                showMonth()
            }
        }
    }


    private fun setUpWeek() {
        button_week.setOnClickListener {
            if (viewModel.weekList.isEmpty()) {
                viewModel.getWeek().observe(this, Observer {
                    showWeek()
                })
            } else {
                showWeek()
            }
        }
    }

    private fun setAverageText(average: String, week: Boolean) { //TODO viewmodel - eller string resource
        val weekMonth = if (week) "week" else "month"
        average_view.text = "Average time this $weekMonth is $average minutes"
    }

    private fun showWeek() {
        button_week.alpha = 1f
        button_month.alpha = 0.6f
        button_week.setTypeface(null, BOLD); button_month.setTypeface(null, NORMAL)
        if (viewModel.observingWeek) return

        viewModel.observingWeek = true
        buildDiagram()
    }

    private fun showMonth() {
        button_week.alpha = 0.6f; button_month.alpha = 1f
        button_month.setTypeface(null, BOLD); button_week.setTypeface(null, NORMAL)

        if (!viewModel.observingWeek) return

        viewModel.observingWeek = false
        buildDiagram()
    }

    private fun buildDiagram() {
        val historyDiagram = HistoryDiagram() //TODO er det nøvdendigt med ny instans
        requireContext().let { context ->
            historyDiagram.buildDiagram(
                chart, viewModel.getList(), context, viewModel.getGoal(),
                viewModel.getAverage(), viewModel.getTodayNum(viewModel.observingWeek)
            )
            setAverageText(viewModel.getAverageString(), viewModel.observingWeek)
        }
    }


    override fun onResume() {
        super.onResume()
        activity?.title = "History" //TODO string resource, nødendig?
    }
}