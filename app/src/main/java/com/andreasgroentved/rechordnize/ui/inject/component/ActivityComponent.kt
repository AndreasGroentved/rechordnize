package com.andreasgroentved.rechordnize.ui.inject.component


import com.andreasgroentved.rechordnize.backend.IGameHub
import com.andreasgroentved.rechordnize.ui.base.BaseViewModel
import com.andreasgroentved.rechordnize.ui.inject.PerActivity
import com.andreasgroentved.rechordnize.ui.inject.module.ActivityModule
import com.andreasgroentved.rechordnize.ui.level.*
import com.andreasgroentved.rechordnize.ui.multiplayer.MultiPlayerViewModel
import dagger.Component
import javax.inject.Named

@PerActivity
@Component(dependencies = [(ApplicationComponent::class)], modules = [(ActivityModule::class)])
interface ActivityComponent {
    /* fun inject(appCompatActivity: AppCompatActivity)*/
    /*    fun inject(mainActivity: MainActivity)
        fun inject(levelViewModel: LevelViewModel)
        fun inject(levelSelection: LevelSelectionViewModel)
        fun inject(learnViewModel: LevelLearnViewModel)
        fun inject(chordInfoViewModel: PlayableInfoViewModel)
        fun inject(customViewModel: CustomViewModel)*/
    fun inject(baseViewModel: BaseViewModel)

    fun inject(levelViewModel: LevelViewModel)
    fun inject(multiPlayerViewModel: MultiPlayerViewModel)

    fun provideGameHub(): IGameHub
    fun provideStateHandler(): LevelStateHandler
    fun provideSaveLevelState(): SaveLevelState
    @Named("LevelTimer")
    fun provideLevelTimer(): ILevelTimer
    @Named("GuessTimer")
    fun provideGuessTimer(): ILevelTimer
}
