package com.andreasgroentved.rechordnize.ui.goal



data class SkillInfoData(val outOfText: String = "? of ?", val levelText: String = "Level ?", val progress: Int = -1, val headerText: String = "Error", val infoText: String = "Error")
