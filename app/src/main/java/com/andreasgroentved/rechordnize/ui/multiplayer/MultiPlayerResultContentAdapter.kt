package com.andreasgroentved.rechordnize.ui.multiplayer

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.result.ResultContentAdapter
import kotlinx.android.synthetic.main.header_and_recycler_card.view.*
import kotlinx.android.synthetic.main.multiplayer_result_card.view.*


class MultiPlayerResultContentAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() { //TODO der er meget stort overlap med resultsAdapter
    lateinit var levelViewData: MultiPlayerResultViewData


    private val info = 0
    private val exp = 1
    private val stats = 2
    private val players = 3

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): RecyclerView.ViewHolder =
            when (getItemViewType(position)) {
                info -> InfoHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.multiplayer_result_card, viewGroup, false))
                exp -> ResultContentAdapter.ExpHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.result_card_exp, viewGroup, false))
                stats -> ResultContentAdapter.StatsHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.result_card_stats, viewGroup, false))
                players -> PlayerHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.header_and_recycler_card, viewGroup, false))
                else -> throw TODO("out of bounds")
            }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = when (holder) {
        is InfoHolder -> holder.set(levelViewData)
        is ResultContentAdapter.ExpHolder -> holder.set(levelViewData)
        is PlayerHolder -> holder.set(levelViewData.adapterData)
        is ResultContentAdapter.StatsHolder -> holder.set(levelViewData.levelAverageTime, levelViewData.levelAccuracy)
        else -> throw TODO("out of bounds")
    }


    override fun getItemCount(): Int = if (::levelViewData.isInitialized) 4 else 0
    override fun getItemViewType(position: Int): Int = if (position == info) info else if (position == exp) exp else if (position == stats) stats else players


    private class InfoHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun set(viewData: MultiPlayerResultViewData) {
            itemView.result_comment.text = viewData.resultComment
        }
    }

    class PlayerHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun set(adapterData: List<Pair<String, Int>>) {
            val adapter = MultiPlayerResultAdapter(adapterData)
            adapter.notifyDataSetChanged()
            itemView.generic_recycler.adapter = adapter
            itemView.title.text = "Players"
            itemView.generic_recycler.layoutManager = GridLayoutManager(itemView.context, 1)
        }
    }


}