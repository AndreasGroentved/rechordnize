package com.andreasgroentved.rechordnize.ui.general

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item_two_spread.view.*

class LeftRightHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun updateView(left: String, right: String) {
        itemView.left_side.text = left
        itemView.right_side.text = right
    }
}