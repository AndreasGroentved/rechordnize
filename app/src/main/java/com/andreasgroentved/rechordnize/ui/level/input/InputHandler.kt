package com.andreasgroentved.rechordnize.ui.level.input

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.andreasgroentved.rechordnize.model.util.AlphaNumComparator
import com.andreasgroentved.rechordnize.model.util.ChordDictionary
import com.andreasgroentved.rechordnize.model.util.PlayableUtil


abstract class InputHandler(val sequenceLength: Int, protected val possiblePlayables: List<String>) {
    protected val chordDictionary = ChordDictionary()
    val input: MediatorLiveData<List<List<String>>> = MediatorLiveData()
    var inputLevel = 0
    var firstInput: String = ""


    init {
        if (sequenceLength !in 1..10) throw IllegalArgumentException("Sequencelength cannot be $sequenceLength")
    }

    fun reset() {
        inputLevel = 0
        input.postValue(getTopLevel())
    }

    fun updateInput(text: String? = null): List<List<String>> {
        if (text == null) return getTopLevel()
        if (inputLevel == 0) firstInput = text
        inputLevel++
        val inputList: List<List<String>> = getInputList(text)
        return formatInput(inputList)
    }

    open fun formatInput(inputList: List<List<String>>): List<List<String>> = inputList
    abstract fun getInputList(text: String): List<List<String>>
    abstract fun getTopLevel(): List<List<String>>
    abstract fun isInputAGuess(text: String): Boolean
    abstract fun getInputs(): LiveData<List<List<String>>>
    abstract fun getGuess(value: String): String


    protected abstract fun buildSecondLevel(): Map<String, List<String>>


}