package com.andreasgroentved.rechordnize.ui.history

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.model.usage.Usage
import com.andreasgroentved.rechordnize.model.util.DateUtil
import com.andreasgroentved.rechordnize.ui.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import java.text.DecimalFormat
import java.util.*

class HistoryViewModel(application: Application) : BaseViewModel(application as ChordApp) {

    var observingWeek = true
    private val week = true
    private val month = false

    var monthList = listOf<CoordinatesXYLabel>(); private set
    var weekList = listOf<CoordinatesXYLabel>(); private set

    fun refreshUsageToday() {
        (GlobalScope + Dispatchers.IO).launch {//TODO scope provider
            domain.updateUsageToday(0, DateUtil.getToday())
        }
    }

    fun getWeek(): LiveData<List<CoordinatesXYLabel>> = getPeriod(week)
    fun getMonth(): LiveData<List<CoordinatesXYLabel>> = getPeriod(month)

    private fun getPeriod(weekElseMonth: Boolean): LiveData<List<CoordinatesXYLabel>> =
        Transformations.map(
            if (weekElseMonth) domain.getUsageFromThisWeek() else domain.getUsageFromThisMonth()
        ) {
            val allMonth = if (weekElseMonth) fillAllWeek(it) else fillAllMonth(it)
            allMonth.mapIndexed { index, (time): Usage ->
                CoordinatesXYLabel(
                    index.toFloat(), getMinutes(time),
                    if (weekElseMonth) intToDayLabel.getValue(index) else (index + 1).toString()
                )
            }.apply { if (weekElseMonth) weekList = this else monthList = this }
        }


    fun getTodayNum(isWeek: Boolean) = if (isWeek) DateUtil.getDayOfTheWeek(Date()) else DateUtil.getDayOfTheMonth(Date())
    fun getGoal() = domain.getUsageGoal().toFloat()
    fun getAverage(): Float = (if (observingWeek) weekList else monthList)
        .let { list ->
            (0..getTodayIndex(list, observingWeek)).map { list[it].y }
                .average()
                .toFloat()
        }

    private fun getTodayIndex(list: List<CoordinatesXYLabel>, isWeek: Boolean): Int {
        list.forEachIndexed { index, (x) ->
            if (isIndexToday(isWeek, x)) return index
        }
        return -1
    }

    private fun isIndexToday(isWeek: Boolean, x: Float): Boolean =
        (if (isWeek) getTodayNum(isWeek) - 1 else getTodayNum(isWeek)).let {today -> x.toInt() == today  }

    fun getAverageString() = DecimalFormat("#.#").format(getAverage())!!

    private fun fillAllWeek(usages: List<Usage>): List<Usage> { //TODO unødvendigt performance problem
        val currentDay = DateUtil.getStartAndEndOfWeek(System.currentTimeMillis()).first
        val daysOfAWeek = 7
        return fillFromWithNumberOfDays(usages, currentDay, daysOfAWeek)
    }

    private fun fillAllMonth(usages: List<Usage>): List<Usage> { //TODO er begrænset til nuværende månede
        val daysOfMonth = DateUtil.getDaysOfMonth(System.currentTimeMillis())
        val currentDay = DateUtil.startEndOfMonth(System.currentTimeMillis()).first
        return fillFromWithNumberOfDays(usages, currentDay, daysOfMonth)
    }

    private fun fillFromWithNumberOfDays(usages: List<Usage>, fromDay: String, numOfDays: Int): List<Usage> {
        var currentDay = fromDay
        val usagesWithEmptyDays = mutableListOf<Usage>()
        var indexOfUsageList = 0

        repeat(numOfDays) {
            var added = false
            if (indexOfUsageList < usages.size) {
                val usage = usages[indexOfUsageList]
                if (currentDay == usage.date) {
                    usagesWithEmptyDays.add(usage)
                    indexOfUsageList++
                    added = true
                }
            }
            if (added.not()) usagesWithEmptyDays.add(Usage(0, currentDay))
            currentDay = DateUtil.getDayAfter(currentDay)
        }
        return usagesWithEmptyDays
    }

    fun getList() = if (observingWeek) weekList else monthList

    private fun getMinutes(ms: Long): Float = ms / 60_000F
    private val intToDayLabel: Map<Int, String> =
        mapOf(0 to "Mon", 1 to "Tue", 2 to "Wed", 3 to "Thu", 4 to "Fri", 5 to "Sat", 6 to "Sun")

    data class CoordinatesXYLabel(val x: Float, val y: Float, val label: String) //TODO med dato, så der kan tages forbehold for start dato er senere end første dag i en uge/måned goal, chart text
}