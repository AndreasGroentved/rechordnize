package com.andreasgroentved.rechordnize.ui.result

import com.andreasgroentved.rechordnize.model.level.LevelModifier
import com.andreasgroentved.rechordnize.model.level.LevelPlay
import com.andreasgroentved.rechordnize.model.util.PlayableUtil
import java.text.DecimalFormat

object ResultTextUtil {


    /*
 * Første gang -> New record, hvis mindre end 5 stjerner ->conditions for næste difficulty level, hvis mere end 2 stjerner -> næste exp unlocked
 * Anden gang -> New record - you improved your time by - hvis mere end 3 stjerner - exp unlocked ellers improve your time by,
 * ellers tid til improvement af gammel rekord
 **/

    /*Good job - altid!!! */
    /* New record /// tid til sidste forsøg -> lang tekst hvis ikke nedenunder - ellers kort  - improvement/det modsatte*/
    /* Level unlocked /// levelDifficulty exp unlocked ///  */

    fun getResultText(oldLevelPlay: LevelPlay?, newLevelPlay: LevelPlay, level: LevelModifier, isCustom: Boolean): List<String> {
        if (level == LevelModifier.ERROR_MODIFIER) TODO()
        val oldTime = PlayableUtil.getAverageTime(oldLevelPlay?.plays, oldLevelPlay?.plays?.size    ?: 0)
        val newTime = PlayableUtil.getAverageTime(newLevelPlay.plays, newLevelPlay.plays.size)
        val oldGrade = PlayableUtil.averageTimeToGrade(true, oldTime.toLong())
        val newGrade = PlayableUtil.averageTimeToGrade(true, newTime.toLong())
        val firstTime = oldLevelPlay == null
        val newMoreThanThreeAndOldLess = newMoreThanThreeOldLess(oldGrade, newGrade)

        val newDifficultyUnlocked = newLevelDifficultyUnlocked(level, newMoreThanThreeAndOldLess)
        val newLevelUnlocked = newLevelUnlocked(level, newMoreThanThreeAndOldLess, isCustom)

        val goodJobText = getGoodJobText()
        val timeText = if (firstTime) getFirstTimeText() else getBetterOrWorseTimeText(oldTime, newTime)
        val unlockText = getLevelOrDifficultyUnlockedText(newDifficultyUnlocked, newLevelUnlocked)

        return ArrayList<String>(3).apply {
            add(goodJobText)
            add(timeText)
            if (newDifficultyUnlocked || newLevelUnlocked) add(unlockText)
        }
    }

    private fun getLevelOrDifficultyUnlockedText(levelDifUnlocked: Boolean, levelUnlocked: Boolean) =
        if (levelUnlocked) "Next level unlocked" else if (levelDifUnlocked) "Harder difficulty unlocked" else ""

    private fun getGoodJobText() = "Good job" //String resource
    private fun getFirstTimeText() = "New record"

    private fun getBetterOrWorseTimeText(oldTime: Double, newTime: Double): String {
        val df = DecimalFormat("#.#") //TODO duplikat
        //TODO condition hvis under 1 s
        return when {
            oldTime < newTime -> "Improve your average time by ${df.format((newTime - oldTime) / 1000.0)}s to beat your previous record"
            else -> "You beat your old average time by ${df.format((oldTime - newTime) / 1000.0)}s "
        }
    }

    private fun newLevelUnlocked(level: LevelModifier, newMoreThanThreeAndOldLess: Boolean, isCustom: Boolean) =
        if (level == LevelModifier.LEVEL_1 && !isCustom) newMoreThanThreeAndOldLess else false

    private fun newMoreThanThreeOldLess(oldGrade: Int, newGrade: Int) = (oldGrade < 3) && (newGrade > 2)

    private fun newLevelDifficultyUnlocked(level: LevelModifier, newMoreThanThreeAndOldLess: Boolean) =
        if (level != LevelModifier.LEVEL_3) newMoreThanThreeAndOldLess else false
}