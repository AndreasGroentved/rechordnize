package com.andreasgroentved.rechordnize.ui.inject.module

import com.andreasgroentved.rechordnize.backend.GameHub
import com.andreasgroentved.rechordnize.backend.IGameHub
import com.andreasgroentved.rechordnize.domain.IDomain
import com.andreasgroentved.rechordnize.ui.level.ILevelTimer
import com.andreasgroentved.rechordnize.ui.level.LevelStateHandler
import com.andreasgroentved.rechordnize.ui.level.LevelTimer
import com.andreasgroentved.rechordnize.ui.level.SaveLevelState
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class ActivityModule {

    @Provides
    fun provideStateHandler(): LevelStateHandler = LevelStateHandler()

    @Provides
    fun provideSaveLevelState(domain: IDomain): SaveLevelState = SaveLevelState(domain)


    @Named("GuessTimer")
    @Provides
    fun provideGuessTimer(): ILevelTimer = LevelTimer()


    @Named("LevelTimer")
    @Provides
    fun provideLevelTimer(): ILevelTimer = LevelTimer()

    @Provides
    fun provideGameHub(gson: Gson): IGameHub = GameHub("https://rechordnizebackend.azurewebsites.net/game", gson)

}