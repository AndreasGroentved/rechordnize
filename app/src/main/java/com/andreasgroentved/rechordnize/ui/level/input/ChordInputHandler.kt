package com.andreasgroentved.rechordnize.ui.level.input

import com.andreasgroentved.rechordnize.model.util.AlphaNumComparator
import com.andreasgroentved.rechordnize.model.util.PlayableUtil

abstract class ChordInputHandler (sequenceLength: Int, possiblePlayables: List<String>) : InputHandler(sequenceLength, possiblePlayables) {

    override fun buildSecondLevel(): Map<String, List<String>> {
        val secondMap = mutableMapOf<String, MutableList<String>>()
        val playableExtensions = possiblePlayables.asSequence().map { PlayableUtil.getChordExtension(it) }.toSet()
        playableExtensions.forEach {
            val mapping = chordDictionary.typeToGrouping[it]
            if (secondMap.containsKey(mapping)) secondMap[mapping]!!.add(it)
            else secondMap[it] = mutableListOf(it)
        }

        return secondMap.toSortedMap(Comparator { o1, o2 -> AlphaNumComparator.compare(o1, o2) })
    }
}
