package com.andreasgroentved.rechordnize.ui.playableinfo


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.PlayableInfoViewModelFactory
import com.andreasgroentved.rechordnize.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_chord_diagram.*
import kotlinx.coroutines.launch



class ChordDiagramFragment : BaseFragment() {

    private lateinit var viewModel: PlayableInfoViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_chord_diagram, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val factory = PlayableInfoViewModelFactory(requireActivity().application as ChordApp)
        viewModel = ViewModelProvider(this, factory).get(PlayableInfoViewModel::class.java)
        setChordDiagram()
    }

    private fun setChordDiagram() {
        chord_diagram.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                if (chord_diagram.viewTreeObserver.isAlive) chord_diagram.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val width = chord_diagram.width
                val height = chord_diagram.height
                if (width == 0) return
                val bitmap = viewModel.getChordBitmap(getPlayableNameFromBundle(), getChordShapeFromBundle(), width, height)
                chord_diagram.setOnClickListener {
                    lifecycleScope.launch {
                        viewModel.playSound(getPlayableNameFromBundle(), getChordShapeFromBundle())
                    }
                }
                chord_diagram.setImageBitmap(bitmap)
            }
        })
    }

    private fun getPlayableNameFromBundle() = arguments!!.getString("chordname") ?: ""
    private fun getChordShapeFromBundle() = arguments!!.getString("shape") ?: ""

}