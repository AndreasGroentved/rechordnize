package com.andreasgroentved.rechordnize.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.R
import kotlinx.android.synthetic.main.main_activity.*


class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        setSupportActionBar(toolbar)
        val factory = MainViewModelFactory(application as ChordApp)
        viewModel = ViewModelProvider(this, factory).get(MainViewModel::class.java)
        val navController = Navigation.findNavController(this, R.id.nav_host)
        setUpBottomNav(navController)
        setupActionBar(navController)
        firstTime()
    }

    private fun firstTime() {
        if (viewModel.getAndSetFirstTime())
            Navigation.findNavController(this, R.id.nav_host).navigate(R.id.destination_help)
    }

    private fun setUpBottomNav(nav: NavController) {
        NavigationUI.setupWithNavController(bottom_navigation, nav)
    }

    private fun setupActionBar(nav: NavController) {
        NavigationUI.setupActionBarWithNavController(this, nav)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val navigated = NavigationUI.onNavDestinationSelected(item!!, Navigation.findNavController(this, R.id.nav_host))
        return navigated || super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean =
            Navigation.findNavController(this, R.id.nav_host).navigateUp()
}
