package com.andreasgroentved.rechordnize.ui.level.input

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.KeyEvent.ACTION_UP
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.base.ClickListener
import com.andreasgroentved.rechordnize.ui.base.RecyclerTouchListener
import com.andreasgroentved.rechordnize.ui.general.InputsAdapter
import kotlinx.android.synthetic.main.dialog_input_multiple.view.*
import kotlinx.android.synthetic.main.list_item_one.view.*


class SelectionDialog(private val viewModel: InputViewModel) : DialogFragment() { //TODO extend AlertDialog i stedet for

    private lateinit var possiblePlayablesNames: List<String>
    private lateinit var correctSequneceNames: List<String>
    private var guessLength: Int = -1
    private val returnData: MutableLiveData<List<String>> = MutableLiveData()
    private var inputAdapter: InputsAdapter? = null
    private var guessAdapter: SimpleAdapter? = null
    private lateinit var dialogView: View


    fun set(possiblePlayablesNames: List<String>, correctSequenceNames: List<String>, guessLength: Int) {
        this.possiblePlayablesNames = possiblePlayablesNames
        this.correctSequneceNames = correctSequenceNames
        this.guessLength = guessLength
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = activity?.let {
        dialogView = if (guessLength == 1) createDialogViewSingle() else createDialogMultiple()
        val builder = AlertDialog.Builder(it)
        builder.setView(dialogView)
        setObservers()
        inputAdapter = setUpAdapter()
        builder.setCancelable(false)
        builder.setOnKeyListener { dialog, keyCode, event ->
            if ((keyCode == android.view.KeyEvent.KEYCODE_BACK) && event.action == ACTION_UP) {
                if (viewModel.back())
                    super.onDismiss(dialog)
            }
            true
        }
        isCancelable = false
        builder.create()
    } ?: throw IllegalStateException("Activity cannot be null")


    private fun setObservers() {
        viewModel.setInput(guessLength, possiblePlayablesNames, correctSequneceNames)
        viewModel.guessSequenceObserver().observe(this, Observer {
            guessAdapter?.guesses = it
        })

        viewModel.finalGuessObserver().observe(this, Observer {
            returnData.postValue(it)
        })
    }

    fun reset() {
        viewModel.resetGuess()
    }


    fun getReturnData(): LiveData<List<String>> = returnData

    @SuppressLint("InflateParams")
    private fun createDialogViewSingle(): View = LayoutInflater.from(context)!!.inflate(R.layout.dialog_input_single, null)

    @SuppressLint("InflateParams")
    private fun createDialogMultiple(): View = LayoutInflater.from(context).inflate(R.layout.dialog_input_multiple, null).apply {
        guessAdapter = SimpleAdapter(guessLength) //TODO der behøves nok kun en -> ryk til init
        this.input_guess_recycler.adapter = guessAdapter
        val gridMan = GridLayoutManager(context, 2520) //--> går op i 1,2,3,4,5,6,7,8,9,10
        gridMan.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int = 2520 / guessLength//TODO
        }
        this.input_guess_recycler.layoutManager = gridMan
    }


    private fun setUpAdapter(): InputsAdapter {
        val inputAdapter = InputsAdapter()

        viewModel.getInputs().observe(this, Observer {
            inputAdapter.inputs = it
        })

        val gridMan = GridLayoutManager(context, GRID_WIDTH_NUM)
        gridMan.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int = if (inputAdapter.isHeader(position)) GRID_WIDTH_NUM else GRID_WIDTH_NUM / 3
        }

        dialogView.input_recycler.layoutManager = gridMan
        dialogView.input_recycler.adapter = inputAdapter
        dialogView.input_recycler.addOnItemTouchListener(RecyclerTouchListener(requireContext(), dialogView.input_recycler, object : ClickListener {
            override fun onLongClick(view: View, position: Int) {}
            override fun onClick(view: View, position: Int) {
                if (inputAdapter.isHeader(position)) return
                viewModel.input(inputAdapter.getItem(position))
            }
        }))
        return inputAdapter
    }


    class SimpleAdapter(length: Int) : RecyclerView.Adapter<SimpleAdapter.SimpleHolder>() {

        var guesses: List<String> = (0 until length).map { "" }
            set(newInputs) {
                field = newInputs; notifyDataSetChanged()
            }

        override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): SimpleHolder =
            SimpleHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.list_item_one, viewGroup, false))

        override fun onBindViewHolder(holder: SimpleHolder, position: Int): Unit = holder.set(getItem(position))
        override fun getItemCount(): Int = guesses.size
        private fun getItem(position: Int): String = guesses[position]

        class SimpleHolder(view: View) : RecyclerView.ViewHolder(view) {
            fun set(text: String) {
                itemView.name.text = if (text == "") "?" else text
            }
        }
    }


    companion object {
        private const val GRID_WIDTH_NUM: Int = 12
    }

}