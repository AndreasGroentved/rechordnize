package com.andreasgroentved.rechordnize.ui.custom

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.model.level.LevelData
import com.andreasgroentved.rechordnize.model.level.LevelDifficulty
import com.andreasgroentved.rechordnize.model.level.LevelDifficulty.*
import com.andreasgroentved.rechordnize.model.level.LevelDifficulty.Companion.getTitle
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra.*
import com.andreasgroentved.rechordnize.model.skill.SkillType
import com.andreasgroentved.rechordnize.model.util.PlayableComparator
import com.andreasgroentved.rechordnize.model.util.ChordDictionary
import com.andreasgroentved.rechordnize.model.util.SortBy
import com.andreasgroentved.rechordnize.model.util.toPlayableExtra
import com.andreasgroentved.rechordnize.ui.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import java.util.*


class CustomViewModel(application: Application) : BaseViewModel(application as ChordApp) {


    private val chordDictionary = ChordDictionary()
    val sortByList: List<String>
    var allChecked = false
    var sortAsc = true
    private lateinit var chordList: MutableList<Playable>
    var sortType = SortBy.NAME
    private val levelMediator: MediatorLiveData<List<Playable>> = MediatorLiveData()
    private val saveLiveData: MutableLiveData<String> = MediatorLiveData()
    private lateinit var typeOfPlayables: LevelDifficulty
    private val sortingEnded: MutableLiveData<Boolean> = MediatorLiveData()
    var selection: List<String> = listOf() //ikke så pænt

    init {
        sortByList = initSortByList()
    }

    fun getSelectAllOfType() = when (getType()) {
        CHORD_PROGRESSION -> listOf("standard", "standard + 7", "dominant 7")
        CHORD -> chordDictionary.chordShapeMap.keys.toList()
        else -> emptyList()
    }

    fun getHeaderText() = "Select the " + when (getType()) {
        CHORD -> "chords"
        CHORD_PROGRESSION -> "chord progression"
        INTERVAL -> "intervals"
        CHORD_TYPE -> "types"
        else -> "errors"
    } + " you want to practise.\n Select at least 3 " + when (getType()) {
        CHORD -> "chords"
        CHORD_PROGRESSION -> "chords"
        INTERVAL -> "intervals"
        CHORD_TYPE -> "types"
        else -> "errors"
    } + "."


    fun getSelectFromType(playableMap: MutableMap<String, Boolean>, selectAllOf: String, playableList: List<Playable>) {
        when (selectAllOf) {
            "standard" -> playableList.filter { it.name in standard }.forEach { playableMap[it.name] = true }
            "standard + 7" -> playableList.filter { it.name in standardAnd7 }.forEach { playableMap[it.name] = true }
            "dominant 7" -> playableList.filter { it.name in dominant7 }.forEach { playableMap[it.name] = true }
            else -> playableList.filter { it.name.split(" ")[1] == selectAllOf }.forEach { playableMap[it.name] = true } //Antag type
        }
    }


    private val standard = listOf("I maj", "II min", "III min", "IV maj", "V maj", "VI min", "VII dim")
    private val standardAnd7 = listOf("I maj7", "II min7", "III min7", "IV maj7", "V maj7", "VI min7", "VII dim7")
    private val dominant7 = listOf("I 7", "II 7", "III 7", "IV 7", "V 7", "VI 7", "VII 7")

    private fun initSortByList() = SortBy.values().map { sortBy -> SortBy.getString(sortBy) }

    fun shouldShowSelectAllOfType(): Boolean = typeOfPlayables == CUSTOM_CHORDS || typeOfPlayables == CUSTOM_PROGRESSION

    fun sortLiveData(): LiveData<Boolean> = sortingEnded


    //TODO skub selected op i top
    fun sort() {
        (viewModelScope + Dispatchers.IO).launch {
            Collections.sort(chordList, PlayableComparator(sortType))
            if (!sortAsc) chordList.reverse()
            sortingEnded.postValue(true)
        }
    }

    fun saveLevel(levelData: LevelData): LiveData<String> {
        (GlobalScope + Dispatchers.IO).launch {
            saveLiveData.postValue(domain.insertLevelData(levelData))
            domain.updateSkillDifference(SkillType.CUSTOM_LEVEL_MADE, 1)
            domain.updateDailySkill(SkillType.CUSTOM_LEVEL_MADE, 1, true)
        }
        return saveLiveData
    }

    fun getAllPlayableOfType(intType: Int): LiveData<List<Playable>> {
        val observableData = when (PlayableExtra.fromInt(intType)) {
            INTERVAL -> {
                typeOfPlayables = CUSTOM_INTERVAL
                domain.loadIntervals()
            }
            CHORD_TYPE -> {
                typeOfPlayables = CUSTOM_CHORD_TYPE
                domain.loadTypes()
            }
            CHORD_PROGRESSION -> {
                typeOfPlayables = CUSTOM_PROGRESSION
                domain.loadProgressions()
            }
            else -> {
                typeOfPlayables = CUSTOM_CHORDS
                domain.getAllChords()
            }
        }

        levelMediator.addSource(observableData) {
            chordList = it.toMutableList()
            sort()
            levelMediator.postValue(chordList)
        }
        return levelMediator
    }

    private fun getType(): PlayableExtra = typeOfPlayables.toPlayableExtra()


    fun getTitle(): String = typeOfPlayables.getTitle()


    fun getLevelData(map: Map<String, Boolean>): LevelData { //TODO måske kigge på at gøre det muligt at mixe typer...
        val type = getType()
        val levelData = LevelData(levelDifficulty = typeOfPlayables)
        levelData.playables = getSelected(map).map { Playable(it, extra = type) }
        return levelData
    }

    fun getSelected(map: Map<String, Boolean>): List<String> = map.filter { it.value }.keys.toList()
}