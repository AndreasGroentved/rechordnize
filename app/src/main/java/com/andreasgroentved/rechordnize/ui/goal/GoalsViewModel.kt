package com.andreasgroentved.rechordnize.ui.goal

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.model.level.LevelDifficulty
import com.andreasgroentved.rechordnize.model.skill.Skill
import com.andreasgroentved.rechordnize.model.util.SkillDictionary
import com.andreasgroentved.rechordnize.model.skill.SkillType
import com.andreasgroentved.rechordnize.ui.base.BaseViewModel
import com.andreasgroentved.rechordnize.model.util.DateUtil
import kotlin.math.roundToInt


class GoalsViewModel(application: Application) : BaseViewModel(application as ChordApp) {

    init {
        AsyncTask.execute {
            domain.updateUsageToday(0,DateUtil.getToday())
        }
    }

    fun getAllSkills(): LiveData<List<SkillInfoData>> {
        val mediatorLiveData: MediatorLiveData<List<SkillInfoData>> = MediatorLiveData()

        mediatorLiveData.addSource(domain.getAllSkills()) {
            AsyncTask.execute {
                mediatorLiveData.postValue(it.map { getSkillInfo(it) })
            }
        }

        return mediatorLiveData
    }

    private fun getSkillInfo(skill: Skill): SkillInfoData {
        val maxValue = getMaxValue(skill.name)
        val level = SkillDictionary.getLevel(skill.best, maxValue)
        val toNext = SkillDictionary.getAmountAtNextLevel(level, maxValue, skill.name)
        val percent = (skill.best.toFloat() / toNext * 100).roundToInt()
        return SkillInfoData(
            "${convertInputIfNecessary(skill.best, skill.name)} of ${convertInputIfNecessary(toNext, skill.name)}",
            "Level  $level", if (percent == 0) 1 else percent,
            SkillType.getStringName(skill.name),
            SkillType.getInfoText(skill.name)
        )
    }

    private fun convertInputIfNecessary(input: Long, skillType: SkillType): String = when (skillType) {
        SkillType.TOTAL_PRACTISE_TIME, SkillType.LONGEST_PRACTISE -> DateUtil.getDurationString(input)
        else -> "$input"
    }


    private fun getMaxValue(skillType: SkillType): Long {
        return when (skillType) {
            SkillType.PROGRESSION_MASTER ->
                getSkillCount(LevelDifficulty.getProgressionLevels().map { it.intVal }.toIntArray())
            SkillType.TYPE_MASTER ->
                getSkillCount(LevelDifficulty.getTypeLevels().map { it.intVal }.toIntArray())
            SkillType.INTERVAL_MASTER ->
                getSkillCount(LevelDifficulty.getIntervalLevels().map { it.intVal }.toIntArray())
            SkillType.NAME_CHORD_MASTER ->
                getSkillCount(LevelDifficulty.getChordLevels().map { it.intVal }.toIntArray())
            SkillType.LEVELS_COMPLETED ->
                getSkillCount(
                    (LevelDifficulty.getProgressionLevels() + LevelDifficulty.getTypeLevels()
                            + LevelDifficulty.getIntervalLevels() + LevelDifficulty.getChordLevels())
                        .map { it.intVal }.toIntArray()
                )
            else -> SkillDictionary.skillToMaxValue[skillType]
                ?: throw RuntimeException("invalid skillType")
        }
    }

    private fun getSkillCount(diffs: IntArray) = domain.getLevelsWithDifficultiesCount(diffs).toLong()
}