package com.andreasgroentved.rechordnize.ui.base

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class LevelSelectionData(val data: IntArray) : Parcelable