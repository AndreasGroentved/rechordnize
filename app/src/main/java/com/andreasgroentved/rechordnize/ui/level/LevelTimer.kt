package com.andreasgroentved.rechordnize.ui.level

import javax.inject.Inject
import kotlin.math.max


class LevelTimer @Inject constructor() : ILevelTimer {
    private var timePaused = 0L
    private var pauseTime = 0L
    override var startTime: Long = -1
    private var stopTime: Long = -1
    private val timeWhenNegativeStartEndTimeDifference = 500L
    override var penaltyTime = 0L
    override var cachedTime = -1L

    //Lidt ekstra for at håndtere den teoretiske sitation, at brugeren gætter alle akkorder før de er færdigspillet
    override fun lap(): Long {
        stopTime = System.currentTimeMillis()
        if (startTime <= 0) startTime = stopTime
        val elapsedTime = ((stopTime - startTime) - pauseTime)
        cachedTime = max(0, elapsedTime) + penaltyTime
        return cachedTime
    }

    override fun startTimer() {
        penaltyTime = 0L; startTime = System.currentTimeMillis(); stopTime = -1L; pauseTime = 0; timePaused = 0
    }

    override fun reset() = run { startTimer() }

    fun pauseTimer() {
        timePaused = System.currentTimeMillis()
    }

    fun resumeTimer() {
        pauseTime += if (timePaused > 0) (System.currentTimeMillis() - timePaused) else 0
    }

}