package com.andreasgroentved.rechordnize.ui.result

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.model.playable.PlayableNameWithExtra
import com.andreasgroentved.rechordnize.ui.base.ClickListener
import com.andreasgroentved.rechordnize.ui.base.RecyclerTouchListener
import com.andreasgroentved.rechordnize.ui.general.AnimationProgressBarViewData
import com.andreasgroentved.rechordnize.ui.general.LevelPlayableListAdapter
import com.andreasgroentved.rechordnize.ui.level.ResultViewData
import com.andreasgroentved.rechordnize.util.animateLevelUpProgress
import kotlinx.android.synthetic.main.header_and_recycler_card.view.*
import kotlinx.android.synthetic.main.result_card_exp.view.*
import kotlinx.android.synthetic.main.result_card_info.view.*
import kotlinx.android.synthetic.main.result_card_stats.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.plus


class ResultContentAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var levelViewData: ResultViewData
    private val info = 0
    private val exp = 1
    private val stats = 2
    private val playables = 3

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): RecyclerView.ViewHolder =
            when (getItemViewType(position)) {
                info -> InfoHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.result_card_info, viewGroup, false))
                exp -> ExpHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.result_card_exp, viewGroup, false))
                stats -> StatsHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.result_card_stats, viewGroup, false))
                playables -> PlayablesHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.header_and_recycler_card, viewGroup, false))
                else -> throw TODO("out of bounds")
            }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = when (holder) {
        is InfoHolder -> holder.set(levelViewData)
        is ExpHolder -> holder.set(levelViewData)
        is PlayablesHolder -> holder.set(levelViewData.adapterData, levelViewData.typeHeader)
        is StatsHolder -> holder.set(levelViewData.levelAverageTime, levelViewData.levelAccuracy)
        else -> throw TODO("out of bounds")
    }


    override fun getItemCount(): Int = if (::levelViewData.isInitialized) 4 else 0
    override fun getItemViewType(position: Int): Int = if (position == info) info else if (position == exp) exp else if (position == stats) stats else playables


    class InfoHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun set(viewData: ResultViewData) {
            setGrade(viewData)
        }

        private fun setGrade(viewData: ResultViewData) {
            itemView.result_stars.text = viewData.grade
            val context = itemView.context
            itemView.grade_info.setOnClickListener {
                android.app.AlertDialog.Builder(context)
                        .setTitle(context.getString(R.string.grading)).setIcon(0)
                        .setMessage(context.getString(R.string.grading_method))
                        .setNegativeButton("ok", null)
                        .show()
            }
        }
    }

    class ExpHolder(view: View) : RecyclerView.ViewHolder(view) {

        private var hasAnimated = false

        fun set(viewData: AnimationProgressBarViewData) {
            if (hasAnimated) {
                itemView.levelText?.text = "Level ${viewData.levelBefore + viewData.levelsGained}"
                itemView.levelUpProgressBar?.let { it.progress = viewData.finalProgress }
                return
            }
            hasAnimated = true
            animateLevelUpProgress(viewData, itemView.levelUpProgressBar, itemView.levelText, itemView.addedXp, (GlobalScope + Dispatchers.Main)) //TODO nope
        }
    }

    class StatsHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun set(levelAverageTime: String, levelAccuracy: String) {
            itemView.level_avg_time.text = levelAverageTime
            itemView.level_acc.text = levelAccuracy
        }
    }

    class PlayablesHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun set(adapterData: List<PlayableNameWithExtra>, header: String) {
            val adapter = LevelPlayableListAdapter(withIndicator = true)
            itemView.generic_recycler.adapter = adapter
            itemView.title.text = header
            adapter.list = adapterData
            itemView.generic_recycler.layoutManager = GridLayoutManager(itemView.context, 2).apply {
                orientation = LinearLayoutManager.HORIZONTAL
            }
            itemView.generic_recycler.addOnItemTouchListener(RecyclerTouchListener(itemView.context, itemView.generic_recycler, object : ClickListener {
                override fun onLongClick(view: View, position: Int) {}
                override fun onClick(view: View, position: Int) {
                    ResultFragmentDirections.actionResultFragmentToChordInfoFragmentLevel().apply {
                        chord = adapterData[position].name
                    }.let { view.findNavController().navigate(it) }
                }
            }))
        }
    }


}