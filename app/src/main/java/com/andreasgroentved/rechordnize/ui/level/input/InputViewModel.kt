package com.andreasgroentved.rechordnize.ui.level.input


import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.util.ChordDictionary
import com.andreasgroentved.rechordnize.model.util.PlayableUtil
import com.andreasgroentved.rechordnize.ui.base.BaseViewModel


class InputViewModel(application: Application) : BaseViewModel(application as ChordApp) {

    private var sequenceIndex = 0
    private var guessList: MutableList<String> = mutableListOf()
    private val guessSequence = MediatorLiveData<List<String>>()
    private val finalGuessLiveData = MediatorLiveData<List<String>>()
    private val chordDictionary = ChordDictionary() //TODO inject
    private var correctSequence = emptyList<String>()
    var sequenceLength = -1; private set
    private var chordInputHandler: InputHandler? = null
    private var intervalInputHandler: InputHandler? = null
    private var chordProgressionInputHandler: InputHandler? = null
    private var chordTypeInputHandler: InputHandler? = null


    fun setInput(sequenceLength: Int, possiblePlayables: List<String>, correctSequence: List<String>) {
        guessList = MutableList(sequenceLength) { "" }
        this.correctSequence = correctSequence
        this.sequenceLength = sequenceLength
        val playableMap = possiblePlayables.groupBy { PlayableUtil.getType(it, chordDictionary) }.toMap()
        playableMap[PlayableExtra.CHORD]?.let { chordInputHandler = ChordNameInputHandler(sequenceLength, it) }
        playableMap[PlayableExtra.INTERVAL]?.let { intervalInputHandler = IntervalInputHandler(sequenceLength, it) }
        playableMap[PlayableExtra.CHORD_TYPE]?.let { chordTypeInputHandler = ChordTypeInputHandler(sequenceLength, it) }
        playableMap[PlayableExtra.CHORD_PROGRESSION]?.let { chordProgressionInputHandler = ChordProgressionInputHandler(sequenceLength, it) }
        resetGuess()
    }

    fun back(): Boolean =
        if (sequenceIndex == 0) {
            resetGuess()
            true
        } else {
            sequenceIndex--
            guessList[sequenceIndex] = ""
            guessSequence.value = guessList
            false
        }

    fun input(text: String): Boolean = when {
        text.isEmpty() -> {
            sequenceIndex = 0
            finalGuessLiveData.value = listOf("")
            false
        }
        isInputAGuess(text, getTypeOfIndex(sequenceIndex)) -> {
            setInputs(sequenceIndex + 1)
            guess(text)

        }
        else -> {
            val inputList = inputToHandler(text, getTypeOfIndex(sequenceIndex))
            inputMediator.postValue(inputList)
            false
        }
    }

    private fun getTypeOfIndex(index: Int) = PlayableUtil.getType(correctSequence[index], chordDictionary)

    private fun isInputAGuess(text: String, type: PlayableExtra): Boolean = when (type) {
        PlayableExtra.CHORD -> chordInputHandler?.isInputAGuess(text)
        PlayableExtra.CHORD_PROGRESSION -> chordProgressionInputHandler?.isInputAGuess(text)
        PlayableExtra.CHORD_TYPE -> chordTypeInputHandler?.isInputAGuess(text)
        PlayableExtra.INTERVAL -> intervalInputHandler?.isInputAGuess(text)
        else -> throw IllegalArgumentException("$type is not valid") //WILL never happen...
    }!!


    private fun inputToHandler(text: String, type: PlayableExtra) =
        when (type) {
            PlayableExtra.CHORD -> chordInputHandler?.updateInput(text)
            PlayableExtra.CHORD_PROGRESSION -> chordProgressionInputHandler?.updateInput(text)
            PlayableExtra.CHORD_TYPE -> chordTypeInputHandler?.updateInput(text)
            PlayableExtra.INTERVAL -> intervalInputHandler?.updateInput(text)
            else -> throw IllegalArgumentException("$type is not valid") //WILL never happen...
        }!!


    private fun getGuessFromHandler(text: String, type: PlayableExtra) =
        when (type) {
            PlayableExtra.CHORD -> chordInputHandler?.getGuess(text)
            PlayableExtra.CHORD_PROGRESSION -> chordProgressionInputHandler?.getGuess(text)
            PlayableExtra.CHORD_TYPE -> chordTypeInputHandler?.getGuess(text)
            PlayableExtra.INTERVAL -> intervalInputHandler?.getGuess(text)
            else -> throw IllegalArgumentException("$type is not valid") //WILL never happen...
        }!!


    private fun setInputs(index: Int) {
        val indexToGet = if (index >= sequenceLength) index - 1 else index
        val topLevelInputs = when (val type = PlayableUtil.getType(correctSequence[indexToGet], chordDictionary)) {
            PlayableExtra.CHORD -> chordInputHandler?.run { reset(); updateInput() }
            PlayableExtra.CHORD_PROGRESSION -> chordProgressionInputHandler?.run { reset(); updateInput() }
            PlayableExtra.CHORD_TYPE -> chordTypeInputHandler?.run { reset(); updateInput() }
            PlayableExtra.INTERVAL -> intervalInputHandler?.run { reset(); updateInput() }
            else -> throw IllegalArgumentException("$type is not valid") //WILL never happen...
        }!!
        inputMediator.postValue(topLevelInputs)
    }


    private fun resetToHandler() {
        chordInputHandler?.reset()
        chordTypeInputHandler?.reset()
        chordProgressionInputHandler?.reset()
        chordTypeInputHandler?.reset()
    }


    fun resetGuess() {
        (0 until guessList.size).forEach { guessList[it] = "" }
        guessSequence.postValue(guessList)
        resetToHandler()
        sequenceIndex = 0
        setInputs(sequenceIndex)
    }


    private val inputMediator = MediatorLiveData<List<List<String>>>()


    fun getInputs(): LiveData<List<List<String>>> = inputMediator


    private fun guess(text: String): Boolean {
        sequenceIndex++
        resetToHandler()
        guessList[sequenceIndex - 1] = getGuessFromHandler(text, getTypeOfIndex(sequenceIndex - 1))
        if (text == "skip") skip()
        return if (sequenceIndex < sequenceLength) {
            guessSequence.value = guessList
            false
        } else {
            finalGuessLiveData.value = guessList
            true
        }
    }

    private fun skip() =
        (0 until sequenceLength).forEach { guessList[it] = "skip" }


    fun guessSequenceObserver(): LiveData<List<String>> = guessSequence
    fun finalGuessObserver(): LiveData<List<String>> = finalGuessLiveData


}