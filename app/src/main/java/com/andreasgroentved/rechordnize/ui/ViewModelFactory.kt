@file:Suppress("UNCHECKED_CAST")

package com.andreasgroentved.rechordnize.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.backend.LevelIdentifierEnum
import com.andreasgroentved.rechordnize.ui.custom.CustomViewModel
import com.andreasgroentved.rechordnize.ui.dailygoal.DailyGoalViewModel
import com.andreasgroentved.rechordnize.ui.goal.GoalsViewModel
import com.andreasgroentved.rechordnize.ui.history.HistoryViewModel
import com.andreasgroentved.rechordnize.ui.intro.IntroViewModel
import com.andreasgroentved.rechordnize.ui.level.LevelViewModel
import com.andreasgroentved.rechordnize.ui.level.input.InputViewModel
import com.andreasgroentved.rechordnize.ui.levelinfo.LevelInfoViewModel
import com.andreasgroentved.rechordnize.ui.levelselection.LevelSelectionViewModel
import com.andreasgroentved.rechordnize.ui.multiplayer.MultiPlayerViewModel
import com.andreasgroentved.rechordnize.ui.playableinfo.PlayableInfoViewModel
import com.andreasgroentved.rechordnize.ui.settings.AboutViewModel
import com.andreasgroentved.rechordnize.ui.settings.SettingsViewModel
import com.andreasgroentved.rechordnize.ui.user.UserViewModel

class MainViewModelFactory(private val app: ChordApp) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel(app) as T
    }
}

class LevelViewModelFactory(private val app: ChordApp) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LevelViewModel(app) as T
    }
}

class MultiViewModelFactory(private val app: ChordApp, private val levelIdentifierEnum: LevelIdentifierEnum) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MultiPlayerViewModel(app, levelIdentifierEnum) as T
    }
}

class LevelInfoViewModelFactory(private val app: ChordApp) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LevelInfoViewModel(app) as T
    }
}

class LevelSelectionViewModelFactory(private val app: ChordApp) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LevelSelectionViewModel(app) as T
    }
}

class HistoryViewModelFactory(private val app: ChordApp) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = HistoryViewModel(app) as T
}


class GoalViewModelFactory(private val app: ChordApp) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = GoalsViewModel(app) as T
}

class UserViewModelFactory(private val app: ChordApp) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = UserViewModel(app) as T
}

class InputViewModelFactory(private val app: ChordApp) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = InputViewModel(app) as T
}

class PlayableInfoViewModelFactory(private val app: ChordApp) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = PlayableInfoViewModel(app) as T
}

class CustomViewModelFactory(private val app: ChordApp) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = CustomViewModel(app) as T
}

class DailyGoalViewModelFactory(private val app: ChordApp) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = DailyGoalViewModel(app) as T
}

class IntroViewModelFactory(private val app: ChordApp) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = IntroViewModel(app) as T
}


class AboutViewModelFactory(private val app: ChordApp) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = AboutViewModel(app) as T
}

class SettingsViewModelFactory(private val app: ChordApp) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = SettingsViewModel(app) as T
}