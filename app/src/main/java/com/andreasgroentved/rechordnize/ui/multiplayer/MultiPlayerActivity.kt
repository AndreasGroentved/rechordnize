package com.andreasgroentved.rechordnize.ui.multiplayer

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.backend.LevelIdentifierEnum
import com.andreasgroentved.rechordnize.ui.MultiViewModelFactory
import com.andreasgroentved.rechordnize.util.SnackUtil
import kotlinx.android.synthetic.main.level_activity.nav_host
import kotlinx.android.synthetic.main.multiplayer_activity.*
import kotlinx.android.synthetic.main.multiplayer_activity.loading_progress_xml

class MultiPlayerActivity : AppCompatActivity() {


    private lateinit var viewModel: MultiPlayerViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.multiplayer_activity)
        val factory = MultiViewModelFactory(application as ChordApp, levelIdentifierFromBundle)
        viewModel = ViewModelProvider(this, factory).get(MultiPlayerViewModel::class.java)
        setupActionBar(Navigation.findNavController(this, R.id.nav_host))
        setUp()

    }


    private fun setupActionBar(nav: NavController) {
        setSupportActionBar(toolbar_multiplayer_level)
        println(supportActionBar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        NavigationUI.setupActionBarWithNavController(this, nav)
    }

    private fun setUp() {
        viewModel.startedMediator.observe(this, Observer {
            Navigation.findNavController(this, R.id.nav_host).navigate(R.id.multiInputFragment)
        })

        viewModel.endGameData.observe(this, Observer {
            Navigation.findNavController(this, R.id.nav_host).navigate(R.id.multiPlayerResultFragment)
        })

        viewModel.showLoadingLiveData.observe(this, Observer {
            if (it) loading_progress_xml.visibility = View.VISIBLE
            else loading_progress_xml.visibility = View.GONE
        })

        viewModel.getResultPlayablesLive().observe(this, Observer {
            SnackUtil.getSnack("You are correct", multiParent.parent as View, this, color = R.color.success).show()
        })

        viewModel.getIncorrectGuess().observe(this, Observer {
            SnackUtil.getSnack(it, nav_host.view!!, this, 2000).show()
        })

        viewModel.connectionProblems.observe(this, Observer {
            SnackUtil.getSnack(it, multiParent.parent as View, this).show()
        })

        viewModel.connectionFailed.observe(this, Observer {
            //TODO mere elegant...
            if (it) finish()
        })

    }


    override fun onBackPressed() {
        AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Leave game?")
                .setPositiveButton("Yes") { _, _ ->
                    finish()
                }
                .setNegativeButton("No") { _, _ -> }.setIcon(0).show()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    private val levelIdentifierFromBundle: LevelIdentifierEnum by lazy {
        intent?.extras?.let { MultiPlayerActivityArgs.fromBundle(it).gameIdentifier }
                ?: throw RuntimeException("invalid data for level activity")
    }

}