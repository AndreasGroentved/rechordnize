package com.andreasgroentved.rechordnize.ui.dailygoal

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.DailyGoalViewModelFactory
import com.andreasgroentved.rechordnize.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_daily_goal.*
import timber.log.Timber

class DailyGoalFragment : BaseFragment() {
    private lateinit var viewModel: DailyGoalViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_daily_goal, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val factory = DailyGoalViewModelFactory(requireActivity().application as ChordApp)
        viewModel = ViewModelProvider(this, factory).get(DailyGoalViewModel::class.java)
        setViewModelBindings()
    }


    private fun setViewModelBindings() {
        val liveData = viewModel.getDailyGoa1()
        liveData.observe(this, Observer<DailyViewData> {
            when (it) {
                is DailyGoalViewError -> {
                    Timber.d("failed to get goal from db and internet")
                    cardReload.visibility = View.VISIBLE
                    loading_progress_xml.visibility = View.GONE
                    cardReload.setOnClickListener {
                        liveData.removeObservers(this@DailyGoalFragment)
                        cardReload.setOnClickListener(null)
                        cardReload.visibility = View.GONE
                        loading_progress_xml.visibility = View.VISIBLE
                        setViewModelBindings()
                    }
                }
                is DailyGoalViewData -> {
                    Timber.d("successfully got daily goal")
                    cardReload.visibility = View.GONE
                    loading_progress_xml.visibility = View.GONE
                    content.visibility = View.VISIBLE
                    daily_goal_description.text = it.text
                    daily_current_value.text = it.currentValue.toString()
                    daily_goal_progressbar.progress = ((it.currentValue.toFloat() / it.outOf) * daily_goal_progressbar.max).toInt()
                    daily_goal_target.text = it.outOf.toString()
                }
            }
        })

        viewModel.getDailyGoalCompletions().observe(this, Observer {
            dailyGoalsCompleted.text = getString(R.string.number_of_daily_goals_conquered, it)
        })

    }

}