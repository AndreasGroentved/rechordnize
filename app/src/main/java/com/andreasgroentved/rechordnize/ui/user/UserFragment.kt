package com.andreasgroentved.rechordnize.ui.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.UserViewModelFactory
import com.andreasgroentved.rechordnize.ui.base.BaseFragment
import kotlinx.android.synthetic.main.user_fragment.*

class UserFragment : BaseFragment() {
    private lateinit var viewModel: UserViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.user_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val factory = UserViewModelFactory(requireActivity().application as ChordApp)
        viewModel = ViewModelProvider(this, factory).get(UserViewModel::class.java)
        setViewModelBindings()
    }


    private fun setViewModelBindings() {
        viewModel.getTotalTime().observe(viewLifecycleOwner, Observer { total_practise.text = it })
        viewModel.longestPractise().observe(viewLifecycleOwner, Observer { longest_practise.text = it })
        viewModel.getDaysInARow().observe(viewLifecycleOwner, Observer { current_days_in_a_row.text = it })
        viewModel.getMostDaysInARow().observe(viewLifecycleOwner, Observer { most_days_in_a_row.text = it })
        viewModel.getPractisedToday().observe(viewLifecycleOwner, Observer { practise_today.text = it })
        viewModel.getLevelCompletions().let { levelCompletions.text = it }
        viewModel.getStartDate().let { startDate.text = it }
        viewModel.getExpBelowAboveAndProgressToNextLevel().let {
            expBelow.text = it.first
            expAbove.text = it.second
            levelProgressBar.progress = it.third * 10 //TODO magisk
        }
        viewModel.getLevel().let { levelText.text = it }
        viewModel.getCurrentExp().let {
            expCurrent.text = it
        }
    }

    override fun onResume() {
        super.onResume()
        activity?.title = "User" //TODO string resource
    }
}