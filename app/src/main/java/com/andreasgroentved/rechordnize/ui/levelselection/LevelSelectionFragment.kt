package com.andreasgroentved.rechordnize.ui.levelselection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.LevelSelectionViewModelFactory
import com.andreasgroentved.rechordnize.ui.base.BaseFragment
import com.andreasgroentved.rechordnize.util.PremiumUtil
import com.andreasgroentved.rechordnize.util.SnackUtil
import kotlinx.android.synthetic.main.fragment_level_selection.*
import kotlinx.android.synthetic.main.main_activity.*


class LevelSelectionFragment : BaseFragment() {

    private lateinit var viewModel: LevelSelectionViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_level_selection, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val factory = LevelSelectionViewModelFactory(requireActivity().application as ChordApp)
        viewModel = ViewModelProvider(this, factory).get(LevelSelectionViewModel::class.java)
        val diffs: IntArray = getDifficultiesFromBundle()
        createListViews(diffs)
        setUpFAB()
    }


    private fun createListViews(diffs: IntArray) {
        val adapter = LevelTopListAdapter()
        level_selection_recycler.adapter = adapter
        level_selection_recycler.layoutManager = GridLayoutManager(requireContext(), 1)

        viewModel.getLevelsWithDifficulties(diffs).observe(this, Observer { viewData ->
            adapter.viewData = viewData
            activity?.title = viewData.title
            println(viewData)
            observeLevelDeleted()
        })
    }

    override fun onResume() {
        super.onResume()
        activity?.bottom_navigation?.visibility = View.VISIBLE
    }


    private fun getDifficultiesFromBundle(): IntArray = arguments?.let { LevelSelectionFragmentArgs.fromBundle(it).levelSelectionData?.data }
            ?: intArrayOf(0)

    private fun setUpFAB() {
        if (!PremiumUtil.isPremium()) {
            fab.hide(); return
        }
        fab.apply {
            setOnClickListener {
                val action = LevelSelectionFragmentDirections.actionDestinationLevelSelectionToCustomLevelFragment()
                        .setPlayabletype(viewModel.getPlayableType(getDifficultiesFromBundle()))
                findNavController().navigate(action)
            }
        }.show()
        addScrollListener()
    }


    private fun addScrollListener() {
        level_selection_recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) fab.hide()
                else fab.show()
            }
        })
    }


    private fun observeLevelDeleted() {
        viewModel.getLevelDeleted().observe(this, Observer {
            if (!it) return@Observer
            showDeleteSnack()
            viewModel.seenDeleted()
        })
    }




    private fun showDeleteSnack() {
        showSnackBar("Level deleted", view)
    }


    companion object {
        fun showSnackBar(text: String, view: View?) = view?.let { SnackUtil.getSnack(text, view, view.context).show() }
    }
}
