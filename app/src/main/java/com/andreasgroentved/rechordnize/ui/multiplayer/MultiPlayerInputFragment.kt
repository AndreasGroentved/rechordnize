package com.andreasgroentved.rechordnize.ui.multiplayer

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.andreasgroentved.rechordnize.ui.level.InputFragment

class MultiPlayerInputFragment : InputFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(requireActivity()).get(MultiPlayerViewModel::class.java)
        setUp()
    }

}