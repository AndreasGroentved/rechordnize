package com.andreasgroentved.rechordnize.ui.dailygoal

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.model.skill.SkillType
import com.andreasgroentved.rechordnize.ui.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class DailyGoalViewModel(application: Application) : BaseViewModel(application as ChordApp) {


    fun getDailyGoa1(): LiveData<DailyViewData> = liveData(viewModelScope.coroutineContext + Dispatchers.IO) {
        val dailyGoal = domain.getTodayGoal()

        if (dailyGoal == null) {
            emit(DailyGoalViewError())
            return@liveData
        }

        val dailySkill = domain.getDailySkill(dailyGoal.date, SkillType.fromInt(dailyGoal.skillType))
        if (dailySkill == null) {
            emit(DailyGoalViewData(
                    dailyGoal.description, 0, dailyGoal.skillValue, dailyGoal.date
            ))
        } else {
            val adjustedDailyValue = if (isTimeSkillType(dailySkill.name)) dailyGoal.skillValue * 1000L else dailyGoal.skillValue
            val adjustedSkillValue = if (isTimeSkillType(dailySkill.name)) dailySkill.value / 60_000L else dailySkill.value
            if (adjustedDailyValue >= dailyGoal.skillValue && !dailyGoal.succeeded) { //TODO dette umuliggør in a row, da den kan nulstilles
                dailyGoal.succeeded = true
                (GlobalScope).launch { domain.updateTodayGoal(dailyGoal) }
            }
            emit(DailyGoalViewData(dailyGoal.description, adjustedSkillValue, dailyGoal.skillValue, dailyGoal.date))
        }

    }

    private fun isTimeSkillType(skillType: SkillType) = when (skillType) {
        SkillType.LONGEST_PRACTISE, SkillType.TOTAL_PRACTISE_TIME -> true
        else -> false
    }

    fun getDailyGoalCompletions(): LiveData<Int> = domain.getDailyGoalCompletions()


}

