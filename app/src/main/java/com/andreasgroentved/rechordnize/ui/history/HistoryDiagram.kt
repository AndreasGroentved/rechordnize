package com.andreasgroentved.rechordnize.ui.history

import android.content.Context
import android.graphics.Typeface
import android.view.View
import com.andreasgroentved.rechordnize.R
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.Legend.LegendForm.SQUARE
import com.github.mikephil.charting.components.Legend.LegendHorizontalAlignment.LEFT
import com.github.mikephil.charting.components.Legend.LegendOrientation.HORIZONTAL
import com.github.mikephil.charting.components.Legend.LegendVerticalAlignment.BOTTOM
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.components.YAxis.YAxisLabelPosition
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import java.text.DecimalFormat


class HistoryDiagram {


    fun buildDiagram(barChart: View, xylabels: List<HistoryViewModel.CoordinatesXYLabel>, context: Context, goal: Float, average: Float, toDayValue: Int) {
        var maxValue = -1f
        if (xylabels.all { it.y < goal }) maxValue = goal + 1
        val chart = barChart as BarChart
        chart.clear()

        val xAxisFormatter: ValueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String = if (value < xylabels.size) xylabels[value.toInt()].label else "0"
        }

        setAxis(xAxisFormatter, chart, maxValue, goal, average)
        setChartLegend(chart)
        setChart(context, xAxisFormatter, chart)
        setData(xylabels, chart)
        highLightToDay(toDayValue, xylabels, chart)
        chart.invalidate()
    }

    private fun setChart(context: Context, xAxisFormatter: ValueFormatter, chart: BarChart) {
        val mv = XYMarkerView(context, xAxisFormatter)
        chart.description.isEnabled = false
        chart.marker = mv // Set the marker to the chart
        chart.setPinchZoom(false)
        chart.setScaleEnabled(false)
        mv.chartView = chart // For bounds control
    }


    private fun setAxis(xAxisFormatter: ValueFormatter, chart: BarChart, maxValue: Float, goal: Float, average: Float) {
        setXAxis(xAxisFormatter, chart)
        val custom = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String = DecimalFormat("###.#").format(value)
        }

        val leftAxis: YAxis = setYAxis(custom, maxValue, chart)
        leftAxis.removeAllLimitLines()
        val goalLine = getLine(goal, "Goal", R.color.colorLightBlack, false)
        val averageLine = getLine(average, "Average", R.color.colorLightBlack, true)
        leftAxis.addLimitLine(goalLine)
        leftAxis.addLimitLine(averageLine)
        val rightAxis = setRightAxis(custom, chart)
        rightAxis.isEnabled = false
    }

    private fun highLightToDay(toDayValue: Int, xylabels: List<HistoryViewModel.CoordinatesXYLabel>, chart: BarChart) {
        if (chart.data != null && chart.data.getDataSetByIndex(0).entryCount >= toDayValue) {
            chart.data.getDataSetByIndex(0).colors.clear()
            chart.data.getDataSetByIndex(0).colors.addAll(getColor(xylabels.size, toDayValue - 1))
        }
    }

    private fun getColor(size: Int, differentColorIndex: Int): List<Int> = (0 until size).map {
        if (it == differentColorIndex) R.color.buttonLight
        else R.color.buttonLight_active
    }

    private fun setXAxis(xAxisFormatter: ValueFormatter, chart: BarChart) {
        chart.xAxis.apply {
            position = XAxisPosition.BOTTOM; setDrawGridLines(false)
            granularity = 1f; labelCount = 7
            textSize = 14f
            axisLineWidth = 3f
            valueFormatter = xAxisFormatter
        }
    }

    private fun setYAxis(custom: ValueFormatter, maxVal: Float, chart: BarChart): YAxis = chart.axisLeft.apply {
        typeface = Typeface.DEFAULT; setLabelCount(8, false)
        valueFormatter = custom; setPosition(YAxisLabelPosition.OUTSIDE_CHART)
        textSize = 14f
        axisLineWidth = 3f
        spaceTop = 15f; axisMinimum = 0f
        if (maxVal >= 0) axisMaximum = maxVal else resetAxisMaximum()
        setDrawGridLines(false)
    }

    private fun getLine(yValue: Float, text: String, color: Int, positionLeft: Boolean): LimitLine =
            LimitLine(yValue).apply {
                textSize = 16f
                lineColor = color; lineWidth = 2f; label = text
                enableDashedLine(6f, 6f, 0f)
                labelPosition = if (positionLeft) LimitLine.LimitLabelPosition.LEFT_TOP else LimitLine.LimitLabelPosition.RIGHT_TOP
            }

    private fun setRightAxis(custom: ValueFormatter, chart: BarChart): YAxis = chart.axisRight.apply {
        setDrawGridLines(false); typeface = Typeface.DEFAULT
        setLabelCount(8, false); valueFormatter = custom
        axisMinimum = 0f
    }

    private fun setChartLegend(chart: BarChart) {
        chart.legend.apply {
            verticalAlignment = BOTTOM; horizontalAlignment = LEFT; orientation = HORIZONTAL; setDrawInside(false)
            textSize = 14f
            form = SQUARE; formSize = 8f; xEntrySpace = 4f
        }
    }

    private fun setData(xylabels: List<HistoryViewModel.CoordinatesXYLabel>, chart: BarChart) {
        val yValues = xylabels.map { BarEntry(it.x, it.y) }
        val barDataSet = BarDataSet(yValues, "Practise time in minutes").apply {
            values = yValues; setDrawIcons(false); setColors(R.color.buttonLight_active)
        }
        val data = BarData(barDataSet).apply {
            setDrawValues(false); setValueTextSize(14f); setValueTypeface(Typeface.DEFAULT); barWidth = 0.85f
        }
        chart.data = data
        chart.data.notifyDataChanged()
        chart.notifyDataSetChanged()
    }

}