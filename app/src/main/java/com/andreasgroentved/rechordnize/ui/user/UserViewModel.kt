package com.andreasgroentved.rechordnize.ui.user

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.model.skill.SkillType
import com.andreasgroentved.rechordnize.model.util.DateUtil
import com.andreasgroentved.rechordnize.model.util.ExpUtil
import com.andreasgroentved.rechordnize.model.util.normalize
import com.andreasgroentved.rechordnize.ui.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import java.text.DateFormat
import java.util.*

class UserViewModel(application: Application) : BaseViewModel(application as ChordApp) {

    init {
        (viewModelScope + Dispatchers.IO).launch {
            domain.updateUsageToday(0, DateUtil.getToday())
        }
    }

    fun getTotalTime(): LiveData<String> =
        Transformations.map(domain.getSkill(SkillType.TOTAL_PRACTISE_TIME)) { DateUtil.getDurationString(it.best) }

    fun longestPractise(): LiveData<String> =
        Transformations.map(domain.getSkill(SkillType.LONGEST_PRACTISE)) { DateUtil.getDurationString(it.best) }

    fun getDaysInARow(): LiveData<String> =
        Transformations.map(domain.getSkill(SkillType.DAYS_IN_A_ROW)) { it.current.toString() }

    fun getMostDaysInARow(): LiveData<String> =
        Transformations.map(domain.getSkill(SkillType.DAYS_IN_A_ROW)) { it.best.toString() }

    fun getPractisedToday(): LiveData<String> =
        Transformations.map(domain.getSkill(SkillType.LONGEST_PRACTISE)) { DateUtil.getDurationString(it.current) }

    fun getStartDate(): String = domain.getStartDate().let { val df = DateFormat.getDateInstance(DateFormat.MEDIUM); ("Start date: " + df.format(Date(it))) }
    fun getLevelCompletions(): String = domain.getLevelCompletions().let { "Level completions: $it" }

    fun getLevel(): String = domain.getExp().let { ExpUtil.getLevelFromExp(it) }.let { "Level $it" }
    fun getExpBelowAboveAndProgressToNextLevel() = domain.getExp().let {
        val (below, above) = ExpUtil.getExpBelowAndAbove(it)
        Triple(below.toString(), above.toString(), (it.normalize(below, above) * 100).toInt())
    }

    fun getCurrentExp() = "${domain.getExp()}xp"

}