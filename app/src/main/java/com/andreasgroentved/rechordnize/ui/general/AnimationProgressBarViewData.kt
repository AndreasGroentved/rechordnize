package com.andreasgroentved.rechordnize.ui.general


interface AnimationProgressBarViewData {
    val expGained: Long
    val levelsGained: Int
    val levelBefore: Int
    val expBefore: Long
    val expAfter: Long
    val finalProgress: Int
    val beforeProgress: Int
}