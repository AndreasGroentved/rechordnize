package com.andreasgroentved.rechordnize.ui.playableinfo

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra


class ChordDiagramAdapter(private var chordName: String, var type: PlayableExtra, private var chordShapes: List<PlayableExtra>, fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment = ChordDiagramFragment().apply {
        arguments = Bundle().apply {
            putString("chordname", chordName)
            putInt("type", type.ext)
            putString("shape", PlayableExtra.getString(chordShapes[position]))
        }
    }

    private var titles = emptyList<String>()

    override fun getCount(): Int = chordShapes.size

    private fun setTitle() {
        titles = chordShapes.mapIndexed { index, _ -> "Variation ${index + 1}" }
    }

    override fun getPageTitle(position: Int): CharSequence {
        if (titles.isEmpty()) setTitle()
        return titles[position]
    }
}