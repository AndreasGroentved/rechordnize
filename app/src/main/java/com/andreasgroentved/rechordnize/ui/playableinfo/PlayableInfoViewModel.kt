package com.andreasgroentved.rechordnize.ui.playableinfo

import android.app.Application
import android.graphics.Bitmap
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.model.util.ChordDictionary
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.domain.sound.SoundShape
import com.andreasgroentved.rechordnize.ui.base.BaseViewModel
import com.andreasgroentved.rechordnize.ui.general.Diagrams
import com.andreasgroentved.rechordnize.model.util.PlayableUtil
import com.andreasgroentved.rechordnize.util.removeParentheses
import kotlinx.coroutines.Dispatchers.IO


class PlayableInfoViewModel(application: Application) : BaseViewModel(application as ChordApp) {

    private val chordDictionary = ChordDictionary()

    fun getPlayable(name: String) = liveData(context = viewModelScope.coroutineContext + IO) {
        emitSource(domain.getChord(name))
    }


    fun getTitle(playable: Playable) = playable.name


    fun getChordBitmap(chordName: String, chordShapeName: String, width: Int, height: Int): Bitmap =
            Diagrams.buildChordDiagram(width, height, chordName, chordShapeName, chordDictionary)


    suspend fun playSound(name: String, shapeString: String) { //TODO ikke her
        val shape = PlayableExtra.getExtra(shapeString)
        val sounds = SoundShape.getSoundShapes(listOf(name to shape), "c3", true, listOf(shape))
        playSound(sounds)
    }

    suspend fun playProgression(playableName: String) {
        val progChord = SoundShape.getSoundShapes(listOf(playableName to PlayableExtra.CHORD_PROGRESSION), "c3", true)
        val prog = SoundShape.getSoundShapes(listOf("I maj" to PlayableExtra.CHORD_PROGRESSION), "c") + progChord.first() //TODO grusom hardcode
        playSound(prog)
    }

    suspend fun playIntervalOrTypeSound(playableName: String, extra: PlayableExtra) {
        val sounds = SoundShape.getSoundShapes(listOf(playableName to extra), "c3", true)
        playSound(sounds)
    }

    private suspend fun playSound(sounds: List<Pair<String, PlayableExtra>>): Boolean {
        val sMap = domain.getSound()
        val soundsToPlay = sounds.map { sMap.getSounds(it.first.toLowerCase(), it.second) }

        if (!sMap.isLoaded(soundsToPlay.flatten(), domain.getSoundType())) {
            sMap.loadSounds(domain.getSoundType(), soundsToPlay.flatten())
        }

        domain.getSound().play(sounds, domain.getSoundType())
        return true
    }


    fun getIntervalString(intervals: String): String =
            intervals.split(" ") //TODO viewmodel
                    .map {
                        chordDictionary.typeStringsToDistance[it.removeParentheses()]
                                ?: error("interval type string to distance fail")
                    }
                    .joinToString(" ") { chordDictionary.getNoteAtDistanceFrom("c", it) }


}