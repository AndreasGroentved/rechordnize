package com.andreasgroentved.rechordnize.ui.level

import com.andreasgroentved.rechordnize.model.playable.PlayableNameWithExtra
import com.andreasgroentved.rechordnize.ui.general.AnimationProgressBarViewData

data class ResultViewData(
        override val expGained: Long, override val levelsGained: Int, override val levelBefore: Int, override val expBefore: Long,
        override val expAfter: Long, override val finalProgress: Int, override val beforeProgress: Int,
        val adapterData: List<PlayableNameWithExtra>, val levelAverageTime: String,
        val levelAccuracy: String, val grade: String, val resultComment: String, val typeHeader: String
) : AnimationProgressBarViewData