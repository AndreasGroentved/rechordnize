package com.andreasgroentved.rechordnize.ui.multiplayer

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.andreasgroentved.rechordnize.R
import kotlinx.android.synthetic.main.list_item_one.view.*

class MultiplayerPlayerListAdapter : RecyclerView.Adapter<MultiplayerPlayerListAdapter.PlayerHolder>() {

    var list: List<String> = mutableListOf()
        set(other) {
            field = other; notifyDataSetChanged()
        }


    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): PlayerHolder =
            PlayerHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.list_item_one, viewGroup, false))

    override fun onBindViewHolder(holder: PlayerHolder, position: Int) =
            list[position].let { holder.set(it) }


    class PlayerHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun set(name: String) {
            itemView.name.text = name
        }
    }

    override fun getItemCount(): Int = list.size
}