package com.andreasgroentved.rechordnize.ui.level

import android.annotation.SuppressLint
import androidx.annotation.WorkerThread
import androidx.lifecycle.*
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.model.level.LevelData
import com.andreasgroentved.rechordnize.model.level.LevelDifficulty
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.playable.PlayableNameWithExtra
import com.andreasgroentved.rechordnize.model.util.*
import com.andreasgroentved.rechordnize.ui.base.BaseViewModel
import com.andreasgroentved.rechordnize.ui.inject.component.DaggerActivityComponent
import com.andreasgroentved.rechordnize.ui.inject.module.ActivityModule
import com.andreasgroentved.rechordnize.ui.result.ResultTextUtil
import com.andreasgroentved.rechordnize.util.DebugUtil
import kotlinx.coroutines.*
import java.text.DecimalFormat
import javax.inject.Inject
import javax.inject.Named


open class LevelViewModel(application: ChordApp) : BaseViewModel(application) {

//TODO fjern alle level references

    @Inject
    lateinit var saveLevelState: SaveLevelState
    @Inject
    lateinit var stateHandler: LevelStateHandler
    @field:[Inject Named("GuessTimer")]
    lateinit var guessTimer: ILevelTimer
    @field:[Inject Named("LevelTimer")]
    lateinit var levelTimer: ILevelTimer
    private val penaltyTime = 10_000L
    protected val decimalFormat = DecimalFormat("#.#")

    var title = "Error"
        set(value) {
            field = value; titleObserver.postValue(value)
        }

    private val outputHandler = OutputHandler() //TODO dep inject
    private val titleObserver = MutableLiveData<String>()
    val canInputMediator = MediatorLiveData<Boolean>()
    private val incorrectGuessMediator = MediatorLiveData<String>()
    private val guessLiveData = MediatorLiveData<List<PlayableNameWithExtra>>()
    private val timeLiveData = MediatorLiveData<String>()
    private val resultInfoTextLiveData = MediatorLiveData<String>()
    private val finalGuessOfSequenceLiveData = MediatorLiveData<Boolean>()
    val showLoadingLiveData = MediatorLiveData<Boolean>()
    val levelEndData = MediatorLiveData<ResultViewData>()


    init {
        setUpDagger(application)
    }

    private fun setUpDagger(application: ChordApp) {
        try {
            DaggerActivityComponent.builder().applicationComponent(application.component).activityModule(ActivityModule())
                .build().inject(this)
        } catch (e: Exception) {
            println("testing..")
        }
    }


    fun initLevelData(id: String, levelDifNumber: Int = 1, questionNum: Int): LiveData<Unit> = liveData(viewModelScope.coroutineContext + Dispatchers.IO) {
        emitSource(
            domain.getLevelWithChords(id).map { levelData: LevelData ->
                setLevelData(levelData, levelDifNumber, questionNum)
            }
        )
    }


    protected fun setLevelData(levelData: LevelData, levelId: Int, questionNum: Int) {
        setLevelModifiers(levelData, levelId)
        val exp = domain.getExp()
        stateHandler.initialize(exp, levelData, isCustom(levelData))
        if (DebugUtil.isDebug()) stateHandler.level.setNumberOfPlays(2) else stateHandler.level.setNumberOfPlays(questionNum)
    }


    @WorkerThread
    fun finalGuess(guesses: List<String>): Boolean {
        guessTimer.lap()
        domain.stopChord()

        val guesses = if (isCheating(guesses)) stateHandler.level.current.map { it.name } else guesses
        stateHandler.updateStateDuringRound(guesses, guessTimer.cachedTime)

        val correct = stateHandler.level.current.map { it.name } == guesses
        return if (correct) {
            updateWithFinalGuess(guesses); true
        } else {
            tryAgain(guesses);false
        }
    }

    private fun isCheating(guesses: List<String>) = guesses.firstOrNull() == "skip"

    private fun updateWithFinalGuess(guesses: List<String>) {
        val levelState = stateHandler.levelState
        val level = stateHandler.level
        titleObserver.postValue("$title - ${DecimalFormat("#.#").format((level.roundNum.toFloat()) / level.numberOfRounds() * 100)}%")
        canInputMediator.postValue(false)
        finalGuessOfSequenceLiveData.postValue(true)
        guessLiveData.postValue(guesses.map { guess ->
            val count = level.current.count { it.name == guess }
            PlayableNameWithExtra(guess, levelState.rounds.last().expMap[guess]?.div(count) ?: 0L)
        })
        resultInfoTextLiveData.postValue(outputHandler.formOutput())
        timeLiveData.postValue(DateUtil.getDurationString(guessTimer.cachedTime)) //TODO
    }

    private fun tryAgain(guesses: List<String>) {
        guessTimer.penaltyTime += (getSequenceLength() * penaltyTime)
        val output = guesses.joinToString(", ")
        incorrectGuessMediator.postValue("$output is not correct")
    }


    private fun getAdapterData() = stateHandler.levelState.newLevelData.playables.map {
        val expMap = stateHandler.levelState.playableToExpMap
        PlayableNameWithExtra(
            it.name, ExpUtil.getLevelFromExp(it.exp), stateHandler.levelState.levelUpSet.contains(it.name), expMap[it.name]
                ?: 0
        )
    }.sortedByDescending { it.expGained }

    suspend fun next() {
        showLoadingLiveData.postValue(true)
        if (levelTimer.startTime == -1L) levelTimer.startTimer()
        when {
            stateHandler.level.hasLevelEnded() -> levelEndUpdate()
            else -> toNextRound()
        }
    }

    private suspend fun toNextRound() {
        guessTimer.reset()
        stateHandler.updateStateBeforeRound()
        playSound()
        canInputMediator.postValue(true)
        showLoadingLiveData.postValue(false)
    }

    private fun levelEndUpdate() {
        stateHandler.updateStateFinal(domain.getLevelSync(stateHandler.levelState.newLevelData.id))
        setTexts()
        (GlobalScope + Dispatchers.IO).launch { saveLevelState.evaluateState(stateHandler.levelState, levelTimer.lap()) }
    }


    val chordDictionary = ChordDictionary()

    protected open fun setLevelModifiers(levelData: LevelData, level: Int) {
        val type = PlayableUtil.getPlayableType(intArrayOf(levelData.levelDifficulty.intVal))
        val levelModifiers = chordDictionary.typeToLevelModifiers(type)[level] ?: emptyList()
        levelData.levelModifiers = levelModifiers.toMutableList()
    }

    @SuppressLint("DefaultLocale")
    suspend fun playSound(replay: Boolean = false) {
        val sounds = stateHandler.level.getSoundDataOrChangedSoundData()

        loadSounds(sounds)

        if (!replay) guessTimer.startTimer()

        domain.getSound().play(sounds, domain.getSoundType())
    }

    private suspend fun loadSounds(sounds: List<Pair<String, PlayableExtra>>) {
        val sMap = domain.getSound()
        val soundsToPlay = sounds.map { sMap.getSounds(it.first.toLowerCase(), it.second) }.flatten()
        if (!sMap.isLoaded(soundsToPlay, domain.getSoundType())) {
            sMap.loadSounds(domain.getSoundType(), soundsToPlay)
        }
    }


    fun getProgress(exp: Long, maxValue: Int = 1000): Int {
        val (min, max) = ExpUtil.getExpBelowAndAbove(exp)
        val percentage = exp.normalize(min, max) * maxValue
        return percentage.toInt()
    }

    protected open fun setTexts() {
        val ls = stateHandler.levelState
        val oldLevelPlay = ls.oldLevelData?.levelHistory?.playMap?.get(ls.difficultyModifiers)
        val newLevelPlay = ls.newLevelData.levelHistory.playMap[ls.difficultyModifiers]!!

        val viewData = ResultViewData(
            ls.expAfter - ls.expBefore, ls.levelUps, ExpUtil.getLevelFromExp(ls.expBefore).toInt(), ls.expBefore,
            ls.expAfter, getProgress(ls.expAfter), getProgress(ls.expBefore), getAdapterData(),
            PlayableUtil.getAverageTimeText(newLevelPlay.plays, newLevelPlay.plays.size), getAcc() + "%", ("${getGrade()}x"),
            ResultTextUtil.getResultText(oldLevelPlay, newLevelPlay, EnumUtil.getLevelDifNum(ls.difficultyModifiers), ls.isCustom).first(), "In this level"
        )
        levelEndData.postValue(viewData)
    }

    @WorkerThread
    fun resetLevel() {
        stateHandler.updateStateFinal(domain.getLevelSync(stateHandler.levelState.newLevelData.id))
        saveLevelState.evaluateState(stateHandler.levelState, levelTimer.lap())
        stateHandler.resetLevelState(domain.getExp())
        levelTimer.reset()
    }


    @WorkerThread
    open suspend fun guess(guesses: List<String>) = (viewModelScope + Dispatchers.IO).async { finalGuess(guesses.map { it }) }.await()

    fun seenFinalGuess() = finalGuessOfSequenceLiveData.postValue(false)
    fun getSkip() = (0 until getSequenceLength()).map { "skip" }
    fun getIncorrectGuess(): LiveData<String> = incorrectGuessMediator
    open fun getAcc(): String = decimalFormat.format((stateHandler.levelState.rounds.size / stateHandler.level.levelPlay.guessCount.toDouble()) * 100)!!
    fun getSequenceLength() = stateHandler.level.getLengthOfCurrent()
    fun hasLevelEnded() = stateHandler.level.hasLevelEnded()
    fun getPossiblePlayables() = if (::stateHandler.isInitialized) stateHandler.level.getPossiblePlayables() else listOf()
    fun getTitle(): LiveData<String> = titleObserver
    fun getResultPlayablesLive(): LiveData<List<PlayableNameWithExtra>> = guessLiveData
    fun getResultPlayables() = stateHandler.level.current
    fun getCorrectSequence() = stateHandler.level.current.map { it.name }
    fun getResultInfoText(): LiveData<String> = resultInfoTextLiveData
    fun getFinalGuess(): LiveData<Boolean> = finalGuessOfSequenceLiveData
    private fun isCustom(levelData: LevelData) = levelData.levelDifficulty in LevelDifficulty.getCustomLevels() || levelData.levelDifficulty == LevelDifficulty.MULTI
    protected fun getGrade(): Int = stateHandler.levelState.newGrade
    fun setLoading() = showLoadingLiveData.postValue(true)
    fun stopLoading() = showLoadingLiveData.postValue(false)
    fun isInitialized() = (!::stateHandler.isInitialized || !stateHandler.isInitialized()).not()

}

