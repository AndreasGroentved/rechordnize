package com.andreasgroentved.rechordnize.ui.general

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.andreasgroentved.rechordnize.R
import kotlinx.android.synthetic.main.green_button_layout.view.*
import kotlinx.android.synthetic.main.list_header_level_list.view.*

open class InputsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var inputs: List<List<String>> = mutableListOf()
        set(newInputs) {
            field = newInputs
            notifyDataSetChanged()
        }

    fun isHeader(position: Int): Boolean {
        var currentIndex = 0
        for (difList in inputs) {
            if (currentIndex == position) return true
            currentIndex += difList.size
        }
        return false
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): RecyclerView.ViewHolder =
        if (isHeader(position))
            ListHeaderHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.list_header_level_list, viewGroup, false))
        else
            LevelHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.green_button_layout, viewGroup, false))


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =
        if (isHeader(position)) (holder as ListHeaderHolder).set(getItem(position))
        else {
            val listItemHolder = holder as LevelHolder
            val text = getItem(position)
            listItemHolder.set(text)
        }


    fun getItem(position: Int): String {
        var currentIndex = 0
        for (difList in inputs) {
            for (lData in difList) if (currentIndex++ == position) return lData
        }
        throw IllegalArgumentException("Adapter position is invalid: $position of $itemCount items")
    }

    class ListHeaderHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun set(header: String) {
            itemView.chord_type_header.text = header
        }
    }

    class LevelHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun set(string: String) {
            itemView.button.text = string
        }
    }

    override fun getItemCount(): Int = inputs.sumBy { it.size }
    override fun getItemViewType(position: Int): Int = if (isHeader(position)) 0 else 1

}