package com.andreasgroentved.rechordnize.ui.history

import android.content.Context
import android.widget.TextView
import com.andreasgroentved.rechordnize.R
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import java.text.DecimalFormat


class XYMarkerView(context: Context, private val xAxisValueFormatter: ValueFormatter) :
        MarkerView(context, R.layout.custom_marker_view) {

    private val tvContent: TextView = findViewById(R.id.tvContent)
    private val format: DecimalFormat = DecimalFormat("###.0")

    override fun refreshContent(e: Entry, highlight: Highlight) {
        tvContent.text = "${format.format(e.y)} min"
        super.refreshContent(e, highlight)
    }

    override fun getOffset() = MPPointF((-(width / 2)).toFloat(), (-height).toFloat())
}