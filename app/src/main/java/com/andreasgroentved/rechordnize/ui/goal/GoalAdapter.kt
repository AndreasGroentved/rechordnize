package com.andreasgroentved.rechordnize.ui.goal

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.andreasgroentved.rechordnize.R
import kotlinx.android.synthetic.main.goal_list_item.view.*

class GoalAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val header = 0

    var skills: List<SkillInfoData> = mutableListOf()
        set(newInputs) {
            field = newInputs; notifyDataSetChanged()
        }

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): RecyclerView.ViewHolder =
            if (getItemViewType(position) == header) GoalHeaderHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.goal_header_list_item, viewGroup, false))
            else GoalItemHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.goal_list_item, viewGroup, false))

    override fun onBindViewHolder(itemHolder: RecyclerView.ViewHolder, position: Int) = when (itemHolder) {
        is GoalHeaderHolder -> Unit
        is GoalItemHolder -> itemHolder.set(skills[position - 1])
        else -> throw RuntimeException("very unintended")
    }

    override fun getItemViewType(position: Int): Int = if (position == header) header else 1
    override fun getItemCount(): Int = skills.size + 1

    class GoalHeaderHolder(view: View) : RecyclerView.ViewHolder(view)
    class GoalItemHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun set(skillInfoHolder: SkillInfoData) {
            itemView.name.text = skillInfoHolder.headerText
            itemView.level.text = skillInfoHolder.levelText
            itemView.expOf.text = skillInfoHolder.outOfText
            itemView.progressBar.progress = skillInfoHolder.progress
            itemView.skill_info.text = skillInfoHolder.infoText
        }
    }
}