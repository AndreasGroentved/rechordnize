package com.andreasgroentved.rechordnize.ui.playableinfo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.util.PlayableUtil
import com.andreasgroentved.rechordnize.model.util.ExpUtil
import com.andreasgroentved.rechordnize.ui.general.LeftRightHolder


class PlayableAdapter(private val playable: Playable, private val names: List<String>) : Adapter<LeftRightHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): LeftRightHolder =
            LeftRightHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.playableinfo_list_item_two_spread, viewGroup, false))

    override fun onBindViewHolder(holder: LeftRightHolder, position: Int) {
        val (left, right) = getItemAtPosition(position)
        holder.updateView(left, right)
    }

    private fun getItemAtPosition(position: Int) = Pair(
            names[position], when (position) {
        0 -> "${ExpUtil.getLevelFromExp(playable.exp)}"
        /*   1 -> DateUtil.getTimeBetweenString(playable.lastPlayed, System.currentTimeMillis())*/
        1 -> "${playable.inARow}"
        2 -> "${playable.correct}"
        3 -> "${playable.plays}"
        4 -> PlayableUtil.getAverageTimeString(playable.performances, playable.performances.count())
        5 -> PlayableUtil.getAccString(playable.performances)
        else -> ""
    }
    )

    override fun getItemCount() = 6


}