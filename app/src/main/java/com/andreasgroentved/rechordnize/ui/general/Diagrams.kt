package com.andreasgroentved.rechordnize.ui.general

import android.content.res.Resources
import android.graphics.*
import android.graphics.Bitmap.Config.ARGB_8888
import android.graphics.Color.BLACK
import android.graphics.Color.WHITE
import android.graphics.Paint.Style.FILL_AND_STROKE
import android.graphics.Paint.Style.STROKE
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.model.util.ChordDictionary
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.domain.sound.SoundMapper
import com.andreasgroentved.rechordnize.model.util.PlayableUtil


//TODO komplet rewrite - 10 af 10 kodekvalitet
object Diagrams {


    fun buildChordDiagram(width: Int, height: Int, chordName: String, chordShapeName: String, chordDictionary: ChordDictionary): Bitmap {
        val (bitmap, canvas) = setUpCanvasBitmap(width, height)
        val extension = PlayableUtil.getChordExtension(chordName)
        val base = PlayableUtil.getChordBase(chordName)

        val shape = PlayableExtra.getExtra(chordShapeName)
        val lines = if (shape == PlayableExtra.OPEN) chordDictionary.openChordMap[chordName.toLowerCase()]!! else chordDictionary.chordShapeMap[extension]!![shape]!!

        val lowestString = SoundMapper.getLowestString(lines)
        val zeroValue = SoundMapper.getFretNumNoteOnString(base, SoundMapper.getLowestString(lines), lines, chordDictionary) - lines[lines.size - lowestString]
        drawLines(lines, canvas, width, height, zeroValue)
        return bitmap
    }


    fun buildIntervalDiagram(intervalDistance: Int, width: Int, height: Int, res: Resources, firstNote: String = "c"): Bitmap {
        val (bitmap, canvas) = setUpCanvasBitmap(width, height)
        drawInterval(intervalDistance, canvas, width, height, res, firstNote)
        return bitmap
    }

    private fun setUpCanvasBitmap(width: Int, height: Int): Pair<Bitmap, Canvas> {
        val bitmap = Bitmap.createBitmap(width, height, ARGB_8888)
        val canvas = Canvas(bitmap)
        canvas.drawColor(WHITE)
        return Pair(bitmap, canvas)
    }

    private fun drawInterval(intervalDistance: Int, canvas: Canvas, width: Int, height: Int, res: Resources, firstNote: String) {
        //TODO relativ position af første tone - ChordDicitionary med Scala trin burde hjælpe
        //TODO tegn baggrund
        //TODO med tal e.g. c4


        val chordDictionary = ChordDictionary()

        val textSize = width / 8f
        val textPaint = Paint().apply { color = BLACK; strokeWidth = width / 50f; this.textSize = textSize }

        val cleffEnd = height * 0.33f


        val linePaint = Paint().apply { color = Color.parseColor("#444444"); style = STROKE; strokeWidth = width / 100f }
        val circlePaint = Paint().apply { color = BLACK; style = FILL_AND_STROKE; strokeWidth = width / 90f }


        val margin = 0f
        val heightDivider = (height - 2 * margin) / 9f
        val widthDivider = (width - cleffEnd - 2 * margin) / 6f

        (3 until 8).map {
            Path().apply {
                moveTo(0 + margin, it * (heightDivider) + margin)
                lineTo(width - margin, it * (heightDivider) + margin)
            }
        }.forEach { canvas.drawPath(it, linePaint) }

        if (firstNote == "c" || firstNote == "d" || firstNote == "db") { //Draw lines below cleff
            Path().apply {
                moveTo(cleffEnd + widthDivider * 0.7f, 8 * heightDivider + margin)
                lineTo(cleffEnd + widthDivider * 2.2f, 8 * heightDivider + margin)
                canvas.drawPath(this, linePaint)
            }
        }

        if (intervalDistance in 18..24) {
            Path().apply {
                moveTo(cleffEnd + widthDivider * 2.7f, 2 * heightDivider + margin)
                lineTo(cleffEnd + widthDivider * 4.2f - margin, 2 * heightDivider + margin)
                canvas.drawPath(this, linePaint)
            }

        }
        if (intervalDistance == 24) {
            Path().run {
                moveTo(cleffEnd + widthDivider * 2.7f, 1 * heightDivider + margin)
                lineTo(cleffEnd + widthDivider * 4.2f, 1 * heightDivider + margin)
                canvas.drawPath(this, linePaint)
            }
        }

        val octaveCount = intervalDistance / 12
        val distanceNoteA = if (firstNote == "c") 0 else chordDictionary.getDistanceLine("c", firstNote, octaveCount)

        val xFirstNote = cleffEnd + widthDivider
        val yFirstNote = heightDivider * 8 - ((distanceNoteA) * (heightDivider)) - (heightDivider / 2)

        val radius = heightDivider
        val rect = RectF(xFirstNote, yFirstNote, (xFirstNote + radius * 1.5f), (yFirstNote + radius))
        canvas.drawOval(rect, circlePaint)

        val secondNote = chordDictionary.getNoteAtDistanceFrom(firstNote, intervalDistance)
        val distanceNoteB = chordDictionary.getDistanceLine(firstNote, secondNote, octaveCount)

        val xSecondNote = cleffEnd + widthDivider * 3
        val ySecondNote = heightDivider * 8 - ((distanceNoteB / 2f) * heightDivider) - (heightDivider / 2)

        val rect2 = RectF(xSecondNote, ySecondNote, (xSecondNote + radius * 1.5f), (ySecondNote + radius))
        canvas.drawOval(rect2, circlePaint)

        if (secondNote.length > 1 && secondNote.last() == 'b') {
            canvas.drawText("b", xSecondNote - ((textSize * 2) / 3), ySecondNote + (heightDivider), textPaint)
        }
        val cleffBitmap = BitmapFactory.decodeResource(res, R.drawable.treble_clef)
        val src = Rect(0, 0, cleffBitmap.width - 1, cleffBitmap.height - 1)
        val dest = Rect(0, 0, cleffEnd.toInt(), height - 1)
        canvas.drawBitmap(cleffBitmap, src, dest, null)


    }

    private fun drawLines(lines: List<Int>, canvas: Canvas, width: Int, height: Int, zeroValue: Int) { //TODO refaktor, mindre cirkler ved åbne strenge
        //TODO måske spille strengelyde - ala Chord appen

        val textSize = width / 8f
        val margin = width / 20f
        val thinPaint: Paint = Paint().apply { color = BLACK; style = STROKE; strokeWidth = width / 60f /*TODO hardcode*/ }
        val thickPaint: Paint = Paint().apply { color = BLACK; style = STROKE; strokeWidth = width / 20f /*//TODO hardcode*/ }
        val textPaint = Paint().apply { color = BLACK; strokeWidth = 2f;this.textSize = textSize }
        val circlePaint = Paint().apply { color = BLACK; style = FILL_AND_STROKE }
        val hollowCirclePaint = Paint().apply { color = BLACK; style = STROKE;strokeWidth = 3f }
        val heightDivider = (height - margin * 2) / 6
        val widthDivider = (width - (margin * 2)) / 6
        val textYOffset = (2 * margin / 3) + (heightDivider / 3)
        var numOffSet = 0

        //Horizon
        for (i in 1 until 7) {
            val minVal = getMinAxisVal(lines, zeroValue)
            Path().apply {
                moveTo(widthDivider + margin, i * heightDivider + margin)
                lineTo(width - margin, i * heightDivider + margin)
                canvas.drawPath(this, /*(if (i == 1 && (if (getZeroOffset(lines, zeroValue) < 2) minVal + i - 1 else minVal + i - 2) == 0)*/if (i == 1 && (minVal + i - 2) in -1..0) thickPaint else thinPaint)
            }

            if (i == 1) continue
            //Fret number
            val fretNum = (minVal + i - 2).let {
                if (it == 0 && numOffSet == 0) ++numOffSet
                it + numOffSet
            }
            val fretText = if (fretNum < 10) " $fretNum" else fretNum.toString()
            canvas.drawText(fretText, 0f, i * heightDivider - (heightDivider / 2) + textYOffset, textPaint)
        }

        val yValues = getYPlacement(lines, zeroValue, heightDivider, margin)
        //Vertical
        for (i in 1 until 7) {
            val curWidth = i * widthDivider + (margin)
            Path().apply {
                moveTo(curWidth, height - margin)
                lineTo(curWidth, heightDivider + margin)
                canvas.drawPath(this, thinPaint)
            }

            if (lines[i - 1] == -1) {
                canvas.drawText("X", curWidth - (textSize / 3), 0f + (3 * (margin) / 2) + heightDivider / 2, textPaint)
                continue
            }
            val isOpenNote = lines[i - 1] == 0 && zeroValue == 0
            if (isOpenNote) {
                canvas.drawCircle(curWidth, yValues[i - 1] - (margin / 4), width / 20f, hollowCirclePaint)
            } else {
                canvas.drawCircle(curWidth, yValues[i - 1], width / 20f, circlePaint)
            }
        }
        if (shouldBar(lines, zeroValue)) canvas.drawPath(getBar(lines, zeroValue, heightDivider, widthDivider, margin), thickPaint)
    }

    private fun shouldBar(lines: List<Int>, zeroValue: Int): Boolean { //TODO dækker ikke alle tilfælde - evt lav dictionary over shapes
        val minVal = lines.filter { it != -1 }.min()!! + zeroValue
        val relativeMin = lines.filter { it != -1 }.min()!!
        val notRelativeMin = lines.minus(relativeMin).filter { it != -1 }.toList()
        if (minVal == 0) return false
        val relativeMinCount = lines.filter { it == relativeMin }.size
        if ((relativeMinCount > 1) && (notRelativeMin.size > 2) && (!minusInMiddleOfChord(lines, relativeMin))) return true
        return false
    }

    private fun minusInMiddleOfChord(lines: List<Int>, minVal: Int): Boolean {
        var minBarIndex = -1
        var maxBarIndex = -1
        lines.forEachIndexed { index, value ->
            if (value == minVal) {
                if (minBarIndex == -1) minBarIndex = index
                maxBarIndex = index
            }
        }
        return if (minBarIndex == -1) false
        else (minBarIndex..maxBarIndex).any { lines[it] == -1 }
    }

    private fun getBar(lines: List<Int>, zeroValue: Int, heightDivider: Float, widthDivider: Float, margin: Float): Path {
        val minVal = lines.filter { it != -1 }.min()!!
        val minValues = lines.mapIndexed { index, value -> if (value == minVal) index else -1 }.filter { it != -1 }.toList()
        return Path().apply {
            moveTo(widthDivider * (minValues.min()!! + 1) + margin, getZeroOffset(lines, zeroValue) * heightDivider + heightDivider / 2 + margin)
            lineTo(widthDivider * (minValues.max()!! + 1) + margin, getZeroOffset(lines, zeroValue) * heightDivider + heightDivider / 2 + margin)
        }
    }

    private fun getMinAxisVal(lines: List<Int>, zeroValue: Int): Int {
        val minVal = lines.filter { it != -1 }.min()!! + zeroValue
        val maxVal = lines.max()!! + zeroValue
        val difference = maxVal - minVal

        if (minVal == 0) return 1
        return if (difference == /*in 1..*/2) minVal - 1
        else minVal
    }

    private fun getZeroOffset(lines: List<Int>, zeroValue: Int): Int {
        val minVal = lines.filter { it != -1 }.min()!! + zeroValue
        val maxVal = lines.max()!! + zeroValue
        val difference = maxVal - minVal

        if (minVal == 0 || minVal == 1) return minVal
        return when (difference) {
            in 1..2 -> 2
            3 -> 1
            else -> 1
        }
    }

    private fun getYPlacement(lines: List<Int>, zeroValue: Int, heightDivider: Float, margin: Float): List<Float> {
        val yLines: MutableList<Float> = mutableListOf()
        for ((index, value) in lines.withIndex()) {
            if (value == -1) {
                yLines.add(-1f); continue
            }
            yLines.add((lines[index] + 1 + getZeroOffset(lines, zeroValue)) * heightDivider + margin - (heightDivider / 2) + if ((value + zeroValue) == 0) 0 else 0)
        }
        return yLines
    }

}