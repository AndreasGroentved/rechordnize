package com.andreasgroentved.rechordnize.ui.custom

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.base.ClickListener
import com.andreasgroentved.rechordnize.ui.base.RecyclerTouchListener
import kotlinx.android.synthetic.main.dialog_sort_select.view.*
import kotlinx.android.synthetic.main.list_item_sort.view.*


class SortDialog(private val sortByList: List<String>) : DialogFragment() {

    private val returnData: MutableLiveData<String> = MutableLiveData()

    fun getReturnData(): LiveData<String> = returnData


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setView(setListView(it))
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }


    @SuppressLint("InflateParams")
    private fun setListView(context: Context): View {
        val inflater = LayoutInflater.from(context)
        val dialogView = inflater.inflate(R.layout.dialog_sort_select, null)
        val adapter = SortAdapter()
        adapter.sorts = sortByList
        adapter.notifyDataSetChanged()
        dialogView.chord_select_listView.layoutManager = GridLayoutManager(context, 1)
        dialogView.chord_select_listView.adapter = adapter
        dialogView.chord_select_listView.addOnItemTouchListener(RecyclerTouchListener(context, dialogView.chord_select_listView, object : ClickListener {
            override fun onLongClick(view: View, position: Int) {}
            override fun onClick(view: View, position: Int) {
                returnData.value = sortByList[position]
            }
        }))
        return dialogView
    }


    class SortAdapter : RecyclerView.Adapter<SortAdapter.SortHolder>() {
        var sorts: List<String> = emptyList()

        override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): SortHolder =
            SortHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.list_item_sort, viewGroup, false))

        override fun onBindViewHolder(holder: SortHolder, position: Int): Unit = holder.set(sorts[position])
        override fun getItemCount(): Int = sorts.size

        class SortHolder(view: View) : RecyclerView.ViewHolder(view) {
            init {
                set("")
            }

            fun set(name: String) {
                itemView.sort_name.text = name
            }

        }
    }
}
