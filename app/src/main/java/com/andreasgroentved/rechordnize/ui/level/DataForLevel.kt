package com.andreasgroentved.rechordnize.ui.level

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class DataForLevel(val levelId: String, val levelTitle: String, val levelDifficultyNum: Int, val questionNumber: Int) : Parcelable //TODO
