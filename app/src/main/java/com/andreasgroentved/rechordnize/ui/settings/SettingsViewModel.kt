package com.andreasgroentved.rechordnize.ui.settings

import android.app.Application
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.ui.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus


class SettingsViewModel(application: Application) : BaseViewModel(application as ChordApp) {

    private var currentInstrument = domain.getSoundType()

    fun update() {
        val newInstrument = domain.getSoundType()
        val changed = newInstrument != currentInstrument
        if (changed) {
            currentInstrument = newInstrument
            (GlobalScope + Dispatchers.IO).launch {
                domain.getSound().loadAllSound(currentInstrument)
            }
        }
    }

}