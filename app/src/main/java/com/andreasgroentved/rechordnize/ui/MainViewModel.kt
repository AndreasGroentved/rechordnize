package com.andreasgroentved.rechordnize.ui

import android.app.Application
import androidx.lifecycle.viewModelScope
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.ui.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import java.lang.System.currentTimeMillis


class MainViewModel(application: Application) : BaseViewModel(application as ChordApp) {

    fun getAndSetFirstTime(): Boolean {
        return if (domain.isFirstTime()) {
            (viewModelScope + Dispatchers.IO).launch {
                domain.initialDb()
                domain.setStartDate(currentTimeMillis())
                domain.setIsFirstTime(false)
            }
            true
        } else false
    }

}