package com.andreasgroentved.rechordnize.ui.intro

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.andreasgroentved.rechordnize.R
import com.github.paolorotolo.appintro.AppIntro

class IntroActivity : AppIntro() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getFragments().forEach {
            addSlide(it)
        }
    }


    private fun getFragments() = listOf(
            IntroFragment().apply {
                arguments = Bundle().apply {
                    putInt("type", 1); putInt("title", R.string.info_title1); putInt("text", R.string.info_text2);putInt("index", 0)
                    putInt("text", R.string.info_text1); putInt("drawable", R.drawable.logonobackground)
                }
            },
            IntroFragment().apply {
                arguments = Bundle().apply {
                    putInt("type", 0); putInt("title", R.string.info_title2)
                    putInt("text", R.string.info_text2); putInt("index", 1)
                }
            },
            IntroFragment().apply {
                arguments = Bundle().apply {
                    putInt("type", 0); putInt("title", R.string.info_title3); putInt("text", R.string.info_text3)
                    putInt("text", R.string.info_text3);putInt("index", 2)
                }
            }
    )


    override fun onSkipPressed(currentFragment: Fragment?) {
        onBackPressed()
    }

    override fun onBackPressed() {
        pager.currentItem = 0
        super.onBackPressed()
        overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out)
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        onBackPressed()
    }
}