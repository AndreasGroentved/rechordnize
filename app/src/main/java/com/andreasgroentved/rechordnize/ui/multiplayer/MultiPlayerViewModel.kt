package com.andreasgroentved.rechordnize.ui.multiplayer

import android.app.Application
import android.os.CountDownTimer
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.viewModelScope
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.backend.IGameHub
import com.andreasgroentved.rechordnize.backend.LevelIdentifierEnum
import com.andreasgroentved.rechordnize.backend.LevelIdentifierEnum.*
import com.andreasgroentved.rechordnize.backend.PlayerState
import com.andreasgroentved.rechordnize.backend.response.Response
import com.andreasgroentved.rechordnize.domain.level.MultiPlayerLevel
import com.andreasgroentved.rechordnize.model.level.LevelData
import com.andreasgroentved.rechordnize.model.level.LevelDifficulty
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.util.PlayableUtil
import com.andreasgroentved.rechordnize.model.util.ExpUtil
import com.andreasgroentved.rechordnize.ui.inject.component.DaggerActivityComponent
import com.andreasgroentved.rechordnize.ui.inject.module.ActivityModule
import com.andreasgroentved.rechordnize.ui.level.LevelViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import javax.inject.Inject
import kotlin.math.roundToInt


class MultiPlayerViewModel constructor(application: Application, levelIdentifierEnum: LevelIdentifierEnum? = null) : LevelViewModel(application as ChordApp) {


    @Inject
    lateinit var gameHub: IGameHub
    private lateinit var gameId: String
    private lateinit var userId: String
    var startedMediator: MediatorLiveData<Unit> = MediatorLiveData()
    val endGameData: MediatorLiveData<MultiPlayerResultViewData> = MediatorLiveData()
    lateinit var endGameEndResponse: Response.GameEndResponse
    var playerListMediator = MediatorLiveData<List<String>>()
    lateinit var connectionId: String
    private lateinit var levelIdentifierEnum: LevelIdentifierEnum
    val connectionProblems = MediatorLiveData<String>()
    val startingTimerMediator = MediatorLiveData<String>()
    val countDownTime = 5_000L
    val connectionFailed = MediatorLiveData<Boolean>()

    private val timer = object : CountDownTimer(countDownTime, 1000) {
        override fun onTick(millisUntilFinished: Long) =
            startingTimerMediator.postValue(
                "Starting in ${(((millisUntilFinished).toFloat() / 1_000).roundToInt().toString())} s"
            )


        override fun onFinish() = Unit
    }

    init {
        showLoadingLiveData.postValue(true)
        try {
            DaggerActivityComponent.builder().applicationComponent((application as ChordApp).component).activityModule(ActivityModule())
                .build().inject(this)
        } catch (e: Exception) {

        }

        (viewModelScope + Dispatchers.IO).launch {
            gameHub.connectionObserver.consumeEach {
                if (it.isNotEmpty()) connectionFailed.postValue(true)
            }
        }
        levelIdentifierEnum?.let { setUp(levelIdentifierEnum) }
    }

    fun setUp(levelIdentifierEnum: LevelIdentifierEnum) {
        showLoadingLiveData.postValue(true)
        playerListMediator.postValue(List(5) { "Searching..." })
        this.levelIdentifierEnum = levelIdentifierEnum
        registerObserver()
        (viewModelScope + Dispatchers.IO).launch {
            var didSucceed = false
            while (!didSucceed) {
                didSucceed = gameHub.start()
                if (!didSucceed) {
                    connectionProblems.postValue("Failed to connect to the server, retrying...")
                }
                println("Connected successfully: $didSucceed")
            }
        }
    }


    var isReady = false; private set

    fun readyForGame() {
        isReady = true
        gameHub.ready(gameId)
    }

    fun unReadyForGame() {
        isReady = false
        gameHub.unReady(gameId)
    }

    override fun setLevelModifiers(levelData: LevelData, level: Int) {
        val levelModifiers = chordDictionary.multiPlayerLevelModifiers
        levelData.levelModifiers = levelModifiers.toMutableList()
    }

    override fun onCleared() {
        super.onCleared()
        gameHub.hubObserver.close()
        gameHub.stop()
    }

    private fun registerObserver() {
        (viewModelScope + Dispatchers.IO).launch {
            gameHub.hubObserver.consumeEach { response: Response ->
                println("server res")
                println(response)
                when (response) {
                    is Response.GameJoinedResponse -> gameJoinedResponse(response)
                    is Response.ConnectedResponse -> connectedResponse(response)
                    is Response.UnStartedGamesResponse -> Unit
                    is Response.GameCreatedResponse -> Unit
                    is Response.GameStartedResponse -> startedGameResponse(response)
                    is Response.NextResponse -> nextResponse(response)
                    is Response.GuessResponse -> guessResponse(response)
                    is Response.GameEndResponse -> gameEndResponse(response)
                    is Response.GamePlayerListResponse -> gamePlayerListResponse(response)
                    is Response.ErrorResponse -> errorResponse(response)
                    is Response.GameIsStartingResponse -> gameIsStartingResponse(response)
                    is Response.GameCancelStartResponse -> gameCancelStartResponse(response)
                }
            }
        }
    }

    private fun gameIsStartingResponse(res: Response.GameIsStartingResponse) {
        timer.start()
    }

    private fun gameCancelStartResponse(res: Response.GameCancelStartResponse) {
        startingTimerMediator.postValue("Waiting for players")
        timer.cancel()
    }

    private fun connectedResponse(res: Response.ConnectedResponse) {
        connectionId = res.ConnectionId
        gameHub.joinOrCreateGame(levelIdentifierEnum)
    }

    private fun errorResponse(res: Response.ErrorResponse) {
        //TODO
    }

    private fun gamePlayerListResponse(res: Response.GamePlayerListResponse) {
        (res.PlayerNameList.filter { it.Name != connectionId }.let {
            it.mapIndexed { index, item -> "Player ${index + 1} is ${if (item.State == PlayerState.Ready) "ready" else "not ready"}" } + (0..(3 - it.size)).map { "Searching..." }
        }).let {
            playerListMediator.postValue(it)
        }
    }

    private fun gameJoinedResponse(res: Response.GameJoinedResponse) {
        val levelData = LevelData(id = "multiPlayer", playables = res.Playables.getPlayablesFromString(), levelDifficulty = LevelDifficulty.MULTI)
        initializeLevelData(levelData)
        userId = res.UserId
        gameId = res.GameId
        showLoadingLiveData.postValue(false)
    }


    private fun initializeLevelData(levelData: LevelData) { //TODO kig på navngivning
        setLevelModifiers(levelData, -1)
        val exp = domain.getExp()
        stateHandler.initialize(exp, levelData, true, MultiPlayerLevel(levelData))
        stateHandler.level.setNumberOfPlays(Integer.MAX_VALUE)
    }

    private fun List<String>.getPlayablesFromString() = this //TODO load fra DB
        .map {
            val extra = PlayableUtil.getType(it, chordDictionary)
            Playable(name = it, extra = extra)
        }


    private fun startedGameResponse(res: Response.GameStartedResponse) {
        gameHub.next(gameId, 1)
        startedMediator.postValue(Unit)
    }

    private suspend fun nextResponse(res: Response.NextResponse) {
        canInputMediator.postValue(false)
        stateHandler.level.setNextOverride(getPlayablesFromStringList(res.CorrectList))
        next()
    }

    private fun getPlayablesFromStringList(playableStrings: List<String>) =
        playableStrings.map { name ->
            stateHandler.levelState.newLevelData.playables.first { it.name == name }
        }


    private fun guessResponse(res: Response.GuessResponse) {
        if (res.Correct) {
            gameHub.next(gameId, stateHandler.level.roundNum + 1)
        }
    }

    private suspend fun gameEndResponse(res: Response.GameEndResponse) {
        endGameEndResponse = res
        stateHandler.level.setEndOverride()
        next()
        setTexts()
    }


    override suspend fun guess(guesses: List<String>): Boolean =
        finalGuess(guesses.map { it }).apply {
            if (this) {
                gameHub.guess(gameId, com.andreasgroentved.rechordnize.backend.GuessInfo(guesses, userId, stateHandler.level.roundNum, guessTimer.cachedTime.toInt()))
            }
        }


    override fun setTexts() {
        val ls = stateHandler.levelState
        val newLevelPlay = ls.newLevelData.levelHistory.playMap[ls.difficultyModifiers]!!
        val viewData = MultiPlayerResultViewData(
            ls.expAfter - ls.expBefore, ls.levelUps, ExpUtil.getLevelFromExp(ls.expBefore).toInt(), ls.expBefore,
            ls.expAfter, getProgress(ls.expAfter), getProgress(ls.expBefore), getDataForResultAdapter(),
            PlayableUtil.getAverageTimeText(newLevelPlay.plays, newLevelPlay.plays.size), getAcc() + "%", ("${getGrade()}x"),
            getCommentText()
        )
        endGameData.postValue(viewData)
    }

    override fun getAcc(): String = decimalFormat.format(((stateHandler.levelState.rounds.size - 1) / stateHandler.level.levelPlay.guessCount.toDouble()) * 100)!!

    private fun getDataForResultAdapter(): List<Pair<String, Int>> {
        var otherPlayerIndex = 1
        return endGameEndResponse.UserIdToScoreDictionary.toList()
            .sortedByDescending { it.second }
            .map { item -> if (item.first == connectionId) Pair("You", item.second) else Pair("Player ${otherPlayerIndex++}", item.second) }
    }

    private fun getCommentText(): String = endGameEndResponse.UserIdToScoreDictionary.toList()
        .sortedByDescending { it.second }.mapIndexed { index, item -> item.first to index + 1 }.toMap()
        .let { placements -> placements[userId] ?: error("illegal state") }
        .let { placed -> "You placed $placed out of ${endGameEndResponse.UserIdToScoreDictionary.size}" }


    companion object {
        fun getMultiPlayerDifficulties() = values().map {
            when (it) {
                EasyChordNames -> "Normal chords"
                MediumChordNames -> "Challenging chords"
                HardChordNames -> "Difficult chords"
                EasyChordTypes -> "Normal chord types"
                MediumChordTypes -> "Challenging chord types"
                HardChordTypes -> "Difficult chord types"
                EasyChordProgressions -> "Normal chord progressions"
                MediumChordProgressions -> "Challenging chord progressions"
                HardChordProgressions -> "Difficult chord progression"
                EasyIntervals -> "Normal intervals"
                MediumIntervals -> "Challenging intervals"
                HardIntervals -> "Difficult intervals"
                EasyEverything -> "Normal everything"
                MediumEverything -> "Challenging everything"
                HardEverything -> "Difficult everything"
            }
        }
    }
}

