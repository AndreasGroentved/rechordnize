package com.andreasgroentved.rechordnize.ui.levelselection

import android.app.Application
import androidx.lifecycle.*
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.model.level.*
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.util.PlayableUtil
import com.andreasgroentved.rechordnize.model.util.toPlayableExtra
import com.andreasgroentved.rechordnize.ui.base.BaseViewModel
import kotlinx.coroutines.Dispatchers


class LevelSelectionViewModel(application: Application) : BaseViewModel(application as ChordApp) {

    private val levelCountMediator: MediatorLiveData<Boolean> = MediatorLiveData()
    private var levelCount = -1

    data class DifficultyList(val header: String, val isCustom: Boolean, val lockedNum: Int, val startIndex: Int, val listIndex: Int, val list: List<LevelSelectionHolder>)
    data class LevelSelectionViewData(val title: String, val list: List<DifficultyList>)

    fun getLevelsWithDifficulties(diffs: IntArray): LiveData<LevelSelectionViewData> = //TODO lige tjek efter
            liveData(viewModelScope.coroutineContext + Dispatchers.IO) {
                emitSource(domain.getLevelIDsWithDifficulties(diffs.toTypedArray())
                        .map {
                            levelCount = it.size
                            createViewData(it)
                        })

            }


    private fun createViewData(list: List<LevelSelectionHolder>): LevelSelectionViewData {
        val sortedList: List<List<LevelSelectionHolder>> =
                list.groupBy { it.levelDifficulty }.toSortedMap().map { it.value }
                        .toMutableList()
        val firstItemDifficulty = sortedList.first().first().let { LevelDifficulty.fromInt(it.levelDifficulty) }

        val unlocked = getUnlockedNumOfLevels(firstItemDifficulty.toPlayableExtra())
        val lists: List<DifficultyList> = sortedList.mapIndexed { index, levelHolders ->
            levelHolders.firstOrNull()?.levelDifficulty?.let { LevelDifficulty.fromInt(it) }?.run {
                val title = getString()
                val isCustom = isCustom()
                val unlockedNum = if (isCustom) levelHolders.size else getLockedNumForList(unlocked, index, sortedList)
                val startIndex = getStartIndex(index, sortedList)
                DifficultyList(title, isCustom, unlockedNum, startIndex, index, levelHolders)
            } ?: throw IllegalStateException("List cannot be empty")

        }

        val title = firstItemDifficulty.getTypeTitle()
        return LevelSelectionViewData(title, lists)
    }


    private fun getUnlockedNumOfLevels(extra: PlayableExtra) = domain.getUnlockedNumberOfLevels(extra)

    fun getLevelDeleted(): LiveData<Boolean> = liveData(viewModelScope.coroutineContext + Dispatchers.IO) {
        emitSource(domain.getLevelCount().map {
            println("l $levelCount new $it")
            val old = levelCount
            levelCount = it
            old > it

        })
    }

    fun seenDeleted() = levelCountMediator.postValue(false)

    private fun getStartIndex(listNum: Int, lists: List<List<LevelSelectionHolder>>) = (0 until listNum).sumBy { lists[it].size }

    private fun getLockedNumForList(unlocked: Int, index: Int, list: List<List<LevelSelectionHolder>>): Int {
        if (index == 0) return unlocked
        val sum = (0..index).sumBy { list[it].size }
        return if (unlocked > sum) list[index].size else unlocked - (sum - list[index].size)
    }

    fun getPlayableType(intArray: IntArray) = PlayableUtil.getPlayableType(intArray)

}