package com.andreasgroentved.rechordnize.ui.level

interface ILevelTimer {
    var cachedTime: Long
    fun lap(): Long
    fun startTimer()
    fun reset()
    val startTime: Long
    var penaltyTime: Long
}