package com.andreasgroentved.rechordnize.ui.settings

import android.app.Application
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.ui.base.BaseViewModel


class AboutViewModel(application: Application) : BaseViewModel(application as ChordApp)