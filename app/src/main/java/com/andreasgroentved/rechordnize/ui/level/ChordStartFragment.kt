package com.andreasgroentved.rechordnize.ui.level


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.base.BaseFragment
import kotlinx.android.synthetic.main.start_level_dialog.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ChordStartFragment : BaseFragment() {

    private lateinit var viewModel: LevelViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_level_start, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(requireActivity()).get(LevelViewModel::class.java)
        activity?.actionBar?.setDisplayShowHomeEnabled(true)
        setUp()
    }


    override fun onResume() {
        super.onResume()
        (activity as? AppCompatActivity)?.supportActionBar?.title = viewModel.title
    }


    private fun setUp() {
        start_level_button.setOnClickListener {
            lifecycleScope.launch {
                if(viewModel.isInitialized().not()) return@launch
                withContext(Dispatchers.Default) { viewModel.next() }
                ChordStartFragmentDirections.actionChordStartFragmentToInputFragment().let {
                    findNavController().navigate(it)
                }
            }
        }
    }
}