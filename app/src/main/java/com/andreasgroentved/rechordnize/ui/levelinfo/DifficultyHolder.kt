package com.andreasgroentved.rechordnize.ui.levelinfo

import android.app.AlertDialog
import android.content.Context
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.BulletSpan
import android.view.View
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.model.level.LevelModifier
import com.andreasgroentved.rechordnize.ui.level.DataForLevel
import com.andreasgroentved.rechordnize.util.SnackUtil
import kotlinx.android.synthetic.main.level_info_card_difficultyselection.view.*

class DifficultyHolder(view: View) : RecyclerView.ViewHolder(view) {
    private var levelSelected = 0
    private val defaultQuestionNum = 20
    private lateinit var radioButtons: List<RadioButton>

    fun set(levelInfoViewData: LevelInfoViewData) {
        setStars(levelInfoViewData)
        setInfoDialogs(levelInfoViewData)
        setStartLevel(levelInfoViewData)
        setUpRadioButtons()
    }

    private fun setUpRadioButtons() {
        radioButtons = listOf(itemView.level1, itemView.level2, itemView.level3)
        radioButtons.forEach {
            it.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    processRadioButtonClick(buttonView)
                    var index = -1
                    radioButtons.forEachIndexed { _index, but -> if (but == buttonView) index = _index }
                    levelSelected = index
                }
            }
        }
    }

    private fun processRadioButtonClick(buttonView: CompoundButton): Unit = radioButtons.filter { it !== buttonView }.forEach { it.isChecked = false }


    private fun setStartLevel(levelInfoViewData: LevelInfoViewData) {
        itemView.start_level.setOnClickListener {
            LevelInfoFragmentDirections.actionLevelInfoFragmentToMainActivity2(
                    DataForLevel(levelInfoViewData.id, levelInfoViewData.title, levelSelected + 1, defaultQuestionNum)
            ).let { itemView.findNavController().navigate(it) }
        }
    }

    private fun setStars(levelInfoViewData: LevelInfoViewData) {
        val stars1 = levelInfoViewData.gradeNormal
        val stars2 = levelInfoViewData.gradeChallenging
        val stars3 = levelInfoViewData.gradeExtreme
        itemView.level_info_stars1.text = stars1.toString() + "x"

        if (stars1 < 3) setLocks(itemView.level2, itemView.note_lock2, itemView.level_info_stars2)
        else removeLocks(itemView.level2, itemView.note_lock2, itemView.level_info_stars2, stars2)
        if (stars2 < 3) setLocks(itemView.level3, itemView.note_lock3, itemView.level_info_stars3)
        else removeLocks(itemView.level3, itemView.note_lock3, itemView.level_info_stars3, stars3)
    }


    private fun setInfoDialogs(levelInfoViewData: LevelInfoViewData) {
        itemView.info1.setOnClickListener {
            openDialog(buildBulletList(itemView.context, levelInfoViewData.modifiers[1]!!))
        }
        itemView.info2.setOnClickListener {
            openDialog(buildBulletList(itemView.context, levelInfoViewData.modifiers[2]!!))
        }
        itemView.info3.setOnClickListener {
            openDialog(buildBulletList(itemView.context, levelInfoViewData.modifiers[3]!!))
        }
    }

    private fun openDialog(text: SpannableStringBuilder) {
        AlertDialog.Builder(itemView.context)
                .setTitle("Difficulty info")
                .setMessage(text)
                .setIcon(0)
                .setNegativeButton("ok", null)
                .show()

    }

    private fun buildBulletList(context: Context, list: List<LevelModifier>): SpannableStringBuilder {
        val arr = list.asSequence().filter { it != LevelModifier.INVERSIONS }.map { LevelModifier.getDescriptionText(it) }.toList()
        val bulletGap: Int = dp(10, context).toInt()
        val ssb = SpannableStringBuilder()

        arr.forEachIndexed { index, item ->
            val ss = SpannableString(item)
            ss.setSpan(BulletSpan(bulletGap), 0, item.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            ssb.append(ss)
            if (index + 1 < arr.size) ssb.append("\n")
        }
        return ssb
    }

    private fun dp(dp: Int, context: Context): Float = context.resources.displayMetrics.density * dp

    private fun removeLocks(radioButton: RadioButton, noteButton: ImageView, infoText: TextView, stars: Int) {
        infoText.text = stars.toString() + "x"
        radioButton.setButtonDrawable(R.drawable.abc_btn_radio_material)
        radioButton.isClickable = true
        (radioButton.parent as View).setOnClickListener(null)
        noteButton.setImageResource(R.drawable.note)
    }

    private fun setLocks(radioButton: RadioButton, noteButton: ImageView, infoText: TextView) {
        infoText.text = ""
        radioButton.isClickable = false
        (radioButton.parent as View).setOnClickListener { makeSnack() }
        radioButton.setButtonDrawable(R.drawable.lock_black)
        noteButton.setImageResource(R.drawable.lock_black)
    }

    private fun makeSnack() {
        SnackUtil.getSnack("Get at least 3 stars on the previous difficulty", itemView.parent as View, itemView.context).show()
    }
}