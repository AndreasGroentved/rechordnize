package com.andreasgroentved.rechordnize.ui.level

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.model.playable.PlayableNameWithExtra
import kotlinx.android.synthetic.main.card_guess.view.*

class GuessListAdapter : RecyclerView.Adapter<GuessListAdapter.GuessHolder>() {

    var list: List<PlayableNameWithExtra> = mutableListOf()
        set(other) {
            field = other; notifyDataSetChanged()
        }


    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): GuessHolder =
        GuessHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.card_guess, viewGroup, false))

    override fun onBindViewHolder(holder: GuessHolder, position: Int) {
        holder.set(list[position].name, list[position].exp)
    }

    class GuessHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun set(text: String, exp: Long) {
            itemView.guess_name.text = text
            itemView.plusExp.text = "+${exp}xp"
        }
    }

    override fun getItemCount(): Int = list.size
}