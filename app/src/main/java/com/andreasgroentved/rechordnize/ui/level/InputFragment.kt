package com.andreasgroentved.rechordnize.ui.level


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.InputViewModelFactory
import com.andreasgroentved.rechordnize.ui.base.BaseFragment
import com.andreasgroentved.rechordnize.ui.level.input.InputViewModel
import com.andreasgroentved.rechordnize.ui.level.input.SelectionDialog
import com.andreasgroentved.rechordnize.util.DebugUtil
import kotlinx.android.synthetic.main.fragment_input_debug.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus

open class InputFragment : BaseFragment() {

    protected lateinit var viewModel: LevelViewModel
    private var dialog: SelectionDialog? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(if (DebugUtil.isDebug()) R.layout.fragment_input_debug else R.layout.fragment_input, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(requireActivity()).get(LevelViewModel::class.java)
        setUp()
    }


    protected fun setUp() {
        setInputDialog()
        if (DebugUtil.isDebug()) skip.setOnClickListener {
            (lifecycleScope).launch { viewModel.guess(viewModel.getSkip()) }
        }

        replay.setOnClickListener {
            (lifecycleScope + Dispatchers.Default).launch { viewModel.playSound(true) }
        }

    }

    private fun setInputDialog() {
        val factory = InputViewModelFactory(requireActivity().application as ChordApp)
        val inputViewModel: InputViewModel = ViewModelProvider(requireActivity(), factory).get(InputViewModel::class.java)
        dialog = SelectionDialog(inputViewModel)

        dialog?.getReturnData()?.observe(this, Observer {
            if (!it.contains("skip") && it.contains("")) return@Observer
            val copy = it.toList()
            (lifecycleScope + Dispatchers.Default).launch {
                viewModel.guess(copy)
            }
            dialog?.reset()
            dialog?.takeIf { it.isAdded }?.dismiss()
        })

        viewModel.levelEndData.observe(this, Observer {
            dialog?.takeIf { it.isAdded }?.dismiss()
        })

        fragmentManager?.let { fragmentManager ->
            select.setOnClickListener {
                dialog?.set(viewModel.getPossiblePlayables().map { it.name }, viewModel.getCorrectSequence(), viewModel.getSequenceLength())
                dialog?.show(fragmentManager, "input")
            }

        }
    }

    override fun onStop() {
        super.onStop()
        dialog?.takeIf { it.isAdded }?.dismiss()
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        println("attached")
    }

}