package com.andreasgroentved.rechordnize.ui.levelinfo

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.model.level.LevelData
import com.andreasgroentved.rechordnize.model.level.LevelDifficulty
import com.andreasgroentved.rechordnize.model.level.LevelDifficulty.CUSTOM_PROGRESSION
import com.andreasgroentved.rechordnize.model.level.LevelModifier
import com.andreasgroentved.rechordnize.model.util.ChordDictionary
import com.andreasgroentved.rechordnize.model.util.PlayableUtil
import com.andreasgroentved.rechordnize.model.util.EnumUtil
import com.andreasgroentved.rechordnize.model.util.ExpUtil
import com.andreasgroentved.rechordnize.ui.base.BaseViewModel
import kotlinx.coroutines.Dispatchers


class LevelInfoViewModel(application: Application) : BaseViewModel(application as ChordApp) {


    private val chordDictionary = ChordDictionary()
    private var levelId = "-1"

    private fun isDeleteAble(levelData: LevelData): Boolean = levelData.levelDifficulty in listOf(LevelDifficulty.CUSTOM_CHORDS, CUSTOM_PROGRESSION, LevelDifficulty.CUSTOM_CHORD_TYPE, LevelDifficulty.CUSTOM_INTERVAL)

    fun getLevelInfoViewData(levelId: String, levelNumberForTitle: Int): LiveData<LevelInfoViewData> = liveData(viewModelScope.coroutineContext + Dispatchers.IO) {
        this@LevelInfoViewModel.levelId = levelId
        emitSource(
                domain.getLevelWithChords(levelId).map { levelData: LevelData ->
                    LevelInfoViewData(
                            gradeNormal = getGrade(LevelModifier.LEVEL_1, levelData),
                            gradeChallenging = getGrade(LevelModifier.LEVEL_2, levelData),
                            gradeExtreme = getGrade(LevelModifier.LEVEL_3, levelData),
                            adapterDataWithNameAndLevel = levelData.playables.map {
                                Pair(it.name, ExpUtil.getLevelFromExp(it.exp).toInt())
                            },
                            infoText = getIntroText(levelData),
                            spanSize = getSpanSize(levelData),
                            isDeleteAble = isDeleteAble(levelData),
                            modifiers = getLevelModifier(levelData),
                            title = getLevelTitle(levelNumberForTitle, levelData),
                            id = levelData.id,
                            playableTypeHeader = "${EnumUtil.getLevelTypeString(levelData.levelDifficulty).capitalize()}s"
                    )
                }
        )
    }

    private fun getIntroText(levelData: LevelData) = when (levelData.levelDifficulty) { //TODO resource
        in LevelDifficulty.getChordLevels() ->
            "Listen to the chord played and guess what chord it is. You have to guess both the name and the type of the chord."/* At higher difficulties you will have to guess multiple chords."*/
        in LevelDifficulty.getTypeLevels() ->
            "Listen to the chord played and guess what chord it is. You have to guess the type of the chord."/* At higher difficulties you will have to guess multiple chords."*/
        in LevelDifficulty.getIntervalLevels() ->
            "Listen to the to the notes played and guess what the interval between them are. You have to guess the name of the interval."/* At higher difficulties you will hear multiple notes and you will have to guess the interval between each."*/
        in LevelDifficulty.getProgressionLevels() ->
            "Listen to the chords played and guess what the progression is. You have to guess both the name and the type of the progression. The progression will always start with an I-chord." /*The sequence of chords will be longer at higher difficulties."*/
        else -> "Error"
    } /*+ "\nGet 3 notes on normal difficulty to unlock the next level and the next difficulty level."*/


    private fun getLevelTitle(id: Int, levelData: LevelData): String = getCustomText(levelData) +
            if (id == -1) EnumUtil.getLevelTypeString(levelData.levelDifficulty) else EnumUtil.getLevelTypeString(levelData.levelDifficulty) + " #" + id

    private fun getCustomText(levelData: LevelData) =
            if (levelData.levelDifficulty in listOf(LevelDifficulty.CUSTOM_CHORD_TYPE, LevelDifficulty.CUSTOM_CHORDS, LevelDifficulty.CUSTOM_INTERVAL, CUSTOM_PROGRESSION)) /*"custom "*/ "" else ""


    fun deleteLevel(): LiveData<Boolean> = liveData(viewModelScope.coroutineContext + Dispatchers.IO) {
        emit(
                domain.deleteLevel(levelId).apply { println("delete?");println(this); println("id $levelId") }
        )
    }


    private fun getGrade(levelModifier: LevelModifier, levelData: LevelData): Int =//TODO ligner kopi
            levelData.levelHistory.playMap.filter { it.key.contains(levelModifier) }.map { it.value }.firstOrNull()?.let {
                PlayableUtil.grade(it, it.numberOfRounds)
            } ?: 0


    private fun getSpanSize(levelData: LevelData) = when (levelData.playables.size) {
        1, 2, 3 -> 2
        else -> 1
    }


    private fun getLevelModifier(levelData: LevelData): Map<Int, List<LevelModifier>> = chordDictionary.typeToLevelModifiers(getPlayableType(intArrayOf(levelData.levelDifficulty.intVal)))
    private fun getPlayableType(intArray: IntArray) = PlayableUtil.getPlayableType(intArray)


}