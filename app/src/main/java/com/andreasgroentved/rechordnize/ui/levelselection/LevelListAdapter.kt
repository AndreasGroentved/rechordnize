package com.andreasgroentved.rechordnize.ui.levelselection

import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.model.level.LevelSelectionHolder
import kotlinx.android.synthetic.main.card_level.view.*


class LevelListAdapter : RecyclerView.Adapter<LevelListAdapter.LevelHolder>() {

    var difficultyList: LevelSelectionViewModel.DifficultyList? = null
        set(other) {
            field = other; notifyDataSetChanged()
        }

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): LevelHolder =
            LevelHolder(
                    LayoutInflater.from(viewGroup.context).inflate(
                            R.layout.card_level, viewGroup, false
                    )
            )

    override fun onBindViewHolder(holder: LevelHolder, position: Int) {
        holder.set(getItem(position).stars, position + difficultyList!!.startIndex, isLocked(position))
    }

    override fun getItemCount(): Int = difficultyList?.list?.size ?: 0

    fun isLocked(position: Int) = if (difficultyList!!.lockedNum == -1) true else difficultyList!!.lockedNum - 1 < position
    fun getItem(position: Int): LevelSelectionHolder = difficultyList!!.list[position]

    class LevelHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun set(stars: Int, position: Int, locked: Boolean) {
            if (locked) {
                itemView.lvl_txt.visibility = INVISIBLE;  itemView.note.visibility = INVISIBLE
                itemView.divider.visibility = INVISIBLE
                itemView.card_level_id.visibility = INVISIBLE;  itemView.card_parent.setBackgroundResource(R.drawable.lock_white)
            } else {
                itemView.card_parent.setBackgroundResource(0);  itemView.lvl_txt.text = "$stars"
                itemView.card_level_id.text = "" + (position + 1);  itemView.lvl_txt.visibility = VISIBLE
                itemView.note.visibility = VISIBLE;  itemView.card_level_id.visibility = VISIBLE
                itemView.note.width =  itemView.lvl_txt.width
                itemView.divider.visibility = VISIBLE
            }
        }
    }
}