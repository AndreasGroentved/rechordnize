package com.andreasgroentved.rechordnize.ui.base

import androidx.fragment.app.Fragment


abstract class BaseFragment : Fragment()
