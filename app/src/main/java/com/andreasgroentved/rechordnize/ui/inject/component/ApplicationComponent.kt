package com.andreasgroentved.rechordnize.ui.inject.component

import android.app.Application
import android.content.Context
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.IData
import com.andreasgroentved.rechordnize.db.PlayableDatabase
import com.andreasgroentved.rechordnize.domain.IDomain
import com.andreasgroentved.rechordnize.domain.sound.SoundPlayer
import com.andreasgroentved.rechordnize.pref.PrefHelper
import com.andreasgroentved.rechordnize.ui.inject.module.ActivityModule
import com.andreasgroentved.rechordnize.ui.inject.module.ApplicationModule
import com.google.gson.Gson
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApplicationModule::class), (ActivityModule::class)])
interface ApplicationComponent {

    fun context(): Context

    fun application(): Application
    fun inject(app: ChordApp)

    fun database(): PlayableDatabase
    fun provideSoundPlayer(): SoundPlayer
    fun provideDatabase(): PlayableDatabase
    fun provideDataAccess(): IData
    fun provideLogicAccess(): IDomain
    fun providesPrefHelper(): PrefHelper
    fun provideGson(): Gson


}
