package com.andreasgroentved.rechordnize.ui.multiplayer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.backend.LevelIdentifierEnum
import com.andreasgroentved.rechordnize.ui.base.BaseFragment
import com.andreasgroentved.rechordnize.ui.base.ClickListener
import com.andreasgroentved.rechordnize.ui.base.RecyclerTouchListener
import kotlinx.android.synthetic.main.multiplayer_fragment_level_type_select.*

class MultiPlayerSelectDifficultyFragment : BaseFragment() {


    private lateinit var adapter: MultiPlayerDifficultyAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.multiplayer_fragment_level_type_select, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setAdapter()
    }


    private fun setAdapter() {
        adapter = MultiPlayerDifficultyAdapter()
        adapter.list = MultiPlayerViewModel.getMultiPlayerDifficulties()
        recycler_multiplayer_select.adapter = adapter
        recycler_multiplayer_select.layoutManager = GridLayoutManager(context, 2).apply {
            spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int) = if (position == 0) 2 else 1
            }
        }
        recycler_multiplayer_select.addOnItemTouchListener(
                RecyclerTouchListener(requireContext(), recycler_multiplayer_select, object : ClickListener {
                    override fun onLongClick(view: View, position: Int) {}
                    override fun onClick(view: View, position: Int) {
                        if (position == 0) return //Header
                        MultiPlayerSelectDifficultyFragmentDirections.actionDestinationSelectmultiplayerdifficultyToMultiPlayerActivity(
                                LevelIdentifierEnum.fromInt(position - 1)).let {
                            findNavController().navigate(it)
                        }
                    }
                })
        )
    }
}