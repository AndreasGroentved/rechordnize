package com.andreasgroentved.rechordnize.ui.settings

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.R
import com.andreasgroentved.rechordnize.ui.AboutViewModelFactory
import com.andreasgroentved.rechordnize.ui.base.BaseFragment
import com.google.android.gms.oss.licenses.OssLicensesMenuActivity
import kotlinx.android.synthetic.main.about_fragment.*
import kotlinx.android.synthetic.main.main_activity.*


class AboutFragment : BaseFragment() {

    private lateinit var viewModel: AboutViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.about_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        activity?.supportFragmentManager?.beginTransaction()?.add(R.id.settings, SettingsFragment())?.commit()
        val factory = AboutViewModelFactory(requireActivity().application as ChordApp)
        viewModel = ViewModelProvider(this, factory).get(AboutViewModel::class.java)

        library_button.setOnClickListener {
            activity?.startActivity(Intent(activity, OssLicensesMenuActivity::class.java))
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.bottom_navigation?.visibility = View.GONE
    }

    override fun onDetach() {
        super.onDetach()
        activity?.bottom_navigation?.visibility = View.VISIBLE
    }

}
