package com.andreasgroentved.rechordnize.util

import com.andreasgroentved.rechordnize.BuildConfig


object DebugUtil {
    fun isDebug() = BuildConfig.BUILD_TYPE.toLowerCase() == "debug" /*false*/
}