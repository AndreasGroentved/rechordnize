package com.andreasgroentved.rechordnize.util

import android.os.Build
import android.text.Html
import android.text.Spanned
import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.ProgressBar
import android.widget.TextView
import com.andreasgroentved.rechordnize.ui.general.AnimationProgressBarViewData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


fun String.toSpanned(): Spanned =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
        } else {
            @Suppress("DEPRECATION")
            Html.fromHtml(this)
        }

fun String.removeParentheses() = this.replace("(", "").replace(")", "")


//TODO nested fragment
fun animateLevelUpProgress(viewData: AnimationProgressBarViewData, progressBar: ProgressBar?, levelText: TextView?, addedXp: TextView?, coroutineScope: CoroutineScope) {
    levelText?.text = "Level ${viewData.levelBefore}"
    addedXp?.text = "+${viewData.expGained}xp" //TODO konsistens i viewData krav og fragment sammensætning
    progressBar?.progress = viewData.beforeProgress
    coroutineScope.launch {
        ((viewData.levelBefore + 1)..(viewData.levelBefore + viewData.levelsGained)).forEach {
            progressBar?.let { animateProgressBar(it.max, progressBar) }
            levelText?.text = "Level $it"
            progressBar?.progress = 0
        }
        levelText?.text = "Level ${viewData.levelBefore + viewData.levelsGained}"
        progressBar?.let { animateProgressBar(viewData.finalProgress, it) }
    }
}

suspend fun animateProgressBar(endValue: Int, progressBar: ProgressBar, duration: Long = 1_500L): Boolean =
        suspendCoroutine { continuation ->
            val anim = ProgressBarAnimation(progressBar, progressBar.progress, endValue)
            var hasEnded = false
            fun ended() {
                if (hasEnded.not()) hasEnded = true; continuation.resume(true)
            }
            anim.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationEnd(animation: Animation?) {
                    ended()
                }

                override fun onAnimationStart(animation: Animation?) = Unit
                override fun onAnimationRepeat(animation: Animation?) = Unit
            })
            anim.duration = duration
            progressBar.startAnimation(anim)
        }

class ProgressBarAnimation(
        private
        val progressBar: ProgressBar, private val from: Int, private val to: Int
) : Animation() {
    override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
        super.applyTransformation(interpolatedTime, t)
        val value = from + (to - from) * interpolatedTime
        progressBar.progress = value.toInt()
    }
}