package com.andreasgroentved.rechordnize.util

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.andreasgroentved.rechordnize.R
import com.google.android.material.snackbar.Snackbar


object SnackUtil {
    fun getSnack(text: String, root: View, context: Context, length: Int = 2000, @ColorRes color: Int = R.color.error) =
        Snackbar.make(root, text, length).apply {
            view.setBackgroundColor(
                ContextCompat.getColor(context, color)
            )
            (view.layoutParams as ViewGroup.MarginLayoutParams).setMargins(0, 0, 0, 0)
            view.requestLayout()
        }
}