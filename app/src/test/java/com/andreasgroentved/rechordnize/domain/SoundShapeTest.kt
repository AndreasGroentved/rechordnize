package com.andreasgroentved.rechordnize.domain

import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.domain.sound.SoundShape
import org.junit.Assert
import org.junit.Test

class SoundShapeTest {


    @Test
    fun getSoundShapes() {
        singleChordTest()
        singleProgressionTest()
        singleChordTypeTest()
        singleIntervalTest()
        multipleDifferentTypesTest()
    }


    private fun multipleDifferentTypesTest() { //Added when should
        val singleChordTestData = listOf(Pair("M2", PlayableExtra.INTERVAL), Pair("M3", PlayableExtra.INTERVAL) //Giver 3
                , Pair("maj", PlayableExtra.CHORD_TYPE) //Giver 1
                , Pair("I maj", PlayableExtra.CHORD_PROGRESSION), Pair("IV maj", PlayableExtra.CHORD_PROGRESSION) //Giver 2
                , Pair("C maj", PlayableExtra.CHORD) //Giver 1
                , Pair("M2", PlayableExtra.INTERVAL) //Giver 2
        )
        val result = SoundShape.getSoundShapes(singleChordTestData, "c")
        Assert.assertTrue("size is 3+1+2+1+2", 9 == result.size)

        Assert.assertTrue("first position is c", result.first().first.dropLast(1) == "c")
        Assert.assertEquals("second position is d", "d", result[1].first.dropLast(1))
        Assert.assertEquals("third position is gb", "gb", result[2].first.dropLast(1))
        Assert.assertTrue("4th position has become a chord", result[3].second in PlayableExtra.getChordGroup())
        //Antag at resten er ok
    }


    private fun singleIntervalTest() {
        val singleChordTestData = listOf(Pair("M2", PlayableExtra.INTERVAL))
        val result = SoundShape.getSoundShapes(singleChordTestData)
        Assert.assertTrue("has added a root to interval", 2 == result.size)
        Assert.assertTrue("returned a valid note", result.first().first.dropLast(1).capitalize() in validNames)
        Assert.assertTrue("returned a valid note", result.last().first.dropLast(1).capitalize() in validNames)
    }

    private fun singleChordTypeTest() {
        val singleChordTestData = listOf(Pair("maj", PlayableExtra.CHORD_TYPE), Pair("min", PlayableExtra.CHORD_TYPE))
        val result = SoundShape.getSoundShapes(singleChordTestData)

        Assert.assertTrue("size is two", 2 == result.size)
        Assert.assertTrue("name did change and is valid chord name", result.first().first.split(" ").first() in validNames)
        Assert.assertTrue("name did change and is valid chord name", result.last().first.split(" ").first() in validNames)
    }


    private fun singleProgressionTest() {
        val singleChordTestData = listOf(Pair("I maj", PlayableExtra.CHORD_PROGRESSION), Pair("IV maj", PlayableExtra.CHORD_PROGRESSION))
        val result = SoundShape.getSoundShapes(singleChordTestData)

        Assert.assertTrue("size is two", 2 == result.size)
        Assert.assertTrue("has returned a chord type", result.first().second in PlayableExtra.getChordGroup())
        Assert.assertTrue("has returned a chord type", result.last().second in PlayableExtra.getChordGroup())
        Assert.assertTrue("name did change and is valid chord name", result.first().first.split(" ").first() in validNames)
        Assert.assertTrue("name did change and is valid chord name", result.last().first.split(" ").first() in validNames)
    }

    private val validNames = listOf("Ab", "A", "Bb", "B", "C", "Db", "D", "Eb", "E", "F", "Gb", "G")

    private fun singleChordTest() {
        val singleChordTestData = listOf(Pair("A maj", PlayableExtra.CHORD))
        val result = SoundShape.getSoundShapes(singleChordTestData)

        Assert.assertTrue("size is one", 1 == result.size)
        Assert.assertTrue("has returned a chord type", result.first().second in PlayableExtra.getChordGroup())
        Assert.assertEquals("name did not change", "A maj", result.first().first)
    }

}