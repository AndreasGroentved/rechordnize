package com.andreasgroentved.rechordnize.domain

import com.andreasgroentved.rechordnize.IData
import com.andreasgroentved.rechordnize.domain.mapper.UsageMapper
import com.andreasgroentved.rechordnize.model.usage.Usage
import com.andreasgroentved.rechordnize.model.util.DateUtil
import org.junit.Assert
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


class UsageMapperTest {


    @Mock
    lateinit var data: IData

    @Test
    fun getInARowTotalDays() {
        MockitoAnnotations.initMocks(this)
        val usageMapper = UsageMapper(data)
        val someDayLongBefore = "20190801"
        val dayAfterAfterSomeDayLongBefore = "20190803"
        val dayBeforeYesterday = "20190806"
        val yesterday = "20190807"
        val today = "20190808"
        val tomorrow = "20190809"

        val someDayLongBeforeUsage = Usage(time = 6 * 60 * 1000)
        val dayAfterAfterSomeDayLongBeforeUsage = Usage(time = 0)
        val dayBeforeYesterdayUsage = Usage(time = 6 * 60 * 1000)
        val yesterdayUsage = Usage(time = 0)
        val todayUsage = Usage(time = 6 * 60 * 1000)
        val tomorrowUsage = Usage(time = 6 * 60 * 1000)


        Mockito.`when`(data.getUsage(someDayLongBefore)).thenReturn(someDayLongBeforeUsage)
        Mockito.`when`(data.getUsage(dayAfterAfterSomeDayLongBefore)).thenReturn(dayAfterAfterSomeDayLongBeforeUsage)
        Mockito.`when`(data.getUsage(dayBeforeYesterday)).thenReturn(dayBeforeYesterdayUsage)
        Mockito.`when`(data.getUsage(yesterday)).thenReturn(yesterdayUsage)
        Mockito.`when`(data.getUsage(today)).thenReturn(todayUsage)
        Mockito.`when`(data.getUsage(tomorrow)).thenReturn(tomorrowUsage)
        val usageGoal = 5 * 60 * 1000
        Mockito.`when`(data.getUsageGoal()).thenReturn(5 * 60 * 1000)

        val initialTotalDays = 2L
        val initialInARow = 5

        val (inARowQ, totalDaysQ) = usageMapper.getInARowAndTotalDays(dayAfterAfterSomeDayLongBefore, usageGoal, initialTotalDays, someDayLongBefore, initialInARow, data)
        Assert.assertEquals("To little today and yesterday -> 0", 0, inARowQ)
        Assert.assertEquals("Total days before was 2, and now added 0 -> 2  ", 2, totalDaysQ)

        val (inARow1, totalDays1) = usageMapper.getInARowAndTotalDays(dayBeforeYesterday, usageGoal, initialTotalDays, someDayLongBefore, initialInARow, data)
        Assert.assertEquals("5 days since last check and current day is more than usage goal -> 1", 1, inARow1)
        Assert.assertEquals("Total days before was 2, and now added 1 -> ", 3L, totalDays1)


        val (inARow2, totalDays2) = usageMapper.getInARowAndTotalDays(yesterday, usageGoal, totalDays1, dayBeforeYesterday, inARow1, data)
        Assert.assertEquals("To little today, but should first count as zero tomorrow-> 1", 1, inARow2)
        Assert.assertEquals("Total days should not have changed -> 3 ", totalDays2, totalDays1)


        val (inARow3, totalDays3) = usageMapper.getInARowAndTotalDays(today, usageGoal, totalDays2, yesterday, inARow2, data)
        Assert.assertEquals("Enough today, but 0 day before -> 1", 1, inARow3)
        Assert.assertEquals("Total days added 1 -> 4 ", 4, totalDays3)

        val (inARow4, totalDays4) = usageMapper.getInARowAndTotalDays(tomorrow, usageGoal, totalDays3, today, inARow3, data)
        Assert.assertEquals("Enough today +1 -> 2", 2, inARow4)
        Assert.assertEquals("Total days added 1 -> 5 ", 5, totalDays4)


        val (inARow5, totalDays5) = usageMapper.getInARowAndTotalDays(tomorrow, usageGoal, totalDays4, DateUtil.getDayAfter(tomorrow), inARow4, data)
        Assert.assertEquals("Nothing changed -> 2", 2, inARow5)
        Assert.assertEquals("Nothing changed -> 5 ", 5, totalDays5)
    }


}