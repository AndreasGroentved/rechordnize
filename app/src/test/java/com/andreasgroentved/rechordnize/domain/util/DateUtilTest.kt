package com.andreasgroentved.rechordnize.domain.util

import com.andreasgroentved.rechordnize.model.util.DateUtil
import com.andreasgroentved.rechordnize.model.util.toDate
import junit.framework.Assert.assertEquals
import org.junit.Test


class DateUtilTest {


    @Test
    fun getTimeBetween() {
        var from = 1000L
        var to = 2000L
        assertEquals("1000ms = 1 s", ">1 sec", DateUtil.getTimeBetweenString(from, to))

        from = 1_000L
        to = 130_000L
        assertEquals("Difference more than two minute", ">2 mins", DateUtil.getTimeBetweenString(from, to))

        to = 1566217413568L
        from = 1565605018030L //Approx 7 day difference
        val timeBetween = DateUtil.getTimeBetweenString(from, to)
        assertEquals("1 week", ">1 week", timeBetween)
    }


    @Test
    fun getDayOfTheWeek() {
        var date = "20190805" //Monday -> day 1
        var dayOfWeek = DateUtil.getDayOfTheWeek(date.toDate())
        assertEquals("is first day of the week -> 1", 1, dayOfWeek)

        date = "20190806" //Add 1 day and you have tuesday
        dayOfWeek = DateUtil.getDayOfTheWeek(date.toDate())
        assertEquals("is second day of the week -> 2", 2, dayOfWeek)
    }

    @Test
    fun getDayOfTheMonth() {
        val date = "20190805" //5th day
        val dayOfMonth = DateUtil.getDayOfTheMonth(date.toDate())
        assertEquals("05 => 5", 5, dayOfMonth)
    }

    @Test
    fun getDayAfter() {
        val date = "20190831" //5th day
        val dayAfter = DateUtil.getDayAfter(date)
        assertEquals("Day after 20190831 -> 20190901", "20190901", dayAfter)
    }


    @Test
    fun getStartAndEndOfMonth() {
        var dateString = "20190815"
        var date = dateString.toDate()
        val (month, year) = DateUtil.getMonthAndYear(date)
        val (start, end) = DateUtil.getStartAndEndOfMonth(month, year)
        assertEquals("start is", "20190801", start)
        assertEquals("end is", "20190831", end)


        dateString = "20190215"
        date = dateString.toDate()
        val (month2, year2) = DateUtil.getMonthAndYear(date)
        val (start2, end2) = DateUtil.getStartAndEndOfMonth(month2, year2)
        assertEquals("start is", "20190201", start2)
        assertEquals("end is", "20190228", end2)
    }


    @Test
    fun getStartAndEndOfWeek() {
        var dateString = "20190807" //Wednesday
        var date = dateString.toDate()
        val (start, end) = DateUtil.getStartAndEndOfWeek(date.time)
        assertEquals("start is", "20190805", start)
        assertEquals("end is", "20190811", end)


        dateString = "20190802" //Friday
        date = dateString.toDate()
        val (start2, end2) = DateUtil.getStartAndEndOfWeek(date.time)
        assertEquals("start is", "20190729", start2)
        assertEquals("end is", "20190804", end2)
    }

    @Test
    fun getDaysOfMonth() {
        var dateString = "20190807"
        var time = dateString.toDate().time
        var daysOfMonth = DateUtil.getDaysOfMonth(time)
        assertEquals("31 days of August", 31, daysOfMonth)

        dateString = "20190207"
        time = dateString.toDate().time
        daysOfMonth = DateUtil.getDaysOfMonth(time)
        assertEquals("28 days of February (2019)", 28, daysOfMonth)
    }

    @Test
    fun getDurationString() {
        var durationInMs = 15 * 1000L //ms
        var durationString = DateUtil.getDurationString(durationInMs)
        assertEquals("15 s", "15 sec", durationString)


        durationInMs = 24/* days */ * 24 * 60 * 60 * 1000 //24 days
        durationString = DateUtil.getDurationString(durationInMs)
        assertEquals("24 days", "24 days", durationString)
    }


    private val MS_DAY_LENGTH = 24 * 60 * 60 * 1_000
    private val MS_LENGTH_OF_JANUARY: Long = 1_000L/*ms*/ * 60/*sec*/ * 60/*min*/ * 24/*tim*/ * 31/*dage*/
    private val MS_LENGTH_OF_FEBRUARY: Long = 1_000L/*ms*/ * 60/*sec*/ * 60/*min*/ * 24/*tim*/ * 28/*dage*/
    private val MS_LENGTH_OF_MARCH: Long = 1_000L/*ms*/ * 60/*sec*/ * 60/*min*/ * 24/*tim*/ * 31/*dage*/ - 1000 * 60 * 60/*-1 timer sommertid*/
    private val MS_LENGTH_OF_APRIL: Long = 1_000L/*ms*/ * 60/*sec*/ * 60/*min*/ * 24/*tim*/ * 30/*dage*/
    private val MS_LENGTH_OF_MAY: Long = 1_000L/*ms*/ * 60/*sec*/ * 60/*min*/ * 24/*tim*/ * 31/*dage*/
    private val MS_LENGTH_OF_JUNE: Long = 1_000L/*ms*/ * 60/*sec*/ * 60/*min*/ * 24/*tim*/ * 30/*dage*/
    private val MS_LENGTH_OF_JULY: Long = 1_000L/*ms*/ * 60/*sec*/ * 60/*min*/ * 24/*tim*/ * 31/*dage*/
    private val MS_LENGTH_OF_AUGUST: Long = 1_000L/*ms*/ * 60/*sec*/ * 60/*min*/ * 24/*tim*/ * 31/*dage*/
    private val MS_LENGTH_OF_SEPTEMBER: Long = 1_000L/*ms*/ * 60/*sec*/ * 60/*min*/ * 24/*tim*/ * 30/*dage*/
    private val MS_LENGTH_OF_OCTOBER: Long = 1_000L/*ms*/ * 60/*sec*/ * 60/*min*/ * 24/*tim*/ * 31/*dage*/ + 1000 * 60 * 60/*+1 timer sommertid*/
    private val MS_LENGTH_OF_NOVEMER: Long = 1_000L/*ms*/ * 60/*sec*/ * 60/*min*/ * 24/*tim*/ * 30/*dage*/
    private val MS_LENGTH_OF_DECEMBER: Long = 1_000L/*ms*/ * 60/*sec*/ * 60/*min*/ * 24/*tim*/ * 31/*dage*/
    private val MS_LENGTH_OF_WEEK: Long = 1_000L/*ms*/ * 60/*sec*/ * 60/*min*/ * 24/*tim*/ * 7/*dage*/


}