package com.andreasgroentved.rechordnize.domain

import com.andreasgroentved.rechordnize.model.util.ExpUtil
import org.junit.Assert
import org.junit.Test

class ExpUtilTest {

    @Test
    fun getLevelFromExp() {
        var exp = 51L
        var level2 = ExpUtil.getLevelFromExpV2(exp)
        var level3 = ExpUtil.getLevelFromExp(exp)
        Assert.assertEquals(level2, level3)

        exp = 106L
        level2 = ExpUtil.getLevelFromExpV2(exp)
        level3 = ExpUtil.getLevelFromExp(exp)
        Assert.assertEquals(level2, level3)

        exp = 250L
        level2 = ExpUtil.getLevelFromExpV2(exp)
        level3 = ExpUtil.getLevelFromExp(exp)
        Assert.assertEquals(level2, level3)

        exp = 500L
        level2 = ExpUtil.getLevelFromExpV2(exp)
        level3 = ExpUtil.getLevelFromExp(exp)
        Assert.assertEquals(level2, level3)

    }

    @Test
    fun getExpBelowAndAbove() {
        var exp = 49L
        val (below4, above4) = ExpUtil.getExpBelowAndAbove(exp)
        Assert.assertEquals(0L, below4); Assert.assertEquals(50L, above4)


        exp = 99L
        val (below, above) = ExpUtil.getExpBelowAndAbove(exp)
        Assert.assertEquals(50L, below); Assert.assertEquals(100L, above)

        exp = 100L
        val (below2, above2) = ExpUtil.getExpBelowAndAbove(exp)
        Assert.assertEquals(100L, below2); Assert.assertEquals(150L, above2)

        //Tjekker overgang mellem groupExpLimit tabel
        exp = 251L
        val (below3, above3) = ExpUtil.getExpBelowAndAbove(exp)
        Assert.assertEquals(250L, below3); Assert.assertEquals(350L, above3)

        //Sidste exp gruppe
        exp = 3403751L
        val (below5, above5) = ExpUtil.getExpBelowAndAbove(exp)
        Assert.assertEquals(3403750L, below5); Assert.assertEquals(exp + 25600 - 1, above5)

    }
}
