package com.andreasgroentved.rechordnize.domain

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.andreasgroentved.rechordnize.ChordApp
import com.andreasgroentved.rechordnize.backend.IGameHub
import com.andreasgroentved.rechordnize.backend.LevelIdentifierEnum
import com.andreasgroentved.rechordnize.backend.response.Response
import com.andreasgroentved.rechordnize.domain.sound.ISoundMapper
import com.andreasgroentved.rechordnize.model.level.LevelState
import com.andreasgroentved.rechordnize.ui.level.ILevelTimer
import com.andreasgroentved.rechordnize.ui.level.LevelStateHandler
import com.andreasgroentved.rechordnize.ui.level.SaveLevelState
import com.andreasgroentved.rechordnize.ui.multiplayer.MultiPlayerViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeoutOrNull
import kotlinx.coroutines.yield
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


@RunWith(MockitoJUnitRunner::class)
class MultiPlayerLevelViewModelTest {


    @Mock
    lateinit var app: ChordApp
    @Mock
    lateinit var domain: IDomain
    @Mock
    lateinit var saveLevelState: SaveLevelState
    @Mock
    lateinit var soundMapper: ISoundMapper
    @Mock
    lateinit var timer: ILevelTimer


    @get:Rule
    @Suppress("unused")
    var instantTaskExecutorRule = InstantTaskExecutorRule()


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

    }

    private val publishSubject = Channel<Response>()

    private fun getMultiViewModel(): MultiPlayerViewModel {
        MockitoAnnotations.initMocks(this)
        Mockito.`when`(soundMapper.getSounds(anyObject(), anyObject())).thenReturn(emptyList())
        Mockito.`when`(timer.cachedTime).thenReturn(2000L)
        Mockito.`when`(domain.getSound()).thenReturn(soundMapper)
        Mockito.`when`(domain.getLevelCompletions()).thenReturn(0L)

        val gameHub = Mockito.mock(IGameHub::class.java)
        Mockito.`when`(gameHub.hubObserver).thenReturn(publishSubject)
        saveLevelState = SaveLevelState(domain)
        gameHub.hubObserver = publishSubject

        Mockito.`when`(gameHub.joinOrCreateGame(anyObject())).then {

        }
        val multiView = MultiPlayerViewModel(app)
        val stateHandler = LevelStateHandler()
        multiView.gameHub = gameHub
        multiView.guessTimer = timer
        multiView.levelTimer = timer
        multiView.domain = domain
        multiView.saveLevelState = saveLevelState
        multiView.stateHandler = stateHandler
        multiView.setUp(LevelIdentifierEnum.EasyChordNames)
        return multiView
    }

    @Test
    fun init() {


        val multiView = getMultiViewModel()
        runBlocking {
            publishToSubjectAndYield(publishSubject, Response.ConnectedResponse("true"))
            val possible = listOf("A maj", "maj", "P8", "I maj")
            publishToSubjectAndYield(publishSubject, Response.GameJoinedResponse("detBedsteGameId", "coolDude85", listOf("otherDude"), possible))
            publishToSubjectAndYield(publishSubject, Response.GameStartedResponse("detBedsteGameId"))
            val correctList = listOf("A maj")
            levelStateHasXRounds(multiView.stateHandler, 0)


            publishToSubjectAndYield(publishSubject, Response.NextResponse(1, correctList))
            waitForInput(multiView)
            verifyDataForInput(multiView, possible, correctList)

            multiView.guess(listOf("wrong guess"))
            publishToSubjectAndYield(publishSubject, Response.GuessResponse("coolDude85", false))
            levelHasXGuesses(multiView.stateHandler, 1, 0)
            levelStateHasXRounds(multiView.stateHandler, 1)


            multiView.guess(correctList)
            levelStateHasXRounds(multiView.stateHandler, 1)
            levelHasXGuesses(multiView.stateHandler, 2, 0)


            publishToSubjectAndYield(publishSubject, Response.GuessResponse("coolDude85", true))
            publishToSubjectAndYield(publishSubject, Response.NextResponse(2, correctList))
            waitForInput(multiView)
            verifyDataForInput(multiView, possible, correctList)

            publishToSubjectAndYield(publishSubject, Response.GameEndResponse(mapOf("coolDude85" to 1, "otherDude" to 185), "detBedsteGamedId"))

            levelHasXGuesses(multiView.stateHandler, 0, 1)
            levelStateHasXRounds(multiView.stateHandler, 2)
            println("reac")
            endStateIsValid(multiView.stateHandler.levelState)

        }

    }


    private fun verifyDataForInput(viewModel: MultiPlayerViewModel, possible: List<String>, correct: List<String>) {
        Assert.assertEquals(possible, viewModel.getPossiblePlayables().map { it.name })
        Assert.assertEquals(correct, viewModel.getCorrectSequence())
        Assert.assertEquals(1, viewModel.getSequenceLength())
    }

    private fun waitForInput(viewModel: MultiPlayerViewModel) {
        viewModel.canInputMediator.blockingObserve()
    }


    fun <T> LiveData<T>.observeOnce(observer: Observer<T>) {
        observeForever(object : Observer<T> {
            override fun onChanged(t: T?) {
                observer.onChanged(t)
                removeObserver(this)
            }
        })
    }


    private fun endStateIsValid(ls: LevelState) {
        Assert.assertEquals("level should be completed", true, ls.isComplete)
        Assert.assertTrue("exp was gained", ls.expAfter > ls.expBefore)
    }

    private suspend fun publishToSubjectAndYield(pubSub: Channel<Response>, res: Response) {
        pubSub.sendBlocking(res)
        withTimeoutOrNull(100L) { pubSub.receive() }
        yield()
    }

    private fun levelStateHasXRounds(levelStateHandler: LevelStateHandler, rounds: Int) {
        Assert.assertEquals(rounds, levelStateHandler.levelState.rounds.size)
    }

    private fun levelHasXGuesses(levelStateHandler: LevelStateHandler, guesses: Int, roundNumb: Int) {
        Assert.assertEquals(guesses, levelStateHandler.levelState.rounds[roundNumb].guessList.size)
    }

    private fun <T> anyObject(): T {
        Mockito.anyObject<T>()
        return uninitialized()
    }

    fun <T> LiveData<T>.blockingObserve(): T? {
        var value: T? = null
        val latch = CountDownLatch(1)
        val innerObserver = Observer<T> {
            value = it
            latch.countDown()
        }
        observeForever(innerObserver)
        latch.await(2, TimeUnit.SECONDS)
        return value
    }

    private fun <T> uninitialized(): T = null as T

/*  fun <T> LiveData<T>.observeOnce(onChangeHandler: (T) -> Unit) {
      val observer = OneTimeObserver(handler = onChangeHandler)
      observe(observer, observer)
  }

  class OneTimeObserver<T>(private val handler: (T) -> Unit) : Observer<T>, LifecycleOwner {
      private val lifecycle = LifecycleRegistry(this)

      init {
          lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
      }

      override fun getLifecycle(): Lifecycle = lifecycle

      override fun onChanged(t: T) {
          handler(t)
          lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
      }
  }*/


}