package com.andreasgroentved.rechordnize.domain

import com.andreasgroentved.rechordnize.model.util.ChordDictionary
import junitparams.JUnitParamsRunner
import junitparams.Parameters
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import java.lang.Exception


@RunWith(JUnitParamsRunner::class)
class ChordDictionaryTest {

    private val chordDictionary = ChordDictionary()

    @Test
    fun lessOrInvalid() {
        assertEquals(true, chordDictionary.isNoteLessOrInvalid("c2", chordDictionary.getMinNote()))
        assertEquals(false, chordDictionary.isNoteLessOrInvalid("e2", chordDictionary.getMinNote()))
        assertEquals(false, chordDictionary.isNoteLessOrInvalid("f2", chordDictionary.getMinNote()))
        assertEquals(false, chordDictionary.isNoteLessOrInvalid("g4", chordDictionary.getMinNote()))
        assertEquals(false, chordDictionary.isNoteLessOrInvalid(chordDictionary.getMinNote(), chordDictionary.getMinNote()))
        assertEquals(true, chordDictionary.isNoteLessOrInvalid("f1", chordDictionary.getMinNote()))
        assertEquals(false, chordDictionary.isNoteLessOrInvalid("f13", chordDictionary.getMinNote()))
    }

    @Test
    fun moreOrInvalid() {
        assertEquals(false, chordDictionary.isNoteMoreOrInvalid("c5", chordDictionary.getMaxNote()))
        assertEquals(false, chordDictionary.isNoteMoreOrInvalid("g5", chordDictionary.getMaxNote()))
        assertEquals(false, chordDictionary.isNoteMoreOrInvalid(chordDictionary.getMaxNote(), chordDictionary.getMaxNote()))
        assertEquals(false, chordDictionary.isNoteMoreOrInvalid(chordDictionary.getMinNote(), chordDictionary.getMaxNote()))
        assertEquals(true, chordDictionary.isNoteMoreOrInvalid("f7", chordDictionary.getMaxNote()))
        assertEquals(true, chordDictionary.isNoteMoreOrInvalid("g8", chordDictionary.getMaxNote()))
        assertEquals(true, chordDictionary.isNoteMoreOrInvalid("g12", chordDictionary.getMaxNote()))
    }

    val octave = 12

    @Test
    fun getCChange() {
        cChange(3, "b3", 1)
        cChange(-3, "d3", -1)
        cChange(3 + octave, "b3", 2)
        cChange(-3 - octave, "d3", -2)
        cChange(3 + octave + octave, "b3", 3)
        cChange(-3 - octave - octave, "d3", -3)
        cChange(0, "c3", 0)
        cChange(8, "d3", 0)
        cChange(-1, "c3", -1)
        cChange(-4, "e3", 0)
    }

    private fun cChange(intervalDistance: Int, noteWithRoot: String, expected: Int) {
        assertEquals(expected, chordDictionary.getCChange(noteWithRoot, intervalDistance))
    }

    @Test
    fun getInterval() {
        interval("M3", "c3", true, Pair("c3", "e3"), false)
        interval("M3", "b3", true, Pair("b3", "eb4"), false)
        interval("P1", "c3", true, Pair("c3", "c3"), false)
        interval("M3", "c3", true, Pair("c3", "e3"), false)
        interval("M3", "c3", false, Pair("c3", "ab2"), false)
        interval("M3", "b3", false, Pair("b3", "g3"), false)
        interval("P1", "c3", false, Pair("c3", "c3"), false)
        interval("M3", "e3", false, Pair("e3", "c3"), false)
        interval("P15", "c", false, Pair("c", "c"), true)
    }

    private fun interval(names: String, root: String, ascending: Boolean, shouldBe: Pair<String, String>, dropLast: Boolean) {
        val notes = chordDictionary.getInterval(names, root, ascending)
        val actual = if (dropLast) Pair(notes.first.dropLast(1), notes.second.dropLast(1)) else notes
        assertEquals(shouldBe, actual)
    }


    @Test
    fun getFretForNote() {
        //6th
        assertEquals("e60", chordDictionary.getFretForNote("e2", 6))
        assertEquals("e610", chordDictionary.getFretForNote("d3", 6))
        //5th
        assertEquals("a50", chordDictionary.getFretForNote("a2", 5))
        assertEquals("a510", chordDictionary.getFretForNote("g3", 5))
        //4th
        assertEquals("d40", chordDictionary.getFretForNote("d3", 4))
        assertEquals("d410", chordDictionary.getFretForNote("c4", 4))
        //3th
        assertEquals("g30", chordDictionary.getFretForNote("g3", 3))
        assertEquals("g310", chordDictionary.getFretForNote("f4", 3))
        //2th
        assertEquals("b20", chordDictionary.getFretForNote("b3", 2))
        assertEquals("b210", chordDictionary.getFretForNote("a4", 2))
        //1th
        assertEquals("e10", chordDictionary.getFretForNote("e4", 1))
        assertEquals("e124", chordDictionary.getFretForNote("e6", 1))

        try {
            chordDictionary.getFretForNote("e2", 5)
            Assert.fail()
        } catch (e: Exception) {
        }
    }


    @Test
    fun getNoteAtDistanceFrom(){
        assertEquals("d",chordDictionary.getNoteAtDistanceFrom("b",3))
        assertEquals("d",chordDictionary.getNoteAtDistanceFrom("b",15))
        assertEquals("d",chordDictionary.getNoteAtDistanceFrom("e",10))
        assertEquals("d",chordDictionary.getNoteAtDistanceFrom("f",9))
    }

    @Test
    fun getIntervalMultiple() {
        intervalMultiple(listOf("M3", "M3"), "c3", true, listOf("c3", "e3", "ab3"))
        intervalMultiple(listOf("M3", "M3"), "c4", false, listOf("c4", "ab3", "e3"))
        intervalMultiple(listOf("P8", "P15"), "c", true, listOf("c", "c", "c"), true)
        intervalMultiple(listOf("P15", "P15"), "c", true, listOf("c", "c", "c"), true)
        intervalMultiple(listOf("P15", "P15"), "c", false, listOf("c", "c", "c"), true)
        intervalMultiple(listOf("M3", "M3", "M3"), "c3", true, listOf("c3", "e3", "ab3", "c4"))
        intervalMultiple(listOf("M3", "M3", "M3"), "c4", false, listOf("c4", "ab3", "e3", "c3"))
        intervalMultiple(listOf("m3", "P4", "M3"), "c3", true, listOf("c3", "eb3", "ab3", "c4"))
        intervalMultiple(listOf("m3", "P4", "M3"), "c4", false, listOf("c4", "a3", "e3", "c3"))
        intervalMultiple(listOf("m3", "m2", "P8"), "c3", true, listOf("c3", "eb3", "e3", "e4"))
        intervalMultiple(listOf("m3", "m2", "P8"), "c4", false, listOf("c4", "a3", "ab3", "ab2"))

        intervalMultiple(listOf("P15"), "c", false, listOf("c5", "c3"), false, false)
        intervalMultiple(listOf("P15"), "c", true, listOf("c3", "c5"), false, false)

    }

    private fun intervalMultiple(names: List<String>, root: String, ascending: Boolean, shouldBe: List<String>, dropLast: Boolean = false, random: Boolean = true) {
        val notes = chordDictionary.getIntervalMultiple(names, root, ascending, random)
        assertEquals(shouldBe, if (dropLast) notes.map { it.dropLast(1) } else notes)
    }

    @Test
    fun getFirstNoteRandom() {
        firstNoteRandom(3, "c", true, "c", true)
        firstNoteRandom(3, "c4", true, "c4", false)
        firstNoteRandom(3, "c", false, "a", true)
        firstNoteRandom(3, "c4", false, "a3", false)
        firstNoteRandom(4, "e3", false, "c3", false)
    }

    private fun firstNoteRandom(intervalDistance: Int, root: String, ascending: Boolean, expected: String, dropLast: Boolean) {
        val actual = chordDictionary.getFirstNoteRandom(intervalDistance, root, ascending)
        assertEquals(expected, if (dropLast) actual.dropLast(1) else actual)
    }

    private val ascending = true
    private val descending = false


    @Test
    fun getFirstNote() {
        firstNote(1, "a", ascending, "a3", false)
        firstNote(1, "a", descending, "ab3", false)
        firstNote(13, "a", descending, "ab3", false)
        firstNote(5, "c", descending, "g3", false)
        firstNote(24, "c", ascending, "c3", false)
        firstNote(24, "c5", descending, "c3", false)

        val firstNote1 = chordDictionary.getFirstNote(24, "c", false)
        firstNote(24, firstNote1, ascending, "c4", false)

    }

    private fun firstNote(intervalDistance: Int, root: String, ascending: Boolean, expected: String, dropLast: Boolean) {
        val actual = chordDictionary.getFirstNote(intervalDistance, root, ascending)
        assertEquals(expected, if (dropLast) actual.dropLast(1) else actual)
    }


    @Test
    fun getDistanceLine() {
        var distanceLine = chordDictionary.getDistanceLine("c", "c", 0)
        assertEquals(0, distanceLine)

        distanceLine = chordDictionary.getDistanceLine("b", "c", 0)
        assertEquals(1, distanceLine)

        distanceLine = chordDictionary.getDistanceLine("b", "db", 0)
        assertEquals(2, distanceLine)

        distanceLine = chordDictionary.getDistanceLine("bb", "c", 0)
        assertEquals(1, distanceLine)

        distanceLine = chordDictionary.getDistanceLine("c", "e", 0)
        assertEquals(2, distanceLine)

        distanceLine = chordDictionary.getDistanceLine("c", "c", 1)
        assertEquals(7, distanceLine)

        distanceLine = chordDictionary.getDistanceLine("c", "d", 1)
        assertEquals(8, distanceLine)

        distanceLine = chordDictionary.getDistanceLine("c", "d", 2)
        assertEquals(15, distanceLine)

        distanceLine = chordDictionary.getDistanceLine("c", "c", 2)
        assertEquals(14, distanceLine)
    }


    @Test
    fun getChordFromProgressionName() {
        assertEquals("G maj", chordDictionary.getChordFromProgressionName("V maj", "c"))
        assertEquals("D min", chordDictionary.getChordFromProgressionName("II min", "c"))
        assertEquals("B dim", chordDictionary.getChordFromProgressionName("VII dim", "c"))
        assertEquals("Gb maj", chordDictionary.getChordFromProgressionName("bV maj", "c"))
    }
}
