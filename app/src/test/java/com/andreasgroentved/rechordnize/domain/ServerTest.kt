package com.andreasgroentved.rechordnize.domain

import com.andreasgroentved.rechordnize.backend.LevelIdentifierEnum
import com.google.gson.Gson
import com.microsoft.signalr.HubConnectionBuilder
import kotlinx.coroutines.*
import org.junit.Assert
import org.junit.Test

class ServerTest {

    @Test
    fun onOpen() {

        Thread().run {
            val hubConnection = HubConnectionBuilder.create("http://localhost:57277/game")
                    .build()


            val gson = Gson()


            hubConnection.on("connected", {
                println(gson.fromJson(it, com.andreasgroentved.rechordnize.backend.response.Response.ConnectedResponse::class.java))
            }, String::class.java)


            hubConnection.start().doOnError {
                println(it.message)
                println("error")
            }.doOnEvent { }.blockingAwait()



            hubConnection.on("JoinGame", {
                val gameJoinedResponse = gson.fromJson(it, com.andreasgroentved.rechordnize.backend.response.Response.GameJoinedResponse::class.java)
                println(gameJoinedResponse)

            }, String::class.java)

            hubConnection.on("availableGames", {
                //  println("woot")
                val unStartedGamesResponse = gson.fromJson(it, com.andreasgroentved.rechordnize.backend.response.Response.UnStartedGamesResponse::class.java)
                println(unStartedGamesResponse)
                //it.UnStartedGames.map { it.IdentifierEnum }.forEach { println(it) }
                hubConnection.send("UnsubscribeAvailableGames")
                /*        hubConnection.send("JoinGame", unStartedGamesResponse.UnStartedGames.first().Id)*/
            }, String::class.java)

            var gamedId = ""
            var playerId = ""

            hubConnection.on("Next", {
                val res = gson.fromJson(it, com.andreasgroentved.rechordnize.backend.response.Response.NextResponse::class.java)
                println(res)
                val guessInfo = com.andreasgroentved.rechordnize.backend.GuessInfo(GuessList = res.CorrectList, RoundNumber = res.RoundNumber, Time = 1000)
                println(guessInfo)
                hubConnection.send("Guess", gamedId, gson.toJson(guessInfo))

                (GlobalScope + Dispatchers.IO).launch {
                    delay(5000)
                    hubConnection.send("Next", gamedId, 2)
                }

            }, String::class.java)


            hubConnection.on("Guess", {
                val res = gson.fromJson(it, com.andreasgroentved.rechordnize.backend.response.Response.GuessResponse::class.java)
                println(res)
            }, String::class.java)


            hubConnection.on("GameEnded", {
                val res = gson.fromJson(it, com.andreasgroentved.rechordnize.backend.response.Response.GameEndResponse::class.java)
                println(res)
            }, String::class.java)

            hubConnection.on("StartGame", {
                var res = gson.fromJson(it, com.andreasgroentved.rechordnize.backend.response.Response.GameStartedResponse::class.java)
                gamedId = res.GameId
                println(res)
                hubConnection.send("Next", res.GameId, 1)


            }, String::class.java)


            hubConnection.on("CreateGame", {
                val res = gson.fromJson(it, com.andreasgroentved.rechordnize.backend.response.Response.GameCreatedResponse::class.java)
                println(res)
                hubConnection.send("StartGame", res.GameId)
            }, String::class.java)



            hubConnection.send("SubscribeAvailableGames")
            hubConnection.send("CreateGame", com.andreasgroentved.rechordnize.backend.LevelIdentifierEnum.EasyChordNames.identifier)
        }


        Thread.sleep(200000)
        Assert.assertEquals("true", "true")
    }
}