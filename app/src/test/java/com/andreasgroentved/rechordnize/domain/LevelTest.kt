package com.andreasgroentved.rechordnize.domain

import com.andreasgroentved.rechordnize.domain.level.Level
import com.andreasgroentved.rechordnize.domain.level.OfflineLevel
import com.andreasgroentved.rechordnize.model.level.LevelData
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import org.junit.Assert
import org.junit.Test

class LevelTest {

    private lateinit var level: Level

    private fun getSingleTypeLevel(): Level {
        val playables = listOf(Playable(name = "A maj", extra = PlayableExtra.CHORD), Playable(name = "D maj", extra = PlayableExtra.CHORD), Playable(name = "E maj", extra = PlayableExtra.CHORD))
        val levelData = LevelData(playables = playables)
        return OfflineLevel(levelData)
    }


    private fun getMultiTypeLevel(): Level {
        val playables = listOf(Playable(name = "A maj", extra = PlayableExtra.CHORD),
                Playable(name = "maj", extra = PlayableExtra.CHORD_TYPE),
                Playable(name = "I maj", extra = PlayableExtra.CHORD_PROGRESSION), Playable(name = "IV maj", extra = PlayableExtra.CHORD_PROGRESSION),
                Playable(name = "M3", extra = PlayableExtra.INTERVAL), Playable(name = "M2", extra = PlayableExtra.INTERVAL))
        val levelData = LevelData(playables = playables)
        return OfflineLevel(levelData)
    }


    @Test
    fun guessNextSingle() {
        val level = getSingleTypeLevel()
        level.sequenceLength = 1
        level.next()
        val result = level.current
        Assert.assertTrue("any of the available playables", result.first() in level.levelData.playables)
    }

    @Test
    fun guessNextMultiple() {
        val level = getMultiTypeLevel()
        level.sequenceLength = 10
        repeat(20) {
            level.next()
            val result = level.current
            //No exceptions thrown
            Assert.assertTrue(true)
        }

    }


    @Test
    fun reset() {
        val level = getMultiTypeLevel()
        level.next();level.next()
        level.reset()

        val assertion = level.levelPlay.plays.size == 0 && level.levelPlay.guessCount == 0 && level.roundNum == 0

        Assert.assertEquals("condtions are true", true, assertion)
    }

    @Test
    fun setGuessOverride() {
        val level = getSingleTypeLevel()
        level.setNextOverride(listOf(Playable("A maj")))
        level.next()
        Assert.assertEquals("is A maj", "A maj", level.current.first().name)
    }

    @Test
    fun setEndOverride() {
        val level = getSingleTypeLevel()
        val roundNumBeforeEnd = level.roundNum
        level.setEndOverride()
        level.next()
        val roundNumAfterEnd = level.roundNum
        Assert.assertEquals("is ended", true, level.hasLevelEnded())
        Assert.assertEquals("round did not increase", roundNumBeforeEnd, roundNumAfterEnd)
    }



}