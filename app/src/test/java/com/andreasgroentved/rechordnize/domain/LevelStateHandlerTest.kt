package com.andreasgroentved.rechordnize.domain

import com.andreasgroentved.rechordnize.domain.DataForTest.getStateHandler
import org.junit.Assert
import org.junit.Test

class LevelStateHandlerTest {


    @Test
    fun beforeRoundState() {
        val sH = getStateHandler()
        val roundsBefore = sH.levelState.rounds.size
        sH.updateStateBeforeRound()
        val roundsAfter = sH.levelState.rounds.size
        Assert.assertEquals(1, roundsAfter - roundsBefore)
    }

    @Test
    fun reset() {
        val sH = getStateHandler()
        val empty = getStateHandler()
        sH.updateStateBeforeRound()
        sH.updateStateDuringRound(listOf("A maj"), 1000L)
        sH.updateStateBeforeRound()
        sH.updateStateDuringRound(listOf("A maj"), 1000L)
        Assert.assertNotEquals(empty.levelState, sH.levelState)
        sH.resetLevelState(0)
        Assert.assertEquals(sH.levelState, empty.levelState)
    }

    @Test
    fun updateStateDuringRound() {
        val sH = getStateHandler()
        sH.updateStateBeforeRound()
        sH.updateStateDuringRound(sH.level.current.map { it.name }, 1000L)
        val beforeExp = sH.levelState.rounds.last().expMap.values.sum()
        Assert.assertEquals(sH.levelState.rounds.last().guessList.first().first(), sH.level.current.map { it.name }.first())
        sH.updateStateDuringRound(sH.level.current.map { it.name }, 1000L)
        val afterExp = sH.levelState.rounds.last().expMap.values.sum()
        Assert.assertTrue(afterExp > beforeExp)
        Assert.assertTrue("should have one round", sH.levelState.rounds.size == 1)
        Assert.assertTrue("has two guesses for current round", sH.levelState.rounds.last().guessList.size == 2)
    }

    @Test
    fun updateStateFinal() {
        var sH = getStateHandler()
        var lS = sH.levelState
        sH.level.setNumberOfPlays(3)

        sH.updateStateBeforeRound()
        sH.updateStateDuringRound(listOf("wrong guess"), 0L)
        sH.updateStateDuringRound(lS.rounds[0].correctAnswer, 10_000L)
        Assert.assertEquals("should have added every guess as a difficultyList to the guesslist", 2, lS.rounds.last().guessList.size)
        Assert.assertEquals("round number is 1", 1, lS.rounds.size)

        sH.updateStateBeforeRound()
        sH.updateStateDuringRound(sH.level.current.map { it.name }, 10_000L)
        sH.updateStateBeforeRound()
        sH.updateStateDuringRound(sH.level.current.map { it.name }, 10_000L)
        sH.updateStateFinal(null)

        Assert.assertTrue("old grade is worse", lS.isBetter)
        Assert.assertEquals("new grade is 3", 3, lS.newGrade)
        Assert.assertTrue("stars is 3", lS.newLevelData.stars == 3)


        val oldLevelDate = sH.levelState.newLevelData
        oldLevelDate.stars = 3
        sH = getStateHandler()
        sH.level.setNumberOfPlays(3)
        lS = sH.levelState
        sH.resetLevelState(0)
        sH.updateStateBeforeRound()
        sH.updateStateDuringRound(sH.level.current.map { it.name }, 100L)
        sH.updateStateBeforeRound()
        sH.updateStateDuringRound(sH.level.current.map { it.name }, 100L)
        sH.updateStateBeforeRound()
        sH.updateStateDuringRound(sH.level.current.map { it.name }, 100L)
        sH.updateStateFinal(oldLevelDate)
        Assert.assertEquals("new grade is 5", 5, lS.newGrade)
        Assert.assertEquals("stars is 5", 5, lS.newLevelData.stars)
        Assert.assertTrue("level is set as completed", lS.isComplete)

        sH.level.setNumberOfPlays(7)
        sH.levelState.isComplete = false
        sH.updateStateFinal(oldLevelDate)
        Assert.assertFalse("level is not completed", lS.isComplete)
    }

}