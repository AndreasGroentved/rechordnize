package com.andreasgroentved.rechordnize.domain

import com.andreasgroentved.rechordnize.model.skill.SkillType
import com.andreasgroentved.rechordnize.ui.level.SaveLevelState
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.any
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class SaveLevelTest {

    @Mock
    lateinit var domain: IDomain

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun evaluateState() {
        val sH = DataForTest.getStateHandler()
        domain = Mockito.mock(IDomain::class.java)

        sH.updateStateBeforeRound()
        var correct = sH.level.current.map { it.name }
        sH.updateStateDuringRound(correct, 100L)
        sH.updateStateBeforeRound()
        correct = sH.level.current.map { it.name }
        sH.updateStateDuringRound(correct, 100L)
        sH.updateStateBeforeRound()
        correct = sH.level.current.map { it.name }
        sH.updateStateDuringRound(listOf("forkertSvar"), 100L) //TODO nok rigtigt
        sH.updateStateDuringRound(correct, 100L)
        sH.updateStateFinal(null)


        var numOfGuessCount = 0
        Mockito.`when`(domain.updateDailySkill(SkillType.NUM_OF_GUESSES, a(Int::class.java), any(Boolean::class.java))).then {
            numOfGuessCount++
        }

        var anyDaily = 0
        Mockito.`when`(domain.updateDailySkill(a(SkillType::class.java), a(Int::class.java), any(Boolean::class.java))).then {
            anyDaily++
        }


        Mockito.`when`(domain.getLevelCompletions()).thenReturn(0)




        println(domain.getLevelCompletions())
        var levelSave = SaveLevelState(domain)

        domain.addExp(2)
        domain.updateDailySkill(SkillType.NUM_OF_GUESSES, 1, false)

    }

    private fun <T> a(type: Class<T>): T = Mockito.any<T>(type)


}