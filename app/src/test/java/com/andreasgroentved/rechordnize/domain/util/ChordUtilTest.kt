package com.andreasgroentved.rechordnize.domain.util

import com.andreasgroentved.rechordnize.model.level.LevelPlay
import com.andreasgroentved.rechordnize.model.playable.PlayData
import com.andreasgroentved.rechordnize.model.util.PlayableUtil
import org.junit.Assert
import org.junit.Test

class ChordUtilTest {

    @Test
    fun isFirstLevelPlayBetterThanSecond() {
        val levelPlay1 = LevelPlay(mutableListOf(PlayData("a", 10_000L, true)))
        val levelPlay2 = LevelPlay(mutableListOf(PlayData("a", 12_000L, true)))
        var better = PlayableUtil.isFirstLevelPlayBetterThanSecond(levelPlay1, levelPlay2, 1)
        Assert.assertTrue("levelplay has shorter time => better", better == true)

        better = PlayableUtil.isFirstLevelPlayBetterThanSecond(levelPlay1, levelPlay1, 1)
        Assert.assertFalse("levelplay are equal and first therefore is not better", better)

        levelPlay2.plays.apply { clear(); add(PlayData("a", 10_000L, false)) }
        better = PlayableUtil.isFirstLevelPlayBetterThanSecond(levelPlay1, levelPlay2, 1)
        Assert.assertTrue("levelplay1 is more correct => better", better == true)

        better = PlayableUtil.isFirstLevelPlayBetterThanSecond(levelPlay2, levelPlay1, 1)
        Assert.assertTrue("opposite from last => not better", better == false)
    }

    @Test
    fun getAverageTime() {
        val levelPlay1 = LevelPlay(mutableListOf(PlayData("a", 10_000L, true), PlayData("a", 0, false)))
        val avgTime = PlayableUtil.getAverageTime(levelPlay1.plays, 2)

        Assert.assertEquals("10 and 0 = 5", 5000.0, avgTime, 0.01)
    }


    @Test
    fun `grade of 4`() {
        val levelPlay1 = LevelPlay(mutableListOf(PlayData("a", 0L, true), PlayData("a", 0L, true), PlayData("a", 0L, true), PlayData("a", 0L, true), PlayData("a", 0L, false)), 5, 5)
        val grade = PlayableUtil.grade(levelPlay1, 4)
        Assert.assertEquals("Time max grade for 0L is 5, correctness is 4/5 - 80% or 4, min of 4 and 5 -> 4", 4, grade)
    }

    @Test
    fun `grade of 3`() {
        val levelPlay1 = LevelPlay(mutableListOf(PlayData("a", 10_000L, true), PlayData("a", 10_000L, true)), 2, 2)
        val grade = PlayableUtil.grade(levelPlay1, 2)
        Assert.assertEquals("Time max grade for 10_000L is 3, correctness is 100% or 5, min of 3 and 5 -> 3", 3, grade)
    }

}
