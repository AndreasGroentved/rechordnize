package com.andreasgroentved.rechordnize.domain

import com.andreasgroentved.rechordnize.model.util.ChordDictionary
import com.andreasgroentved.rechordnize.model.level.LevelData
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.ui.level.LevelStateHandler

object DataForTest {

    //TODO duplicate
    fun getStateHandler(): LevelStateHandler {
        val playables = listOf(Playable("A maj"), Playable("D maj"), Playable("E maj"))
        val newLevelData = getLevelData(playables)
        return LevelStateHandler().apply { initialize(0, newLevelData, isCustomLevel = false) }
    }


    fun getLevelData(playables: List<Playable>): LevelData {
        val newLevelData = LevelData("1", playables)
        val difficultyModifiers = ChordDictionary().typeToLevelModifiers(PlayableExtra.CHORD.ext)[1]
        newLevelData.levelModifiers = difficultyModifiers!!.toMutableList()
        return newLevelData
    }
}