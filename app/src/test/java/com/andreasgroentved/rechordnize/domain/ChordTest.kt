package com.andreasgroentved.rechordnize.domain

import com.andreasgroentved.rechordnize.domain.sound.SoundMapper
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.util.ChordDictionary
import com.andreasgroentved.rechordnize.model.util.PlayableUtil
import org.junit.Assert
import org.junit.Test

class ChordTest {




    @Test
    fun removeDuplicate() {
        val name = "a maj"
        val extension = PlayableUtil.getChordExtension(name)
        val open = listOf(PlayableExtra.OPEN)
        val other = open + ChordDictionary().chordShapeMap[extension]!!.keys
        val cleaned = PlayableUtil.removeDuplicate(open, other, name)
        Assert.assertEquals(3, cleaned.size)
        Assert.assertEquals(cleaned[0], PlayableExtra.OPEN)
    }

    @Test
    fun lowestString() {
        val shape = listOf(-1, 3, 2, 0, 1, 0)
        val lowest = SoundMapper.getLowestString(shape)
        Assert.assertEquals(5, lowest)
        val shape2 = listOf(0, 2, 2, 0, 0, 0)
        val lowest2 = SoundMapper.getLowestString(shape2)
        Assert.assertEquals(6, lowest2)
        val shape3 = listOf(-1, -1, 0, 2, 3, 2)
        val lowest3 = SoundMapper.getLowestString(shape3)
        Assert.assertEquals(4, lowest3)
    }

    @Test
    fun fretNumNote() {
        val note = "a"
        val string = 5
        val shape = listOf(-1, 3, 2, 0, 1, 0)
        val fretNum = SoundMapper.getFretNumNoteOnString(note, string, shape, ChordDictionary())
        Assert.assertEquals(12, fretNum)
    }

    @Test
    fun getType() {
        val chordDictionary = ChordDictionary()
        var name = "A maj"
        var type = PlayableUtil.getType(name, chordDictionary)
        Assert.assertEquals("Expected chord type", PlayableExtra.CHORD, type)

        name = "II maj"
        type = PlayableUtil.getType(name, chordDictionary)
        Assert.assertEquals("Expected chord type", PlayableExtra.CHORD_PROGRESSION, type)

        name = "Definitely invalid"
        type = PlayableUtil.getType(name, chordDictionary)
        Assert.assertEquals("Expected chord type", PlayableExtra.INVALID, type)

        name = "P8"
        type = PlayableUtil.getType(name, chordDictionary)
        Assert.assertEquals("Expected chord type", PlayableExtra.INTERVAL, type)

        name = "maj7"
        type = PlayableUtil.getType(name, chordDictionary)
        Assert.assertEquals("Expected chord type", PlayableExtra.CHORD_TYPE, type)


    }

}
