package com.andreasgroentved.rechordnize.domain

import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.util.PlayableComparator
import com.andreasgroentved.rechordnize.model.util.SortBy
import org.junit.Assert

import org.junit.Test
import java.util.*

class PlayableComparatorTest {


    @Test
    fun `chordSorting Ab less than B`() {
        val b = Playable("B min")
        val a = Playable("Ab maj7")

        val chordList: MutableList<Playable> = mutableListOf(a, b)
        Collections.sort(chordList, PlayableComparator(sortBy = SortBy.NAME))
        Assert.assertEquals("Ab maj7 is first", "Ab maj7", chordList[0].name)
    }

    @Test
    fun `chordSorting Ab less than A`() {
        val b = Playable("A min")
        val a = Playable("Ab maj7")

        val chordList: MutableList<Playable> = mutableListOf(a, b)
        Collections.sort(chordList, PlayableComparator(sortBy = SortBy.NAME))
        Assert.assertEquals("Ab maj7 is first", "Ab maj7", chordList[0].name)
    }


}