package com.andreasgroentved.rechordnize.domain

import com.andreasgroentved.rechordnize.LevelDSL
import com.andreasgroentved.rechordnize.model.util.AlphaNumComparator
import org.junit.Test

class CreationDataForServer {


    @Test
    fun levelCreation() {
        LevelDSL.LevelBuilder().run {
            let {
                val easyNames = listOf(allOpenChords)
                easyNames.flatten().toSet().sortedBy { (a, b) -> AlphaNumComparator.compare(a, b) }.joinToString(",") { "\"" + it.name + "\"" }.let { println(it) }
                val mediumNames = listOf(powerAll) + easyNames
                mediumNames.flatten().toSet().sortedBy { (a, b) -> AlphaNumComparator.compare(a, b) }.joinToString(",") { "\"" + it.name + "\"" }.let { println(it) }
                val hardNames = listOf(barreAll) + mediumNames
                hardNames.flatten().toSet().sortedBy { (a, b) -> AlphaNumComparator.compare(a, b) }.joinToString(",") { "\"" + it.name + "\"" }.let { println(it) }
            }

            let {
                val easyTypes = listOf(typeD1, typeD2, typeD3, typeD4)
                easyTypes.flatten().toSet().sortedBy { (a, b) -> AlphaNumComparator.compare(a, b) }.joinToString(",") { "\"" + it.name + "\"" }.let { println(it) }
                val mediumTypes = listOf(typeD5, typeD6, typeD7, typeD8, typeD9, typeD10) + easyTypes
                mediumTypes.flatten().toSet().sortedBy { (a, b) -> AlphaNumComparator.compare(a, b) }.joinToString(",") { "\"" + it.name + "\"" }.let { println(it) }
                val hardTypes = listOf(typeD11, typeD12, typeD13, typeD14, typeD15, typeD16) + mediumTypes
                hardTypes.flatten().toSet().sortedBy { (a, b) -> AlphaNumComparator.compare(a, b) }.joinToString(",") { "\"" + it.name + "\"" }.let { println(it) }
            }

            let {
                val easyProgression = listOf(progressionAllStandard)
                easyProgression.flatten().toSet().sortedBy { (a, b) -> AlphaNumComparator.compare(a, b) }.joinToString(",") { "\"" + it.name + "\"" }.let { println(it) }
                val mediumProgression = listOf(progressionV7All) + easyProgression
                mediumProgression.flatten().toSet().sortedBy { (a, b) -> AlphaNumComparator.compare(a, b) }.joinToString(",") { "\"" + it.name + "\"" }.let { println(it) }
                val hardProgression = listOf(progressionAll)
                hardProgression.flatten().toSet().sortedBy { (a, b) -> AlphaNumComparator.compare(a, b) }.joinToString(",") { "\"" + it.name + "\"" }.let { println(it) }
            }

            let {
                val easyInterval = listOf(intervalT1, intervalT2, intervalT3)
                easyInterval.flatten().toSet().sortedBy { (a, b) -> AlphaNumComparator.compare(a, b) }.joinToString(",") { "\"" + it.name + "\"" }.let { println(it) }
                val mediumInterval = listOf(intervalT4, intervalT5, intervalT6) + easyInterval
                mediumInterval.flatten().toSet().sortedBy { (a, b) -> AlphaNumComparator.compare(a, b) }.joinToString(",") { "\"" + it.name + "\"" }.let { println(it) }
                val hardInterval = mediumInterval + listOf(intervalT7, intervalT8)
                hardInterval.flatten().toSet().sortedBy { (a, b) -> AlphaNumComparator.compare(a, b) }.joinToString(",") { "\"" + it.name + "\"" }.let { println(it) }
            }
        }

    }


}