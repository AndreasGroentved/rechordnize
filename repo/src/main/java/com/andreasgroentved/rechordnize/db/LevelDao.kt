package com.andreasgroentved.rechordnize.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.andreasgroentved.rechordnize.model.level.LevelData
import com.andreasgroentved.rechordnize.model.level.LevelSelectionHolder


@Dao
interface LevelDao {

    @Query("SELECT * FROM LevelData WHERE id = :id")
    fun loadLevel(id: String): LiveData<LevelData>

    @Query("SELECT * FROM LevelData WHERE id = :id")
    fun getLevelSync(id: String): LevelData?

    @Query("SELECT * FROM LevelData WHERE levelDifficulty = :intVal")
    fun getLevelsWithDifficulty(intVal: Int): LiveData<List<LevelData>>

    @Query("SELECT id FROM LevelData WHERE levelDifficulty = :intVal")
    fun getLevelIdsFromDifficulty(intVal: Int): LiveData<List<String>>

    @Query("SELECT id FROM LevelData WHERE levelDifficulty IN (:difficulties)")
    fun getLevelIdsFromDifficulties(difficulties: Array<Int>): LiveData<List<String>>

    @Query("SELECT * FROM LevelData")
    fun loadLevels(): LiveData<List<LevelData>>

    @Query("SELECT playables FROM LevelData WHERE id = :name")
    fun getChordsOfLevel(name: String): String

    @Query("SELECT * FROM LevelData WHERE levelDifficulty IN (:difficulties)")
    fun loadLevelsWithDifficulties(difficulties: Array<Int>): LiveData<List<LevelData>>


    @Query("SELECT id,stars,levelDifficulty FROM LevelData WHERE levelDifficulty IN (:difficulties) ORDER BY id ASC")
    fun loadLevelIDsWithDifficulties(difficulties: Array<Int>): LiveData<List<LevelSelectionHolder>>


    @Query("SELECT COUNT() FROM LevelData WHERE levelDifficulty IN (:difficulties)")
    fun loadLevelsWithDifficultiesCount(difficulties: Array<Int>): Int

    @Query("SELECT COUNT(id) FROM LevelData")
    fun getLevelCount(): LiveData<Int>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLevelData(vararg levelData: LevelData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLevelDatas(levelData: List<LevelData>)

    @Update
    fun updateLevelData(vararg users: LevelData)

    @Query("DELETE from LevelData WHERE id = :id AND levelDifficulty NOT in (21,22,23,34,25) ")
    fun deleteLevelData(id: String): Int
}