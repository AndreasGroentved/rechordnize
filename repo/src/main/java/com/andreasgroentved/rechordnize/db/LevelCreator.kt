package com.andreasgroentved.rechordnize.db

import com.andreasgroentved.rechordnize.LevelDSL
import com.andreasgroentved.rechordnize.model.level.LevelData
import com.andreasgroentved.rechordnize.model.level.LevelDifficulty
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra

class LevelCreator {

    private val levelBuilder = LevelDSL.LevelBuilder()
    fun getAllLevels(): List<LevelData> {
        val chordLevels = getAllChordLevels()
        val typeLevels = getAllTypeLevels()
        val progressionLevels = getAllProgressionLevels()
        val intervalLevels = getAllIntervalLevels()
        return (chordLevels + typeLevels + progressionLevels + intervalLevels).map {
            LevelData(
                id = getID(), playables = it.playables.map { Playable(name = it.name, extra = PlayableExtra.getExtra(it.extra)) },
                levelDifficulty = LevelDifficulty.fromInt(it.difficulty)
            )
        }
    }

    private var id = 0
    private fun getID() = "${id++}"

    private fun getAllChordLevels() = levelBuilder.run {
        levelGroup(
            LevelDifficulty.OPEN_CHORDS.intVal, listOf(
                openGroupMaj1, openGroupMin1, openGroupMaj2, openGroup71,
                openGroup72, openGroup7Min, openGroup7Maj, openGroupSus1,
                openGroupSus2, openGroupMajAll, openGroup7All, openGroup7MajMinAll,
                openGroupTriadAll, openGroupSusAll, allOpenChords
            )
        ) + levelGroup(
            LevelDifficulty.POWER_CHORDS.intVal, listOf(
                powerGroupT1, powerGroupT2, powerGroupT3, powerGroupT4,
                powerGroupQ1, powerGroupQ2, powerAll
            )
        ) + levelGroup(
            LevelDifficulty.BARRE_CHORDS.intVal, listOf(
                barreMaj1, barreMaj2, barreMin1, barreMin2, barreMajAll, barreMinAll, barre71, barre72,
                barre7All, barreMin71, barreMin72, barreMin7All, barreMaj71, barreMaj72, barreMaj7All, barreAll
            )
        )
    }

    private fun getAllIntervalLevels() = levelBuilder.run {
        levelGroup(
            LevelDifficulty.INTERVAL_EASY.intVal, listOf(
                intervalD1, intervalD2, intervalD3, intervalD4, intervalD5, intervalD6
            )
        ) + levelGroup(
            LevelDifficulty.INTERVAL_MEDIUM.intVal, listOf(
                intervalT1, intervalT2, intervalT3, intervalT4, intervalT5, intervalT6, intervalT7, intervalT8
            )
        ) + levelGroup(
            LevelDifficulty.INTERVAL_HARD.intVal, listOf(
                intervalQ1, intervalQ2, intervalQ3, intervalS1, intervalS2, intervalS3, intervalS4
            )
        ) + levelGroup(
            LevelDifficulty.INTERVAL_THE_END.intVal, listOf(
                intervalSS1, intervalSS2, intervalAll
            )
        )
    }


    private fun getAllProgressionLevels() = levelBuilder.run {

        levelGroup(
            LevelDifficulty.CHORD_PROGRESSION_EASY.intVal, listOf(
                progressionD1, progressionD2, progressionD3, progressionT1, progressionT2, progressionQ1, progressionQ2, progressionAllStandard
            )
        ) + levelGroup(
            LevelDifficulty.CHORD_PROGRESSION_MEDIUM.intVal, listOf(
                progressionV7T1, progressionV7T2, progressionV7Q1, progressionV7Q2, progressionV7All, progressionV7Q1Standard, progressionV7Q2Standard, progressionV7AllStandard
            )
        ) + levelGroup(
            LevelDifficulty.CHORD_PROGRESSION_HARD.intVal, listOf(
                progressionOtherT1, progressionOtherT2, progressionOtherT3, progressionOtherT4, progressionOtherT5, progressionOtherStandardOtherT1, progressionOtherStandardOtherT2,
                progressionOtherStandardOtherT3, progressionOtherStandardOtherT4, progressionOtherStandardOtherT5
            )
        ) + levelGroup(
            LevelDifficulty.CHORD_PROGRESSION_THE_END.intVal, listOf(
                progressionOtherStandardOtherD1, progressionOtherStandardOtherD2, progressionOtherStandardOtherD3, progressionOtherStandardOther7T1, progressionOtherStandardOther7T2,
                progressionOtherStandardOther7T3, progressionOtherStandardOther7T4, progressionOtherStandardOther7T5,
                progressionOtherStandardOther7D1, progressionOtherStandardOther7D2, progressionOtherStandardOther7D3, progressionAll
            )
        )

    }

    private fun getAllTypeLevels() = levelBuilder.run {
        levelGroup(
            LevelDifficulty.CHORD_TYPE_EASY.intVal, listOf(
                typeD1, typeD2, typeD3, typeD4, typeD5, typeD6, typeD7, typeD8, typeD9, typeD10, typeD11, typeD12, typeD13, typeD14,typeD15,typeD16
            )
        ) + levelGroup(
            LevelDifficulty.CHORD_TYPE_MEDIUM.intVal, listOf(
                typeQ1, typeQ2, typeQ3, typeQ4, typeQ5, typeQ6, typeQ7,typeQ8
            )
        ) + levelGroup(
            LevelDifficulty.CHORD_TYPE_HARD.intVal, listOf(
                typeO1, typeO2, typeO3, typeO4
            )
        ) + levelGroup(
            LevelDifficulty.CHORD_TYPE_THE_END.intVal, listOf(
                typeH1, typeH2, typeAll
            )
        )
    }
}