package com.andreasgroentved.rechordnize.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.andreasgroentved.rechordnize.model.DailyGoal

@Dao
interface DailyGoalDao {

    @Query("SELECT * FROM DailyGoal WHERE date = :date")
    fun getDailyGoal(date: String): LiveData<DailyGoal>

    @Query("SELECT * FROM DailyGoal WHERE date = :date")
    suspend fun getDailyGoalSync(date: String): DailyGoal?

    @Query("SELECT COUNT(*) FROM DailyGoal WHERE succeeded = 1")
    fun getDailyCompletions(): LiveData<Int>

    @Update
    fun updateGoal(dailyGoal: DailyGoal)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDailyGoal(dailyGoal: DailyGoal)

}