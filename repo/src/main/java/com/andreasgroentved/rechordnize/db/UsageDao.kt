package com.andreasgroentved.rechordnize.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.andreasgroentved.rechordnize.model.usage.Usage
import kotlinx.coroutines.flow.Flow


@Dao
interface UsageDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateUsage(usage: Usage)

    @Query("SELECT * FROM usage")
    fun getAllUsage(): List<Usage>


    @Query("SELECT * FROM usage")
    fun getAllUsageSuspend(): Flow<List<Usage>>

    @Query("SELECT * FROM usage WHERE date = :date") //3 timer buffer
    fun getUsageLive(date: String): LiveData<Usage>

    @Query("SELECT * FROM usage WHERE date  = :date ")
    fun getUsage(date: String): Usage?

    @Query("SELECT * FROM usage WHERE date BETWEEN :fromDate AND :toDate")
    fun getUsageFromTo(fromDate: String, toDate: String): LiveData<List<Usage>>

    @Query("SELECT SUM(time) FROM usage")
    fun getTotalTime(): LiveData<Long>

    @Query("SELECT COUNT(*) FROM usage")
    fun getDaysPractised(): LiveData<Int>

    @Query("SELECT MAX(time) FROM usage")
    fun getMostUsageDay(): LiveData<Int>

    @Query("SELECT AVG(time) FROM usage")
    fun getTotalUsageAverage(): LiveData<Int>

}