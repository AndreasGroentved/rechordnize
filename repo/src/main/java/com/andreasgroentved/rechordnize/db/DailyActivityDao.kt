package com.andreasgroentved.rechordnize.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.andreasgroentved.rechordnize.model.skill.DailySkill
import com.andreasgroentved.rechordnize.model.skill.SkillType


@Dao
interface DailyActivityDao {

    //TODO pruning af gammel data
    @Query("SELECT * FROM DailySkill WHERE name = :name AND date = :date")
    suspend fun getDailySkill(date: String, name: SkillType): DailySkill?

    @Query("SELECT * FROM DailySkill WHERE name = :name AND date = :date")
    fun getDailySkillSync(date: String, name: SkillType): DailySkill?

    @Query("SELECT * FROM DailySkill WHERE date = :date")
    fun getDailySkills(date: String): LiveData<List<DailySkill>>


    @Update
    fun updateDailySkill(dailySkill: DailySkill)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDailySkill(dailySkill: DailySkill)

}