package com.andreasgroentved.rechordnize.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.andreasgroentved.rechordnize.model.skill.Skill
import com.andreasgroentved.rechordnize.model.skill.SkillType


@Dao
interface SkillDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateSkill(skill: Skill)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertSkill(skill: Skill)

    @Query("SELECT * FROM skill WHERE name = :name")
    fun getSkill(name: SkillType): LiveData<Skill>

    @Query("SELECT * FROM skill WHERE name = :name")
    fun getSkillSync(name: SkillType): Skill

    @Query("SELECT * FROM skill")
    fun getSkills(): LiveData<List<Skill>>

}