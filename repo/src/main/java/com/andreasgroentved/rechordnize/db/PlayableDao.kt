package com.andreasgroentved.rechordnize.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.playable.PlayableNameWithExtra

@Dao
interface PlayableDao {
    @Query("SELECT * FROM playable WHERE extra in (0,1,2,3,4,5)") //TODO giv argument i stedet for, metode mere general
    fun loadChords(): LiveData<List<Playable>>


    @Query("SELECT * FROM playable WHERE extra = 9") //TODO giv argument i stedet for, metode mere general
    fun loadIntervals(): LiveData<List<Playable>>

    @Query("SELECT * FROM playable WHERE extra = 13") //TODO giv argument i stedet for, metode mere general
    fun loadProgressions(): LiveData<List<Playable>>

    @Query("SELECT * FROM playable WHERE extra = 14") //TODO giv argument i stedet for, metode mere general
    fun loadTypes(): LiveData<List<Playable>>


    @Query("SELECT * FROM playable WHERE extra = 5") //TODO where
    fun loadChordsS(): List<Playable>


    @Query("SELECT * FROM playable WHERE name = :chordName") //TODO where
    fun loadChord(chordName: String): LiveData<Playable>

    @Query("SELECT * FROM playable WHERE name IN (:chordNames)") //TODO where
    fun loadChordsByNames(chordNames: Array<String>): LiveData<List<Playable>>

    @Query("SELECT name, exp FROM playable WHERE name IN (:playableNames)")
    fun loadSimplePlayables(playableNames: Array<String>): LiveData<List<PlayableNameWithExtra>>


    @Query("SELECT * FROM playable WHERE name IN (:chordNames)") //TODO where
    fun loadChordsByNamesRaw(chordNames: Array<String>): List<Playable>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun createPlayable(playables: List<Playable>)

    @Update
    fun updatePlayable(playable: Playable)

    @Update
    fun updatePlayables(playable: List<Playable>)

}