package com.andreasgroentved.rechordnize.db

import androidx.room.TypeConverter
import com.andreasgroentved.rechordnize.model.level.LevelDifficulty
import com.andreasgroentved.rechordnize.model.level.LevelModifier
import com.andreasgroentved.rechordnize.model.level.LevelPlay
import com.andreasgroentved.rechordnize.model.playable.PlayData
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.skill.SkillType
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken



class DBConverter {

    private val gson = Gson()

    @TypeConverter
    fun chordPlayStringToList(value: String): MutableList<PlayData> = gson.fromJson(value, object : TypeToken<List<PlayData>>() {}.type)

    @TypeConverter
    fun listOfChordPlaysToString(plays: MutableList<PlayData>): String = gson.toJson(plays, object : TypeToken<List<PlayData>>() {}.type)

    @TypeConverter
    fun chordStringToList(value: String): List<Playable> = value.split(',').map {
        val pair: List<String> = it.split(':')
        Playable(name = pair[0], extra = PlayableExtra.fromInt(pair[1].toInt()))
    }


    @TypeConverter
    fun mapOfLevelPlaysToString(levelPlays: MutableMap<List<LevelModifier>, LevelPlay>): String {
        val newMap: Map<String, LevelPlay> = levelPlays.map { gson.toJson(it.key, object : TypeToken<List<LevelModifier>>() {}.type) to it.value }.toMap()
        return gson.toJson(newMap, object : TypeToken<MutableMap<String, LevelPlay>>() {}.type)
    }

    @TypeConverter
    fun stringToMapOfLevelPlays(levelPlays: String): MutableMap<List<LevelModifier>, LevelPlay> {
        val mapWithStringKey: MutableMap<String, LevelPlay> = gson.fromJson(levelPlays, object : TypeToken<MutableMap<String, LevelPlay>>() {}.type)
        return mapWithStringKey.map { gson.fromJson(it.key, object : TypeToken<List<LevelModifier>>() {}.type) as List<LevelModifier> to it.value }.toMap().toMutableMap()
    }


    @TypeConverter
    fun listOfChordsToString(chords: List<Playable>): String = chords.asSequence().map { chord -> chord.name + ":" + chord.extra.ext }.joinToString(",")

    @TypeConverter
    fun enumDifficultyToInt(diff: LevelDifficulty): Int = diff.intVal

    @TypeConverter
    fun intToEnumDifficulty(diff: Int): LevelDifficulty? = LevelDifficulty.fromInt(diff)

    @TypeConverter
    fun enumExtraToInt(playableExtra: PlayableExtra): Int = playableExtra.ext

    @TypeConverter
    fun intToEnumExtra(ext: Int): PlayableExtra? = PlayableExtra.fromInt(ext)

    @TypeConverter
    fun enumSkillTypeToInt(skill: SkillType): Int = skill.s

    @TypeConverter
    fun intToEnumSkillType(skill: Int): SkillType? = SkillType.fromInt(skill)

}