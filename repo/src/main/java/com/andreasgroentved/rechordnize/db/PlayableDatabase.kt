package com.andreasgroentved.rechordnize.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.andreasgroentved.rechordnize.model.DailyGoal
import com.andreasgroentved.rechordnize.model.level.LevelData
import com.andreasgroentved.rechordnize.model.level.LevelIdModifier
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.skill.DailySkill
import com.andreasgroentved.rechordnize.model.skill.Skill
import com.andreasgroentved.rechordnize.model.usage.Usage


@Database(
        entities = [(LevelData::class), (Playable::class), (Usage::class), (Skill::class), (LevelIdModifier::class), DailyGoal::class, DailySkill::class], version = 1
)
@TypeConverters(DBConverter::class)
abstract class PlayableDatabase : RoomDatabase() {
    abstract fun levelDao(): LevelDao
    abstract fun playableDao(): PlayableDao
    abstract fun usageDao(): UsageDao
    abstract fun skillDao(): SkillDao
    abstract fun modifierDao(): ModifierDao
    abstract fun dailyGoalDao(): DailyGoalDao
    abstract fun dailyActivityDao(): DailyActivityDao
}