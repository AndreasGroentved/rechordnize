package com.andreasgroentved.rechordnize.db

import androidx.room.*
import com.andreasgroentved.rechordnize.model.level.LevelIdModifier



@Dao
interface ModifierDao {

    @Query("SELECT * FROM levelidmodifier WHERE modifier IN (:modifiers)")
    fun getLevelsWithModifiers(modifiers: Array<Int>): List<LevelIdModifier>

    @Query("SELECT * FROM levelidmodifier WHERE id = :levelId")
    fun getLevelModifiers(levelId: String): List<LevelIdModifier>

    @Query("DELETE FROM levelidmodifier WHERE id = :id")
    fun deleteModifierById(id: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertModifiers(modifiers: Array<LevelIdModifier>)

    @Update
    fun updateModifier(vararg modifier: LevelIdModifier)

    @Delete
    fun deleteModifier(vararg modifier: LevelIdModifier)
}