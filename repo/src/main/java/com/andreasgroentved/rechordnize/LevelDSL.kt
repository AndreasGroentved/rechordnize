package com.andreasgroentved.rechordnize

class LevelDSL {

    class SimpleLevel(var playables: List<SimplePlayable>, val difficulty: Int)

    data class SimplePlayable(val name: String, val extra: String)


    class LevelBuilder {

        fun levelGroup(difficulty: Int, levelPlayables: List<List<SimplePlayable>>) =
            levelPlayables.map { SimpleLevel(it, difficulty) }


        val openGroupMaj1 = listOf(play("A maj", "open"), play("D maj", "open"), play("E maj", "open"))
        val openGroupMin1 = listOf(play("A min", "open"), play("D min", "open"), play("E min", "open"))
        val openGroupMaj2 = listOf(play("C maj", "open"), play("F maj", "open"), play("G maj", "open"))
        val openGroup71 = listOf(play("C 7", "open"), play("F 7", "open"), play("G 7", "open"))
        val openGroup72 = listOf(play("A 7", "open"), play("B 7", "open"), play("E 7", "open"))
        val openGroup7Min = listOf(play("A min7", "open"), play("D min7", "open"), play("E min7", "open"))
        val openGroup7Maj = listOf(play("A maj7", "open"), play("C maj7", "open"), play("F maj7", "open"))
        val openGroupSus1 = listOf(play("A sus2", "open"), play("A sus4", "open"), play("C sus2", "open"))
        val openGroupSus2 = listOf(play("D sus2", "open"), play("D sus4", "open"), play("G sus2", "open"))
        val openGroup7All = (openGroup71 + openGroup72).distinct()
        val openGroupMajAll = (openGroupMaj1 + openGroupMaj2).distinct()
        val openGroup7MajMinAll = (openGroup7Maj + openGroup7Min).distinct()
        val openGroupTriadAll = (openGroupMaj1 + openGroupMaj2 + openGroupMin1).distinct()
        val openGroupSusAll = (openGroupSus1 + openGroupSus2).distinct()
        val allOpenChords = (openGroup7All + openGroup7MajMinAll + openGroupTriadAll + openGroupSusAll).distinct()


        val powerGroupT1 = listOf(play("A 5", "power"), play("B 5", "power"), play("C 5", "power"))
        val powerGroupT2 = listOf(play("D 5", "power"), play("E 5", "power"), play("F 5", "power"))
        val powerGroupT3 = listOf(play("Ab 5", "power"), play("Bb 5", "power"), play("Db 5", "power"))
        val powerGroupT4 = listOf(play("Eb 5", "power"), play("Gb 5", "power"), play("G 5", "power"))
        val powerGroupQ1 = powerGroupT1 + powerGroupT2
        val powerGroupQ2 = powerGroupT3 + powerGroupT4
        val powerAll = powerGroupQ1 + powerGroupQ2

        val barreMaj1 = listOf(
            play("A maj", "chord_any"), play("B maj", "chord_any"), play("C maj", "chord_any"),
            play("D maj", "chord_any"), play("E maj", "chord_any"), play("F maj", "chord_any")
        )
        val barreMaj2 = listOf(
            play("Ab maj", "chord_any"), play("Bb maj", "chord_any"), play("Db maj", "chord_any"),
            play("Eb maj", "chord_any"), play("Gb maj", "chord_any"), play("G maj", "chord_any")
        )
        val barreMajAll = barreMaj1 + barreMaj2

        val barreMin1 = listOf(
            play("A min", "chord_any"), play("B min", "chord_any"), play("C min", "chord_any"),
            play("D min", "chord_any"), play("E min", "chord_any"), play("F min", "chord_any")
        )
        val barreMin2 = listOf(
            play("Ab min", "chord_any"), play("Bb min", "chord_any"), play("Db min", "chord_any"),
            play("Eb min", "chord_any"), play("Gb min", "chord_any"), play("G min", "chord_any")
        )
        val barreMinAll = barreMin1 + barreMin2
        val barre71 = listOf(
            play("A 7", "chord_any"), play("B 7", "chord_any"), play("C 7", "chord_any"),
            play("D 7", "chord_any"), play("E 7", "chord_any"), play("F 7", "chord_any")
        )
        val barre72 = listOf(
            play("Ab 7", "chord_any"), play("Bb 7", "chord_any"), play("Db 7", "chord_any"),
            play("Eb 7", "chord_any"), play("Gb 7", "chord_any"), play("G 7", "chord_any")
        )
        val barre7All = barre71 + barre72
        val barreMin71 = listOf(
            play("A min7", "chord_any"), play("B min7", "chord_any"), play("C min7", "chord_any"),
            play("D min7", "chord_any"), play("E min7", "chord_any"), play("F min7", "chord_any")
        )
        val barreMin72 = listOf(
            play("Ab min7", "chord_any"), play("Bb min7", "chord_any"), play("Db min7", "chord_any"),
            play("Eb min7", "chord_any"), play("Gb min7", "chord_any"), play("G min7", "chord_any")
        )
        val barreMin7All = barreMin71 + barreMin72
        val barreMaj71 = listOf(
            play("A maj7", "chord_any"), play("B maj7", "chord_any"), play("C maj7", "chord_any"),
            play("D maj7", "chord_any"), play("E maj7", "chord_any"), play("F maj7", "chord_any")
        )
        val barreMaj72 = listOf(
            play("Ab maj7", "chord_any"), play("Bb maj7", "chord_any"), play("Db maj7", "chord_any"),
            play("Eb maj7", "chord_any"), play("Gb maj7", "chord_any"), play("G maj7", "chord_any")
        )
        val barreMaj7All = barreMaj71 + barreMaj72
        val barreAll = barre7All + barreMaj7All + barreMin7All + barreMinAll + barreMajAll


        val intervalD1 = listOf(play("P1", "interval"), play("m2", "interval"))
        val intervalD2 = listOf(play("M2", "interval"), play("m3", "interval"))
        val intervalD3 = listOf(play("M3", "interval"), play("P4", "interval"))
        val intervalD4 = listOf(play("A4", "interval"), play("P5", "interval"))
        val intervalD5 = listOf(play("m6", "interval"), play("M6", "interval"))
        val intervalD6 = listOf(play("m7", "interval"), play("M7", "interval"))

        val intervalT1 = listOf(play("P1", "interval"), play("m2", "interval"), play("M2", "interval"))
        val intervalT2 = listOf(play("m3", "interval"), play("M3", "interval"), play("P4", "interval"))
        val intervalT3 = listOf(play("A4", "interval"), play("P5", "interval"), play("m6", "interval"))
        val intervalT4 = listOf(play("M6", "interval"), play("m7", "interval"), play("M7", "interval"))

        val intervalT5 = listOf(play("P8", "interval"), play("m9", "interval"), play("M9", "interval"))
        val intervalT6 = listOf(play("m10", "interval"), play("M10", "interval"), play("P11", "interval"))
        val intervalT7 = listOf(play("A11", "interval"), play("P12", "interval"), play("m13", "interval"))
        val intervalT8 = listOf(play("M13", "interval"), play("m14", "interval"), play("M14", "interval"))

        val intervalQ1 = (intervalD1 + intervalD2).distinct()
        val intervalQ2 = (intervalD3 + intervalD4).distinct()
        val intervalQ3 = (intervalD5 + intervalD6).distinct()

        val intervalS1 = (intervalT1 + intervalT2).distinct()
        val intervalS2 = (intervalT3 + intervalT4).distinct()
        val intervalS3 = (intervalT5 + intervalT6).distinct()
        val intervalS4 = (intervalT7 + intervalT8).distinct()

        val intervalSS1 = (intervalS1 + intervalS2).distinct()
        val intervalSS2 = (intervalS3 + intervalS4).distinct()

        val intervalAll = (intervalSS1 + intervalSS2).distinct()


        val progressionD1 = listOf(play("I maj", "progression"), play("IV maj", "progression"), play("V maj", "progression"))
        val progressionD2 = listOf(play("I maj", "progression"), play("II min", "progression"), play("III min", "progression"))
        val progressionD3 = listOf(play("I maj", "progression"), play("VI min", "progression"), play("VII dim", "progression"))

        val progressionT1 = listOf(
            play("I maj", "progression"),
            play("IV maj", "progression"),
            play("V maj", "progression"),
            play("II min", "progression")
        )
        val progressionT2 = listOf(
            play("I maj", "progression"),
            play("VI min", "progression"),
            play("VII dim", "progression"),
            play("III min", "progression")
        )


        val progressionQ1 = (progressionD1 + progressionD2).distinct()
        val progressionQ2 = (progressionD2 + progressionD3).distinct()
        val progressionAllStandard = (progressionT1 + progressionT2).distinct()


        val progressionV7T1 = listOf(
            play("I maj7", "progression"),
            play("IV maj7", "progression"),
            play("V maj7", "progression"),
            play("II min7", "progression")
        )
        val progressionV7T2 = listOf(
            play("I maj7", "progression"),
            play("VI min7", "progression"),
            play("VII dim7", "progression"),
            play("III min7", "progression")
        )

        val progressionV7Q1 = progressionV7T1 + play("VI min7", "progression")
        val progressionV7Q2 = progressionV7T2 + play("II min7", "progression")
        val progressionV7Q1Standard = progressionV7Q1 + progressionAllStandard
        val progressionV7Q2Standard = progressionV7Q2 + progressionAllStandard


        val progressionV7All = (progressionV7T1 + progressionV7T2 + play("VII dim7", "progression")).distinct()
        val progressionV7AllStandard = (progressionV7T1 + progressionV7T2 + play("VII dim7", "progression") + progressionAllStandard).distinct()

        val progressionOtherT1 = listOf(
            play("bVI 7", "progression"),
            play("bII dim", "progression"),
            play("bV 7", "progression")
        )
        val progressionOtherT2 = listOf(
            play("II 7", "progression"),
            play("bV min7", "progression"),
            play("bVI min7", "progression")
        )
        val progressionOtherT3 = listOf(
            play("II dim", "progression"),
            play("bIII maj7", "progression"),
            play("V 7", "progression")
        )

        val progressionOtherT4 = listOf(
            play("bV maj7", "progression"),
            play("bIII dim", "progression"),
            play("bII maj7", "progression")
        )

        val progressionOtherT5 = listOf(
            play("bIII min7", "progression"),
            play("bII 7", "progression"),
            play("VI 7", "progression")
        )

        val progressionOtherStandardOtherT1 = progressionAllStandard + progressionOtherT1
        val progressionOtherStandardOtherT2 = progressionAllStandard + progressionOtherT2
        val progressionOtherStandardOtherT3 = progressionAllStandard + progressionOtherT3
        val progressionOtherStandardOtherT4 = progressionAllStandard + progressionOtherT4
        val progressionOtherStandardOtherT5 = progressionAllStandard + progressionOtherT5

        val progressionOtherStandardOtherD1 = (progressionAllStandard + progressionOtherT1 + progressionOtherT2).distinct()
        val progressionOtherStandardOtherD2 = (progressionAllStandard + progressionOtherT3 + progressionOtherT4).distinct()
        val progressionOtherStandardOtherD3 = (progressionAllStandard + progressionOtherT2 + progressionOtherT5).distinct()

        val progressionOtherStandardOther7T1 = progressionAllStandard + progressionOtherT1 + progressionV7All
        val progressionOtherStandardOther7T2 = progressionAllStandard + progressionOtherT2 + progressionV7All
        val progressionOtherStandardOther7T3 = progressionAllStandard + progressionOtherT3 + progressionV7All
        val progressionOtherStandardOther7T4 = progressionAllStandard + progressionOtherT4 + progressionV7All
        val progressionOtherStandardOther7T5 = progressionAllStandard + progressionOtherT5 + progressionV7All

        val progressionOtherStandardOther7D1 = (progressionAllStandard + progressionOtherT1 + progressionOtherT2 + progressionV7All).distinct()
        val progressionOtherStandardOther7D2 = (progressionAllStandard + progressionOtherT3 + progressionOtherT4 + progressionV7All).distinct()
        val progressionOtherStandardOther7D3 = (progressionAllStandard + progressionOtherT2 + progressionOtherT5 + progressionV7All).distinct()

        val progressionAll = (progressionOtherStandardOther7D1 + progressionOtherStandardOther7D2 + progressionOtherStandardOther7D3).distinct()


        val typeD1 = listOf(play("maj", "chord_type"), play("min", "chord_type"))
        val typeD2 = listOf(play("dim", "chord_type"), play("7", "chord_type"))
        val typeD3 = listOf(play("maj7", "chord_type"), play("min7", "chord_type"))
        val typeD4 = listOf(play("dim7", "chord_type"), play("b5", "chord_type"))
        val typeD5 = listOf(play("sus2", "chord_type"), play("sus4", "chord_type"))
        val typeD6 = listOf(play("sus2sus4", "chord_type"), play("5", "chord_type"))
        val typeD7 = listOf(play("6", "chord_type"), play("6/9", "chord_type"))
        val typeD8 = listOf(play("min#5", "chord_type"), play("aug", "chord_type"))
        val typeD9 = listOf(play("min6", "chord_type"), play("7b5", "chord_type"))
        val typeD10 = listOf(play("min7b5", "chord_type"), play("aug7", "chord_type"))
        val typeD11 = listOf(play("minmaj7", "chord_type"), play("9", "chord_type"))
        val typeD12 = listOf(play("min9", "chord_type"), play("11", "chord_type"))
        val typeD13 = listOf(play("11", "chord_type"), play("min11", "chord_type"))
        val typeD14 = listOf(play("13", "chord_type"), play("min13", "chord_type"))
        val typeD15 = listOf(play("maj9", "chord_type"), play("maj11", "chord_type"))
        val typeD16 = listOf(play("maj13", "chord_type"), play("min6/9", "chord_type"))


        val typeQ1 = (typeD1 + typeD2).distinct()
        val typeQ2 = (typeD3 + typeD4).distinct()
        val typeQ3 = (typeD5 + typeD6).distinct()
        val typeQ4 = (typeD7 + typeD8).distinct()
        val typeQ5 = (typeD9 + typeD10).distinct()
        val typeQ6 = (typeD11 + typeD12).distinct()
        val typeQ7 = (typeD13 + typeD14).distinct()
        val typeQ8 = (typeD15 + typeD16).distinct()


        val typeO1 = (typeQ1 + typeQ2).distinct()
        val typeO2 = (typeQ3 + typeQ4).distinct()
        val typeO3 = (typeQ5 + typeQ6).distinct()
        val typeO4 = (typeQ7 + typeQ8).distinct()


        val typeH1 = (typeO1 + typeO2).distinct()
        val typeH2 = (typeO3 + typeO4).distinct()
        val typeAll = (typeH1 + typeH2).distinct()
    }

    companion object {
        fun play(name: String, extra: String) = SimplePlayable(name, extra)
    }
}