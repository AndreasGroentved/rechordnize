package com.andreasgroentved.rechordnize.pref

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.playable.getSimpleString
import com.andreasgroentved.rechordnize.model.util.DateUtil

class AppPreferencesHelper(context: Context) : PrefHelper /*TODO liveData //TODO flere filer eller bedre opdeling*/ {

    override fun addExp(exp: Long) = setExp(getExp() + exp)

    private val otherPref: SharedPreferences //TODO inject
    private val chordInARowPref: SharedPreferences
    private val daysInARowPref: SharedPreferences
    private val levelUnlockedPref: SharedPreferences
    private val settingsScreenPref: SharedPreferences

    private val getDefaultLastChecked = DateUtil.getDayBefore(DateUtil.getToday())

    init {
        settingsScreenPref = PreferenceManager.getDefaultSharedPreferences(context)
        chordInARowPref = context.getSharedPreferences(IN_A_ROW_PREF, Context.MODE_PRIVATE)
        daysInARowPref = context.getSharedPreferences(DAYS_IN_A_ROW_PREF, Context.MODE_PRIVATE)
        levelUnlockedPref = context.getSharedPreferences(UNLOCKED_LEVELS, Context.MODE_PRIVATE)
        otherPref = context.getSharedPreferences(LEARN_PREF, Context.MODE_PRIVATE)
    }

    override fun getTotalDays(): Long = daysInARowPref.getLong(PREF_TOTAL_DAYS, 0)
    override fun setTotalDays(totalDays: Long) = daysInARowPref.edit().putLong(PREF_TOTAL_DAYS, totalDays).apply()
    override fun getNumOfChordsInARow(): Int = chordInARowPref.getInt(PREF_CHORD_NUM_IN_A_ROW, DEFAULT_IN_A_ROW)
    override fun addChordsInARow(correct: Boolean): Unit = chordInARowPref.edit().putInt(PREF_IS_FIRST_TIME, if (correct) getNumOfChordsInARow() else 0).apply()
    override fun getIsFirstTime(): Boolean = otherPref.getBoolean(PREF_IS_FIRST_TIME, true)
    override fun setIsFirstTime(firstTime: Boolean): Unit = otherPref.edit().putBoolean(PREF_IS_FIRST_TIME, firstTime).apply()
    override fun getExp(): Long = otherPref.getLong(PREF_EXP, 0L)
    override fun setExp(exp: Long): Unit = otherPref.edit().putLong(PREF_EXP, exp).apply()
    override fun getSoundType(): String = settingsScreenPref.getString(PREF_SOUND, "guitar")!!
    override fun setToken(token: String) = otherPref.edit().putString(TOKEN, token).apply()
    override fun getToken(): String? = otherPref.getString(TOKEN, null)
    override fun getUnlockedNumberOfLevels(playableExtra: PlayableExtra): Int = levelUnlockedPref.getInt(playableExtra.getSimpleString(), 1)
    override fun increaseNumberOfUnlockedLevels(playableExtra: PlayableExtra) = levelUnlockedPref.edit().putInt(playableExtra.getSimpleString(), getUnlockedNumberOfLevels(playableExtra) + 1).apply()
    override fun setNumOfUnlockedLevels(playableExtra: PlayableExtra, number: Int) = levelUnlockedPref.edit().putInt(playableExtra.getSimpleString(), number).apply()
    override fun getHasPremium(): Boolean = otherPref.getBoolean(PREF_PREMIUM, DEFAULT_PREMIUM) //TODO en pref eller mange //TODO premiumpref
    override fun setHasPremium(hasPremium: Boolean) = otherPref.edit().putBoolean(PREF_PREMIUM, hasPremium).apply()
    override fun getStartDate(): Long = otherPref.getLong(PREF_START_DATE, -1L)
    override fun setStartDate(startDate: Long) = otherPref.edit().putLong(PREF_START_DATE, startDate).apply()
    override fun getLevelCompletions(): Long = otherPref.getLong(PREF_LEVEL_COMPLETIONS, 0L)
    override fun setLevelCompletions(levelCompletions: Long) = otherPref.edit().putLong(PREF_LEVEL_COMPLETIONS, levelCompletions).apply()
    override fun setUsageGoal(usageGoal: Int) = settingsScreenPref.edit().putString(PREF_USAGE_GOAL, usageGoal.toString()).apply()
    override fun getUsageGoal(): Int = settingsScreenPref.getString(PREF_USAGE_GOAL, DEFAULT_USAGE_GOAL)!!.toInt()
    override fun getDaysInARow(): Int = daysInARowPref.getInt(PREF_LAST_DAYS_IN_A_ROW, DEFAULT_IN_A_ROW)
    override fun getLastChecked(): String = daysInARowPref.getString(PREF_LAST_CHECKED, getDefaultLastChecked)!!
    override fun setLastChecked(lastChecked: String) = daysInARowPref.edit().putString(PREF_LAST_CHECKED, lastChecked).apply()
    override fun setDaysInARow(inARow: Int, lastChecked: String) =
        daysInARowPref.edit()
            .putInt(PREF_LAST_DAYS_IN_A_ROW, inARow)
            .putString(PREF_LAST_CHECKED, lastChecked).apply()

    companion object {
        @JvmStatic
        private val PREF_IS_FIRST_TIME = "FIRST_TIME"
        @JvmStatic
        private val LEARN_PREF = "CHORD"
        @JvmStatic
        private val IN_A_ROW_PREF = "INAROW"
        @JvmStatic
        private val DEFAULT_IN_A_ROW = 0
        @JvmStatic
        private val PREF_CHORD_NUM_IN_A_ROW = "CHORD_NUM_IN_A_ROW"
        @JvmStatic
        private val DAYS_IN_A_ROW_PREF = "DAYS"
        @JvmStatic
        private val PREF_LAST_CHECKED = "LAST_CHECKED"
        @JvmStatic
        private val PREF_LAST_DAYS_IN_A_ROW = "LAST_IN_A_ROW"
        @JvmStatic
        private val PREF_TOTAL_DAYS = "TOTAL_DAYS"
        @JvmStatic
        private val UNLOCKED_LEVELS = "UNLOCKED"
        @JvmStatic
        private val PREF_USAGE_GOAL = "USAGE_GOAL"
        @JvmStatic
        private val DEFAULT_USAGE_GOAL = "5"
        @JvmStatic
        private val PREF_EXP = "EXP"
        @JvmStatic
        private val PREF_LEVEL_COMPLETIONS = "LEVEL_COMPLETIONS"
        @JvmStatic
        private val PREF_START_DATE = "START_DATE"
        @JvmStatic
        private val PREF_SOUND = "PREF_SOUND"
        @JvmStatic
        private val PREF_PREMIUM = "PREMIUM"
        @JvmStatic
        private val DEFAULT_PREMIUM = false
        @JvmStatic
        private val TOKEN = "TOKEN"
    }
}