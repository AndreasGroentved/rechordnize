package com.andreasgroentved.rechordnize.pref

import com.andreasgroentved.rechordnize.model.playable.PlayableExtra

interface PrefHelper {
    fun getIsFirstTime(): Boolean
    fun setIsFirstTime(firstTime: Boolean)
    fun getNumOfChordsInARow(): Int
    fun addChordsInARow(correct: Boolean)
    fun getLastChecked(): String
    fun setLastChecked(lastChecked: String)
    fun setDaysInARow(inARow: Int, lastChecked: String)
    fun getDaysInARow(): Int
    fun setUsageGoal(usageGoal: Int)
    fun getUsageGoal(): Int
    fun getHasPremium(): Boolean
    fun setHasPremium(hasPremium: Boolean)
    fun getTotalDays(): Long
    fun setTotalDays(totalDays: Long)
    fun getUnlockedNumberOfLevels(playableExtra: PlayableExtra): Int
    fun increaseNumberOfUnlockedLevels(playableExtra: PlayableExtra)
    fun setNumOfUnlockedLevels(playableExtra: PlayableExtra, number: Int)
    fun getSoundType(): String
    fun getExp(): Long
    fun setExp(exp: Long)
    fun addExp(exp: Long)
    fun getStartDate(): Long
    fun setStartDate(startDate: Long)
    fun getLevelCompletions(): Long
    fun setLevelCompletions(levelCompletions: Long)
    fun setToken(token: String)
    fun getToken(): String?
}