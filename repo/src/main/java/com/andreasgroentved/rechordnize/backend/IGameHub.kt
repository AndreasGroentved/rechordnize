package com.andreasgroentved.rechordnize.backend

import androidx.annotation.WorkerThread
import com.andreasgroentved.rechordnize.backend.response.Response
import kotlinx.coroutines.channels.Channel

interface IGameHub {
    @WorkerThread
    fun start(): Boolean

    fun stop()
    fun joinOrCreateGame(levelIdentifierEnum: LevelIdentifierEnum)
    fun next(gameId: String, roundNumber: Int)
    fun guess(gameId: String, guessInfo: GuessInfo)
    fun ready(gameId: String)
    fun unReady(gameId: String)
    var hubObserver: Channel<Response>
    var connectionObserver: Channel<String>
}
