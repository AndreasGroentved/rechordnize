package com.andreasgroentved.rechordnize.backend

import java.util.*

data class GameState(var StartTime: Date = Date(), var Playables: List<String> = emptyList(), val Rounds: List<ServerRound> = emptyList())


data class ServerRound(val Number: Int = -1, val PlayableNames: List<String> = emptyList())