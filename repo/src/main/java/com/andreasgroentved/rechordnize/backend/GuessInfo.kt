package com.andreasgroentved.rechordnize.backend

data class GuessInfo(val GuessList: List<String> = emptyList(), val PlayerId: String = "-1", val RoundNumber: Int = -1, val Time: Int = -1)