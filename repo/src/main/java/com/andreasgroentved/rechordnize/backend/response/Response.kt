package com.andreasgroentved.rechordnize.backend.response

import com.andreasgroentved.rechordnize.backend.GamePlayer
import com.andreasgroentved.rechordnize.backend.UnStartedGame

sealed class Response {

    data class GameJoinedResponse(val GameId: String = "Client invalid", val UserId: String = "error", val UserIds: List<String> = emptyList(), val Playables: List<String> = emptyList()) : Response()
    data class ConnectedResponse(val ConnectionId: String = "ClientError") : Response()
    data class UnStartedGamesResponse(var UnStartedGames: List<UnStartedGame> = emptyList()) : Response()
    data class GameCreatedResponse(val GameId: String = "Client error") : Response()
    data class GameStartedResponse(val GameId: String = "Client error") : Response()
    data class GameIsStartingResponse(val GameId: String = "Client error") : Response()
    data class GameCancelStartResponse(val GameId: String = "Client error") : Response()
    data class NextResponse(val RoundNumber: Int = -1, val CorrectList: List<String> = emptyList()) : Response()
    data class GuessResponse(val UserId: String = "error", val Correct: Boolean = false) : Response()
    data class GameEndResponse(val UserIdToScoreDictionary: Map<String, Int> = emptyMap(), val GameId: String = "error") : Response()
    data class GamePlayerListResponse(val PlayerNameList: List<GamePlayer> = emptyList()) : Response()
    data class ErrorResponse(val Message: String) : Response()
}