package com.andreasgroentved.rechordnize.backend

import java.io.Serializable

data class GamePlayer(val Name: String = "", val Score: Int = -1, val State: PlayerState = PlayerState.Disconnected) : Serializable