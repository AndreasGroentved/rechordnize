package com.andreasgroentved.rechordnize.backend

import com.google.gson.annotations.SerializedName


enum class LevelIdentifierEnum(val identifier: Int) {

    @SerializedName("0")
    EasyChordNames(0),
    @SerializedName("1")
    MediumChordNames(1),
    @SerializedName("2")
    HardChordNames(2),
    @SerializedName("3")
    EasyChordTypes(3),
    @SerializedName("4")
    MediumChordTypes(4),
    @SerializedName("5")
    HardChordTypes(5),
    @SerializedName("6")
    EasyChordProgressions(6),
    @SerializedName("7")
    MediumChordProgressions(7),
    @SerializedName("8")
    HardChordProgressions(8),
    @SerializedName("9")
    EasyIntervals(9),
    @SerializedName("10")
    MediumIntervals(10),
    @SerializedName("11")
    HardIntervals(11),
    @SerializedName("12")
    EasyEverything(12),
    @SerializedName("13")
    MediumEverything(13),
    @SerializedName("14")
    HardEverything(14);


    companion object {

        private val map = values().associateBy(LevelIdentifierEnum::identifier)
        fun fromInt(type: Int): LevelIdentifierEnum = map[type]!!
    }
}