package com.andreasgroentved.rechordnize.backend

import com.google.gson.annotations.SerializedName

enum class PlayerState(val intVal: Int) {
    @SerializedName("0")
    NotReady(0),
    @SerializedName("1")
    Disconnected(1),
    @SerializedName("2")
    Ready(2),
    @SerializedName("3")
    InGame(3);
}