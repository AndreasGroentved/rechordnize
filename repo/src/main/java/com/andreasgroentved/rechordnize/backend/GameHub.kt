package com.andreasgroentved.rechordnize.backend

import androidx.annotation.WorkerThread
import com.andreasgroentved.rechordnize.backend.response.Response
import com.google.gson.Gson
import com.microsoft.signalr.HubConnection
import com.microsoft.signalr.HubConnectionBuilder
import com.microsoft.signalr.HubConnectionState
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.sendBlocking
import timber.log.Timber


open class GameHub(private val address: String, private val gson: Gson) : IGameHub {

    private var hubConnection: HubConnection? = null

    override var hubObserver: Channel<Response> = Channel()
    override var connectionObserver: Channel<String> = Channel()

    @WorkerThread
    override fun start(): Boolean {
        try {
            hubConnection = HubConnectionBuilder.create(address)
                    .build()
            if (hubConnection == null) return false
            hubConnection!!.on("connected", {
                val res = gson.fromJson(it, Response.ConnectedResponse::class.java)
                println(res)
                hubObserver.sendBlocking(res)
            }, String::class.java)
            hubConnection!!.start().doOnError {
                println(it.message)
                println("error")
            }.blockingAwait()
            setUpListeners(hubConnection!!, gson)
            return true
        } catch (e: Exception) {
            //TODO
            return false
        }

    }

    private fun setUpListeners(hubConnection: HubConnection, gson: Gson) {
        hubConnection.on("JoinGame", {
            println(it)
            println("JOIN")
            val res = gson.fromJson(it, Response.GameJoinedResponse::class.java)
            println(res)
            Timber.d(res.toString())
            hubObserver.sendBlocking(res)
        }, String::class.java)

        hubConnection.on("availableGames", {
            //TODO capitalize()
            println(it)
            val res = gson.fromJson(it, Response.UnStartedGamesResponse::class.java)
            Timber.d(res.toString())
            hubObserver.sendBlocking(res)
        }, String::class.java)


        hubConnection.on("PlayerMap", {
            println(it)
            val res = gson.fromJson(it, Response.GamePlayerListResponse::class.java)
            Timber.d(res.toString())
            hubObserver.sendBlocking(res)
        }, String::class.java)

        hubConnection.on("Next", {
            println(it)
            val res = gson.fromJson(it, Response.NextResponse::class.java)
            Timber.d(res.toString())
            hubObserver.sendBlocking(res)
        }, String::class.java)


        hubConnection.on("Guess", {
            println(it)
            val res = gson.fromJson(it, Response.GuessResponse::class.java)
            Timber.d(res.toString())
            hubObserver.sendBlocking(res)
        }, String::class.java)


        hubConnection.on("GameEnded", {
            println(it)
            val res = gson.fromJson(it, Response.GameEndResponse::class.java)
            Timber.d(res.toString())
            hubObserver.sendBlocking(res)
        }, String::class.java)

        hubConnection.on("StartGame", {
            println(it)
            val res = gson.fromJson(it, Response.GameStartedResponse::class.java)
            Timber.d(res.toString())
            hubObserver.sendBlocking(res)
        }, String::class.java)

        hubConnection.on("GameCancelStart", {
            println(it)
            val res = gson.fromJson(it, Response.GameCancelStartResponse::class.java)
            Timber.d(res.toString())
            hubObserver.sendBlocking(res)
        }, String::class.java)

        hubConnection.on("GameIsStarting", {
            println(it)
            val res = gson.fromJson(it, Response.GameIsStartingResponse::class.java)
            Timber.d(res.toString())
            hubObserver.sendBlocking(res)
        }, String::class.java)


        hubConnection.on("CreateGame", {
            println(it)
            val res = gson.fromJson(it, Response.GameCreatedResponse::class.java)
            Timber.d(res.toString())
            hubObserver.sendBlocking(res)
        }, String::class.java)

        hubConnection.on("Error", {
            println(it)
            val res = gson.fromJson(it, Response.ErrorResponse::class.java)
            Timber.d(res.toString())
            hubObserver.sendBlocking(res)
        }, String::class.java)
    }

    fun isAvailable() = hubConnection?.connectionState == HubConnectionState.CONNECTED

    private fun alertError(message: String = "") {
        connectionObserver.sendBlocking(
                when {
                    message.isNotEmpty() -> message
                    hubConnection == null -> "HubConnection is null"
                    else -> "Connection is dead"
                }
        )
    }


    override fun joinOrCreateGame(levelIdentifierEnum: LevelIdentifierEnum) = send {
        hubConnection?.send("JoinCreateGame", levelIdentifierEnum.identifier)
    }

    override fun next(gameId: String, roundNumber: Int) = send { hubConnection?.send("Next", gameId, roundNumber) }

    override fun guess(gameId: String, guessInfo: GuessInfo): Unit = send { hubConnection?.send("Guess", gameId, gson.toJson(guessInfo)) }


    private val send: (block: () -> (Unit)) -> Unit = {
        if (!isAvailable()) alertError()
        else {
            try {
                it.invoke()
            } catch (e: Exception) {
                alertError(e.message.toString())
            }
        }
    }

    override fun ready(gameId: String): Unit = send { hubConnection?.send("Ready", gameId) ?: Unit }
    override fun unReady(gameId: String): Unit = send {
        hubConnection?.send("UnReady", gameId)
    }

    override fun stop() {
        hubConnection?.stop()?.blockingAwait()
        hubConnection = null
    }
}