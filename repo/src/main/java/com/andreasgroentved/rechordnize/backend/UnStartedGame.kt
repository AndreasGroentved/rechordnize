package com.andreasgroentved.rechordnize.backend

data class UnStartedGame(var IdentifierEnum: LevelIdentifierEnum = LevelIdentifierEnum.EasyIntervals, var Id: String = "")