package com.andreasgroentved.rechordnize.backend

import com.andreasgroentved.rechordnize.model.DailyGoal
import retrofit2.Call
import retrofit2.http.GET


interface DailyGoalService {

    @GET("/api/dailygoal/")
    fun getTodayGoal(): Call<DailyGoal>
}