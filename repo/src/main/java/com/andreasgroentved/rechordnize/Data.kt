package com.andreasgroentved.rechordnize

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.andreasgroentved.rechordnize.backend.DailyGoalService
import com.andreasgroentved.rechordnize.db.*
import com.andreasgroentved.rechordnize.model.DailyGoal
import com.andreasgroentved.rechordnize.model.level.LevelData
import com.andreasgroentved.rechordnize.model.level.LevelIdModifier
import com.andreasgroentved.rechordnize.model.level.LevelSelectionHolder
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.playable.PlayableNameWithExtra
import com.andreasgroentved.rechordnize.model.skill.DailySkill
import com.andreasgroentved.rechordnize.model.skill.Skill
import com.andreasgroentved.rechordnize.model.skill.SkillType
import com.andreasgroentved.rechordnize.model.usage.Usage
import com.andreasgroentved.rechordnize.pref.PrefHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import retrofit2.Call
import javax.inject.Inject
import javax.inject.Singleton


interface IData {
    val playableDao: PlayableDao
    val levelDao: LevelDao
    val usageDao: UsageDao
    val modifierDao: ModifierDao
    val skillDao: SkillDao
    val dailyActivityDao: DailyActivityDao
    val dailyGoalDao: DailyGoalDao
    fun getTodayGoal(): Call<DailyGoal>
    suspend fun getDailySkill(date: String, name: SkillType): DailySkill?
    fun getDailySkills(date: String): LiveData<List<DailySkill>>
    fun getDailySkillSync(date: String, name: SkillType): DailySkill?
    fun updateDailySkill(dailySkill: DailySkill)

    @WorkerThread
    suspend fun getDailyGoalSync(date: String): DailyGoal?

    fun getAllInitialLevels(): List<LevelData>
    fun getLevelsWithDifficulties(diff: IntArray): LiveData<List<LevelData>>
    fun loadLevelIDsWithDifficulties(difficulties: Array<Int>): LiveData<List<LevelSelectionHolder>>
    fun getDailyGoalCompletions(): LiveData<Int>
    fun updateGoal(dailyGoal: DailyGoal)
    fun insertDailyGoal(dailyGoal: DailyGoal)
    fun getLevelsWithDifficultiesCount(diff: IntArray): Int
    fun loadChords(chordNames: Array<String>): List<Playable>
    fun getLevelCount(): LiveData<Int>
    fun getSimplePlayables(playableNames: Array<String>): LiveData<List<PlayableNameWithExtra>>
    fun getUnlockedNumberOfLevels(playableExtra: PlayableExtra): Int
    fun increaseNumberOfUnlockedLevels(playableExtra: PlayableExtra)
    fun setNumOfUnlockedLevels(playableExtra: PlayableExtra, number: Int)
    fun loadIntervals(): LiveData<List<Playable>>
    fun loadProgressions(): LiveData<List<Playable>>
    fun loadTypes(): LiveData<List<Playable>>
    fun getLevel(levelId: String): LiveData<LevelData>
    fun getChord(chordName: String): LiveData<Playable>
    fun getAllChords(): LiveData<List<Playable>>
    fun setIsFirstTime(isFirstTime: Boolean): Unit
    fun getIsFirstTime(): Boolean
    fun deleteLevel(levelId: String): Boolean
    fun createInitialChords(chords: List<Playable>)
    fun updateLevelData(levelData: LevelData): Unit
    fun insertLevelDatas(levelData: List<LevelData>): Unit
    fun insertLevelData(levelData: LevelData): Unit
    fun updateChord(chord: Playable)
    fun updatePlayables(playable: List<Playable>)
    fun updateUsage(usage: Usage): Unit
    fun getUsage(date: String): Usage?
    fun getUsageFromTo(fromDate: String, toDate: String): LiveData<List<Usage>>
    fun getLastChecked(): String
    fun setDaysInARow(inARow: Int, lastChecked: String)
    fun getDaysInARow(): Int
    fun getUsageGoal(): Int
    fun getLevelModifiers(levelId: String): List<LevelIdModifier>
    fun deleteModifierById(id: String)
    fun insertModifiers(modifiers: Array<LevelIdModifier>)
    fun getLevelSync(id: String): LevelData?
    fun updateSkill(skill: Skill)
    fun getSkill(name: SkillType): LiveData<Skill>
    fun getSkills(): LiveData<List<Skill>>
    fun setLastChecked(lastChecked: String)
    fun insertSkill(skill: Skill): Unit
    fun getSkillSync(name: SkillType): Skill
    fun getTotalDays(): Long
    fun setTotalDays(totalDays: Long)
    fun getSoundType(): String
    fun getExp(): Long
    fun setExp(exp: Long)
    fun addExp(exp: Long)
    fun getStartDate(): Long
    fun setStartDate(startDate: Long)
    fun getLevelCompletions(): Long
    fun setLevelCompletions(levelCompletions: Long)
    fun setToken(token: String)
    fun getToken(): String?
}

@Singleton
class Data @Inject constructor(database: PlayableDatabase, private val pref: PrefHelper, private val dailyGoalService: DailyGoalService) : IData {

    override val playableDao: PlayableDao = database.playableDao()
    override val levelDao: LevelDao = database.levelDao()
    override val usageDao: UsageDao = database.usageDao()
    override val modifierDao: ModifierDao = database.modifierDao()
    override val skillDao: SkillDao = database.skillDao()
    override val dailyActivityDao = database.dailyActivityDao()
    override val dailyGoalDao = database.dailyGoalDao()

    init {
        (GlobalScope + Dispatchers.IO).launch {
            /* println(usageDao.getAllUsage())
             val sub = usageDao.getAllUsageSuspend()
             sub(sub)
             usageDao.updateUsage(Usage(Random.nextLong(), "saddsa"))
             Thread.sleep(1000)
             usageDao.updateUsage(Usage(Random.nextLong(), "saddsa"))
             usageDao.updateUsage(Usage(Random.nextLong(), "saddsa"))
             usageDao.updateUsage(Usage(Random.nextLong(), "saddsa"))*/
        }
    }


    override fun getTodayGoal(): Call<DailyGoal> = dailyGoalService.getTodayGoal()
    override suspend fun getDailySkill(date: String, name: SkillType): DailySkill? = dailyActivityDao.getDailySkill(date, name)
    override fun getDailySkills(date: String): LiveData<List<DailySkill>> = dailyActivityDao.getDailySkills(date)
    override fun getDailySkillSync(date: String, name: SkillType): DailySkill? = dailyActivityDao.getDailySkillSync(date, name)
    override fun updateDailySkill(dailySkill: DailySkill) = dailyActivityDao.insertDailySkill(dailySkill)
    @WorkerThread
    override suspend fun getDailyGoalSync(date: String): DailyGoal? = dailyGoalDao.getDailyGoalSync(date)

    override fun getAllInitialLevels(): List<LevelData> = LevelCreator().getAllLevels()
    override fun getLevelsWithDifficulties(diff: IntArray): LiveData<List<LevelData>> = levelDao.loadLevelsWithDifficulties(diff.toTypedArray())
    override fun loadLevelIDsWithDifficulties(difficulties: Array<Int>): LiveData<List<LevelSelectionHolder>> = levelDao.loadLevelIDsWithDifficulties(difficulties)
    override fun getDailyGoalCompletions(): LiveData<Int> = dailyGoalDao.getDailyCompletions()
    override fun updateGoal(dailyGoal: DailyGoal) = dailyGoalDao.updateGoal(dailyGoal)
    override fun insertDailyGoal(dailyGoal: DailyGoal) = dailyGoalDao.insertDailyGoal(dailyGoal)
    override fun getLevelsWithDifficultiesCount(diff: IntArray): Int = levelDao.loadLevelsWithDifficultiesCount(diff.toTypedArray())
    override fun loadChords(chordNames: Array<String>): List<Playable> = playableDao.loadChordsByNamesRaw(chordNames)
    override fun getLevelCount(): LiveData<Int> = levelDao.getLevelCount()
    override fun getSimplePlayables(playableNames: Array<String>): LiveData<List<PlayableNameWithExtra>> = playableDao.loadSimplePlayables(playableNames)
    override fun getUnlockedNumberOfLevels(playableExtra: PlayableExtra): Int = pref.getUnlockedNumberOfLevels(playableExtra)
    override fun increaseNumberOfUnlockedLevels(playableExtra: PlayableExtra) = pref.increaseNumberOfUnlockedLevels(playableExtra)
    override fun setNumOfUnlockedLevels(playableExtra: PlayableExtra, number: Int) = pref.setNumOfUnlockedLevels(playableExtra, number)
    override fun loadIntervals(): LiveData<List<Playable>> = playableDao.loadIntervals()
    override fun loadProgressions(): LiveData<List<Playable>> = playableDao.loadProgressions()
    override fun loadTypes(): LiveData<List<Playable>> = playableDao.loadTypes()
    override fun getLevel(levelId: String): LiveData<LevelData> = levelDao.loadLevel(levelId)
    override fun getChord(chordName: String): LiveData<Playable> = playableDao.loadChord(chordName)
    override fun getAllChords(): LiveData<List<Playable>> = playableDao.loadChords()
    override fun setIsFirstTime(isFirstTime: Boolean): Unit = pref.setIsFirstTime(isFirstTime)
    override fun getIsFirstTime(): Boolean = pref.getIsFirstTime()
    override fun deleteLevel(levelId: String): Boolean = levelDao.deleteLevelData(levelId) > 0
    override fun createInitialChords(chords: List<Playable>) = playableDao.createPlayable(chords)
    override fun updateLevelData(levelData: LevelData): Unit = levelDao.updateLevelData(levelData)
    override fun insertLevelDatas(levelData: List<LevelData>): Unit = levelDao.insertLevelDatas(levelData)
    override fun insertLevelData(levelData: LevelData): Unit = levelDao.insertLevelData(levelData)
    override fun updateChord(chord: Playable) = playableDao.updatePlayable(chord)
    override fun updatePlayables(playable: List<Playable>) = playableDao.updatePlayables(playable)
    override fun updateUsage(usage: Usage): Unit = usageDao.updateUsage(usage)
    override fun getUsage(date: String): Usage? = usageDao.getUsage(date)
    override fun getUsageFromTo(fromDate: String, toDate: String): LiveData<List<Usage>> = usageDao.getUsageFromTo(fromDate, toDate)
    override fun getLastChecked(): String = pref.getLastChecked()
    override fun setLastChecked(lastChecked: String) = pref.setLastChecked(lastChecked)
    override fun setDaysInARow(inARow: Int, lastChecked: String) = pref.setDaysInARow(inARow, lastChecked)
    override fun getDaysInARow(): Int = pref.getDaysInARow()
    override fun getUsageGoal(): Int = pref.getUsageGoal()
    override fun getLevelModifiers(levelId: String): List<LevelIdModifier> = modifierDao.getLevelModifiers(levelId)
    override fun deleteModifierById(id: String) = modifierDao.deleteModifierById(id)
    override fun insertModifiers(modifiers: Array<LevelIdModifier>) = modifierDao.insertModifiers(modifiers)
    override fun getLevelSync(id: String): LevelData? = levelDao.getLevelSync(id)
    override fun updateSkill(skill: Skill) = skillDao.updateSkill(skill)
    override fun getSkill(name: SkillType): LiveData<Skill> = skillDao.getSkill(name)
    override fun getSkills(): LiveData<List<Skill>> = skillDao.getSkills()
    override fun insertSkill(skill: Skill): Unit = skillDao.insertSkill(skill)
    override fun getSkillSync(name: SkillType): Skill = skillDao.getSkillSync(name)
    override fun getTotalDays(): Long = pref.getTotalDays()
    override fun setTotalDays(totalDays: Long) = pref.setTotalDays(totalDays)
    override fun getSoundType(): String = pref.getSoundType()
    override fun getExp(): Long = pref.getExp()
    override fun setExp(exp: Long) = pref.setExp(exp)
    override fun addExp(exp: Long) = pref.addExp(exp)
    override fun getStartDate(): Long = pref.getStartDate()
    override fun setStartDate(startDate: Long) = pref.setStartDate(startDate)
    override fun getLevelCompletions(): Long = pref.getLevelCompletions()
    override fun setLevelCompletions(levelCompletions: Long) = pref.setLevelCompletions(levelCompletions)
    override fun setToken(token: String) = pref.setToken(token)
    override fun getToken(): String? = pref.getToken()
}
