package com.andreasgroentved.rechordnize.model.level

import androidx.room.*
import com.andreasgroentved.rechordnize.model.playable.Playable

import java.io.Serializable


@Entity(indices = [Index("id"), Index("levelDifficulty")])
data class LevelData constructor(
    @PrimaryKey var id: String = "-1",
    var playables: List<Playable> = mutableListOf(),
    @Embedded var levelHistory: LevelHistory = LevelHistory(), var levelDifficulty: LevelDifficulty = LevelDifficulty.OPEN_CHORDS, var stars: Int = 0, var plays: Int = 0
) : Serializable {

    @Ignore
    var levelModifiers: MutableList<LevelModifier> = mutableListOf()

    override fun equals(other: Any?): Boolean = when {
        other == null -> false
        other !is LevelData -> false
        this.id != other.id -> false
        this.levelDifficulty != other.levelDifficulty -> false
        else -> true
    }

    override fun hashCode(): Int = id.hashCode()
}