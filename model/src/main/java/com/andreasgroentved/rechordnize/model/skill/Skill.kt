package com.andreasgroentved.rechordnize.model.skill

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey



@Entity(indices = [Index("name")])
data class  Skill constructor(@PrimaryKey var name: SkillType = SkillType.ERROR, var best: Long = 0, var current: Long = 0)