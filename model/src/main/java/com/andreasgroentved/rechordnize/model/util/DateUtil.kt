package com.andreasgroentved.rechordnize.model.util

import org.joda.time.Period
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.*


object DateUtil {


    fun getTimeBetweenString(from: Long, to: Long): String { //TODO --> brug string resorces
        val p = Period(from, to)
        val returnString: String
        var count: Long = 1
        when {
            p.years > 10 -> returnString = "never played"
            p.years > 0 -> {
                returnString = ">${p.years} year"
                count = p.years.toLong()
            }
            p.months > 0 -> {
                returnString = ">${p.months} month"
                count = p.months.toLong()
            }
            p.weeks > 0 -> {
                returnString = ">${p.weeks} week"
                count = p.weeks.toLong()
            }
            p.days > 0 -> {
                returnString = "${p.days} day"
                count = p.days.toLong()
            }
            p.hours > 0 -> {
                returnString = ">${p.hours} hour"
                count = p.hours.toLong()
            }
            p.minutes > 0 -> {
                returnString = ">${p.minutes} min"
                count = p.minutes.toLong()
            }
            else -> {
                returnString = ">${p.seconds} sec"
                count = p.seconds.toLong()
            }
        }
        return if (count > 1) "${returnString}s" else returnString
    }

    fun getDayOfTheWeek(date: Date): Int {
        val calendar = getInstance() //TODO gem instans?
        calendar.time = date
        calendar.firstDayOfWeek = MONDAY
        return weekIntToInt(calendar.get(DAY_OF_WEEK))
    }

    private fun weekIntToInt(weekday: Int) = when (weekday) {
        MONDAY -> 1
        TUESDAY -> 2
        WEDNESDAY -> 3
        THURSDAY -> 4
        FRIDAY -> 5
        SATURDAY -> 6
        SUNDAY -> 7
        else -> throw IllegalArgumentException("Invalid weekday int")
    }

    fun getDayOfTheMonth(date: Date): Int {
        val calendar = getInstance().apply { time = date; firstDayOfWeek = MONDAY } //TODO gem instans?
        return calendar.get(DAY_OF_MONTH)
    }

    fun getToday(): String {
        return DateUtil.dateFormat.format(Date())
    }


    fun getDayAfter(dateString: String): String {
        val date = dateFormat.parse(dateString)
        val calendar = getInstance().apply { time = date; add(DATE, 1) }
        return dateFormat.format(calendar.time)
    }

    fun getDayBefore(dateString: String): String {
        val date = dateFormat.parse(dateString)
        val calendar = getInstance().apply { time = date; add(DATE, -1) }
        return dateFormat.format(calendar.time)
    }

    private fun midnight(calendar: Calendar) {
        calendar.apply { set(MILLISECOND, 0); set(SECOND, 0); set(HOUR_OF_DAY, 0); set(MINUTE, 0) }
    }


    fun getMonthAndYear(date: Date): Pair<Int, Int> {
        val calendar = Calendar.getInstance()
        calendar.time = date
        return Pair(calendar.get(MONTH), calendar.get(YEAR))
    }

    fun getStartAndEndOfMonth(month: Int, year: Int): Pair<String, String> {
        val calendar = getInstance()
        calendar.set(year, month, 1)
        midnight(calendar)
        val start = calendar.time.toDayDate()
        val daysOfMonth = calendar.getActualMaximum(DAY_OF_MONTH)
        calendar.set(year, month, daysOfMonth)
        midnight(calendar)
        return Pair(start, calendar.time.toDayDate())
    }

    fun getStartAndEndOfWeek(time: Long): Pair<String, String> {
        val calendar = getInstance().apply {
            timeInMillis = time; firstDayOfWeek = MONDAY; set(DAY_OF_WEEK, MONDAY)
        } //TODO gem instans?

        midnight(calendar)
        val start = calendar.time.toDayDate()
        calendar.add(DATE, 6)
        calendar.set(MILLISECOND, 0)
        midnight(calendar)
        return Pair(start, calendar.time.toDayDate())
    }

    fun getDaysOfMonth(time: Long): Int {
        val calendar = getInstance()
        calendar.timeInMillis = time
        return calendar.getActualMaximum(DAY_OF_MONTH)
    }


    fun getDurationString(duration: Long): String = when {
        duration < 1_000f -> "$duration ms"
        duration < 60_000f -> "${duration / 1000} sec"
        duration < 3_600_000f -> "${duration / 60000} min"
        duration < 86_400_000f -> "${duration / 3600000} hr"
        else -> (duration / (3_600_000 * 24)).let { if (it > 1) "$it days" else "$it day" }
    }

    fun startEndOfMonth(time: Long): Pair<String, String> {
        val calendar = getInstance().apply { timeInMillis = time }
        return getStartAndEndOfMonth(calendar.get(MONTH), calendar.get(YEAR))
    }

    var dateFormat = SimpleDateFormat("yyyyMMdd")
}

fun Date.toDayDate() = DateUtil.dateFormat.format(this)!!
fun String.toDate() = DateUtil.dateFormat.parse(this)
fun String.toDayDate() = DateUtil.dateFormat.parse(this).toDayDate()

