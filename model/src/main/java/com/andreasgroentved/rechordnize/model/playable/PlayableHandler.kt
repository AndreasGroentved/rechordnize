package com.andreasgroentved.rechordnize.model.playable

fun Playable.addGuess(time: Long, wasCorrect: Boolean) { //TODO
    performances.add(PlayData(name, time, wasCorrect))
    if (performances.size > 100) performances.removeAt(0)
    plays += 1
    inARow = if (wasCorrect) inARow + 1 else 0
    correct += if (wasCorrect) 1 else 0
    lastPlayed = System.currentTimeMillis()
}



