package com.andreasgroentved.rechordnize.model.skill


enum class SkillType(val s: Int) {
    CUSTOM_LEVEL_MADE(0),
    DAYS_IN_A_ROW(1),
    NUM_OF_CORRECT(2),
    NUM_OF_GUESSES(3),
    LONGEST_PRACTISE(4),
    LEVEL_1_LEVELS_COMPLETED(5),
    LEVEL_2_LEVELS_COMPLETED(6),
    LEVEL_3_LEVELS_COMPLETED(7),
    LEVELS_COMPLETED(8), //Bare et niveau
    INTERVAL_MASTER(9),
    PROGRESSION_MASTER(10),
    TYPE_MASTER(11),
    NAME_CHORD_MASTER(12),
    ERROR(13),
    TOTAL_DAYS(14),
    TOTAL_PRACTISE_TIME(15),
    CORRECT_IN_A_ROW(16);

    companion object {
        private val map = values().associateBy(SkillType::s)
        fun fromInt(type: Int) = map[type]!!
        fun getStringName(skillType: SkillType): String = when (skillType) { //TODO temp names
            CUSTOM_LEVEL_MADE -> "Custom creation"
            DAYS_IN_A_ROW -> "Days in a row"
            NUM_OF_CORRECT -> "Truth finder"
            NUM_OF_GUESSES -> "I tried"
            LONGEST_PRACTISE -> "Longest practise"
            LEVEL_1_LEVELS_COMPLETED -> "Beat normal"
            LEVEL_2_LEVELS_COMPLETED -> "Nothing is difficult"
            LEVEL_3_LEVELS_COMPLETED -> "Extreme progress"
            LEVELS_COMPLETED -> "Level completer"
            INTERVAL_MASTER -> "Long intervals"
            PROGRESSION_MASTER -> "Progressive"
            TYPE_MASTER -> "Typical"
            NAME_CHORD_MASTER -> "Chord named"
            ERROR -> "ERROR_MODIFIER ERROR_MODIFIER ERROR_MODIFIER"
            TOTAL_DAYS -> "Days practised"
            TOTAL_PRACTISE_TIME -> "Practise time"
            CORRECT_IN_A_ROW -> "In a row"
        }



        fun getInfoText(skillType: SkillType): String = when (skillType) { //TODO DETTE ER PLACEHOLDER, SKAL NOK FINDES I RESOURCEFILER!!!
            CUSTOM_LEVEL_MADE -> "Custom levels made"
            DAYS_IN_A_ROW -> "Days practised in a row"
            NUM_OF_CORRECT -> "Total number of correct guesses"
            NUM_OF_GUESSES -> "Total number of guesses"
            LONGEST_PRACTISE -> "Longest time practised"
            LEVEL_1_LEVELS_COMPLETED -> "Normal difficulty levels completed"
            LEVEL_2_LEVELS_COMPLETED -> "Challenging difficulty levels completed"
            LEVEL_3_LEVELS_COMPLETED -> "Extreme difficulty levels completed"
            LEVELS_COMPLETED -> "Any level on any difficulty completed (except custom levels)"
            INTERVAL_MASTER -> "Interval levels completed"
            PROGRESSION_MASTER -> "Chord progression levels completed"
            TYPE_MASTER -> "Chord type levels completed"
            NAME_CHORD_MASTER -> "Chord name levels completed"
            ERROR -> "ERROR_MODIFIER ERROR_MODIFIER ERROR_MODIFIER"
            TOTAL_DAYS -> "Total days practised"
            TOTAL_PRACTISE_TIME -> "Total practise time"
            CORRECT_IN_A_ROW -> "Correct in a row"
        }

    }
}