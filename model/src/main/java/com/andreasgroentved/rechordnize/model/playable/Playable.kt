package com.andreasgroentved.rechordnize.model.playable

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra.CHORD_ANY
import java.io.Serializable


@Entity(indices = [(Index("name"))])
data class Playable(@field:PrimaryKey var name: String = "error", var plays: Int = 0, var correct: Int = 0, var inARow: Int = 0, var exp: Long = 0, var lastPlayed: Long = -1, var performances: MutableList<PlayData> = mutableListOf(), var extra: PlayableExtra = CHORD_ANY) : Serializable {

    override fun equals(other: Any?): Boolean = when {
        other == null -> false
        other !is Playable -> false
        other.name.equals(name, true) -> true
        else -> false
    }


    override fun hashCode() = name.hashCode()
}
