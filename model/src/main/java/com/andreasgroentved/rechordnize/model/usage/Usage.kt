package com.andreasgroentved.rechordnize.model.usage

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey


@Entity(indices = [Index("date")])
data class Usage constructor(var time: Long = -1, @PrimaryKey var date: String = "error") //"yyyyMMdd"