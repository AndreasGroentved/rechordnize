package com.andreasgroentved.rechordnize.model.playable

import com.andreasgroentved.rechordnize.model.playable.PlayableExtra.*
import java.io.Serializable

enum class PlayableExtra(val ext: Int) : Serializable {
    /*Chord*/
    CHORD(0),
    OPEN(1),
    POWER(2),
    POWER_E_SHAPE(3),
    POWER_A_SHAPE(4),
    CHORD_ANY(5),
    BARRE(6),
    BARRE_E_SHAPE(7),
    BARRE_A_SHAPE(8),
    BARRE_D_SHAPE(17),
    BARRE_C_SHAPE(15),
    /*Interval*/
    INTERVAL(9),
    /*Progression*/
    CHORD_PROGRESSION(13),
    /*Type*/
    CHORD_TYPE(14),
    INVALID(16),
    ANY(18);

    companion object {
        private val map = values().associateBy(PlayableExtra::ext)
        fun fromInt(type: Int): PlayableExtra = map[type] ?: error("Invalid int value")
        fun getString(l: PlayableExtra): String = when (l) {
            CHORD -> "chord"
            OPEN -> "open"
            CHORD_ANY -> "chord_any"
            POWER -> "power"
            POWER_E_SHAPE -> "power_e_shape"
            POWER_A_SHAPE -> "power_a_shape"
            BARRE -> "barre"
            BARRE_E_SHAPE -> "barre_e_shape"
            BARRE_A_SHAPE -> "barre_a_shape"
            INTERVAL -> "interval"
            CHORD_PROGRESSION -> "progression"
            CHORD_TYPE -> "chord_type"
            BARRE_C_SHAPE -> "barre_c_shape"
            INVALID -> TODO()
            BARRE_D_SHAPE -> "barre_d_shape"
            ANY -> "any"
        }

        fun getChordGroup() = listOf(CHORD, OPEN, CHORD_ANY, POWER_E_SHAPE, POWER_A_SHAPE, BARRE_E_SHAPE, BARRE_A_SHAPE, BARRE_D_SHAPE, BARRE_C_SHAPE, POWER)

        fun getExtra(extra: String): PlayableExtra = when (extra) {
            "chord" -> CHORD
            "open" -> OPEN
            "chord_any" -> CHORD_ANY
            "power" -> POWER
            "power_e_shape" -> POWER_E_SHAPE
            "power_a_shape" -> POWER_A_SHAPE
            "barre" -> BARRE
            "barre_e_shape" -> BARRE_E_SHAPE
            "barre_a_shape" -> BARRE_A_SHAPE
            "interval" -> INTERVAL
            "progression" -> CHORD_PROGRESSION
            "chord_type" -> CHORD_TYPE
            "barre_c_shape" -> BARRE_C_SHAPE
            "barre_d_shape" -> BARRE_D_SHAPE
            else -> TODO()
        }
    }
}

fun PlayableExtra.getSimpleString(): String = when (this) {
    CHORD, CHORD_ANY, POWER, POWER_A_SHAPE, POWER_E_SHAPE, BARRE_A_SHAPE, BARRE_C_SHAPE, BARRE_D_SHAPE, BARRE_E_SHAPE, OPEN -> "Chord"
    CHORD_TYPE -> "Type"
    CHORD_PROGRESSION -> "Progression"
    INTERVAL -> "Interval"
    else -> "Invalid"
}