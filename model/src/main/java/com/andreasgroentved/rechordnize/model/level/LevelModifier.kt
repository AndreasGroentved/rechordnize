package com.andreasgroentved.rechordnize.model.level


import com.google.gson.annotations.SerializedName
import java.io.Serializable



enum class LevelModifier(val mod: Int) : Serializable {
    @SerializedName("0")
    STATIC_ROOT(0),
    @SerializedName("1")
    INVERSIONS(1),
    @SerializedName("2")
    ASCENDING(2),
    @SerializedName("3")
    DESCENDING(3),
    @SerializedName("4")
    CHORD_NOTES_INDIVIDUALLY(4),
    @SerializedName("5")
    NONE(5),
    @SerializedName("6")
    LENGTH_1(6),
    @SerializedName("7")
    LENGTH_2(7),
    @SerializedName("8")
    LENGTH_3(8),
    @SerializedName("9")
    LENGTH_4(9),
    @SerializedName("10")
    LENGTH_5(10),
    @SerializedName("11")
    LENGTH_6(11),
    @SerializedName("12")
    LENGTH_7(12),
    @SerializedName("13")
    REPEAT_PLAYS_NEW(13),
    @SerializedName("14")
    PLAY_TOGETHER(14),
    @SerializedName("15")
    C_SCALE(15),
    @SerializedName("16")
    LEVEL_1(16),
    @SerializedName("17")
    LEVEL_2(17),
    @SerializedName("18")
    LEVEL_3(18),
    @SerializedName("19")
    CUSTOM(19),
    @SerializedName("20")
    PROGRESSION_ROOT_FIRST(20),
    @SerializedName("21")
    ANY_FIRST_PROGRESSION_CHORD(21),
    @SerializedName("22")
    RANDOM_ROOT(22),
    @SerializedName("23")
    ERROR_MODIFIER(23),
    @SerializedName("24")
    MULTI_PLAYER(24);

    companion object {
        private val map = values().associateBy(LevelModifier::mod)
        fun fromInt(type: Int) = map[type] ?: error("Invalid int value")
        fun lengthMatcher(l: LevelModifier) = when (l) {
            LENGTH_1 -> 1
            LENGTH_2 -> 2
            LENGTH_3 -> 3
            LENGTH_4 -> 4
            LENGTH_5 -> 5
            LENGTH_6 -> 6
            LENGTH_7 -> 7
            else -> 1
        }

        fun getDescriptionText(l: LevelModifier) = when (l) { //TODO returner resource ider
            STATIC_ROOT -> "The root note is the same for all questions"
            INVERSIONS -> "The root note might not be the first note played"
            ASCENDING -> "Intervals are ascending"
            CHORD_NOTES_INDIVIDUALLY -> "Notes in the chord are played one at a time"
            NONE -> "Error"
            LENGTH_1 -> "Every question is a sequence of 1 question"
            LENGTH_2 -> "Every question is a sequence of 2 questions"
            LENGTH_3 -> "Every question is a sequence of 3 questions"
            LENGTH_4 -> "Every question is a sequence of 4 questions"
            LENGTH_5 -> "Every question is a sequence of 5 questions"
            LENGTH_6 -> "Every question is a sequence of 6 questions"
            LENGTH_7 -> "Every question is a sequence of 7 questions"
            REPEAT_PLAYS_NEW -> "The replay button gives a new question"
            PLAY_TOGETHER -> "All notes in the chord are played together"
            C_SCALE -> "The c major scale is the starting point"
            LEVEL_1 -> "Normal difficulty"
            LEVEL_2 -> "Challenging difficulty"
            LEVEL_3 -> "Extreme difficulty"
            CUSTOM -> "You made this exp"
            PROGRESSION_ROOT_FIRST -> "The I chord can be used as the first chord in the progression"
            ANY_FIRST_PROGRESSION_CHORD -> "Any I chord can be used as the first chord in the progression"
            DESCENDING -> "Intervals are descending"
            RANDOM_ROOT -> "Intervals are random places on the guitar"
            ERROR_MODIFIER -> "Error"
            MULTI_PLAYER -> "Multiplayer"
        }
    }
}