package com.andreasgroentved.rechordnize.model.playable

import androidx.room.Ignore

data class PlayableNameWithExtra(var name: String = "", var exp: Long = -1, @Ignore var boolean: Boolean = false, @Ignore var expGained: Long = 0) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as PlayableNameWithExtra
        if (name != other.name) return false
        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}