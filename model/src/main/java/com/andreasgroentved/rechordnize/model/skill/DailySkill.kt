package com.andreasgroentved.rechordnize.model.skill

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(indices = [Index("name"), Index("date")])
data class DailySkill constructor(var name: SkillType = SkillType.ERROR, var value: Long = 0, @PrimaryKey var date: String)