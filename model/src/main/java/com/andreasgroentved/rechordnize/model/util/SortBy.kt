package com.andreasgroentved.rechordnize.model.util


enum class SortBy(val sort: String) {
    NAME("Name"),
    ACCURACY("Accuracy"),
    LEVEL("Level"),
    STREAK("Streak"),
    TIME("Time"),
    LAST_PLAYED("Last played"),
    CORRECT("Correct"),
    PLAYS("Plays");

    companion object {
        private val map = values().associateBy(SortBy::sort)
        fun fromString(type: String): SortBy = map[type] ?: NAME

        fun getString(sortBy: SortBy): String = when (sortBy) {
            NAME -> "Name"
            ACCURACY -> "Accuracy"
            LEVEL -> "Level"
            STREAK -> "Streak"
            TIME -> "Time"
            LAST_PLAYED -> "Last played"
            CORRECT -> "Correct"
            PLAYS -> "Plays"
        }
    }
}