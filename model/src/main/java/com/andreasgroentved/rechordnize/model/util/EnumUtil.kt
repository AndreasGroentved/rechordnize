package com.andreasgroentved.rechordnize.model.util

import com.andreasgroentved.rechordnize.model.level.LevelDifficulty
import com.andreasgroentved.rechordnize.model.level.LevelDifficulty.*
import com.andreasgroentved.rechordnize.model.level.LevelModifier
import com.andreasgroentved.rechordnize.model.level.SimpleLevelType
import com.andreasgroentved.rechordnize.model.level.SimpleLevelType.*
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra.*
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra.INTERVAL
import com.andreasgroentved.rechordnize.model.skill.SkillType

object EnumUtil {



    fun getLevelTypeString(levelDifficulty: LevelDifficulty): String = when (levelDifficulty) {
        in LevelDifficulty.getProgressionLevels() -> "chord progression"
        in LevelDifficulty.getIntervalLevels() -> "interval"
        in LevelDifficulty.getChordLevels() -> "chord"
        in LevelDifficulty.getTypeLevels() -> "chord type"
        MULTI -> "In this level"
        else -> "error"
    }

    fun getLevelDifNum(levelModifiers: List<LevelModifier>) = when {
        levelModifiers.contains(LevelModifier.LEVEL_1) -> LevelModifier.LEVEL_1
        levelModifiers.contains(LevelModifier.LEVEL_2) -> LevelModifier.LEVEL_2
        levelModifiers.contains(LevelModifier.LEVEL_3) -> LevelModifier.LEVEL_3
        else -> LevelModifier.ERROR_MODIFIER
    }


    fun getLevelSkillTypeModifier(levelModifiers: List<LevelModifier>) = when {
        levelModifiers.contains(LevelModifier.LEVEL_1) -> SkillType.LEVEL_1_LEVELS_COMPLETED
        levelModifiers.contains(LevelModifier.LEVEL_2) -> SkillType.LEVEL_2_LEVELS_COMPLETED
        levelModifiers.contains(LevelModifier.LEVEL_3) -> SkillType.LEVEL_3_LEVELS_COMPLETED
        else -> SkillType.ERROR
    }


    fun getExtraGroupFromSimpleLevelType(simpleLevelType: SimpleLevelType): List<PlayableExtra> = when (simpleLevelType) {
        NAME -> PlayableExtra.getChordGroup()
        TYPE -> listOf(CHORD_TYPE)
        PROGRESSION -> listOf(CHORD_PROGRESSION)
        SimpleLevelType.INTERVAL -> listOf(INTERVAL)
        SimpleLevelType.ANY -> listOf(PlayableExtra.ANY)
    }


    fun getLevelTypeSkillType(levelDifficulty: LevelDifficulty) = when (levelDifficulty) {
        in LevelDifficulty.getProgressionLevels() -> SkillType.PROGRESSION_MASTER
        in LevelDifficulty.getIntervalLevels() -> SkillType.INTERVAL_MASTER
        in LevelDifficulty.getChordLevels() -> SkillType.NAME_CHORD_MASTER
        in LevelDifficulty.getTypeLevels() -> SkillType.TYPE_MASTER
        else -> SkillType.ERROR
    }

}

fun PlayableExtra.toSimpleLevelType() = when (this) {
    in PlayableExtra.getChordGroup() -> NAME
    INTERVAL -> SimpleLevelType.INTERVAL
    CHORD_PROGRESSION -> PROGRESSION
    CHORD_TYPE -> TYPE
    else -> NAME
}

fun LevelDifficulty.toPlayableExtra(): PlayableExtra = when (this) { //TODO figurerer nok et par steder
    CUSTOM_CHORDS, OPEN_CHORDS, POWER_CHORDS, BARRE_CHORDS, ALL_CHORDS -> CHORD
    INTERVAL_EASY, INTERVAL_MEDIUM, INTERVAL_HARD, INTERVAL_THE_END, CUSTOM_INTERVAL -> INTERVAL
    CHORD_PROGRESSION_EASY, CHORD_PROGRESSION_MEDIUM, CHORD_PROGRESSION_HARD, CUSTOM_PROGRESSION -> CHORD_PROGRESSION
    CHORD_TYPE_EASY, CHORD_TYPE_MEDIUM, CHORD_TYPE_HARD, CUSTOM_CHORD_TYPE -> CHORD_TYPE
    else -> CHORD
}
