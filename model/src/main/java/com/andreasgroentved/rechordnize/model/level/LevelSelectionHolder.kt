package com.andreasgroentved.rechordnize.model.level

//TODO navn?
data class LevelSelectionHolder(val id: String = "-1", val stars: Int = 0, val levelDifficulty: Int = -1)