package com.andreasgroentved.rechordnize.model.util


fun Long.normalize(min: Long, max: Long) =
    ((this - min).toFloat() / (max - min))


fun inRange(low: Float, high: Float, value: Float): Boolean = value < high && value > low
