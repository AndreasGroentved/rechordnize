package com.andreasgroentved.rechordnize.model.level

import androidx.room.Entity
import androidx.room.Index
import java.io.Serializable


@Entity(indices = [(Index("modifier"))], primaryKeys = ["id", "modifier"])
data class LevelIdModifier(var id: String = "-1", var modifier: Int = -1) : Serializable