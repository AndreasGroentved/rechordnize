package com.andreasgroentved.rechordnize.model.util


import com.andreasgroentved.rechordnize.model.level.LevelDifficulty
import com.andreasgroentved.rechordnize.model.level.LevelPlay

import com.andreasgroentved.rechordnize.model.playable.PlayData
import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra.*
import com.andreasgroentved.rechordnize.model.util.SortBy.*
import java.lang.System.currentTimeMillis
import java.text.DecimalFormat
import kotlin.math.min


object PlayableUtil {

    fun removeDuplicate(open: List<PlayableExtra>, shapes: List<PlayableExtra>, playableName: String): List<PlayableExtra> { //TODO viewmodel
        if (open.isEmpty()) return shapes
        if (shapes.isEmpty()) return open
        val extension = getChordExtension(playableName)
        val chordDictionary = ChordDictionary()
        val openShape = chordDictionary.openChordMap[playableName] ?: return shapes
        return open + shapes.filter {
            val other = chordDictionary.chordShapeMap[extension]?.get(it) ?: return@filter false
            openShape != other
        }
    }

    //TODO kig på performance, når denne klasse kan skrives i uden performance problemer....
    fun getType(name: String, chordDictionary: ChordDictionary) = when (name.split(" ").first()) {
        in chordDictionary.nameToDistanceMap.keys -> INTERVAL
        in chordDictionary.romanNumMap.keys -> CHORD_PROGRESSION
        in chordDictionary.chordShapeMap.keys -> CHORD_TYPE
        in chordDictionary.order.map { it.capitalize() } -> CHORD
        else -> INVALID
    }



    fun getChordBase(name: String): String = name.split(" ")[0]

    //TODO håndtere forskellige typer af playable - akkorder, intervaller osv.
    fun getChordExtension(name: String): String { //TODO giv playable med
        val nameSplit = name.split(" ")
        return if (nameSplit.size == 2) nameSplit[1] else ""
    }




    fun getAverageTime(chordPlays: List<PlayData>?, count: Int): Double {
        if (chordPlays == null || chordPlays.isEmpty()) return 1000000.0 //Arbitrær høj værdi - til sammenligning af levelPlays
        val sum: Int = chordPlays.sumBy { it.time.toInt() }
        val size = chordPlays.size
        return if (size == 0) -1.0 else if (sum == 0) 0.0 else (sum.toDouble() / count)
    }

    fun getAcc(chordPlays: List<PlayData>): Float {
        val sum = chordPlays.sumBy { if (it.correct) 1 else 0 }
        val size = chordPlays.size
        return if (size == 0) -1f else if (sum == 0) 0f else sum.toFloat() / size
    }

    fun getAccString(chordPlays: List<PlayData>): String {
        val acc: Float = getAcc(chordPlays)
        val df = DecimalFormat("#.#")
        return if (acc < 0) "n/a" else "${df.format(acc * 100)}%"
    }

    fun getAverageTimeString(chordPlays: List<PlayData>, count: Int): String {
        val time: Double = getAverageTime(chordPlays, count)
        return getTimeString(time)
    }

    private fun getTimeString(time: Double): String {
        val df = DecimalFormat("#.#")
        return if (time < 0 || time == 1000000.0) "n/a" else "${df.format(time / 1000)}s"
    }

    fun getValue(sortBy: SortBy, playable: Playable): String = when (sortBy) {
        NAME -> ""
        ACCURACY -> getAccString(playable.performances)
        LEVEL -> ExpUtil.getLevelFromExp(playable.exp).toString()
        STREAK -> playable.inARow.toString()
        TIME -> getAverageTimeString(playable.performances, playable.performances.size)
        LAST_PLAYED -> DateUtil.getTimeBetweenString(playable.lastPlayed, currentTimeMillis())
        CORRECT -> playable.correct.toString()
        PLAYS -> playable.plays.toString()
    }

    fun averageTimeToGrade(correct: Boolean, time: Long): Int = (when {
        time == -1L -> 0
        time <= 5000L -> 5
        time <= 7500L -> 4
        time <= 10000L -> 3
        time <= 20000L -> 2
        else -> 1
    } * if (correct) 1 else 0)


    fun getAverageTimeText(levelPlay: List<PlayData>, count: Int) = if (levelPlay.isEmpty()) "n/a" else getAverageTimeString(levelPlay, count)

    fun grade(levelPlay: LevelPlay?, count: Int) = gradeVal(levelPlay, count)


    fun isFirstLevelPlayBetterThanSecond(levelPlayFirst: LevelPlay?, levelPlaySecond: LevelPlay?, count: Int) = when {
        levelPlayFirst === levelPlaySecond -> false //Dobbelt null
        levelPlayFirst == null -> false
        levelPlaySecond == null -> true
        getAverageTime(levelPlayFirst.plays, count).toLong() < getAverageTime(levelPlaySecond.plays, count).toLong() -> true
        else -> {
            firstMoreCorrectThanSecond(levelPlayFirst, levelPlaySecond)
        }
    }

    private fun firstMoreCorrectThanSecond(levelPlayFirst: LevelPlay, levelPlaySecond: LevelPlay) =
            levelPlayFirst.run { plays.sumBy { if (it.correct) 1 else 0 } } > levelPlaySecond.run {
                plays.sumBy { if (it.correct) 1 else 0 }
            }


    fun correctnessGrade(levelPlay: LevelPlay) =
            (levelPlay.plays.filter { it.correct }.size.toFloat() / levelPlay.guessCount).let { correctRatio ->
                when {
                    correctRatio > 0.96 -> 5
                    correctRatio > 0.799 -> 4
                    correctRatio > 0.699 -> 3
                    correctRatio > 0.499 -> 2
                    correctRatio > 0.299 -> 1
                    else -> 0
                }
            }


    fun gradeVal(levelPlay: LevelPlay?, count: Int) = when {
        levelPlay == null -> 0
        levelPlay.plays.isEmpty() || levelPlay.guessCount == 0 -> 0
        else -> {
            val averageTimeGrade = averageTimeToGrade(true, getAverageTime(levelPlay.plays, count).toLong())
            val correctness = correctnessGrade(levelPlay)
            min(averageTimeGrade, correctness)
        }
    }

    fun getPlayableType(diffs: IntArray): Int { //TODO ryk
        return when (diffs.first()) {
            LevelDifficulty.CUSTOM_CHORDS.intVal, LevelDifficulty.OPEN_CHORDS.intVal, LevelDifficulty.POWER_CHORDS.intVal, LevelDifficulty.BARRE_CHORDS.intVal, LevelDifficulty.ALL_CHORDS.intVal -> CHORD.ext
            LevelDifficulty.INTERVAL_EASY.intVal, LevelDifficulty.INTERVAL_MEDIUM.intVal, LevelDifficulty.INTERVAL_HARD.intVal, LevelDifficulty.INTERVAL_THE_END.intVal, LevelDifficulty.CUSTOM_INTERVAL.intVal -> INTERVAL.ext
            LevelDifficulty.CHORD_PROGRESSION_EASY.intVal, LevelDifficulty.CHORD_PROGRESSION_MEDIUM.intVal, LevelDifficulty.CHORD_PROGRESSION_HARD.intVal, LevelDifficulty.CUSTOM_PROGRESSION.intVal -> CHORD_PROGRESSION.ext
            LevelDifficulty.CHORD_TYPE_EASY.intVal, LevelDifficulty.CHORD_TYPE_MEDIUM.intVal, LevelDifficulty.CHORD_TYPE_HARD.intVal, LevelDifficulty.CUSTOM_CHORD_TYPE.intVal -> CHORD_TYPE.ext
            else -> CHORD.ext
        }
    }
}