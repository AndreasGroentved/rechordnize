package com.andreasgroentved.rechordnize.model.level

import java.io.Serializable


data class LevelHistory(var playMap: MutableMap<List<LevelModifier>, LevelPlay> = mutableMapOf()) : Serializable