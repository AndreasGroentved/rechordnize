package com.andreasgroentved.rechordnize.model.util

import com.andreasgroentved.rechordnize.model.skill.SkillType
import com.andreasgroentved.rechordnize.model.skill.SkillType.*
import kotlin.math.roundToLong


object SkillDictionary {

    val skillToMaxValue: Map<SkillType, Long> = mapOf(
        DAYS_IN_A_ROW to 365L,
        CUSTOM_LEVEL_MADE to 100L,
        NUM_OF_CORRECT to 10000L,
        NUM_OF_GUESSES to 10000L,
        LONGEST_PRACTISE to 7200000L,
        LEVEL_1_LEVELS_COMPLETED to 50L, //TODO måske find præcis antal
        LEVEL_2_LEVELS_COMPLETED to 50L,
        LEVEL_3_LEVELS_COMPLETED to 50L,
        LEVELS_COMPLETED to 100L,
        INTERVAL_MASTER to 25L/*All*/,
        NAME_CHORD_MASTER to 25L/*All*/,
        TYPE_MASTER to 25L/*All*/,
        PROGRESSION_MASTER to 25L/*All*/,
        TOTAL_DAYS to 1000L,
        TOTAL_PRACTISE_TIME to 360000000L,
        CORRECT_IN_A_ROW to 100L,
        ERROR to 0L
    )

    private val levelPercentages = listOf(1, 2, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 105, 110, 115, 120, 125) //TODO flere

    //TODO vis tætteste progress til name efter hvert exp (aktivitet), hvis der ikke er exp op - muligvis også bare en tilfældig name

    fun getLevel(exp: Long, maxValue: Long): Int {
        if (exp == 0L) return 1
        var level = 0
        while (isExpHigherThanLevel(level, exp, maxValue)) {
            level++
            if (isLevelHigherThanMaxLevel(level, exp, maxValue)) return level
        }
        return level + 1
    }

    private fun isLevelHigherThanMaxLevel(level: Int, exp: Long, maxValue: Long) =
        level == levelPercentages.size && (exp.toFloat() / maxValue) * 100 >= levelPercentages[level - 1]

    private fun isExpHigherThanLevel(level: Int, exp: Long, maxValue: Long) =
        level < levelPercentages.size && (exp.toFloat() / maxValue) * 100 >= levelPercentages[level]

    fun getAmountAtNextLevel(level: Int, maxValue: Long, skill: SkillType): Long {
        if (skill == ERROR) return 0
        val maxAtNextLevel = (if (level < levelPercentages.size - 1) levelPercentages[level] else levelPercentages[levelPercentages.size - 1]) * maxValue / 100.0
        val roundedReturn = maxAtNextLevel.roundToLong()
        return if (roundedReturn == 0L) return 1 else roundedReturn
    }

}

