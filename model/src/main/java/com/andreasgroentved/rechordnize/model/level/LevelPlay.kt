package com.andreasgroentved.rechordnize.model.level

import com.andreasgroentved.rechordnize.model.playable.PlayData
import java.io.Serializable



//TODO gammel datastruktur, der gemmer for meget data
data class LevelPlay(var plays: MutableList<PlayData> = mutableListOf(), var numberOfRounds: Int = DEFAULT_NUMBER_OF_PLAYS, var guessCount: Int = 0) : Serializable {
    companion object {
        const val DEFAULT_NUMBER_OF_PLAYS = 20
    }
}



