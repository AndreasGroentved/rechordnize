package com.andreasgroentved.rechordnize.model.usage

import java.io.Serializable


enum class UsageGoal(val goal: Int) : Serializable {
    SHORT(0),
    MEDIUM(1),
    LONG(2);

    companion object {
        private val map = values().associateBy(UsageGoal::goal)
        fun fromInt(type: Int): UsageGoal? = map[type]
        fun getLength(usageGoal: UsageGoal): Int = when (usageGoal) {
            SHORT -> 5
            MEDIUM -> 10
            LONG -> 15
        }
    }
}