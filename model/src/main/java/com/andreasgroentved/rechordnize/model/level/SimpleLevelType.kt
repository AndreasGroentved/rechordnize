package com.andreasgroentved.rechordnize.model.level


enum class SimpleLevelType {
    NAME,
    TYPE,
    PROGRESSION,
    INTERVAL,
    ANY
}