package com.andreasgroentved.rechordnize.model.util

import kotlin.math.roundToInt

object ExpUtil {

    private const val baseExp = 10
    fun gradeGuess(grade: Int, sequenceLength: Int, correctInSequnce: Int, possibleChoices: Int): Int {
        val gradeMultiplier = (grade) * 0.4
        val sequenceMulti = 0.5 + (0.5) * sequenceLength
        val possibleMulti = 0.5 + (possibleChoices / 5) * 0.3
        val correctnessMul = if (correctInSequnce == 0) 0.0 else (correctInSequnce.toDouble() / sequenceLength)
        return ((if (grade == 0) 0 else 1) * baseExp * (gradeMultiplier * sequenceMulti * possibleMulti) * correctnessMul).roundToInt()
    }

    /**
     * returns level calculated from exp, adds 1 level to actual level, so level can never be 0.
     */
    /*  Lidt knudret, men kører i konstant tid
        bruger $maxLevelToMaxExpToExpPerLevelInGroup til at finde den sidste gruppe, hvor max exp er mindre
        end input exp. Derved max exp fra gruppen før trakkes fra nuværende exp, og ud fra exp krævet til hvert
        level, kan level udregnes
   */
    fun getLevelFromExp(exp: Long): Long { //TODO hvis sidste skaler op e.g. med 5%
        var index = 0
        while (maxLevelToMaxExpToExpPerLevelInGroup[index].second < exp) index++
        val divFactor = maxLevelToMaxExpToExpPerLevelInGroup[index].third
        val toDivideBy = exp - if (index == 0) 0 else maxLevelToMaxExpToExpPerLevelInGroup[index - 1].second
        return 1 + (if (index == 0) 0 else maxLevelToMaxExpToExpPerLevelInGroup[index - 1].first) + (toDivideBy / divFactor)
    }

    /**
     *  Returns exp required to level up to current level and the exp required to reach next level.
     */
    fun getExpBelowAndAbove(exp: Long): Pair<Long, Long> {
        var index = 0
        while (maxLevelToMaxExpToExpPerLevelInGroup[index].second < exp) index++
        val divFactor = maxLevelToMaxExpToExpPerLevelInGroup[index].third
        val toDivideBy = exp - if (index == 0) 0 else maxLevelToMaxExpToExpPerLevelInGroup[index - 1].second
        val levelDifference = (toDivideBy / divFactor) + 1
        val levelBeforeExp = if (levelDifference == 0L) maxLevelToMaxExpToExpPerLevelInGroup.getOrNull(index - 1)?.second
            ?: 0 else (maxLevelToMaxExpToExpPerLevelInGroup.getOrNull(index - 1)?.second
            ?: 0) + ((levelDifference - 1) * divFactor)
        return Pair(
            levelBeforeExp, levelBeforeExp + divFactor
        )

    }

    //Jo højere level, des højere exp per level. Værdier i midten kan regnes ud fra forrige gruppes midt værdi,
    //lagt sammen med første værdi gange sidste værdi
    private val maxLevelToMaxExpToExpPerLevelInGroup = listOf(
        Triple(5, 250L, 50L), Triple(10, 750L, 100L),
        Triple(15, 1750L, 200L), Triple(20, 3750L, 400L),
        Triple(30, 11750L, 800L), Triple(50, 43750L, 1600L),
        Triple(100, 203750L, 3200L), Triple(200, 843750L, 6400L),
        Triple(400, 3403750L, 12800L), Triple(Int.MAX_VALUE, Long.MAX_VALUE, 25600L)
    )

    /**
     * To verify correctness of level calculation
     */
    //Langsom, O(level) = level, leveludregninger bruges ofte, så bruges kun som verifikation
    fun getLevelFromExpV2(exp: Long, currentLevel: Int = 1): Long {
        var level = currentLevel.toLong()
        var cumulativeExp = getExpAtNextLevel(level)
        while (exp >= cumulativeExp) {
            cumulativeExp = getExpAtNextLevel(level)
            level++
        }
        return level - 1L
    }


    private fun getExpAtNextLevel(level: Long) = when (level) {
        0L -> 50L
        in 1..5 -> 50L * level
        in 6..10 -> 250L + (100L * (level - 5))
        in 11..15 -> 250L + 500L + (200L * (level - 10))
        in 16..20 -> 1000L + 500L + 250L + +400L * (level - 15)
        in 21..30 -> 2000L + 1000L + 500L + 250L + 800L * (level - 20)
        in 31..50 -> 8000L + 2000L + 1000L + 500L + 250L + 1600L * (level - 30)
        in 51..100 -> 32000L + 8000L + 2000L + 1000L + 500L + 250L + 3200L * (level - 50)
        in 101..200 -> 160000L + 32000L + 8000L + 2000L + 1000L + 500L + 250L + 6400L * (level - 100)
        in 201..400 -> 640000L + 160000L + 32000L + 8000L + 2000L + 1000L + 500L + 250L + 12800L * (level - 200)
        else -> 2560000L + 640000L + 160000L + 32000L + 8000L + 2000L + 1000L + 500L + 250L + 25600L * (level - 400)
    }
}