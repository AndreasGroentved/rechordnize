package com.andreasgroentved.rechordnize.model.util


import com.andreasgroentved.rechordnize.model.playable.Playable
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.util.SortBy.*
import java.util.*


class PlayableComparator(private val sortBy: SortBy = NAME) : Comparator<Playable> {


    private val chordDictionary = ChordDictionary()

    /**
     * Sorting after with the rule that: "xb"<"x"
     */
    override fun compare(a: Playable, b: Playable): Int = compareType(a, b)

    private fun compareType(a: Playable, b: Playable): Int = when (sortBy) {
        NAME -> comparePlayableName(a, b)
        ACCURACY -> PlayableUtil.getAcc(a.performances).compareTo(PlayableUtil.getAcc(b.performances))
        LEVEL -> a.exp.compareTo(b.exp)
        STREAK -> a.inARow.compareTo(b.inARow)
        TIME -> PlayableUtil.getAverageTime(a.performances, a.performances.size).compareTo(PlayableUtil.getAverageTime(b.performances, b.performances.size))
        LAST_PLAYED -> a.lastPlayed.compareTo(b.lastPlayed)
        CORRECT -> a.correct.compareTo(b.correct)
        PLAYS -> a.plays.compareTo(b.plays)
    }

    private fun comparePlayableName(a: Playable, b: Playable): Int = when (a.extra) {
        PlayableExtra.INTERVAL -> compareStringInterval(a.name, b.name, chordDictionary)
        PlayableExtra.CHORD_PROGRESSION -> compareStringProgression(a.name, b.name, chordDictionary)
        PlayableExtra.CHORD_TYPE -> compareStringType(a.name, b.name)
        else -> compareStringChord(a.name, b.name)
    }


    companion object {

        /**
         *  returns the lesser of two chord names -> 'b' means flat or less -> Bb is less than b
         */
        fun compareStringChord(a: String, b: String): Int {
            if (a.split(" ").size == 1) return AlphaNumComparator.compare(a, b) //Extension
            val first = a[0] == b[0]
            return if (!first) a.compareTo(b)
            else {
                if (a.length == 1 && b.length == 1) return AlphaNumComparator.compare(a, b)
                if (a.length == 1 && b[1] == 'b') return -1
                if (b.length == 1 && a[1] == 'b') return 1
                if (a[1] == 'b' && b[1] == 'b') AlphaNumComparator.compare(a, b)
                else if (a[1] == 'b') -1
                else if (b[1] == 'b') 1
                else AlphaNumComparator.compare(a, b)
            }
        }

        fun compareStringProgression(a: String, b: String, chordDictionary: ChordDictionary): Int {
            val distanceA = chordDictionary.progressionToDistance.getValue(a.split(" ")[0])
            val distanceB = chordDictionary.progressionToDistance.getValue(b.split(" ")[0])
            return distanceA.compareTo(distanceB)
        }

        fun compareStringType(a: String, b: String): Int = AlphaNumComparator.compare(a, b)


        fun compareStringInterval(a: String, b: String, chordDictionary: ChordDictionary): Int {
            val distanceA = chordDictionary.nameToDistanceMap.getValue(a)
            val distanceB = chordDictionary.nameToDistanceMap.getValue(b)
            return distanceA.compareTo(distanceB)
        }
    }

}