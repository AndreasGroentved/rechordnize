package com.andreasgroentved.rechordnize.model.level

import com.andreasgroentved.rechordnize.model.skill.SkillType
import java.io.Serializable


enum class LevelDifficulty(val intVal: Int) : Serializable {
    OPEN_CHORDS(1),
    POWER_CHORDS(2),
    BARRE_CHORDS(3),
    ALL_CHORDS(4),
    INTERVAL_EASY(5),
    INTERVAL_MEDIUM(6),
    INTERVAL_HARD(7),
    INTERVAL_THE_END(8),
    CHORD_PROGRESSION_EASY(10),
    CHORD_PROGRESSION_MEDIUM(11),
    CHORD_PROGRESSION_HARD(12),
    CHORD_PROGRESSION_THE_END(13),
    CHORD_TYPE_EASY(15),
    CHORD_TYPE_MEDIUM(16),
    CHORD_TYPE_HARD(17),
    CHORD_TYPE_THE_END(18),
    LEARN_CHORDS(19),
    CUSTOM_INTERVAL(21),
    CUSTOM_CHORD_TYPE(22),
    CUSTOM_PROGRESSION(23),
    CUSTOM_CHORDS(24),
    MULTI(25);

    companion object {
        private val map = values().associateBy(LevelDifficulty::intVal)
        fun getChordLevels() = listOf(OPEN_CHORDS, POWER_CHORDS, BARRE_CHORDS/*, ALL_CHORDS*/, CUSTOM_CHORDS)
        fun getTypeLevels() = listOf(CHORD_TYPE_EASY, CHORD_TYPE_MEDIUM, CHORD_TYPE_HARD, CHORD_TYPE_THE_END, CUSTOM_CHORD_TYPE)
        fun getProgressionLevels() = listOf(CHORD_PROGRESSION_EASY, CHORD_PROGRESSION_MEDIUM, CHORD_PROGRESSION_HARD, CHORD_PROGRESSION_THE_END, CUSTOM_PROGRESSION)
        fun getIntervalLevels() = listOf(INTERVAL_EASY, INTERVAL_MEDIUM, INTERVAL_HARD, INTERVAL_THE_END, CUSTOM_INTERVAL)
        fun getCustomLevels() = listOf(CUSTOM_CHORDS, CUSTOM_CHORD_TYPE, CUSTOM_INTERVAL, CUSTOM_PROGRESSION)


        //TODO der er en hvis skrøbelighed i det her

        fun isChord(diff: Int) = diff in OPEN_CHORDS.intVal..ALL_CHORDS.intVal
        fun isInterval(diff: Int) = diff in INTERVAL_EASY.intVal..INTERVAL_THE_END.intVal
        fun isType(diff: Int) = diff in CHORD_TYPE_EASY.intVal..CHORD_TYPE_THE_END.intVal
        fun isProgression(diff: Int) = diff in CHORD_PROGRESSION_EASY.intVal..CHORD_PROGRESSION_THE_END.intVal

        fun fromInt(type: Int): LevelDifficulty = map[type]!!


        fun LevelDifficulty.getTitle(): String = /*"Custom " +*/ when (this) {
            CUSTOM_INTERVAL -> "interval"
            CUSTOM_CHORD_TYPE -> "chord type"
            CUSTOM_PROGRESSION -> "progression"
            CUSTOM_CHORDS -> "chord"
            else -> "chord"
        }


    }
}

fun LevelDifficulty.isCustom() = when (this) {
    LevelDifficulty.CUSTOM_INTERVAL, LevelDifficulty.CUSTOM_CHORD_TYPE, LevelDifficulty.CUSTOM_CHORDS, LevelDifficulty.CUSTOM_PROGRESSION -> true
    else -> false
}

fun LevelDifficulty.getTypeTitle() = when {
    LevelDifficulty.isInterval(this.intVal) -> "Interval" //TODO string resource
    LevelDifficulty.isProgression(this.intVal) -> "Progression"
    LevelDifficulty.isChord(this.intVal) -> "Chords"
    LevelDifficulty.isType(this.intVal) -> "Chord type"
    else -> "Error"
}

fun LevelDifficulty.getString(): String = when (this) {
    LevelDifficulty.OPEN_CHORDS -> "Open chords"
    LevelDifficulty.POWER_CHORDS -> "Power chords"
    LevelDifficulty.BARRE_CHORDS -> "Barre chords"
    LevelDifficulty.ALL_CHORDS -> "All chords"
    LevelDifficulty.LEARN_CHORDS -> "Learn"
    LevelDifficulty.CUSTOM_CHORDS -> "Custom"
    LevelDifficulty.CUSTOM_INTERVAL -> "Custom"
    LevelDifficulty.CUSTOM_PROGRESSION -> "Custom"
    LevelDifficulty.INTERVAL_EASY -> "Easy"
    LevelDifficulty.INTERVAL_MEDIUM -> "Medium"
    LevelDifficulty.INTERVAL_HARD -> "Hard"
    LevelDifficulty.INTERVAL_THE_END -> "The end"
    LevelDifficulty.CHORD_TYPE_EASY -> "Easy"
    LevelDifficulty.CHORD_TYPE_MEDIUM -> "Medium"
    LevelDifficulty.CHORD_TYPE_HARD -> "Hard"
    LevelDifficulty.CUSTOM_CHORD_TYPE -> "Custom"
    LevelDifficulty.CHORD_PROGRESSION_EASY -> "Easy"
    LevelDifficulty.CHORD_PROGRESSION_MEDIUM -> "Medium"
    LevelDifficulty.CHORD_PROGRESSION_HARD -> "Hard"
    LevelDifficulty.CHORD_PROGRESSION_THE_END -> "The end"
    LevelDifficulty.CHORD_TYPE_THE_END -> "The end"
    LevelDifficulty.MULTI -> "Multiplayer"
}

fun LevelDifficulty.getLevelTypeSkillType() = when (this) {
    in LevelDifficulty.getProgressionLevels() -> SkillType.PROGRESSION_MASTER
    in LevelDifficulty.getIntervalLevels() -> SkillType.INTERVAL_MASTER
    in LevelDifficulty.getChordLevels() -> SkillType.NAME_CHORD_MASTER
    in LevelDifficulty.getTypeLevels() -> SkillType.TYPE_MASTER
    else -> SkillType.ERROR
}


