package com.andreasgroentved.rechordnize.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(indices = [(Index("date"))])
data class DailyGoal constructor(val description: String = "", val skillType: Int = 0, val skillValue: Long = -1L, @PrimaryKey var date: String = "", var succeeded: Boolean = false)