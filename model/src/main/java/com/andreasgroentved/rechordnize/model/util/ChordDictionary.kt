package com.andreasgroentved.rechordnize.model.util

import com.andreasgroentved.rechordnize.model.level.LevelModifier
import com.andreasgroentved.rechordnize.model.level.LevelModifier.*
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra
import com.andreasgroentved.rechordnize.model.playable.PlayableExtra.*
import java.util.*


class ChordDictionary {

    val openChordMap: Map<String, List<Int>> by lazy {
        mapOf( //TODO merge
            "a maj" to listOf(-1, 0, 2, 2, 2, 0), "a maj7" to listOf(-1, 0, 2, 1, 2, 0), "a min" to listOf(-1, 0, 2, 2, 1, 0),
            "a 7" to listOf(-1, 0, 2, 0, 2, 0), "a sus2" to listOf(-1, 0, 2, 2, 0, 0),
            "a sus4" to listOf(-1, 0, 2, 2, 3, 0), "a min7" to listOf(-1, 0, 2, 0, 1, 0),
            "b 7" to listOf(-1, 2, 1, 2, 0, 2), "c maj" to listOf(-1, 3, 2, 0, 1, 0), "c maj7" to listOf(-1, 3, 2, 0, 0, 0),
            "c 7" to listOf(-1, 3, 2, 3, 1, 0), "c sus2" to listOf(-1, 3, 0, 0, 3, 3), "d maj" to listOf(-1, -1, 0, 2, 3, 2),
            "d min" to listOf(-1, -1, 0, 2, 3, 1), "d min7" to listOf(-1, -1, 0, 2, 1, 1),
            "d 7" to listOf(-1, -1, 0, 2, 1, 2), "d sus2" to listOf(-1, -1, 0, 2, 3, 0), "d sus4" to listOf(-1, -1, 0, 2, 3, 3),
            "e maj" to listOf(0, 2, 2, 1, 0, 0), "e min" to listOf(0, 2, 2, 0, 0, 0), "e min7" to listOf(0, 2, 0, 0, 0, 0),
            "e 7" to listOf(0, 2, 0, 1, 0, 0), "f maj" to listOf(-1, -1, 3, 2, 1, 1),
            "f maj7" to listOf(-1, -1, 3, 2, 1, 0), "f 7" to listOf(-1, -1, 1, 2, 1, 1),
            "g maj" to listOf(3, 2, 0, 0, 0, 3), "g 7" to listOf(3, 2, 0, 0, 0, 1), "g sus2" to listOf(3, 0, 0, 0, 0, 0)
        )
    }

    val chordShapeMap: Map<String, Map<PlayableExtra, List<Int>>> by lazy {
        mapOf(
            "maj" to mapOf(
                BARRE_A_SHAPE to listOf(-1, 0, 2, 2, 2, 0),
                BARRE_C_SHAPE to listOf(-1, 3, 2, 0, 1, 0),
                BARRE_E_SHAPE to listOf(0, 2, 2, 1, 0, 0)
            ),

            "min" to mapOf(
                BARRE_A_SHAPE to listOf(-1, 0, 2, 2, 1, 0),
                BARRE_C_SHAPE to listOf(-1, 3, 1, 0, 1, -1),
                BARRE_E_SHAPE to listOf(0, 2, 2, 0, 0, 0)
            ),

            "dim" to mapOf(
                BARRE_A_SHAPE to listOf(-1, 0, 1, 2, 1, -1),
                BARRE_E_SHAPE to listOf(0, 1, 2, 0, -1, -1)
            ),

            "b5" to mapOf(
                BARRE_A_SHAPE to listOf(-1, 0, 1, 2, 2, -1),
                BARRE_E_SHAPE to listOf(0, 1, 2, 1, -1, -1)
            ),

            "min#5" to mapOf(BARRE_A_SHAPE to listOf(-1, 2, 0, 0, 0, -1)),

            "aug" to mapOf(
                BARRE_A_SHAPE to listOf(-1, 2, 1, 0, 0, 0),
                BARRE_E_SHAPE to listOf(3, 2, 1, 0, 0, -1)
            ),

            "sus2" to mapOf(BARRE_A_SHAPE to listOf(-1, 0, 2, 2, 0, 0)),

            "sus4" to mapOf(
                BARRE_A_SHAPE to listOf(-1, 0, 2, 2, 3, 0),
                BARRE_E_SHAPE to listOf(0, 2, 2, 2, 0, 0)
            ),

            "sus2sus4" to mapOf(BARRE_E_SHAPE to listOf(-1, 0, 0, 2, 0, 0)),

            "5" to mapOf(
                POWER_A_SHAPE to listOf(-1, 0, 2, 2, -1, -1),
                POWER_E_SHAPE to listOf(0, 2, 2, -1, -1, -1)
            ),

            "6" to mapOf(
                BARRE_A_SHAPE to listOf(-1, 0, 2, -1, 2, 2),
                BARRE_D_SHAPE to listOf(-1, -1, 0, 2, 0, 2)
            ),

            "6/9" to mapOf(
                BARRE_E_SHAPE to listOf(1, 0, 0, 0, 1, 1),
                BARRE_D_SHAPE to listOf(-1, 1, 0, 0, 1, 1)
            ),

            "min6" to mapOf(
                BARRE_E_SHAPE to listOf(0, 2, 2, 0, 2, 0),
                BARRE_D_SHAPE to listOf(-1, -1, 0, 2, 0, 1)
            ),
            "min6/9" to mapOf(
                BARRE_E_SHAPE to listOf(2, 0, 1, 1, -1, -1),
                BARRE_A_SHAPE to listOf(-1, 2, 0, 1, 2, -1)
            ),

            "7b5" to mapOf(
                BARRE_E_SHAPE to listOf(0, 1, 0, 1, -1, -1),
                BARRE_A_SHAPE to listOf(-1, 0, 1, 0, 2, -1)
            ),

            "min7b5" to mapOf(
                BARRE_A_SHAPE to listOf(-1, 0, 1, 0, 1, -1),
                BARRE_D_SHAPE to listOf(-1, -1, 0, 1, 1, 1)
            ),

            "min7" to mapOf(
                BARRE_A_SHAPE to listOf(-1, 0, 2, 0, 1, 0),
                BARRE_E_SHAPE to listOf(0, 2, 0, 0, 0, 0)
            ),

            "7" to mapOf(
                BARRE_A_SHAPE to listOf(-1, 0, 2, 0, 2, 0),
                BARRE_E_SHAPE to listOf(0, 2, 0, 1, 0, 0)
            ),

            "maj7" to mapOf(
                BARRE_A_SHAPE to listOf(-1, 0, 2, 1, 2, 0),
                BARRE_E_SHAPE to listOf(0, -1, 1, 1, 0, -1)
            ),

            "aug7" to mapOf(
                BARRE_E_SHAPE to listOf(3, 2, 1, 0, -1, -1),
                BARRE_C_SHAPE to listOf(-1, 1, 0, 1, -1, 2)
            ),

            "minmaj7" to mapOf(
                BARRE_A_SHAPE to listOf(-1, 0, 2, 1, 1, 0),
                BARRE_E_SHAPE to listOf(0, 2, 1, 0, 0, 0)
            ),

            "dim7" to mapOf(
                BARRE_C_SHAPE to listOf(-1, 2, 0, 1, 0, 1),
                BARRE_D_SHAPE to listOf(-1, -1, 0, 1, 0, 1)
            ),

            "9" to mapOf(
                BARRE_E_SHAPE to listOf(0, 2, 0, 1, 0, 2),
                BARRE_C_SHAPE to listOf(-1, 1, 0, 1, 1, -1)
            ),

            "min9" to mapOf(BARRE_E_SHAPE to listOf(0, 2, 0, 0, 0, 2)),
            "maj9" to mapOf(BARRE_C_SHAPE to listOf(-1, 3, 0, 0, 0, 0)),

            "11" to mapOf(BARRE_E_SHAPE to listOf(0, 0, 0, 1, 0, 2)),

            "min11" to mapOf(BARRE_E_SHAPE to listOf(0, 0, 0, 0, 0, 2)),
            "maj11" to mapOf(BARRE_A_SHAPE to listOf(-1, 0, 0, 1, 2, 0)),

            "13" to mapOf(BARRE_A_SHAPE to listOf(-1, 0, 2, 0, 2, 2)),

            "min13" to mapOf(BARRE_E_SHAPE to listOf(0, 0, 0, 0, 2, 2)),

            "maj13" to mapOf(BARRE_E_SHAPE to listOf(1, 0, 0, 0, 1, 0))
        )
    }

    val typeToGrouping: Map<String, String> by lazy {
        mapOf(
            "maj" to "5",
            "min" to "5",
            "dim" to "5",
            "b5" to "5",
            "min#5" to "5",
            "aug" to "5",
            "sus2" to "5",
            "sus4" to "5",
            "sus2sus4" to "5",
            "5" to "5",

            "6" to "6",
            "6/9" to "6",
            "min6" to "6",
            "min6/9" to "6",

            "min7b5" to "7",
            "min7" to "7",
            "7" to "7",
            "7b5" to "7",
            "maj7" to "7",
            "aug7" to "7",
            "minmaj7" to "7",
            "dim7" to "7",

            "9" to "9",
            "min9" to "9",
            "maj9" to "9",

            "min11" to "11",
            "11" to "11",
            "maj11" to "11",

            "13" to "13",
            "maj13" to "13",
            "min13" to "13"
        )
    }

    val chordTypeToNotes: Map<String, String> by lazy {
        mapOf(
            "maj" to "1 3 5",
            "min" to "1 b3 5",
            "dim" to "1 b3 b5",
            "b5" to "1 3 b5",
            "min#5" to "1 b3 #5",
            "aug" to "1 3 #5",
            "sus2" to "1 2 5",
            "sus4" to "1 4 5",
            "sus2sus4" to "1 2 4 (5)",
            "5" to "1 5",

            "6" to "1 3 (5) 6",
            "6/9" to "1 3 (5) 6 9",
            "min6" to "1 b3 (5) 6",
            "min6/9" to "1 b3 (5) 6 9",

            "min7b5" to "1 b3 b5 7",
            "min7" to "1 b3 (5) 7",
            "7" to "1 3 (5) 7",
            "7b5" to "1 3 b5 7",
            "maj7" to "1 3 5 #7",
            "aug7" to "1 3 #5 7",
            "minmaj7" to "1 b3 (5) #7",
            "dim7" to "1 b3 b5 bb7",

            "9" to "1 3 (5) 7 9",
            "min9" to "1 b3 (5) 7 9",
            "maj9" to "1 3 (5) 7# 9",

            "min11" to "1 b3 (5) 7 (9) 11",
            "11" to "1 3 (5) 7 (9) 11",
            "maj11" to "1 3 (5) 7# (9) 11",

            "13" to "1 3 (5) 7 (9) (11) 13",
            "maj13" to "1 3 (5) #7 (9) (11) 13",
            "min13" to "1 b3 (5) 7 (9) (11) 13"
        )
    }
    val typeStringsToDistance: Map<String, Int> by lazy {
        mapOf(
            "1" to 0, "b2" to 1, "2" to 2, "b3" to 3,
            "3" to 4, "4" to 5, "b5" to 6, "5" to 7,
            "#5" to 8, "b6" to 8, "6" to 9,/*TODO ser forkert ud   "b7" to 10,*/"bb7" to 9,
            "7" to 10, "#7" to 11, "8" to 12, "b9" to 13,
            "9" to 14, "b10" to 15, "M10" to 16, "11" to 17,
            "b12" to 18, "12" to 19, "b13" to 20, "13" to 21,
            "b14" to 22, "14" to 23, "15" to 24
        )
    }

    val progressionToName: Map<String, String> by lazy {
        mapOf(
            "I" to "1st", "II" to "2nd", "bII" to "flattened 2nd", "III" to "3rd", "bIII" to "flattened 3rd", "IV" to "4th", "V" to "5th",
            "bV" to "flattened 5th", "VI" to "6th", "bVI" to "flattened 6th", "VII" to "7th", "bVII" to "flattened 7th"
        )
    }

    val progressionToDistance: Map<String, Int> by lazy {
        mapOf("I" to 0, "II" to 2, "bII" to 1, "III" to 4, "bIII" to 3, "IV" to 5, "V" to 7, "bV" to 6, "VI" to 9, "bVI" to 8, "VII" to 11, "bVII" to 10)
    }

    private val noteToStringNoteMapE6: MutableMap<String, String> by lazy {
        mutableMapOf(
            "e2" to "e60", "f2" to "e61", "gb2" to "e62", "g2" to "e63",
            "ab2" to "e64", "a2" to "e65", "bb2" to "e66", "b2" to "e67",
            "c3" to "e68", "db3" to "e69", "d3" to "e610", "eb3" to "e611",
            "e3" to "e612", "f3" to "e613", "gb3" to "e614", "g3" to "e615"
        )
    }
    private val noteToStringNoteA5: MutableMap<String, String> by lazy {
        mutableMapOf(
            "a2" to "a50", "bb2" to "a51", "b2" to "a52", "c3" to "a53",
            "db3" to "a54", "d3" to "a55", "eb3" to "a56", "e3" to "a57",
            "f3" to "a58", "gb3" to "a59", "g3" to "a510", "ab3" to "a511",
            "a3" to "a512", "bb3" to "a513", "b3" to "a514", "c4" to "a515"
        )
    }

    private val noteToStringNoteD4: MutableMap<String, String> by lazy {
        mutableMapOf(
            "d3" to "d40", "eb3" to "d41", "e3" to "d42", "f3" to "d43",
            "gb3" to "d44", "g3" to "d45", "ab3" to "d46", "a3" to "d47",
            "bb3" to "d48", "b3" to "d49", "c4" to "d410", "db4" to "d411",
            "d4" to "d412", "eb4" to "d413", "e4" to "d414", "f4" to "d415"
        )
    }

    private val noteToStringNoteG3: MutableMap<String, String> by lazy {
        mutableMapOf(
            "g3" to "g30", "ab3" to "g31", "a3" to "g32", "bb3" to "g33",
            "b3" to "g34", "c4" to "g35", "db4" to "g36", "d4" to "g37",
            "eb4" to "g38", "e4" to "g39", "f4" to "g310", "gb4" to "g311",
            "g4" to "g312", "ab4" to "g313", "a4" to "g314", "bb4" to "g315"
        )
    }

    private val noteToStringNoteB2: MutableMap<String, String> by lazy {
        mutableMapOf(
            "b3" to "b20", "c4" to "b21", "db4" to "b22", "d4" to "b23",
            "eb4" to "b24", "e4" to "b25", "f4" to "b26", "gb4" to "b27",
            "g4" to "b28", "ab4" to "b29", "a4" to "b210", "bb4" to "b211",
            "b4" to "b212", "c5" to "b213", "db5" to "b214", "d5" to "b215"
        )
    }

    private val noteToStringNoteE1: Map<String, String> by lazy {
        mapOf(
            "e4" to "e10", "f4" to "e11", "gb4" to "e12", "g4" to "e13", "ab4" to "e14", "a4" to "e15", "bb4" to "e16", "b4" to "e17",
            "c5" to "e18", "db5" to "e19", "d5" to "e110", "eb5" to "e111", "e5" to "e112", "f5" to "e113", "gb5" to "e114", "g5" to "e115",
            "ab5" to "e116", "a5" to "e117", "bb5" to "e118", "b5" to "e119", "c6" to "e120", "db6" to "e121", "d6" to "e122", "eb6" to "e123",
            "e6" to "e124", "f6" to "e125", "gb6" to "e126", "g6" to "e127", "ab6" to "e128", "a6" to "e129", "bb6" to "e130", "b6" to "e131",
            "c7" to "e132", "db7" to "e133", "d7" to "e134", "eb7" to "e135", "e7" to "e136"
        )
    }

    private val stringSoundE6: List<String> by lazy {
        listOf("e60", "e61", "e62", "e63", "e64", "e65", "e66", "e67", "e68", "e69", "e610", "e611", "e612", "e613", "e614", "e615")
    }
    private val stringSoundA5: List<String> by lazy {
        listOf("a50", "a51", "a52", "a53", "a54", "a55", "a56", "a57", "a58", "a59", "a510", "a511", "a512", "a513", "a514", "a515")
    }

    private val stringSoundD4: List<String> by lazy {
        listOf("d40", "d41", "d42", "d43", "d44", "d45", "d46", "d47", "d48", "d49", "d410", "d411", "d412", "d413", "d414", "d415")
    }
    private val stringSoundG3: List<String> by lazy {
        listOf("g30", "g31", "g32", "g33", "g34", "g35", "g36", "g37", "g38", "g39", "g310", "g311", "g312", "g313", "g314", "g315")
    }
    private val stringSoundB2: List<String> by lazy {
        listOf("b20", "b21", "b22", "b23", "b24", "b25", "b26", "b27", "b28", "b29", "b210", "b211", "b212", "b213", "b214", "b215")
    }
    private val stringSoundE1: List<String> by lazy {
        listOf(
            "e10", "e11", "e12", "e13", "e14", "e15", "e16", "e17", "e18", "e19", "e110", "e111", "e112", "e113", "e114", "e115",
            "e116", "e117", "e118", "e119", "e120", "e121", "e122", "e123", "e124", "e125", "e126", "e127", "e128", "e129", "e130",
            "e131", "e132", "e133", "e134", "e135", "e136"
        )
    }

    fun getSoundNameOnString(stringNum: Int) = when (stringNum) {
        1 -> stringSoundE1
        2 -> stringSoundB2
        3 -> stringSoundG3
        4 -> stringSoundD4
        5 -> stringSoundA5
        6 -> stringSoundE6
        else -> throw RuntimeException("invalid string")
    }

    fun getSoundOnString(stringNum: Int) = when (stringNum) {
        1 -> soundsOnE1String
        2 -> soundsOnB2String
        3 -> soundsOnG3String
        4 -> soundsOnD4String
        5 -> soundsOnA5String
        6 -> soundsOnE6String
        else -> throw RuntimeException("invalid string")
    }

    private val soundsOnE1String: List<String> by lazy {
        listOf(
            "e4", "f4", "gb4", "g4", "ab4", "a4", "bb4", "b4", "c5", "db5", "d5", "eb5", "e5", "f5", "gb5", "g5", "ab5", "a5", "bb5", "b5",
            "c6", "db6", "d6", "eb6", "e6", "f6", "gb6", "g6", "ab6", "a6", "bb6", "b6", "c7", "db7", "d7", "eb7", "e7"
        )
    }
    private val soundsOnB2String: List<String> by lazy { listOf("b3", "c4", "db4", "d4", "eb4", "e4", "f4", "gb4", "g4", "ab4", "a4", "bb4", "b4", "c5", "db5", "d5") }
    private val soundsOnG3String: List<String> by lazy { listOf("g3", "ab3", "a3", "bb3", "b3", "c4", "db4", "d4", "eb4", "e4", "f4", "gb4", "g4", "ab4", "a4", "bb4") }
    private val soundsOnD4String: List<String> by lazy { listOf("d3", "eb3", "e3", "f3", "gb3", "g3", "ab3", "a3", "bb3", "b3", "c4", "db4", "d4", "eb4", "e4", "f4") }
    private val soundsOnA5String: List<String> by lazy { listOf("a2", "bb2", "b2", "c3", "db3", "d3", "eb3", "e3", "f3", "gb3", "g3", "ab3", "a3", "bb3", "b3", "c4") }
    private val soundsOnE6String: List<String> by lazy { listOf("e2", "f2", "gb2", "g2", "ab2", "a2", "bb2", "b2", "c3", "db3", "d3", "eb3", "e3", "f3", "gb3", "g3") }

    val order by lazy { listOf("ab", "a", "bb", "b", "c", "db", "d", "eb", "e", "f", "gb", "g") }

    val nameToDistanceMap: Map<String, Int> by lazy {
        mutableMapOf(
            "P1" to 0, "m2" to 1,
            "M2" to 2, "m3" to 3,
            "M3" to 4, "P4" to 5,
            "A4" to 6, "P5" to 7,
            "m6" to 8, "M6" to 9,
            "m7" to 10, "M7" to 11,
            "P8" to 12, "m9" to 13,
            "M9" to 14, "m10" to 15,
            "M10" to 16, "P11" to 17,
            "A11" to 18, "P12" to 19,
            "m13" to 20, "M13" to 21,
            "m14" to 22, "M14" to 23,
            "P15" to 24
        )
    }
    val romanNumMap: Map<String, Int> by lazy {
        mapOf( //Kun 12 muligheder --> nemmere end at skrive kode til håndtering af roman --> int
            "I" to 0, "bII" to 1, "II" to 2, "bIII" to 3, "III" to 4, "IV" to 5, "bV" to 6,
            "V" to 7, "bVI" to 8, "VI" to 9, "bVII" to 10, "VII" to 11, "VIII" to 12
        )
    }

    private val random: Random = Random()

    fun getMaxNote() = "e7" //hardcode for at sparre itering af map
    fun getMinNote() = "e2"

    fun isNoteLessOrInvalid(noteA: String, noteB: String): Boolean {
        val noteANum = lastInt(noteA)
        val noteBNum = lastInt(noteB)
        return when {
            noteA.isEmpty() || noteB.isEmpty() -> true
            noteANum < noteBNum -> true
            noteANum > noteBNum -> false
            else -> {
                var index = order.indexOf(noteA.dropLast(1))
                while (order[index mod order.size] != noteB.dropLast(1)) {
                    if (order[index-- mod order.size] == "c") return true
                }
                false
            }
        }
    }

    private fun lastInt(s: String): Int {
        var i = s.length
        while (i > 0 && Character.isDigit(s[i - 1])) {
            i--
        }
        if (s.substring(i).isEmpty()) return Int.MAX_VALUE
        return s.substring(i).toInt()
    }

    fun isNoteMoreOrInvalid(noteA: String, noteB: String): Boolean {
        val noteANum = lastInt(noteA)
        val noteBNum = lastInt(noteB)
        return when {
            noteANum > noteBNum -> true
            noteANum < noteBNum -> false
            noteA.isEmpty() || noteB.isEmpty() -> true
            else -> {
                var index = order.indexOf(noteA.dropLast(1))
                while (order[index mod order.size] != noteB.dropLast(1)) {
                    if (order[++index mod order.size] == "c") return true
                }
                false
            }
        }
    }


    fun getChordFromProgressionName(prog: String, root: String): String {
        val progPair = prog.split(" ")
        if (progPair.size != 2) throw RuntimeException("invalid input")
        val b = progPair[0].last() == 'b'
        val sharp = progPair[0].last() == '#'
        val progName = if (b) progPair[0].dropLast(1) else progPair[0]

        val extension = progPair[1]
        val progInterval = (romanNumMap[progName]
            ?: error("Invalid roman number")) + if (b) -1 else if (sharp) +1 else 0 //TODO nok mere end metode...

        val indexOfRoot = order.indexOf(root)
        val chordRoot: String = order[(indexOfRoot + progInterval) mod order.size]
        return "${chordRoot.capitalize()} $extension"
    }

    fun getIntervalMultiple(names: List<String>, root: String, ascending: Boolean, preferDefaultRoot: Boolean = true): List<String> {
        val totalDistance = names.sumBy { nameToDistanceMap[it] ?: error("$it has no distance") }
        val (firstNote, lastNote) = getIntervalFromDistance(totalDistance, root, ascending, preferDefaultRoot)
        val firstAdd = if (ascending) firstNote else lastNote
        val lastAdd = if (ascending) lastNote else firstNote

        val returnList = mutableListOf(firstAdd)

        (1 until names.size).map {
            val index = if (ascending) it - 1 else names.size - it
            val toAdd = getIntervalFromDistance(nameToDistanceMap[names[index]]!!, returnList.last(), true)
            returnList.add(toAdd.second)
        }

        returnList.add(lastAdd)
        return if (ascending) returnList else returnList.reversed()
    }


    //TODO vælg grund rod number - e.g. c"2"
    fun getInterval(name: String, root: String, ascending: Boolean): Pair<String, String> = getIntervalFromDistance(
        nameToDistanceMap[name]!!, root, ascending
    )

    private fun getFirstNoteMethod(preferDefaultRoot: Boolean): (Int, String, Boolean) -> String =
        if (preferDefaultRoot) { i, s, b -> getFirstNoteRandom(i, s, b) } else { i, s, b -> getFirstNote(i, s, b) }

    private fun getIntervalFromDistance(distance: Int, root: String, ascending: Boolean, preferDefaultRoot: Boolean = true): Pair<String, String> {
        var firstNote: String
        var secondNote: String
        val firstNoteMethod = getFirstNoteMethod(preferDefaultRoot)
        do {
            firstNote = firstNoteMethod(distance, root, ascending)
            secondNote = getSecondNote(distance, firstNote)
            if (root.last().isDigit() && isNoteMoreOrInvalid(secondNote, getMaxNote())) throw RuntimeException("Invalid root note $root")
        } while (isIntervalInvalid(firstNote, secondNote)) //TODO lav optimering
        return if (ascending) firstNote to secondNote else secondNote to firstNote
    }


    private fun isIntervalInvalid(firstNote: String, secondNote: String): Boolean =
        isNoteLessOrInvalid(firstNote, getMinNote()) || isNoteMoreOrInvalid(secondNote, getMaxNote())

    fun getFirstNoteRandom(intervalDistance: Int, root: String, ascending: Boolean) =
        if (ascending)
            if (!root.last().isDigit()) {
                root + (random.nextInt(6) + 2)
            } else root
        else
            if (!root.last().isDigit()) order[order.indexOf(root) - (intervalDistance mod order.size)] + (random.nextInt(6) + 2)
            else order[order.indexOf(root.dropLast(1)) - (intervalDistance mod order.size)] + (root.last() + getCChange(root, -intervalDistance))


    private val rootTryOrder = listOf('3', '4', '2', '5', '6', '7')

    fun getFirstNote(intervalDistance: Int, root: String, ascending: Boolean): String {//TODO
        return if (ascending) {
            if (!root.last().isDigit()) root + rootTryOrder[0]
            else {
                val oldIndex = rootTryOrder.indexOf(root.last())
                root.dropLast(1) + rootTryOrder[oldIndex + 1]
            }
        } else {
            if (!root.last().isDigit()) {
                order[order.indexOf(root) - intervalDistance mod order.size] + rootTryOrder[0]
            } else {
                order[order.indexOf(root.dropLast(1)) - intervalDistance mod order.size] + (root.last() + getCChange(root, -intervalDistance))
            }
        }
    }


    fun getCChange(noteWithRoot: String, distance: Int): Int {
        if (distance == 0) return 0
        var counter = distance
        val direction = if (distance > 0) -1 else 1
        var cChange = 0
        var change = order.indexOf(noteWithRoot.dropLast(1))

        while (counter != 0) {
            counter += direction
            change -= direction
            if (order[change mod order.size] == "c" && direction < 1 || order[change mod order.size] == "b" && direction > -1)
                cChange -= direction
        }
        return cChange
    }

    private fun getSecondNote(interval: Int, firstNote: String): String {
        var secondNote = firstNote.dropLast(1)
        var countDown = interval
        var index = order.indexOf(firstNote.dropLast(1))
        var differenceInNumber = 0
        while (countDown > 0) {
            index++
            secondNote = order[index mod order.size]
            if (secondNote == "c") differenceInNumber++
            countDown--
        }
        val diff = firstNote.last().toString().toInt() + differenceInNumber
        return secondNote + diff.toString()
    }

    fun getFretForNote(note: String, startString: Int = 6): String {
        var currentString = startString
        while (currentString > 0) {
            val firstFretNote = getNoteToFretNoteMapFromNum(currentString)[note]
            if (firstFretNote == null) currentString--
            else return firstFretNote
        }
        throw RuntimeException("Could not get fret on any string for note $note")
    }

    infix fun Int.mod(modulo: Int): Int = (this % modulo + modulo) % modulo

    private fun getNoteToFretNoteMapFromNum(num: Int): Map<String, String> = when (num) {
        1 -> noteToStringNoteE1
        2 -> noteToStringNoteB2
        3 -> noteToStringNoteG3
        4 -> noteToStringNoteD4
        5 -> noteToStringNoteA5
        6 -> noteToStringNoteMapE6
        else -> throw RuntimeException("Invalid string number")
    }

    private val typeLevels: Map<Int, List<LevelModifier>> by lazy {
        mapOf(
            1 to listOf(LEVEL_1, LENGTH_1),
            2 to listOf(LEVEL_2, LENGTH_2),
            3 to listOf(LEVEL_3, LENGTH_3, INVERSIONS, REPEAT_PLAYS_NEW)
        )
    }

    private val nameLevels: Map<Int, List<LevelModifier>> by lazy {
        mapOf(
            1 to listOf(LEVEL_1, LENGTH_1),
            2 to listOf(LEVEL_2, LENGTH_2),
            3 to listOf(LEVEL_3, LENGTH_3, INVERSIONS, REPEAT_PLAYS_NEW)
        )
    }

    private val intervalLevels: Map<Int, List<LevelModifier>> by lazy {
        mapOf(
            1 to listOf(LEVEL_1, LENGTH_1, STATIC_ROOT, ASCENDING),
            2 to listOf(LEVEL_2, LENGTH_1, ASCENDING, DESCENDING, RANDOM_ROOT),
            3 to listOf(LEVEL_3, LENGTH_2, ASCENDING, DESCENDING, REPEAT_PLAYS_NEW, PLAY_TOGETHER, RANDOM_ROOT)
        )
    }

    private val progressionLevels: Map<Int, List<LevelModifier>> by lazy {
        mapOf(
            1 to listOf(LEVEL_1, LENGTH_1, C_SCALE, STATIC_ROOT, PROGRESSION_ROOT_FIRST),
            2 to listOf(LEVEL_2, LENGTH_2, PROGRESSION_ROOT_FIRST, STATIC_ROOT),
            3 to listOf(LEVEL_3, LENGTH_3, PROGRESSION_ROOT_FIRST, REPEAT_PLAYS_NEW, ANY_FIRST_PROGRESSION_CHORD, INVERSIONS)
        )
    }

    val multiPlayerLevelModifiers: List<LevelModifier> = listOf(LENGTH_1, STATIC_ROOT, PROGRESSION_ROOT_FIRST, ASCENDING, DESCENDING, MULTI_PLAYER)

    fun typeToLevelModifiers(extraInt: Int): Map<Int, List<LevelModifier>> = when (extraInt) {
        CHORD.ext -> nameLevels
        CHORD_TYPE.ext -> typeLevels
        CHORD_PROGRESSION.ext -> progressionLevels
        INTERVAL.ext -> intervalLevels
        else -> nameLevels
    }


    /**
     * returns the note distance e.g. c to d = 1, bb to c = 1
     */
    fun getDistanceLine(noteA: String, noteB: String, octaveCount: Int): Int {
        var octaveCount = octaveCount
        val index = order.indexOf(noteA)
        var distance = 0
        var lineDistance = 0
        var compare: String
        do {
            compare = order[(index + distance++) mod order.size]
            if (compare.length == 1) lineDistance++
            if (noteB == compare) octaveCount--
        } while (noteB != compare || octaveCount >= 0)
        if (compare.length > 1 && compare.last() == 'b') lineDistance++
        return --lineDistance
    }


    fun getNoteAtDistanceFrom(noteA: String, _distance: Int): String {
        var distance = _distance
        var index = order.indexOf(noteA)
        var noteB: String = noteA
        while (distance > 0) {
            distance--
            index++
            noteB = order[index mod order.size]
        }
        return noteB
    }
}