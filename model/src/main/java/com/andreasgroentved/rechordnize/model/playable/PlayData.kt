package com.andreasgroentved.rechordnize.model.playable

import java.io.Serializable


data class PlayData(val name: String, val time: Long, val correct: Boolean) : Serializable {
    override fun equals(other: Any?): Boolean = when (other) {
        null -> false
        !is PlayData -> false
        else -> this.name == other.name
    }

    override fun hashCode(): Int = name.hashCode()

}