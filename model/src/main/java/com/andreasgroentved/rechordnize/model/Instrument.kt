package com.andreasgroentved.rechordnize.model


enum class Instrument {
    GUITAR,
    PIANO;
}