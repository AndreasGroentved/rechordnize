package com.andreasgroentved.rechordnize.model.level

data class LevelState(
    var levelUps: Int = 0, val rounds: MutableList<Round> = mutableListOf(),
    var expBefore: Long, var playableToExpMap: MutableMap<String, Long> = mutableMapOf(),
    var expAfter: Long, var oldLevelData: LevelData? = null, var newLevelData: LevelData,
    var difficultyModifiers: List<LevelModifier>, var isBetter: Boolean = false,
    var oldGrade: Int = 0, var newGrade: Int = 0, var isCustom: Boolean = false, var totalTime: Long = 0,
    var isComplete: Boolean = false, val levelUpSet: MutableSet<String> = mutableSetOf()
)