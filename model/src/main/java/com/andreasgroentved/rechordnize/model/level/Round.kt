package com.andreasgroentved.rechordnize.model.level

data class Round(val number: Int, var guessList: MutableList<List<String>> = mutableListOf(), val correctAnswer: List<String>, var time: Long = 0L, val expMap: MutableMap<String, Long> = mutableMapOf())